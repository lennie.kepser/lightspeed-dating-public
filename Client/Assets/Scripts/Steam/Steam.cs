using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using SocketIO;
using UnityEngine;
using Steamworks;

public class Steam : MonoBehaviour
{
    public SocketConnect Connect;
    public LoadingFirstUI loadScreen;
    
    private DateTime applicationStartTime = DateTime.Now;
    private DateTime voiceChatJoinedTime = DateTime.MinValue;

    public DemoManager DemoManager;

    private AuthTicket ticket;
    // Start is called before the first frame update
    void Start()
    {
        SteamClient.Init("REMOVED");
        Debug.Log(SteamClient.Name);
        DemoManager.Init();
        SteamUserStats.RequestCurrentStats();

    }

    public void joinGame()
    {
        voiceChatJoinedTime = DateTime.Now;
    }

    public void leaveGame()
    {
        if (voiceChatJoinedTime != DateTime.MinValue)
        {
            var hoursVoiceChatUsed = DateTime.Now - voiceChatJoinedTime;
            if (hoursVoiceChatUsed.TotalHours > 24f)
            {
                return;
            }
            SteamUserStats.AddStat("VOICECHAT_USAGE",(float) hoursVoiceChatUsed.TotalHours);
            voiceChatJoinedTime = DateTime.MinValue;
        }
    }

    public static void updateVoiceSkinStat(string vs, float time)
    {
        if (time > 24f)
        {
            return;
        }
        SteamUserStats.AddStat("VOICESKIN_USAGE_" + vs, time);
        SteamUserStats.StoreStats();
        
    }
    
    private static string ByteArrayToString(byte[] ba)
    {
        return BitConverter.ToString(ba).Replace("-","");
    }

    public void Authenticate()
    {
        ticket = SteamUser.GetAuthSessionTicket();
        Connect.SteamAuth(ByteArrayToString(ticket.Data));
    }

    public void OnAuthenticate(SocketIOEvent e)
    {
        var receivedID = e.data.GetField("steamId").str;
        if (receivedID != SteamClient.SteamId.ToString())
        {
            Debug.LogError("Wrong SteamID received. Shutting down.");
            Application.Quit();
        }
        else
        {
            loadScreen.FinishedLoading("steam");

            Debug.Log("Steam login success");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnApplicationQuit()
    {
        var hoursPlayed =  (float) (DateTime.Now - applicationStartTime).TotalHours;
        if (hoursPlayed > 24f)
        {
            Debug.LogError("Time too high.");
            return;
        }
        SteamUserStats.AddStat("HOURS_PLAYED_NEW",hoursPlayed);
        SteamUserStats.StoreStats();
        leaveGame();
        if (ticket != null)
        {
            ticket.Cancel();
        }
        SteamClient.Shutdown();

    }
}
