// Copyright Epic Games, Inc. All Rights Reserved.

namespace Epic.OnlineServices.UnityIntegration
{
	public enum VoiceState
	{
		NotConnected,
		Connecting,
		Connected,
		Disconnecting
	}
}
