using Epic.OnlineServices.Platform;
using Epic.OnlineServices.Logging;
using System;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;
using DefaultNamespace;

namespace Epic.OnlineServices.UnityIntegration{
    public class EOSManager: MonoBehaviour{

        private EOSAuthManager _authManager;
        public EOSVoiceManager _voiceManager;
        private EOSConnectManager _connectManager;

        public UIJoinServer uiJoinServer;
        public ErrorUI ErrorUI;
        public UISettings uiSettings;

        public UIAuth uiAuth;

        public static PlatformInterface s_PlatformInterface;
        private const float c_PlatformTickInterval = 0.1f;
        private float m_PlatformTickTimer = 0f;


#region UnityStartupStuff
      // If we're in editor, we should dynamically load and unload the SDK between play sessions.
    // This allows us to initialize the SDK each time the game is run in editor.

#if UNITY_EDITOR
#if UNITY_EDITOR_WIN
    [DllImport("Kernel32.dll")]

    private static extern IntPtr LoadLibrary(string lpLibFileName);

    [DllImport("Kernel32.dll")]
    private static extern int FreeLibrary(IntPtr hLibModule);

    [DllImport("Kernel32.dll")]
    private static extern IntPtr GetProcAddress(IntPtr hModule, string lpProcName);

#elif UNITY_EDITOR_OSX

    [DllImport("libdl")]
    private static extern IntPtr dlopen(string lpLibFileName, int flags);

    [DllImport("libdl.dylib")]
    private static extern IntPtr dlclose(IntPtr hLibModule);
    [DllImport("libdl.dylib")]
    private static extern IntPtr dlerror();

    [DllImport("libdl.dylib")]
    private static extern IntPtr dlsym(IntPtr hLibModule, string lpProcName);

#endif

    private IntPtr m_LibraryPointer;
#endif

    private void Awake() {
#if UNITY_ANDROID
      AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
      AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
      AndroidJavaClass EOSSDK = new AndroidJavaClass("com.epicgames.mobile.eossdk.EOSSDK");
      AndroidJavaObject context = activity.Call<AndroidJavaObject>("getApplicationContext");
      EOSSDK.CallStatic("init",context);
      #endif

#if UNITY_EDITOR

#if UNITY_EDITOR_WIN
      var libraryPath =
        $"{Directory.GetCurrentDirectory()}\\Assets\\EOSSDK\\Bin\\{Config.LibraryName}";
      m_LibraryPointer = LoadLibrary(libraryPath);
#elif UNITY_EDITOR_OSX
      var libraryPath =
        $"{Directory.GetCurrentDirectory()}/Assets/EOSSDK/Bin/{Config.LibraryName}.dylib";

      m_LibraryPointer = dlopen(libraryPath, 2);
#endif

      if(m_LibraryPointer == IntPtr.Zero) {
          #if UNITY_EDITOR_OSX
        var error = Marshal.PtrToStringAnsi(dlerror());
        #else
        var error = "";
        #endif
        
        throw new Exception($"Failed to load library {libraryPath} with error {error}");
      }

#if UNITY_EDITOR_WIN
        WindowsBindings.Hook(m_LibraryPointer, GetProcAddress);
        Bindings.Hook(m_LibraryPointer, GetProcAddress);
#elif UNITY_EDITOR_OSX
      Bindings.Hook(m_LibraryPointer, dlsym);

#endif

#endif
    }

    private void OnApplicationQuit() {
      if(s_PlatformInterface != null) {
        s_PlatformInterface.Release();
        s_PlatformInterface = null;
        PlatformInterface.Shutdown();
      }

#if UNITY_EDITOR
      if(m_LibraryPointer != IntPtr.Zero) {
        Bindings.Unhook();

// Free until the module ref count is 0
#if UNITY_EDITOR_WIN
        while(FreeLibrary(m_LibraryPointer) != 0) {
        }
#elif UNITY_EDITOR_OSX
        while(dlclose(m_LibraryPointer) != IntPtr.Zero) {
        }
#endif
        m_LibraryPointer = IntPtr.Zero;
      }
#endif
    }

#endregion

        public void Start(){
            _authManager = new EOSAuthManager();
            _connectManager = new EOSConnectManager(this);
            InitializePlatformInterface();
        }

        public void DeviceIDLogin(string username){
            _connectManager.DeviceIDLogin(username);
        }

        public void LoggedIn(ProductUserId puid){

            _voiceManager.userId = puid;
            if (!PlayerPrefs.HasKey("firstTime")){
            
              uiSettings.openSettingsBut();
            }else{
              uiJoinServer.show();
              uiSettings.ShowSettingsButton();
            }
        }

        public void Login(string credName, string port){
            _authManager.DeveloperLogin(credName, port,(bool success)=>{
                if (success){
                    _connectManager.ConnectLogin(ExternalCredentialType.Epic, credName);
                }else{
                    ErrorUI.DisplayError("Authentication failed. Is your EOS Authentication Manager running?","auth");
                    
                }
            });
        }

        public void ConnectLoginFailed(string error){
            ErrorUI.DisplayError("Unknown error occured while trying to: '"+error+"'","connect");
            uiAuth.show();
        }

        
        // Calling tick on a regular interval is required for callbacks to work.
        public void Update()
        {
            if (s_PlatformInterface != null)
            {
                m_PlatformTickTimer += Time.deltaTime;

                if (m_PlatformTickTimer >= c_PlatformTickInterval)
                {
                    m_PlatformTickTimer = 0;
                    s_PlatformInterface.Tick();
                }
            }

        }

        public EpicAccountId GetEpicAccountID(){
            return EOSAuthManager.m_EpicAccountId;
        }

         private void InitializePlatformInterface(){
         
      #if UNITY_ANDROID

      var initializeOptions =
        new AndroidInitializeOptions() { ProductName = EOSSettings.m_ProductName,
                                  ProductVersion = EOSSettings.m_ProductVersion  };
      #else
      var initializeOptions =
        new InitializeOptions() { ProductName = EOSSettings.m_ProductName,
                                  ProductVersion = EOSSettings.m_ProductVersion };
      #endif
            var initializeResult = PlatformInterface.Initialize(ref initializeOptions);
            if (initializeResult != Result.Success)
            {
                ErrorUI.DisplayError("Failed to initialize platform: " + initializeResult,"platform");
                uiAuth.hide();
                throw new Exception("Failed to initialize platform: " + initializeResult);
            }

            // The SDK outputs lots of information that is useful for debugging.
            // Make sure to set up the logging interface as early as possible: after initializing.
            LoggingInterface.SetLogLevel(LogCategory.AllCategories, LogLevel.Warning);
            LoggingInterface.SetCallback((ref LogMessage logMessage) => Debug.LogWarning(logMessage.Message));
            
          

#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
      // Find helper library
      var XAudio29DllPath = $"{Application.streamingAssetsPath}\\x64\\xaudio2_9redist.dll";
      if(!File.Exists(XAudio29DllPath)) {
        ErrorUI.DisplayError($"Unable to find xaudio2 library at '{XAudio29DllPath}'","platform");

                uiAuth.hide();

        throw new Exception($"Unable to find xaudio2 library at '{XAudio29DllPath}'.");
      }

      var options = new WindowsOptions{
        ProductId = EOSSettings.m_ProductId,
        SandboxId = EOSSettings.m_SandboxId,
        DeploymentId = EOSSettings.m_DeploymentId,
        IsServer = false,
        ClientCredentials = new ClientCredentials() { ClientId = EOSSettings.m_ClientId,
                                                      ClientSecret = EOSSettings.m_ClientSecret },
        RTCOptions =
          new WindowsRTCOptions() {

            PlatformSpecificOptions =
              new WindowsRTCOptionsPlatformSpecificOptions() {
                XAudio29DllPath = XAudio29DllPath
              }

          },
          #if UNITY_EDITOR_WIN
        Flags = PlatformFlags.LoadingInEditor
        #else
        //Flags = PlatformFlags.DisableOverlay
        #endif

      };
      
#else
      var options =
        new Options() { ProductId = EOSSettings.m_ProductId,
                        SandboxId = EOSSettings.m_SandboxId,
                        DeploymentId = EOSSettings.m_DeploymentId,
                        ClientCredentials =
                          new ClientCredentials() { ClientId = EOSSettings.m_ClientId,
                                                    ClientSecret = EOSSettings.m_ClientSecret },
                        RTCOptions =
                          new RTCOptions() {

                          },
        IsServer = false,
                          
                          #if UNITY_EDITOR
                        Flags = PlatformFlags.LoadingInEditor
                        #else
                        //Flags = PlatformFlags.DisableOverlay
                        #endif
                         };
#endif
            
            s_PlatformInterface = PlatformInterface.Create(ref options);
            s_PlatformInterface.SetApplicationStatus(ApplicationStatus.Foreground);
            s_PlatformInterface.SetNetworkStatus(NetworkStatus.Online);
            if (s_PlatformInterface == null)
            {
                ErrorUI.DisplayError("Failed to create platform. Did you set your credentials right?","platform");
                uiAuth.hide();
                throw new Exception("Failed to create platform");
            }
            RTC.SetSettingOptions settingOptions = new RTC.SetSettingOptions{
              SettingName = "DisableEchoCancelation",
              SettingValue = "True"
            };
            var res = s_PlatformInterface.GetRTCInterface().SetSetting(ref settingOptions);
            if (res != Result.Success){
              throw new Exception("Failed setting echo cancellation "+ res);
            }
           
            settingOptions = new RTC.SetSettingOptions{
              SettingName = "DisableNoiseSupression",
              SettingValue = "False"
            };
           res = s_PlatformInterface.GetRTCInterface().SetSetting(ref settingOptions);
            if (res != Result.Success){
              throw new Exception("Failed setting noise suppression "+ res);
            }
            settingOptions = new RTC.SetSettingOptions{
              SettingName = "DisableDtx",
              SettingValue = "True"
            };
          res = s_PlatformInterface.GetRTCInterface().SetSetting(ref settingOptions);
            if (res != Result.Success){
              throw new Exception("Failed setting dtx "+ res);
            }

         }
    }
}