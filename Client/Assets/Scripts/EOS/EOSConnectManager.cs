using UnityEngine;
using Epic.OnlineServices.Auth;
using Epic.OnlineServices;

namespace Epic.OnlineServices.UnityIntegration{
    public class EOSConnectManager{
        
        public delegate void ConnectLoginCallback();

        public ProductUserId m_ProductUserId;

        private EOSManager _EOSManager;

        public EOSConnectManager(EOSManager newManager){
            _EOSManager = newManager;
        }
        

        private Token? CopyUserAuthToken()
		{
			Token? token;

			var copyUserAuthTokenOptions = new CopyUserAuthTokenOptions();
			var result = EOSManager.s_PlatformInterface.GetAuthInterface().CopyUserAuthToken(ref copyUserAuthTokenOptions,EOSAuthManager.m_EpicAccountId,  out token);
			Debug.Log($"CopyUserAuthToken"+ result);

			return token;
		}

		public void DeviceIDLogin(string username){
			var options = new Connect.CreateDeviceIdOptions(){
				DeviceModel = SystemInfo.operatingSystem + SystemInfo.processorType
			};
			EOSManager.s_PlatformInterface.GetConnectInterface().CreateDeviceId(ref options, username, DeviceIDCallback);
		}

		public void DeviceIDCallback(ref Connect.CreateDeviceIdCallbackInfo info){
			if (info.ResultCode == Result.Success || info.ResultCode == Result.DuplicateNotAllowed){
				ConnectLogin(ExternalCredentialType.DeviceidAccessToken, (string) info.ClientData);
			}else{
				Debug.Log("Error connecting with device ID: "+info.ResultCode);
			}
		}

		// A username only needs to be provided if authorized through deviceid
        public void ConnectLogin(ExternalCredentialType type, string username = "")
		{
			var connectLoginOptions = new Connect.LoginOptions()
			{
				
			};
			var Credentials = new Connect.Credentials()
			{
				Type = type, //EOSSettings.ExternalCredentialType,
			};
			if (type == ExternalCredentialType.DeviceidAccessToken){
				connectLoginOptions.UserLoginInfo = new Connect.UserLoginInfo(){
					DisplayName = username
				};
				Credentials.Token = null;
			}else{
				var token = CopyUserAuthToken();
				if (token == null)
				{
					_EOSManager.ConnectLoginFailed("Get Auth Token");
					return;
				}else{
					Credentials.Token = token.Value.AccessToken;
				}
			}

			connectLoginOptions.Credentials = Credentials;
			EOSManager.s_PlatformInterface.GetConnectInterface().Login(ref connectLoginOptions, null, OnConnectLogin);
		}

        private void OnConnectLogin(ref Connect.LoginCallbackInfo loginCallbackInfo)
		{
			Debug.Log($"OnConnectLogin" + loginCallbackInfo.ResultCode);

			if (loginCallbackInfo.ResultCode == Result.Success)
			{
                m_ProductUserId = loginCallbackInfo.LocalUserId;
                _EOSManager.LoggedIn(m_ProductUserId);

			}
			else if (loginCallbackInfo.ResultCode == Result.InvalidUser)
			{
				CreateConnectUser(loginCallbackInfo.ContinuanceToken);
			}
			else if (Common.IsOperationComplete(loginCallbackInfo.ResultCode))
			{
				_EOSManager.ConnectLoginFailed("Login to Connect");
			}
		}


		private void CreateConnectUser(ContinuanceToken continuanceToken)
		{
			var createUserOptions = new Connect.CreateUserOptions()
			{
				ContinuanceToken = continuanceToken
			};

			EOSManager.s_PlatformInterface.GetConnectInterface().CreateUser(ref createUserOptions, null, OnCreateUser);
		}

		private void OnCreateUser(ref Connect.CreateUserCallbackInfo createUserCallbackInfo)
		{
			Debug.Log($"OnCreateUser" + createUserCallbackInfo.ResultCode);

			if (createUserCallbackInfo.ResultCode == Result.Success)
			{
				ConnectLogin(ExternalCredentialType.Epic);
			}
			else if (Common.IsOperationComplete(createUserCallbackInfo.ResultCode))
			{
				_EOSManager.ConnectLoginFailed("Create Connect User");
			}
		}
    }
}