using Epic.OnlineServices.Auth;
using Epic.OnlineServices.Connect;
using Epic.OnlineServices;
using UnityEngine;
using System;

namespace Epic.OnlineServices.UnityIntegration{
    public class EOSSettings{
        // Set these values as appropriate. For more information, see the Developer Portal documentation.

        public static string m_ProductName = "Lightspeed Dating";
        public static string m_ProductVersion = "1.0.1";
        public static string m_ProductId = "767d33e164e945e6894d21f11b0edf63";
        public static string m_SandboxId = "11feec286ca04333aede57314faf055d";
        public static string m_DeploymentId = "fa2180a4076349ca9d27305466599eb7";
        public static string m_ClientId = "xyza7891ZpIA7wOviUYRaG6F1AhAQO0p";
        public static string m_ClientSecret = "aGlOhPfjn5NDMNGkjPtfBPPXUA99UZHXkLSlp+Q/sGA";
		public static ExternalCredentialType ExternalCredentialType { get; private set; } = ExternalCredentialType.Epic;
        
		public static LoginCredentialType m_LoginCredentialType { get; private set; } = LoginCredentialType.Developer;
		/// These fields correspond to <see cref="Credentials.Id" /> and <see cref="Credentials.Token" />, and their use differs based on the login type.
		/// For more information, see <see cref="Credentials" /> and the Auth Interface documentation.
    
    }
}