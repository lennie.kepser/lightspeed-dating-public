// This code is provided for demonstration purposes and is not intended to represent ideal practices.
using Epic.OnlineServices.RTC;
using Epic.OnlineServices.RTCAudio;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Modulate;
using System;
using System.IO;
using SocketIO;
using DefaultNamespace;
using System.Timers;
using System.Collections;
using System.Runtime.InteropServices;
using System.Threading;
using Epic.OnlineServices.Stats;
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
using WebRtcVadSharp;
#endif

namespace Epic.OnlineServices.UnityIntegration{

    public class EOSVoiceManager : MonoBehaviour
    {
		public VoiceState State;
		public string RoomName;
		public static EOSVoiceManager instance;
		public ErrorUI ErrorUI;
		public ModulateManager modulateManager;
		int audioPosition = 0;
		public ProductUserId userId;

		public List<string> joinedRooms = new List<string>();
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN

	    private WebRtcVad vad = new WebRtcVad(){
		    OperatingMode = OperatingMode.HighQuality,
		    FrameLength = FrameLength.Is20ms,
		    SampleRate = WebRtcVadSharp.SampleRate.Is48kHz,
	    };
#endif

		public delegate void JoinRoomCallback(JoinRoomCallbackInfo info);


		public JoinRoomCallback joinRoomCallback = null;

		public int SampleRate = 48000;
		private float counter = 0f;
		public float micAmp = 1f;

		private Dictionary<ProductUserId, float> changedVolumes = new Dictionary<ProductUserId, float>();
		public void Start()
		{
			EOSVoiceManager.instance = this;
		}

		public void updateVoiceWearStat(string voiceSkinNumber, double minutes)
		{
			var ingestData = new IngestData()
			{
				IngestAmount = (int) minutes,
				StatName = "VOICESKIN_USAGE_"+voiceSkinNumber
			};
			var ingestArray = new IngestData[]
			{
				ingestData
			};
			var options = new IngestStatOptions()
			{
				LocalUserId = userId,
				TargetUserId = userId,
				Stats = ingestArray
			};
			EOSManager.s_PlatformInterface.GetStatsInterface().IngestStat(ref options,null,
				delegate(ref IngestStatCompleteCallbackInfo data)
				{
					Debug.Log("Data submitted: "+voiceSkinNumber+" minutes: "+minutes+" result code: "+data.ResultCode);
				});

		}
		
		public void updatePlayerVolume(ProductUserId otherUserId, float volume)
		{
			UpdateParticipantVolumeOptions options = new UpdateParticipantVolumeOptions()
			{
				LocalUserId = userId,
				ParticipantId = otherUserId,
				RoomName = RoomName,
				Volume = volume
			};
			changedVolumes[otherUserId] = volume;
			EOSManager.s_PlatformInterface.GetRTCInterface().GetAudioInterface().UpdateParticipantVolume(ref options, null, null);

		}

		private void getPlayerVolumesAndUpdate(string RoomName)
		{
			foreach (var keyValue in changedVolumes)
			{
				UpdateParticipantVolumeOptions options = new UpdateParticipantVolumeOptions()
				{
					LocalUserId = userId,
					ParticipantId = keyValue.Key,
					RoomName = RoomName,
					Volume = keyValue.Value
				};
				EOSManager.s_PlatformInterface.GetRTCInterface().GetAudioInterface().UpdateParticipantVolume(ref options, null, null);

			}
		}

		public void JoinVCRoomEvent(SocketIOEvent e){
            var cur = e.data;
			Join(e.data.GetField("roomname").str,e.data.GetField("baseurl").str,e.data.GetField("token").str, e.data.GetField("muted").b, (int) e.data.GetField("voiceskin").n, e.data.GetField("echo").b);
		}

        public void setMute(bool mute, string roomName)
        {
	        RTCAudioStatus status  = RTCAudioStatus.Disabled;
	        if (!mute)
	        {
		        status = RTCAudioStatus.Enabled;
	        }
	        UpdateSendingOptions sendingOptions = new UpdateSendingOptions()
	        {
		        LocalUserId = userId,
		        RoomName = roomName,
		        AudioStatus = status
	        };
	        EOSManager.s_PlatformInterface.GetRTCInterface().GetAudioInterface().UpdateSending(ref sendingOptions, null , null);
        }


        public void Join(string roomName, string baseUrl, string token, bool muted, int voiceskin, bool echo)
		{
			var flags = JoinRoomFlags.None;
			if (echo){
				flags = JoinRoomFlags.EnableEcho;
			}
			var joinRoomOptions = new JoinRoomOptions()
			{
				LocalUserId = userId,
				RoomName = roomName,
				ClientBaseUrl = baseUrl,
				ParticipantToken = token,
				Flags = flags
				
			};
			if (IsReplayRoom(roomName)){
				joinRoomOptions.ManualAudioInputEnabled = true;
				joinRoomOptions.Flags = JoinRoomFlags.EnableEcho;
			}
			if (muted){
				joinRoomOptions.ManualAudioInputEnabled = true;
			}
			joinedRooms.Add(roomName);
			State = VoiceState.Connecting;
			EOSManager.s_PlatformInterface.GetRTCInterface().JoinRoom(ref joinRoomOptions, voiceskin, OnJoinRoom);
		}

		private void OnJoinRoom(ref JoinRoomCallbackInfo joinRoomCallbackInfo)
		{
			RoomName = joinRoomCallbackInfo.RoomName;
			var options = new RTCAudio.AddNotifyParticipantUpdatedOptions(){
				LocalUserId = joinRoomCallbackInfo.LocalUserId,
				RoomName = RoomName
			};
			EOSManager.s_PlatformInterface.GetRTCInterface().GetAudioInterface().AddNotifyParticipantUpdated(ref options, null, ParticipantUpdateCallback);
			Debug.Log($"JoinRoom " + joinRoomCallbackInfo.ResultCode);
			if (joinRoomCallbackInfo.ResultCode == Result.Success)
			{

				if (!joinedRooms.Contains(RoomName)){
				leaveRoom(RoomName);
					return;
				}

				State = VoiceState.Connected; 


				if (joinRoomCallback != null){
					joinRoomCallback(joinRoomCallbackInfo);
				}
		
					var _callbackOptionsLoopback = new RTCAudio.AddNotifyAudioBeforeRenderOptions(){
				LocalUserId = joinRoomCallbackInfo.LocalUserId,
				UnmixedAudio = true
			};
				//SubscribeToRoomNotifications();
				var _callbackOptions = new RTCAudio.AddNotifyAudioBeforeSendOptions(){
					LocalUserId = joinRoomCallbackInfo.LocalUserId,
					RoomName = joinRoomCallbackInfo.RoomName
				};
				EOSManager.s_PlatformInterface.GetRTCInterface().GetAudioInterface().
				AddNotifyAudioBeforeSend(ref _callbackOptions, null, ConversionCallback);
				var _renderCallbackOptions = new RTCAudio.AddNotifyAudioBeforeRenderOptions(){
					UnmixedAudio = true,
					RoomName = joinRoomCallbackInfo.RoomName,
					LocalUserId = joinRoomCallbackInfo.LocalUserId
				};
				Debug.Log((int) joinRoomCallbackInfo.ClientData != -1 );
				if ((int) joinRoomCallbackInfo.ClientData != -1 || IsReplayRoom(joinRoomCallbackInfo.RoomName)){

					RTC.SetRoomSettingOptions settingOptions = new RTC.SetRoomSettingOptions{
						LocalUserId = joinRoomCallbackInfo.LocalUserId,
						RoomName = joinRoomCallbackInfo.RoomName,
						SettingName = "DisableEchoCancelation",
						SettingValue = "True"
					};
					var res = EOSManager.s_PlatformInterface.GetRTCInterface().SetRoomSetting(ref settingOptions);
					if (res != Result.Success){
						throw new Exception("Failed setting echo cancelation "+ res);
					}
				}

				if (!IsReplayRoom(joinRoomCallbackInfo.RoomName))
				{
					getPlayerVolumesAndUpdate(joinRoomCallbackInfo.RoomName);
				}
			
			}else if(joinRoomCallbackInfo.ResultCode == Result.NoConnection)
			{
				ErrorUI.TerminalCrash("The voice chat servers are currently unavailable. Please try again later. Sorry!");
			}
			else if (Common.IsOperationComplete(joinRoomCallbackInfo.ResultCode))
			{
				State = VoiceState.NotConnected;
				
			}
		}

		public void SetAudioSetting(string settingName, bool value)
		{
			var settingOptions = new RTC.SetSettingOptions{
				SettingName = settingName,
				SettingValue = (value ? "True" : "False")
			};
           
			var res = EOSManager.s_PlatformInterface.GetRTCInterface().SetSetting(ref settingOptions);
			if (res != Result.Success){
				throw new Exception("Failed setting "+settingName+ res);
			}
			else
			{
				Debug.Log(res);
				Debug.Log("Set "+settingName + " to "+settingOptions.SettingValue);
			}

			var roomSettingOptions = new RTC.SetRoomSettingOptions()
			{
				SettingName = settingName,
				SettingValue = (value ? "True" : "False"),
				LocalUserId = userId
			};

			foreach (var room in joinedRooms)
			{
				roomSettingOptions.RoomName = room;
				res = EOSManager.s_PlatformInterface.GetRTCInterface().SetRoomSetting(ref roomSettingOptions);
				if (res != Result.Success){
					throw new Exception("Failed setting room setting "+settingName+" for room "+ room+ res);
				}
			}
		}

		public void AddSpeakingCallback(OnParticipantUpdatedCallback callback, string roomName){
			var _callbackOptions = new RTCAudio.AddNotifyParticipantUpdatedOptions(){
				LocalUserId = userId,
				RoomName = roomName
			};
			EOSManager.s_PlatformInterface.GetRTCInterface().GetAudioInterface().
				AddNotifyParticipantUpdated(ref _callbackOptions, null, callback);
				
		}

		public void ParticipantUpdateCallback(ref ParticipantUpdatedCallbackInfo info){
			//Debug.Log(info.ParticipantId + " speaking: "+info.Speaking+" in room "+info.RoomName);
		}

		public void ResetSampleRate(){
			SampleRate = 48000;
		}


		private byte[] ShortBufferToByteBuffer(short[] frames)
		{
			var output = new List<byte>();
			foreach (short frame in frames)
			{
				output.Add((byte) frame);
				output.Add((byte) (frame >> 8));
			}

			return output.ToArray();
		}

		
		bool DoesFrameContainSpeech(byte[] audioFrame)
		{
			//TODO: adjustable sample rate
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN

			return vad.HasSpeech(audioFrame);
#endif
			return true;
		}

		public void ConversionCallback(ref AudioBeforeSendCallbackInfo data)
		{
			byte[] buffer = ShortBufferToByteBuffer(data.Buffer.Value.Frames);
			var micAmp = this.micAmp;
			bool vadResult = DoesFrameContainSpeech(buffer);

				SampleRate = (int) data.Buffer.Value.SampleRate;
				if (vadResult)
				{
					counter += 0.05f;
					if (counter > 1)
					{
						counter = 1;
					}
				}
				else
				{
					counter -= 0.05f;
					if (counter < 0)
					{
						counter = 0;
					}

				}
				micAmp *= (counter);

			modulateManager.convert_audio(data.Buffer.Value.InternalBuffer.m_Frames,(int) data.Buffer.Value.Frames.Length, (int) data.Buffer.Value.SampleRate, (int) data.Buffer.Value.Channels, micAmp);

		}

		// Finds a room that has the -replays suffix. Returns an empty string if it can't be found
		public string GetReplayRoom(){
			var output = "";
			foreach(var room in joinedRooms){
				if (IsReplayRoom(room)){
					output = room;
				}
			}
			return output;
		}

		public bool IsReplayRoom(string roomid){
			return roomid.Split('-').Last<string>() == "replays";
		}

		public void InjectAudio(AudioBuffer buffer, string room){
			var sendAudioOptions = new SendAudioOptions(){
					LocalUserId = userId,
					RoomName = room,
					Buffer = buffer
				};
            
			EOSManager.s_PlatformInterface.GetRTCInterface().GetAudioInterface().SendAudio(ref sendAudioOptions);
		}

		public void leaveRoom(string room){
			State = VoiceState.Disconnecting;

			var leaveRoomOptions = new LeaveRoomOptions(){
					LocalUserId = userId,
					RoomName = room
				};
				
			EOSManager.s_PlatformInterface.GetRTCInterface().LeaveRoom(ref leaveRoomOptions,null,(ref LeaveRoomCallbackInfo info)=>{
				if (joinedRooms.Contains(room)){
					joinedRooms.Remove(room);
				}
				if (joinedRooms.Count == 0){
					State = VoiceState.NotConnected;
				}

			});

		}

		public void leaveChannels(){
			foreach(var room in joinedRooms.ToArray()){
				leaveRoom(room);
			}
			joinedRooms.Clear();
		}

	

		public void OnApplicationQuit(){
			//leaveChannels();
		}

    }
}