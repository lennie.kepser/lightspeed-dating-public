using Epic.OnlineServices.Auth;
using UnityEngine;

namespace Epic.OnlineServices.UnityIntegration{
    public class EOSAuthManager{
        public delegate void AuthLoginCallback(bool success);

        public static EpicAccountId m_EpicAccountId;



        public void DeveloperLogin(string credName, string port, AuthLoginCallback callback){
           

            var loginOptions = new LoginOptions()
            {
                Credentials = new Credentials()
                {
                    Type = EOSSettings.m_LoginCredentialType,
                    Id = "localhost:"+port,
                    Token = credName
                }
            };

            // Ensure platform tick is called on an interval, or this will not callback.
            EOSManager.s_PlatformInterface.GetAuthInterface().Login(ref loginOptions, null, (ref LoginCallbackInfo loginCallbackInfo) =>
            {
                if (loginCallbackInfo.ResultCode == Result.Success)
                {
                    Debug.Log("Login succeeded");
                    m_EpicAccountId = loginCallbackInfo.LocalUserId;
                    callback(true);


                }
                else if (Common.IsOperationComplete(loginCallbackInfo.ResultCode))
                {
                    Debug.Log("Login failed: " + loginCallbackInfo.ResultCode);
                    callback(false);
                }
            });
            
        }
    }
} 