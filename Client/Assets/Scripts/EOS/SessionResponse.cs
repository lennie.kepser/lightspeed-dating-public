// Copyright Epic Games, Inc. All Rights Reserved.

using System.Collections.Generic;

namespace Epic.OnlineServices.UnityIntegration
{
	public class SessionResponse
	{
		public string RoomName;
		public string OwnerLock;
		public string ClientBaseUrl;
		public Dictionary<string, string> JoinTokens;
	}
}
