using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace{

public class ReplayUser : MonoBehaviour
{
    // Start is called before the first frame update
    public Text nameObject;

    public GameObject died;
    public GameObject monsterObject;

    public void SetUp(string name, string disguise, bool dead, bool monster){
        nameObject.text = name + (monster ? " as " + disguise : "");
        died.SetActive(dead);
        monsterObject.SetActive(monster);
    }
    
}
}