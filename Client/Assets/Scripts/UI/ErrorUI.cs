using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;
using Epic.OnlineServices.UnityIntegration;
using UnityEngine.UI;

namespace DefaultNamespace{
public class ErrorUI : MonoBehaviour
{
    // Start is called before the first frame update
    public Button closeError;
    public GameObject errorWindow;
    public Text errorText;
    public UIJoinServer uiJoinServer;
    public EOSVoiceManager voiceManager;
    public UIAuth UIAuth;

    void Start()
    {
        closeError.onClick.AddListener(CloseError);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void CloseError()
        {
            errorWindow.gameObject.SetActive(false);
            
        }

        public void DisplayError(string text, string type)
        {
            if (text == "socket")
            {
                text =
                    "Connection to the game servers could not be established. Please check your internet connection or restart the game.";
            }
            errorWindow.gameObject.SetActive(true);
            var ErrorText = errorText.GetComponent<Text>();
            ErrorText.text = text;
            if (type == "join" || type == "create server")
            {
                
                uiJoinServer.show();
            }

            if (type == "auth")
            {
                UIAuth.show();
            }
            
        }

    public void Crash(SocketIOEvent e){
        errorText.GetComponent<Text>().text = e.data.GetField("msg").str;
        errorWindow.gameObject.SetActive(true);
        voiceManager.leaveChannels();
        uiJoinServer.show();

    }

    public void TerminalCrash(string message)
    {
        errorText.GetComponent<Text>().text = message;
        errorWindow.gameObject.SetActive(true);
        voiceManager.leaveChannels();
        uiJoinServer.show();
        closeError.onClick.AddListener((() =>
        {
            Application.Quit();
        }));

    }
}
}