using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using SocketIO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIQuery : MonoBehaviour
{
    public Button RefreshQuery;
    public SocketConnect Connect;
    public TextMeshProUGUI query;

    private void RefreshQueryButton()
    {
        RefreshQuery.gameObject.SetActive(false);
        Debug.Log("pressed");
        Debug.Log(RefreshQuery.gameObject.activeSelf);

        Connect.SendEmptyGameAction("refresh question");
    }

    private void ChangeQuery(SocketIOEvent ev)
    {
            
        String newQuery = ev.data.GetField("newQuery").str;
        query.text = newQuery;
    }

    void Start()
    {
        RefreshQuery.onClick.AddListener(RefreshQueryButton);
        Connect.AddSocketListener("new query",ChangeQuery);
    }
}
