using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using JsonStuff;

namespace DefaultNamespace{
    public class MonsterSelectUI : UIWindowTimer
    {
        public Text MonsterInfo;
        public SocketConnect Connect;

        private List<GameObject> _createdui;

        public GameObject MonsterUIElement;

        public GameObject WaitForOtherMonsters;
        public GameObject MonsterSelectContainer;

        public Color32 highlightedColor = new Color32(255, 251, 114, 100);

        [SerializeField] private InteractiveTutorialUI tutorial;
        
        private void ChooseTarget(string target)
        {
            tutorial.Next();
            Connect.PickTarget(target);
            
            hide();
        }
        
        private void addMonsterPickTargetButton(string label, string send)
        {
            var newui = Instantiate(MonsterUIElement,MonsterSelectContainer.transform) as GameObject;
            newui.transform.GetChild(0).GetComponent<Text>().text = label;
            newui.name = "MonsterTarget" + label;
            if (send == "skip")
            {
                newui.GetComponent<Image>().color = highlightedColor;
            }
            newui.SetActive(true);
            var but = newui.GetComponent<Button>();
            but.onClick.AddListener(()=>ChooseTarget(send));
            _createdUI.Add(newui);
        }

        public void createMonsterUI(List<Player> players, int gameMode)
        {
            WaitForOtherMonsters.SetActive(false);
            MonsterSelectContainer.SetActive(true);
            show();
            cleanUI();
            MonsterInfo.text =
                "Choose who you want to impersonate:";
            int amntalive = 0;
            foreach (var player in players)
            {
                if (player.alive && !player.spectating)
                {
                    amntalive++;
                }

                // add your own disguise button on top if mayhem mode
                if (gameMode == 1 && player.username == manager.GM.username)
                {
                    addMonsterButton(player, players, gameMode);
                }
            }
            foreach (var player in players)
            {
                //TODO: Make work for multiple monsters
                if ((!player.monster || (gameMode == 1 && player.username != manager.GM.username)) && !(amntalive==2 && player.alive) && !player.spectating)
                {
                    addMonsterButton(player, players, gameMode);
                }
            }
            setTime(manager.timers[1]);
        }

        public void createMonsterPlayerPickUI(List<Player> players, string selectedplayer)
        {
            cleanUI();
            show();
            MonsterInfo.text =
                "Choose who you want to be in a room with:";

            var amntplayers = 0;
            foreach (var player in players)
            {
                if (!player.monster && player.alive && player.username != selectedplayer  && !player.spectating)
                {
                    amntplayers++;
                    addMonsterPickTargetButton(player.username, player.username);
                }
            }

            if (amntplayers > 0)
            {
                addMonsterPickTargetButton("Skip","!skip");
            }
        }

        
        
        private void addMonsterButton(Player player, List<Player> players, int gameMode)
        {
            var newui = Instantiate(MonsterUIElement,MonsterSelectContainer.transform) as GameObject;
            string name = player.username;
            if (gameMode == 1 && player.username == manager.GM.username)
            {
                name += " (Yourself)";
                newui.GetComponent<Image>().color = highlightedColor;

            }
            newui.transform.GetChild(0).GetComponent<Text>().text = name;
            newui.name = "Monster" + player.username;
            newui.SetActive(true);
            var but = newui.GetComponent<Button>();
            but.onClick.AddListener(()=>ChooseDisguise(player.username, players, gameMode));
            _createdUI.Add(newui);
        }

        private void ChooseDisguise(string username, List<Player> players, int gameMode)
        {
            tutorial.Next();
            Connect.PickDisguise(username);
            if (gameMode != 1)
            {
                StartCoroutine(pickPhaseNextStepWait(username, players));
            }
            else
            {
                WaitForOtherMonsters.SetActive(true);
            }
            MonsterSelectContainer.SetActive(false);
        }

        IEnumerator pickPhaseNextStepWait(string username, List<Player> players)
        {
            yield return new WaitForSeconds(0.2f);
            createMonsterPlayerPickUI(players, username);
            MonsterSelectContainer.SetActive(true);

        }

        public override void show()
        {
            base.show();
            manager.deathAnimation.HideEyes();
            manager.spriteManager.HideAllCharacters();
        }
    }
}