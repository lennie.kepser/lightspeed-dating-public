using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ResolutionUI : MonoBehaviour
{
    public TMP_Dropdown resolution;
    // Start is called before the first frame update
    void Start()
    {
        foreach (var res in Screen.resolutions)
        {
            resolution.options.Add(new TMP_Dropdown.OptionData(text: res.width +" * "+res.height));
        }

        if (PlayerPrefs.HasKey("resolution"))
        {
            resolution.value = PlayerPrefs.GetInt("resolution");
        }
    }

    public void valChange(int val)
    {
        PlayerPrefs.SetInt("resolution", val);
        Screen.SetResolution(Screen.resolutions[val].width, Screen.resolutions[val].height, Screen.fullScreen);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
