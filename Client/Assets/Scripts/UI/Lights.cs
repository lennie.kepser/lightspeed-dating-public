using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lights : MonoBehaviour
{
    public GameObject lightsLeft;

    public GameObject lightsLeftSpriteOn;
    public GameObject lightsLeftSpriteOff;

    public GameObject lightsRight;
    public GameObject lightsRightSpriteOn;
    public GameObject lightsRightSpriteOff;

    
    public GameObject lightsTop;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void LightsLeft(bool val)
    {
        lightsLeftSpriteOn.SetActive(val);
        lightsLeftSpriteOff.SetActive(!val);
        lightsLeft.SetActive(val);
    }
    
    public void LightsRight(bool val)
    {
        lightsRightSpriteOn.SetActive(val);
        lightsRightSpriteOff.SetActive(!val);
        lightsRight.SetActive(val);
    }
    
    public void LightsTop(bool val)
    {
        lightsTop.SetActive(val);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
