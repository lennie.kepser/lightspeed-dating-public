using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.EventSystems;


public class RoomButtonHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public LightColors.Mode color = LightColors.Mode.Off;

    public SocketConnect connect;

    private static bool timeout = false;

    private static LightColors.Mode currentColorNoTimeout = LightColors.Mode.Off;

    private static LightColors.Mode lastSendColor = LightColors.Mode.Off;

    private readonly float cooldown = 1f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public static void resetStaticVariables()
    {
        timeout = false;
        currentColorNoTimeout = LightColors.Mode.Off;
        lastSendColor = LightColors.Mode.Off;
    }

    private void sendColor(LightColors.Mode color)
    {
        if (!timeout)
        {
            timeout = true;
            lastSendColor = color;
            connect.SendColor((int) color);
            
            StartCoroutine(timeoutComplete());
        }
        
    }

    private IEnumerator timeoutComplete()
    {
        yield return new WaitForSeconds(cooldown);
        timeout = false;

        if (gameObject.activeInHierarchy)
        {

            if (lastSendColor != currentColorNoTimeout)
            {
                sendColor(currentColorNoTimeout);
            }
            
        }
        else
        {
            lastSendColor = LightColors.Mode.Off;
            currentColorNoTimeout = LightColors.Mode.Off;
        }
       

    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        currentColorNoTimeout = color;
        sendColor(color);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        currentColorNoTimeout = LightColors.Mode.Off;
        sendColor(0);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
