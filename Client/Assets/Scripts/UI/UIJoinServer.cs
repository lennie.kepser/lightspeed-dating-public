﻿using Steamworks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class UIJoinServer : UIWindow
    {
        public Button joinServer;
        public Button createServer;
        public TMP_InputField usernameInput;
        public TMP_InputField serverIDInput;
        public GameObject JoinCreateMenu;
        public GameObject JoinGameMenu;
        public Button openJoinMenu;
        public Button backButton;
        public SocketConnect SocketManager;
        public GameObject socials;

        public ErrorUI error;

        [SerializeField] private Steam _steam;
        [SerializeField] private DeathAnimation _deathAnimation;
        void Start()
        {
            joinServer.onClick.AddListener(JoinServer);
            createServer.onClick.AddListener(CreateServer);
            openJoinMenu.onClick.AddListener(OpenJoinMenu);
            backButton.onClick.AddListener(BackButton);
        }

        private void BackButton()
        {
            SetJoinMenuOpen(false);
        }

        private void OpenJoinMenu()
        {
            SetJoinMenuOpen(true);
        }

        private void SetJoinMenuOpen(bool val)
        {
            JoinGameMenu.SetActive(val);
            JoinCreateMenu.SetActive(!val);
        }

        public void LeaveServer()
        {
            show();
            manager.GM.SetInGame(false);
            manager.Connect.LeaveSession();
            manager.voiceManager.leaveChannels();
            manager.VoiceWearManager.selectSkin(-1);
            manager.spriteManager.CancelCoroutines();
            _deathAnimation.CancelAnimation();
            manager.spriteManager.SetEmptyStage();
            manager.GM.setState(0);
            _steam.joinGame();
            socials.SetActive(true);

        }

        private string getUsername()
        {
            var username = usernameInput.text;
            if (username == "")
            {
                username = SteamClient.Name;
            }

            return username;
        }

        private void JoinServer()
        {
            if (serverIDInput.text == "")
            {
                error.DisplayError("Please enter a ServerID.","");
                return;
            }
            hide();
            var username = getUsername();
            
            SocketManager.joinServer(username,serverIDInput.text);
        }

        private void CreateServer()
        {
            hide();
            var username = getUsername();

            SocketManager.CreateServer(username);
        }

        
    }
}