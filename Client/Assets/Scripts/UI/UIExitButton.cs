using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using Epic.OnlineServices.UnityIntegration;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class UIExitButton : MonoBehaviour
{
    [SerializeField] 
    private Button self;

    [SerializeField]
    private GameManager _gameManager;

    [SerializeField] private UIJoinServer _uiJoinServer;

    [SerializeField] private UITutorial uiTutorial;

    [SerializeField] private List<GameObject> contextWindows;
    
    // Start is called before the first frame update
    void Start()
    {
        self.onClick.AddListener(OnClick);
    }

    private void OnClick()
    {
        foreach (var window in contextWindows)
        {
            if (window.activeInHierarchy)
            {
                window.SetActive(false);
                return;
            }
        }
        if (uiTutorial.isActiveAndEnabled)
        {
            uiTutorial.closeTutorial();
        }else if (_gameManager.GetInGame())
        {   
            _uiJoinServer.LeaveServer();
        }
        else
        {
            Application.Quit();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
