using System;
using System.Collections;
using System.Collections.Generic;
using Epic.OnlineServices.RTCAudio;
using SocketIO;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;

public class LightColors : MonoBehaviour
{
   

    public enum Mode : int
    {
        Off = 0,
        Green = 1,
        Red = 2
    }

    public float speed = 1;

    [Serializable] public class LightData
    {
        public Light2D Light;
        public SpriteRenderer Sprite;
        public Mode mode;
    }
        
    public List<LightData> Lights;
    private bool changing = false;

    [SerializeField]
    private Mode mode = Mode.Off;
    
    // Start is called before the first frame update

    private void OnValidate()
    {
        SetMode(mode);
    }

    public void Update()
    {
        if (!changing)
            return;
        foreach (LightData lightData in Lights)
        {
            var light = lightData.Light;
            float change = -Time.deltaTime;
            if (lightData.mode == mode)
            {
                change = Time.deltaTime;
            }

            change *= speed;
            var alpha = light.color.a;
            var newAlpha = alpha + change;
            if (newAlpha < 0)
            {
                newAlpha = 0;

            }
            
            

            if (newAlpha > 1)
            {
                newAlpha = 1;
                changing = false;
            }

            var newColor = light.color;
            newColor.a = newAlpha;
            light.color = newColor;
            if (lightData.mode != Mode.Off)
            {
                lightData.Sprite.color = new Color(1, 1, 1, newAlpha);
            }
        }
    }

    public void SetMode(Mode mode)
    {
        this.mode = mode;
        changing = true;
    }

    public void SocketEvent(SocketIOEvent e)
    {
        SetMode((Mode) e.data.GetField("color").n);
    }
}
