using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DefaultNamespace;

using SocketIO;
using TMPro;

public class PlayerSettingsUI : MonoBehaviour
{
    public Toggle Spectate;
   
    public SocketConnect _connect;


    
    
    // Start is called before the first frame update
    void Start()
    {
        Spectate.onValueChanged.AddListener(delegate {
            sendUpdate();
        });
       
        
    }


    private void sendUpdate()
    {
        
        var settingDictionary = new Dictionary<string, JSONObject>();
        settingDictionary["spectating"] = new JSONObject((Spectate.isOn ? true : false));
       
        _connect.UpdatePlayerSettings(settingDictionary);
    }

   
    // Update is called once per frame
    void Update()
    {
        
    }
}
