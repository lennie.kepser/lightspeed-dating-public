using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Subtitles : MonoBehaviour
{
    private float timeOut = 0f;
    [SerializeField]

    private float textTimeout = 5f;
    [SerializeField]
    private TextMeshProUGUI text;

    private string currentlySpeaking = "";
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (timeOut > 0)
        {
            timeOut -= Time.deltaTime;
        }else if (timeOut <= 0)
        {
            text.text = "";
        }
        
    }

    public void addText(string newText, string speaker)
    {
        text.text += " "+ newText;
        if (speaker != currentlySpeaking || text.textInfo.lineCount > 2)
        {
            text.text = speaker +": " +newText;
        }

        currentlySpeaking = speaker;

        timeOut = textTimeout;
    }
}
