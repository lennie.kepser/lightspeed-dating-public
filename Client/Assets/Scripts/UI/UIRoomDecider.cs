using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Epic.OnlineServices.UnityIntegration;
using Epic.OnlineServices.RTCAudio;
using Michsky.UI.ModernUIPack;

namespace DefaultNamespace{
    public class UIRoomDecider : UIRoom
    {
        private string infoText = "Choose the real [Name]!";

        [SerializeField]
        private TextMeshProUGUI infoTextObject;

        
        [SerializeField]
        private ButtonManagerBasic playerA;

        [SerializeField]
        private ButtonManagerBasic playerB;
        

        [SerializeField]
        private GameObject speakingA;

        [SerializeField]
        private GameObject speakingB; 

        [SerializeField]
        private EOSVoiceManager voiceManager;


        private Dictionary<string, ButtonManagerBasic> puidToButtonDict = new Dictionary<string, ButtonManagerBasic>();

        private void Start()
        {
            playerA.clickEvent.AddListener(()=>{
                Debug.Log("Select A");
                manager.Connect.SendTripleChoice(0);
            });
            playerB.clickEvent.AddListener(()=>{
                Debug.Log("Select B");
                manager.Connect.SendTripleChoice(1);
            });
        }

        public void JoinRoom(string name, string[] puids, string roomname, string query,int voiceskin, string recordingID, int otherPlayerSkin){
            float turnTime = float.Parse(MatchSettings.GetComponent<MatchSettingsUI>().Timer.text);

            base.JoinRoom(query, voiceskin, recordingID, otherPlayerSkin, 3, name, "", turnTime);
            puidToButtonDict.Clear();
            infoTextObject.text = name;
            Debug.Log("Join Triple 1");
            puidToButtonDict.Add(puids[0],playerA);
            puidToButtonDict.Add(puids[1],playerB);
            Debug.Log("Join Triple 2");
            
            playerA.buttonText = name +" A";
            playerB.buttonText = name +" B";
            playerA.UpdateUI();
            playerB.UpdateUI();
            voiceManager.AddSpeakingCallback(SpeakingCallback, roomname);
           
        }

        public void SpeakingCallback(ref ParticipantUpdatedCallbackInfo info){
            
            ButtonManagerBasic button;
            puidToButtonDict.TryGetValue(info.ParticipantId.ToString(), out button);
            Debug.Log(info.ParticipantId);
            Debug.Log(info.Speaking);
            if (button != default){
                if (speakingA.transform.IsChildOf(button.transform)){
                    speakingA.SetActive(info.Speaking);
                }
                if (speakingB.transform.IsChildOf(button.transform)){
                    speakingB.SetActive(info.Speaking);
                }
            }
        }

    }

}