using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using JsonStuff;

namespace DefaultNamespace{
public class SpectateElement : MonoBehaviour
{
    // Start is called before the first frame update
    public Text questionText;

    public GameObject playerPrefab;
    public GameObject userContainer;

    public Button watchButton;

    public void SetUp(List<Player> players, string question){
        questionText.text = question;
        foreach(var player in players){
            var newui = Instantiate(playerPrefab,userContainer.transform) as GameObject;
            newui.GetComponent<ReplayUser>().SetUp(player.username, player.disguisedas,!player.alive, player.monster);
        }
            
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
}
