using System.Collections;
using System.Collections.Generic;
using Epic.OnlineServices;
using UnityEngine;
using UnityEngine.UI;
using Epic.OnlineServices.UnityIntegration;
using TMPro;

public class PlayerLobby : MonoBehaviour
{
    [SerializeField]
    private GameObject _admin;
    [SerializeField]
    private TextMeshProUGUI _username;
    [SerializeField]
    private GameObject _monster;

    public bool isSelf = false;
    private ProductUserId _userId;

    public void Start()
    {
    }

    public void setUserID(ProductUserId _userId)
    {
        this._userId = _userId;
    }

    public ProductUserId getUserID()
    {
        return this._userId;
    }

    public void setAdmin(bool val){
        _admin.SetActive(val);
    }

    public void setMonster(bool val){
        _monster.SetActive(val);
    }

    public void setUsername(string val){
        _username.text = val;
    }

    public string getUsername()
    {
        return _username.text;
    }


}
