using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SocketIO;
using Epic.OnlineServices.UnityIntegration;

namespace DefaultNamespace{
public class ReplayUI : UIWindow
{

    public enum ReplayState : uint{
        Stopped = 0,
        Playing,
        Paused
    }

    public SocketConnect socketConnect;
    public GameObject replayPrefab;

    public GameObject replayContent;

    public string lobbyvc;

    public Button anotherRound;

    public Button leaveLobby;
    public UILobby UILobby;
    public EOSVoiceManager voiceManager;
    public MatchSettingsUI MatchSettings;

    public AudioRecorder audioRecorder;
    
    public ScrollRect scrollRect;
    
    private float timeout = 0f;

    public void setLobbyVC(string lobbyvc){
        this.lobbyvc = lobbyvc;
    }

    public void Start(){
        anotherRound.onClick.AddListener(AnotherRound);
        leaveLobby.onClick.AddListener(LeaveLobby);
        socketConnect.AddSocketListener("replay corrupted",ReplayCorrupted);
        socketConnect.AddSocketListener("toggle voice filter",RecieveToggleVoiceFilter);
    }

    private void RecieveToggleVoiceFilter(SocketIOEvent ev)
    {
        foreach(var _ui in _createdUI)
        {
            var replayComponent = _ui.GetComponent<Replay>();
            if (replayComponent.id == ev.data.GetField("id").str){
                replayComponent.ChangeVoiceFilterValue(ev.data.GetField("val").b);
                if (replayComponent.IsPlaying())
                {
                    audioRecorder.setPlayUnfiltered(ev.data.GetField("val").b);
                }
            }
        }
    }

    public void ReplayCorrupted(SocketIOEvent ev){
        foreach(var _ui in _createdUI){
             if (_ui.GetComponent<Replay>().id == ev.data.GetField("id").str){
                _ui.GetComponent<Replay>().Corrupt();
             }
        }
        audioRecorder.StopTimers();
    }

    public void Update()
    {
        timeout -= Time.deltaTime;
        if (timeout < 0)
        {
            timeout = 0;
        }
    }

    public void AnotherRound(){
        //TODO: Make work with new EOS
        //VivoxManager.joinChannel(lobbyvc);
        cleanUI();
        MatchSettings.gameObject.SetActive(true);
        UILobby.show();
    }

    public void LeaveLobby(){
        Application.Quit();
    }

    public void PlayReplay(string id, int position){
        foreach(var r in _createdUI){
            
            if (r.GetComponent<Replay>().IsPlaying()){
                SetReplay(r.GetComponent<Replay>().id,r.GetComponent<Replay>().getPosition(), false);
            }
        }
        SetReplay(id, position, true);
        
    }
     public void StopReplay(string id){
        SetReplay(id, 0, false);

    }
     public void PauseReplay(string id, int position){
        SetReplay(id, position, false);
    }
     
     public void SetReplay(string id, int position, bool playing)
     {
         // Loop protection
         if (timeout > 10f)
         {
             return;
         }

         timeout += 1f;
         Dictionary<string, string> data = new Dictionary<string, string>();
         data["action"] = "replay";
         var details = new Dictionary<string, string>();
         details["id"] = id;
         var JSONdata = new JSONObject(data);
         JSONdata["details"] = new JSONObject(details);
         JSONdata["details"]["position"] = new JSONObject(position);
         JSONdata["details"]["playing"] = new JSONObject(playing);
         socketConnect.SendGameAction(JSONdata);
     }

     public static void SnapTo( ScrollRect scroller, RectTransform child )
     {
         Canvas.ForceUpdateCanvases();

         var contentPos = (Vector2)scroller.transform.InverseTransformPoint( scroller.content.position );
         var childPos = (Vector2)scroller.transform.InverseTransformPoint( child.position );
         var endPos = contentPos - childPos;
         endPos.y -= 200;
         endPos.x = scroller.content.anchoredPosition.x;
         scroller.content.anchoredPosition = endPos;
     }

    public void PlayReplaySet(string id, int positionIn10MS){
        var replay = GetReplay(id);
        if (!replay){
            return;
        }
        Canvas.ForceUpdateCanvases();
        
        SnapTo(scrollRect, replay.gameObject.GetComponent<RectTransform>());
      
        audioRecorder.setPlayUnfiltered(replay.getVoiceFilterToggle());
        
        replay.SetState(ReplayState.Playing, positionIn10MS);
    }

    public void StopReplaySet(string id, int positionIn10MS){
        var replay = GetReplay(id);
        if (!replay){
            return;
        }
        var state = ReplayState.Stopped;
        if (positionIn10MS != 0){
            state = ReplayState.Paused;
        }
        replay.SetState(state, positionIn10MS);
    }

    public Replay GetReplay(string selectedReplay){
        Replay replay = null;
        foreach(var r in _createdUI){
            if (r.GetComponent<Replay>().id == selectedReplay){
                replay = r.GetComponent<Replay>();
            }
        }
        return replay;
    }


   
    
        public void getAfterRoundData(SocketIOEvent e){
            cleanUI();
            Debug.Log("After round data");
            Debug.Log(e.data);
            foreach(var round in e.data.GetField("rounds").list)
            {
                var newui = Instantiate(replayPrefab,replayContent.transform) as GameObject;
         
                newui.GetComponent<Replay>().setParams(round.GetField("players").list,
                round.GetField("question").str,
                round.GetField("id").str,
                (int) round.GetField("maxTimeInMS").n);
                newui.GetComponent<Replay>().parent = this;
                _createdUI.Add(newui);
            }
        }

    
}
}