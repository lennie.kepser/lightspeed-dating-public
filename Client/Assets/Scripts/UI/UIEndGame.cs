using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SocketIO;
using TMPro;

namespace DefaultNamespace{
public class UIEndGame : UIWindow
{
    public GameObject EndGameMonsterWin;
    public GameObject EndGameInnocentWin;
    public Text EndGamePlayerLeft;

    public TextMeshProUGUI EndGameMayhemWin;

    public UISettings UISettings;

    public UILobby UILobby;
    public ReplayUI ReplayUI;

    public MatchSettingsUI MatchSettings;

    public Button GoToNext;

    [SerializeField]
    private MatchResultsUI _matchResults;

    // Start is called before the first frame update
    void Start()
    {
            GoToNext.onClick.AddListener(EndGameButtonPressed);
        
    }

    
        private void EndGameButtonPressed()
        {
            manager.spriteManager.SetEmptyStage();
            UILobby._matchResults.HideResult();
            if (MatchSettings.Replays.isOn){
                ReplayUI.show();
            }else{
                UILobby.show();
            }
        }


        private void ResetWinners()
        {
            EndGameInnocentWin.SetActive(false);
            EndGameMonsterWin.SetActive(false);
            EndGamePlayerLeft.gameObject.SetActive(false);
            EndGameMayhemWin.gameObject.SetActive(false);

        }
        
        public void EndgameMayhem(SocketIOEvent e)
        {
            manager.GM.setState(0);
            UILobby.reset();
            manager.audioController.StopWhispers();
            ResetWinners();
            if (manager.GM.GetInGame())
            {
                show();
            }

            var winner = e.data.GetField("winner").str;

            EndGameMayhemWin.gameObject.SetActive(true);
            EndGameMayhemWin.text = winner + " has won!";
        }

        public void EndGameOtherReason(SocketIOEvent e)
        {
            manager.GM.setState(0);
            manager.audioController.StopWhispers();

            ResetWinners();
            show();
            EndGamePlayerLeft.gameObject.SetActive(true);
            EndGamePlayerLeft.text = e.data.GetField("reason").str;
        }
        public void Endgame(SocketIOEvent e)
        {
            manager.GM.setState(0);
            UILobby.reset();
            _matchResults.HideResult();

            manager.audioController.StopWhispers();
            if (manager.GM.GetInGame())
            {
                show();
            }
            ResetWinners();
            
            if (e.data.GetField("monster").b)
            {

                EndGameMonsterWin.SetActive(true);
            }
            else
            {

                EndGameInnocentWin.SetActive(true);
            }
        }
    
}
}