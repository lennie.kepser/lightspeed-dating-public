using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace DefaultNamespace{
    public class UIRoomDecidee : UIRoom
    {
    

        [SerializeField]
        private TextMeshProUGUI yourNameTextBox;
        [SerializeField]
        private TextMeshProUGUI theirNameTextBox;


        public void JoinRoom(string yourName, string theirName, string query,int voiceskin, string recordingID, int otherPlayerSkin)
        {
            float turnTime = float.Parse(MatchSettings.GetComponent<MatchSettingsUI>().Timer.text);
            base.JoinRoom(query, voiceskin, recordingID, otherPlayerSkin, 2, theirName, yourName, turnTime);

            yourNameTextBox.text = yourName;
            theirNameTextBox.text = theirName;
        }
    }
}
