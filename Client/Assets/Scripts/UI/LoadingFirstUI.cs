using System.Collections;
using System.Collections.Generic;
using Modulate;
using Steamworks;
using UnityEngine;
using UnityEngine.UI;
namespace DefaultNamespace{

public class LoadingFirstUI : UIWindow
{
    public GameObject LoadingText;
    public Button ContinueButton;
    public UIWindow NextWindow;
    public ModulateManager modulateManager;
    public UISettings UISettings;
    public UIWindow debugWindow;
    public Button debugButton;
    public UIAuth Auth;
    private Dictionary<string, bool> loadingDone = new()
    {
        {"voiceskins", false},
        {"steam", false}
        
        
    };
    // Start is called before the first frame update
    void Start()
    {    
        
        ContinueButton.onClick.AddListener(delegate
        {
            hide();
            Auth.DeviceIDLogin(SteamClient.Name);
            if (PlayerPrefs.HasKey("firstTime"))
            {
                UISettings.setupSettings();
            }

            manager.audioController.PlayMainMenuMusic();

        });
        modulateManager.gameObject.SetActive(true);
        debugButton.onClick.AddListener(delegate
        {
            debugWindow.show();
        });
    }
    
    

    public void FinishedLoading(string loadingID)
    {
        loadingDone[loadingID] = true;
        foreach (var keyValPair in loadingDone)
        {
            if (keyValPair.Value == false)
            {
                return;
            }
                
        }
                
        LoadingText.SetActive(false);
        ContinueButton.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
}