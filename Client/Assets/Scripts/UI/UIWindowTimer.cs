using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

namespace DefaultNamespace{
public class UIWindowTimer : UIWindow
{
    protected float timeRemaining = 0.0f;

    private float maxTimeTimer = 0.0f;
    
    private static float barWidth = 140f;
    // Start is called before the first frame update
    public TextMeshProUGUI timeText;

    public RectTransform timeBar;
    
    public void setTime(float val){
        timeRemaining = val;
        maxTimeTimer = val;
    }

    // Update is called once per frame
    virtual protected void Update()
    {
        if (timeRemaining > 0)
        {
            timeRemaining -= Time.deltaTime;
            timeText.text = Math.Ceiling(timeRemaining).ToString();
            if (timeBar != null)
            {
                timeBar.sizeDelta = new Vector2( (barWidth * (timeRemaining / (maxTimeTimer + 0.0001f))) , 21);
            }
        }
    }
}
}
