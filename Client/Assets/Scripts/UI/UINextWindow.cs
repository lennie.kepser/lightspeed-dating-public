﻿using UnityEngine;

namespace DefaultNamespace
{
    public class UINextWindow : UIWindow
    {
        [SerializeField] private UIWindow nextWindow;

        public void ShowNextWindow()
        {
            nextWindow.show();
        }
    }
}