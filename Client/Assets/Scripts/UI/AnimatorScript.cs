using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimatorScript : MonoBehaviour
{
    public List<GameObject> frames;
    private int currentFrame = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetNextTransparency(float transparency){
        if (currentFrame+1<frames.Count){
            frames[currentFrame+1].SetActive(true);
            frames[currentFrame+1].GetComponent<Image>().color = new Color(1f,1f,1f,transparency);
        }
    }

    public void FadeInFrame(int newFrame, float time){
         currentFrame = newFrame;
        foreach(var frame in frames){
            frame.SetActive(false);
        }
        if (newFrame > 0){
            frames[newFrame-1].SetActive(true);
        }

        frames[newFrame].SetActive(true);
        if (frames[newFrame].TryGetComponent<SpriteRenderer>(out SpriteRenderer image))
        {
            StartCoroutine(FadeTo(1.0f, time, image));

        }
    }

    IEnumerator FadeTo(float aValue, float aTime, SpriteRenderer image)
    {
        float alpha = image.color.a;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
        {
            Color newColor = new Color(1, 1, 1, Mathf.Lerp(alpha,aValue,t));
            image.color = newColor;
            yield return null;
        }
    }

    public void updateFrame(int newFrame){
        currentFrame = newFrame;
        foreach(var frame in frames){
            frame.SetActive(false);
        }
        frames[newFrame].SetActive(true);
        if (frames[newFrame].TryGetComponent<Image>(out Image image)){
            image.color = new Color(1f,1f,1f,1f);

        }

    }
}
