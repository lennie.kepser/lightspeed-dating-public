using System;
using System.Collections;
using System.Collections.Generic;
using Epic.OnlineServices.UnityIntegration;
using Michsky.UI.ModernUIPack;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class VolumeControlUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private CanvasGroup _canvasGroup;
    [SerializeField] private Toggle volumeToggle;
    [SerializeField] private Slider volumeSlider;
    [SerializeField] private PlayerLobby _playerLobby;
    private float volume = 50f;

     private EOSVoiceManager _eosVoiceManager;
    // Start is called before the first frame update
    void Start()
    {
        if (_playerLobby.isSelf)
        {
            _canvasGroup.interactable = false;
            _canvasGroup.alpha = 0;
            return;
        }
        // not a race condition because this is only created once players are in a lobby
        _eosVoiceManager = EOSVoiceManager.instance;
        
        volumeToggle.onValueChanged.AddListener((bool enabled) =>
        {
            if (!enabled)
            {
                volumeSlider.interactable = true;
                volume = volumeSlider.value;
                
            }
            else
            {
                volume = 0f;
                volumeSlider.interactable = false;
            }
            updateVolume();

        });

        
        volumeSlider.onValueChanged.AddListener((float value) =>
        {
            volume = value;
            updateVolume();
        });
        if (PlayerPrefs.HasKey("PlayerVolume" + _playerLobby.name))
        {
            volumeSlider.value = PlayerPrefs.GetFloat("PlayerVolume" + _playerLobby.name);
        }
    }

    private void updateVolume()
    {
        _eosVoiceManager.updatePlayerVolume(_playerLobby.getUserID(),volume);
        PlayerPrefs.SetFloat("PlayerVolume" + _playerLobby.name, volume);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!_playerLobby.isSelf)
        {
            _canvasGroup.alpha = 1;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!_playerLobby.isSelf)
        {
            _canvasGroup.alpha = 0.3f;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
