using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity;
using UnityEngine.UI;


namespace DefaultNamespace
{
    public class UIWindow : MonoBehaviour
    {
        public GameObject me;
        public UIManager manager;

        public List<GameObject> _createdUI;

        public void cleanUI()
        {
            foreach (var elem in _createdUI)
            {
                Destroy(elem);
            }
            _createdUI.Clear();
        }

        protected void removeUI(GameObject ui)
        {
            _createdUI.Remove(ui);
            Destroy(ui);
        }


        public virtual void show(){
            manager.hideWindows();

            me.SetActive(true);

        }


        public virtual void hide(){
            me.SetActive(false);
        }
    }
}