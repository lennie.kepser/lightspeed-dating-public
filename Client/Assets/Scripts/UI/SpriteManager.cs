using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity;
using Epic.OnlineServices.UnityIntegration;
using UnityEngine.UI;

namespace DefaultNamespace
{
public class SpriteManager : MonoBehaviour
{
    public AnimatorScript backgroundAnimator;
    
    public List<AnimatorScript> charAnimators;

    public DeathAnimation DeathAnimation;
    public AudioController audioController;
    public AnimatorScript chair;

    public ErrorUI error;
    public Lights lights;
    
    private int currentCharacter = -1;

    private bool audienceSetUp = false;

    // Start is called before the first frame update
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void CancelCoroutines()
    {
        StopAllCoroutines();
    }

    public void ShowCharacterPreview(int character){
        if (character == -1)
        {
            error.DisplayError("Unkown error occured. Please report. Your game should be unaffected.","misc");
            return;
        }
        currentCharacter = character;
        charAnimators[currentCharacter].updateFrame(1);
    }


    // 0 = Off, 1 = Side Lights, 2 = All lights
    public void Lights(int lightsInt){
        if (lightsInt == 0)
        {
            backgroundAnimator.updateFrame(4);
            lights.LightsLeft(false);
            lights.LightsRight(false);
            lights.LightsTop(false);
            chair.updateFrame(0);
        }
        else if (lightsInt == 1)
        {
            backgroundAnimator.updateFrame(4);
            lights.LightsLeft(false);
            lights.LightsRight(true);
            lights.LightsTop(false);
            chair.updateFrame(0);
        }
        else if (lightsInt == 2)
        {
            backgroundAnimator.updateFrame(5);

            lights.LightsLeft(true);
            lights.LightsRight(true);
            lights.LightsTop(true);
            chair.updateFrame(1);

        }

       

        if (currentCharacter != -1)
        {
            charAnimators[currentCharacter].updateFrame(lightsInt == 0 ? 1 : 2);
        }

    }

    public void ShowCharacterDark(int character){
        currentCharacter = character;
        Lights(0);
    }
    
    
    public void ShowCharacter(int character){
        currentCharacter = character;
        Lights(1);
        
        audioController.LightSounds();
        audienceSetUp = false;
        StartCoroutine(ShowCharacterAnimation());
        
    }

    public void ShowRandomCharacterMonster()
    {
        int character = UnityEngine.Random.Range(0, 2);
        SimpleShowCharacter(character);
        DeathAnimation.ShowEyes(character);
    }

    public void SimpleShowCharacter(int character)
    {
        if (character == -1)
        {
            return;
        }
        charAnimators[character].updateFrame(1);

    }

    private IEnumerator ShowCharacterAnimation(){
        yield return new WaitForSeconds(1);
        Lights(2);
    }

    public void HideCharacter(){
        if (currentCharacter == -1)
        {
            return;
        }
        charAnimators[currentCharacter].updateFrame(0);
        currentCharacter = -1;
    }

    public void SetUpAudience(){
        audienceSetUp = true;
        backgroundAnimator.updateFrame(0);
        audioController.StartWhispers();
        StartCoroutine(SetUpAudienceAnimation()); 
    }

    private IEnumerator SetUpAudienceAnimation(){
        for(int i = 1; i < 5; i++){
            
            yield return new WaitForSeconds(1);
            audioController.IncreaseWhisperVolume(0.05f);
            backgroundAnimator.FadeInFrame(i,1.0f);

            
        }
   
    }

    public void HideAllCharacters(){
        foreach(AnimatorScript anScript in charAnimators){
            anScript.updateFrame(0);
        }
        currentCharacter = -1;

    }



    public void SetEmptyStage(){
        Lights(0);
        backgroundAnimator.updateFrame(0);
        HideAllCharacters();
    }
}
}