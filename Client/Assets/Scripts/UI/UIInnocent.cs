using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace{
public class UIInnocent : UIRoom
{
    public GameObject SparedInnocent;

    public Button SpareButton;
    public Button KillButton;


    public TextMeshProUGUI RoomInfo;
    // Start is called before the first frame update
    void Start()
    {
        
            SpareButton.onClick.AddListener(Spare);
            KillButton.onClick.AddListener(MonsterButtonPressed);
    }

    public void Spare()
    {
        manager.Connect.spare();
        SpareButton.gameObject.SetActive(false);
    }
    
    public void Spared()
    {
        SparedInnocent.SetActive(true);
    }

  
    private void MonsterButtonPressed()
    {
        manager.Connect.Sus();
    }

    
    public void JoinRoom(string otherplayer, string query, int voiceskin, string recordingID , int otherPlayerSkin)
    {
        float turnTime = float.Parse(MatchSettings.GetComponent<MatchSettingsUI>().Timer.text);

        base.JoinRoom(query, voiceskin, recordingID, otherPlayerSkin, 0, otherplayer, manager.GM.username, turnTime);

        SparedInnocent.SetActive(false);
        SpareButton.gameObject.SetActive(true);
        KillButton.gameObject.SetActive(true);
        RoomInfo.text = otherplayer; 
    }
}
}