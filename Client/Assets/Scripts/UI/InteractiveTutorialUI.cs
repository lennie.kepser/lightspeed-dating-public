using System;
using System.Collections;
using System.Collections.Generic;
using Michsky.UI.ModernUIPack;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InteractiveTutorialUI : MonoBehaviour
{
    [SerializeField] private ButtonManager next;
    [SerializeField] private CanvasGroup prevGroup;
    [SerializeField] private ButtonManager prev;


    [Serializable]
    public class TutorialElement
    {
        public GameObject element;
        private int siblingIndex = -1;

        public void show()
        {
            siblingIndex = element.transform.GetSiblingIndex();
            element.transform.SetAsLastSibling();
        }

        public void hide()
        {
            // If still in start position, skip me
            if (siblingIndex == -1)
            {
                return;
            }
            
            element.transform.SetSiblingIndex(siblingIndex);
        }
    }
    
    [Serializable]
    public class TutorialStep
    {
        public List<TutorialElement> elements;
        
        public float angle;
        public string text;
        public bool disableButtons = false;
        public List<TextMeshProUGUI> textReplacers;
    }

    [SerializeField] private string playerPrefID;

    [SerializeField] private TextMeshProUGUI text;

    [SerializeField] private List<TutorialStep> steps;

    [SerializeField] private RectTransform arrowTransform;

    private int currentIndex = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("Tutorial" + playerPrefID))
        {
            currentIndex = PlayerPrefs.GetInt("Tutorial" + playerPrefID);
        }
        GoTo(currentIndex, currentIndex);
        next.clickEvent.AddListener(Next);
        prev.clickEvent.AddListener(Prev);
    }

    public void Next()
    {
        if (!gameObject.activeInHierarchy)
        {
            return;
        }
        currentIndex += 1;
        GoTo(currentIndex - 1, currentIndex);
    }
    
    public void Prev()
    {
        if (!gameObject.activeInHierarchy)
        {
            return;
        }
        currentIndex -= 1;
        GoTo(currentIndex + 1, currentIndex);
    }

    private void GoTo(int prevIndex, int index)
    {
        PlayerPrefs.SetInt("Tutorial"+playerPrefID, index);
        if (prevIndex != index)
        {
            foreach (TutorialElement element in steps[prevIndex].elements)
            {
                element.hide();
            }
        }
       
        if (index == steps.Count)
        {
            gameObject.SetActive(false);
            return;
        }
        

        string newText = steps[index].text;
        int b = 0;
        foreach (TextMeshProUGUI replacer in steps[index].textReplacers)
        {
            newText = newText.Replace("[ TEXT" + b + " ]", replacer.text);
            b++;
        }
        

        text.text = newText;
        arrowTransform.rotation = Quaternion.Euler(0f, 0f, steps[index].angle);
        foreach (TutorialElement element in steps[index].elements)
        {
            element.show();
        }
       
        if (steps[index].disableButtons)
        {
            next.gameObject.SetActive(false);
            prev.gameObject.SetActive(false);
        }
        else
        {
            next.gameObject.SetActive(true);
            prev.gameObject.SetActive(true);
        }

        if (steps.Count - 1 == index)
        {
            next.buttonText = "Close";
        }
        else
        {
            next.buttonText = "Next";
        }
        next.UpdateUI();

        if (index == 0)
        {
            prevGroup.interactable = false;
            prevGroup.alpha = 0.5f;
        }
        else
        {
            prevGroup.interactable = true;
            prevGroup.alpha = 1f;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
