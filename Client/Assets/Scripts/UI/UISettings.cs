﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;
using Modulate;
using Epic.OnlineServices.UnityIntegration;
using Epic.OnlineServices.RTCAudio;
using Button = UnityEngine.UI.Button;
using Slider = UnityEngine.UI.Slider;

namespace DefaultNamespace
{
    public class UISettings : MonoBehaviour
    {
        public SocketConnect connect;
        public UIAuth AuthUI;
        public UIWindow uiJoinServer;
        public EOSVoiceManager voiceManager;
        public EOSManager eOSManager;
        public VoiceWearManagerLSD voiceWearManager;
        
        public Button openSettings;
        public Dropdown microphones;
        public Dropdown speakers;
        public Dropdown VoiceSkins;
        public GameObject settingsWindow;
        public GameObject EchoSettings;
        public CanvasScaler gameCanvas;

        public ErrorUI error;

        public Slider uiScaling;
        public Slider inputGain;
        public Slider outputGain;
        
        public Toggle EchoOn;

        public Toggle LowQualAudio;

        private List<AudioDevice> _inputDevices = new List<AudioDevice>();
        private List<AudioDevice> _outputDevices = new List<AudioDevice>();

        private Dropdown _microphones;
        private Dropdown _speakers;
        private Dropdown _voiceskins;
        private Toggle _echoOn;
        private Slider _inputGain;

        private Slider _outputGain;
        private Button _openSettings;

        public Slider audioEffectSlider;
        public Slider musicVolumeSlider;
        public AudioController audioController;
        
        private bool epilepsyProtection = false;
        public Toggle epilepsyProtectionToggle;
        public Button firstTime;

        public bool mainMusicStarted = false;
        private bool openedOnce = false;

        private bool shownSampleRateError;

        public ModulateManager modulateManager;


        public Toggle autoGain;
        public CanvasGroup inputVolumeGroup;
        public Toggle noiseSupression;
        
        void Update(){
            if (voiceManager.SampleRate<44100 && !shownSampleRateError){
				error.DisplayError("The sample rate ["+voiceManager.SampleRate+ " Hz] of the microphone you have selected is too low. Please change your selected microphone in the settings.", "local");
                shownSampleRateError = true;
            }
        }


        private void Start()
        {
            _openSettings = openSettings.GetComponent<Button>();
            _microphones = microphones.GetComponent<Dropdown>();
            _inputGain = inputGain.GetComponent<Slider>();
            _outputGain = outputGain.GetComponent<Slider>();
            _echoOn = EchoOn.GetComponent<Toggle>();
            _voiceskins = VoiceSkins.GetComponent<Dropdown>();

            audioEffectSlider.onValueChanged.AddListener(delegate {
                audioController.SetVolumeMultiplier(audioEffectSlider.value);
                PlayerPrefs.SetFloat("audioEffectVolume", audioEffectSlider.value);
                if (openedOnce){
                    audioController.CrowdCheerTestSound();

                }
            });
            musicVolumeSlider.onValueChanged.AddListener(delegate {
                audioController.SetMusicMultiplier(musicVolumeSlider.value);
                PlayerPrefs.SetFloat("musicVolume", musicVolumeSlider.value);
                
            });

            epilepsyProtectionToggle.onValueChanged.AddListener(delegate {
                epilepsyProtection = epilepsyProtectionToggle.isOn;
                if (epilepsyProtection){
                    PlayerPrefs.SetInt("epilepsyProtection", 1);

                }else{
                    PlayerPrefs.SetInt("epilepsyProtection", 0);

                }
                
            });

            LowQualAudio.onValueChanged.AddListener(delegate {
                voiceWearManager.SetLowQualAudioFiliter(LowQualAudio.isOn);
                modulateManager.SetExtendBufferMode(LowQualAudio.isOn);
                if (LowQualAudio.isOn){
                    PlayerPrefs.SetInt("lowQualAudio", 1);
                    
                }else{
                    PlayerPrefs.SetInt("lowQualAudio", 0);
                  

                }
                
            });
            
            _inputGain.onValueChanged.AddListener(delegate {
                changeInputLevel();
            });
            _outputGain.onValueChanged.AddListener(delegate {changeOutputLevel(); });
            uiScaling.onValueChanged.AddListener(delegate { changeScaling(); });
            
            _voiceskins.onValueChanged.AddListener(delegate { changeEcho();});
            
            _speakers = speakers.GetComponent<Dropdown>();
            _openSettings.onClick.AddListener(openSettingsBut);
            
            _microphones.onValueChanged.AddListener(delegate {
                SetAudioInput(_inputDevices[_microphones.value]);
            });
            _speakers.onValueChanged.AddListener(delegate {

                SetAudioOutput(_outputDevices[_speakers.value]);
            });
            
            _echoOn.onValueChanged.AddListener(delegate
            {
                changeEcho();
            });

            if (PlayerPrefs.HasKey("scaling"))
            {
                uiScaling.value = PlayerPrefs.GetFloat("scaling", uiScaling.value);
            }
            
            autoGain.onValueChanged.AddListener( SetAutoGainEnabled );
            noiseSupression.onValueChanged.AddListener(SetNoiseSupressionEnabled);
            
            //randomButton.onClick.AddListener(delegate {
            //    voiceWearManager.randomizeParams(2);
            //});

        }

        private void SetNoiseSupressionEnabled(bool val)
        {
            voiceManager.SetAudioSetting("DisableNoiseSupression", !val);
            PlayerPrefs.SetInt("DisableNoiseSupression", (val ? 1 : 0));
        }

        private void SetAutoGainEnabled(bool val)
        {
            voiceManager.SetAudioSetting("DisableAutoGainControl", !val);
            PlayerPrefs.SetInt("DisableAutoGainControl", (val ? 1 : 0));
            inputVolumeGroup.interactable = !val;
            inputVolumeGroup.alpha = (val ? 0.5f : 1f);
            if (val == true)
            {
                _inputGain.value = 50;
            }
        }

        public bool IsEpilepsyProtectionOn(){
            return epilepsyProtection;
        }

        private void SetAudioInput(AudioDevice device){

            PlayerPrefs.SetString("microphone", device.Name);
            UpdateInputSettings();
        }

        private void SetAudioOutput(AudioDevice device){
            PlayerPrefs.SetString("speaker", device.Name);
            UpdateOutputSettings();
        }

        public void hideEchoSettings()
        {
            voiceWearManager.endEcho();
            voiceWearManager.selectSkin(-1);
            EchoSettings.SetActive(false);
        }
        public void showEchoSettings()
        {
            //EchoSettings.SetActive(true);
        }



        private void changeScaling()
        {
            PlayerPrefs.SetFloat("scaling", uiScaling.value);
            gameCanvas.scaleFactor = uiScaling.value;
        }
        
        private void changeEcho()
        {
            if (_echoOn.isOn)
            {
                voiceWearManager.startEcho();
                voiceWearManager.selectSkin(_voiceskins.value);

            }
            else
            {
                voiceWearManager.endEcho();
                voiceWearManager.selectSkin(-1);

            }
        }
        

        private void UpdateInputSettings(){
            shownSampleRateError = false;
            //voiceManager.micAmp = _inputGain.value / 50f;
            var inputSettings = new SetAudioInputSettingsOptions(){
                LocalUserId = voiceManager.userId,
                DeviceId = _inputDevices[_microphones.value].Id,
                Volume = _inputGain.value,
                PlatformAEC = false
            };
            EOSManager.s_PlatformInterface.GetRTCInterface().GetAudioInterface().SetAudioInputSettings(ref inputSettings);
        
        }

        private void UpdateOutputSettings(){
            var outputSettings = new SetAudioOutputSettingsOptions(){
                LocalUserId = voiceManager.userId,
                DeviceId = _outputDevices[_speakers.value].Id,
                Volume = _outputGain.value
            };
            EOSManager.s_PlatformInterface.GetRTCInterface().GetAudioInterface().SetAudioOutputSettings(ref outputSettings);

        }

        private void changeInputLevel()
        {
            
            PlayerPrefs.SetFloat("input-level", _inputGain.value);
            UpdateInputSettings();
        }

        private void changeOutputLevel()
        {
            PlayerPrefs.SetFloat("output-level", _outputGain.value);
            UpdateOutputSettings();
        }

        private void devicesToDropdown(Dropdown dropdown, List<AudioDevice> devices, string savedName)
        {
            var list = new List<string>();
            int i = 0;
            int saved = -1;
            int defaultdevice = 0;
            foreach (var device in devices)
            {
                
                list.Add(device.Name);
                if (PlayerPrefs.HasKey(savedName) && PlayerPrefs.GetString(savedName) == device.Name)
                {
                    saved = i;
                }
                if (device.IsDefault){
                    defaultdevice = i;
                }
                i++;
            }
            dropdown.AddOptions(list);
            if (saved > -1)
            {
                dropdown.value = saved;
            }
            else
            {
                dropdown.value = defaultdevice;
            }
        }
        
        
        public void setupSettings()
        {
            _inputDevices.Clear();
            _outputDevices.Clear();
            _speakers.ClearOptions();
            _microphones.ClearOptions();
            loadInputDevices();
            loadOutputDevices();
             if (PlayerPrefs.HasKey("input-level"))
            {
                _inputGain.value = PlayerPrefs.GetFloat("input-level");
            }
            if (PlayerPrefs.HasKey("output-level"))
            {
                _outputGain.value = PlayerPrefs.GetFloat("output-level");
            }
            if (PlayerPrefs.HasKey("audioEffectVolume"))
            {
                audioEffectSlider.value = PlayerPrefs.GetFloat("audioEffectVolume", audioEffectSlider.value);
            }
            if (PlayerPrefs.HasKey("musicVolume"))
            {
                musicVolumeSlider.value = PlayerPrefs.GetFloat("musicVolume", musicVolumeSlider.value);
            }
            
            if (PlayerPrefs.HasKey("epilepsyProtection")){
                epilepsyProtectionToggle.isOn = (PlayerPrefs.GetInt("epilepsyProtection") == 1);

            }
            if (PlayerPrefs.HasKey("lowQualAudio")){
                LowQualAudio.isOn = (PlayerPrefs.GetInt("lowQualAudio") == 1);

            }

            if (PlayerPrefs.HasKey("DisableAutoGainControl"))
            {
                autoGain.isOn = (PlayerPrefs.GetInt("DisableAutoGainControl") == 1);
            }
            
            if (PlayerPrefs.HasKey("DisableNoiseSupression"))
            {
                autoGain.isOn = (PlayerPrefs.GetInt("DisableNoiseSupression") == 1);
            }        
            devicesToDropdown(_microphones,_inputDevices, "microphone");
            devicesToDropdown(_speakers,_outputDevices, "speaker");
            if (PlayerPrefs.HasKey("firstTime")){
                firstTime.gameObject.SetActive(false);
                
            }else{
                openedOnce = true;   
                    connect.EnterEchoRoom();

                firstTime.onClick.AddListener(delegate {
                    settingsWindow.SetActive(false);
                    audioController.StopCrowdCheer();
                    PlayerPrefs.SetInt("firstTime", 1);
                    uiJoinServer.show();
                    voiceManager.leaveChannels();
                    ShowSettingsButton();

                });
            }
        }

        public void ShowSettingsButton(){
            _openSettings.gameObject.SetActive(true);
        }

        public void loadOutputDevices(){
			var getAudioOutputDevicesCountOptions = new GetAudioOutputDevicesCountOptions();
			uint audioOutputDevicesCount = EOSManager.s_PlatformInterface.GetRTCInterface().GetAudioInterface().GetAudioOutputDevicesCount(ref getAudioOutputDevicesCountOptions);
			for (uint index = 0; index < audioOutputDevicesCount; ++index)
			{
				var getAudioOutputDeviceByIndexOptions = new GetAudioOutputDeviceByIndexOptions()
				{
					DeviceInfoIndex = index
				};

				var audioOutputDeviceInfo = EOSManager.s_PlatformInterface.GetRTCInterface().GetAudioInterface().GetAudioOutputDeviceByIndex(ref getAudioOutputDeviceByIndexOptions);
				if (audioOutputDeviceInfo == null)
				{
					Debug.Log($"GetAudioOutputDeviceByIndex failed at index {index}");
				}
				else
				{
					Debug.Log($"GetAudioOutputDeviceByIndex {audioOutputDeviceInfo.Value.DeviceName}");
					var newAudioDevice = new AudioDevice()
					{
						Id = audioOutputDeviceInfo.Value.DeviceId,
						Name = audioOutputDeviceInfo.Value.DeviceName,
						IsDefault = audioOutputDeviceInfo.Value.DefaultDevice
					};

					_outputDevices.Add(newAudioDevice);
				}
			}
        }

        
        public void loadInputDevices(){
			var getAudioInputDevicesCountOptions = new GetAudioInputDevicesCountOptions();
			uint audioOutputDevicesCount = EOSManager.s_PlatformInterface.GetRTCInterface().GetAudioInterface().GetAudioInputDevicesCount(ref getAudioInputDevicesCountOptions);
			for (uint index = 0; index < audioOutputDevicesCount; ++index)
			{
				var getAudioInputDeviceByIndexOptions = new GetAudioInputDeviceByIndexOptions()
				{
					DeviceInfoIndex = index
				};

				var audioInputDeviceInfo = EOSManager.s_PlatformInterface.GetRTCInterface().GetAudioInterface().GetAudioInputDeviceByIndex(ref getAudioInputDeviceByIndexOptions);
				if (audioInputDeviceInfo == null)
				{
					Debug.Log($"GetAudioInputDeviceByIndex failed at index {index}");
				}
				else
				{
					Debug.Log($"GetAudioInputDeviceByIndex {audioInputDeviceInfo.Value.DeviceName} {audioInputDeviceInfo.Value.DeviceId}");
					var newAudioDevice = new AudioDevice()
					{
						Id = audioInputDeviceInfo.Value.DeviceId,
						Name = audioInputDeviceInfo.Value.DeviceName,
						IsDefault = audioInputDeviceInfo.Value.DefaultDevice
					};

					_inputDevices.Add(newAudioDevice);
				}
			}
        }

     

        
        public void openSettingsBut()
        {
            if (settingsWindow.activeSelf)
            {
                voiceWearManager.endEcho();
                if (EchoSettings.activeSelf)
                {
                    voiceWearManager.selectSkin(-1);
                }
                settingsWindow.SetActive(false);
                audioController.StopCrowdCheer();
                return;
            }
            settingsWindow.SetActive(true);

            setupSettings();
        }
    }
}