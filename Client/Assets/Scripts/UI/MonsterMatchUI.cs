using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using JsonStuff;
using TMPro;

namespace DefaultNamespace{
    public class MonsterMatchUI : UIRoom
    {
        public TextMeshProUGUI PlayerSkin;
        public TextMeshProUGUI OtherPlayer;

        public GameObject SparedMonster;
        public Button SpareButton;

        public void Start(){
            SpareButton.onClick.AddListener(Spare);
        }
        public void Spare()
        {
            manager.Connect.spare();
            SpareButton.gameObject.SetActive(false);
        }

        public void JoinRoom(string otherplayer, string query, int voiceskin, string recordingID, int otherPlayerSkin, string myskin)
        {
            
            float turnTime = float.Parse(MatchSettings.GetComponent<MatchSettingsUI>().Timer.text);
            base.JoinRoom(query, voiceskin, recordingID, otherPlayerSkin, 1, otherplayer, myskin, turnTime);
            SparedMonster.SetActive(false);
            SpareButton.gameObject.SetActive(true);
            OtherPlayer.text = otherplayer;
            PlayerSkin.text = myskin;
            
        }

    

        public void Spared()
        {
            
            SparedMonster.SetActive(true);
        }

    }
}