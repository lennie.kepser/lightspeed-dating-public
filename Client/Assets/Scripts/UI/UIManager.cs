using Unity;
using UnityEngine;
using Epic.OnlineServices.UnityIntegration;

namespace DefaultNamespace{

    public class UIManager : MonoBehaviour{
        public UIWindow[] windows;

        public GameManager GM;
        public VoiceWearManagerLSD VoiceWearManager;
        public VoskSpeechToText STT;
        public EOSVoiceManager voiceManager;
        public SocketConnect Connect;
        public SpriteManager spriteManager;
        public AudioController audioController;
        public DeathAnimation deathAnimation;
        public float[] timers = {0.0f, 0.0f};
        public void hideWindows(){
            foreach(var window in windows){
                window.hide();
            }
        }

        public void setUpTimers(float pregame, float charselect)
        {
            timers = new[] {pregame, charselect};

        }
    }
}