using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using JsonStuff;
using TMPro;

namespace DefaultNamespace{
public class UISpectatorRoom : UIWindow
{
    // Start is called before the first frame update

    public Button back;
    public GameObject playerList;
    public GameObject playerPrefab;
    public TextMeshProUGUI questionObject;


    public void SetUp(List<Player> players, string question){
        cleanUI();
        questionObject.text = question;
        foreach(var player in players){
            var newui = Instantiate(playerPrefab,playerList.transform) as GameObject;
            newui.GetComponent<ReplayUser>().SetUp(player.username, player.disguisedas,!player.alive, player.monster);
            _createdUI.Add(newui);
        }
        Canvas.ForceUpdateCanvases();
    }
  
}
}