using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class MultiSelectUI : MonoBehaviour
{
    [FormerlySerializedAs("QuestionPacks")] public List<Selectable> Selectables = new List<Selectable>();
    
    [System.Serializable]
    public class Selectable
    {
        public Toggle toggle;
        public string backEndName;
        public bool defaultValue;
    }

    [SerializeField] private string eventName;
    [SerializeField] private SocketConnect _connect;

    private SocketConnect connect;
    // Start is called before the first frame update
    void Start()
    {
        foreach (var Tog in Selectables)
        {
            if (Tog.toggle.gameObject.GetComponent<CanvasGroup>().interactable)
            {
                if (Tog.defaultValue)
                {
                    Tog.toggle.gameObject.GetComponent<CanvasGroup>().alpha = 1;
                    Tog.toggle.isOn = true;
                }
                else
                {
                    Tog.toggle.gameObject.GetComponent<CanvasGroup>().alpha = 0.44f;
                    Tog.toggle.isOn = false;
                }
                AddValueListener(Tog.toggle);
            }
           
            
        }
        SetTogglables();
    }

    void AddValueListener(Toggle tog)
    {
        tog.onValueChanged.AddListener((bool val) =>
        {
            if (val)
            {
                tog.gameObject.GetComponent<CanvasGroup>().alpha = 1;
            }
            else
            {
                tog.gameObject.GetComponent<CanvasGroup>().alpha = 0.44f;

            }
            SetTogglables();
           
            
        });
    }

    private void SetTogglables()
    {
        int totalEnabled = 0;
        foreach (var OtherTog in Selectables)
        {
            if (OtherTog.toggle.gameObject.GetComponent<CanvasGroup>().interactable)
            {
                if (OtherTog.toggle.isOn == true )
                {
                    totalEnabled += 1;
                }

                OtherTog.toggle.interactable = true;
            }
              
        }

        if (totalEnabled == 1)
        {
            foreach (var OtherTog in Selectables)
            {
                if (OtherTog.toggle.gameObject.GetComponent<CanvasGroup>().interactable)
                {
                    if (OtherTog.toggle.isOn == true)
                    {
                        OtherTog.toggle.interactable = false;
                    }
                }
            }
        }
    }

    public void SendSelection()
    {
        List<JSONObject> selections = new List<JSONObject>();
        foreach (var question in Selectables)
        {
            if (question.toggle.isOn && question.toggle.gameObject.GetComponent<CanvasGroup>().interactable)
            {
                selections.Add(JSONObject.CreateStringObject(question.backEndName));
            }
        }

        _connect.SendListGameAction(eventName, selections);
    }

   
    // Update is called once per frame
    void Update()
    {
        
    }
}
