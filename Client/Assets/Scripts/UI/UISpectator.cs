using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SocketIO;

namespace DefaultNamespace{
public class UISpectator : UIWindowTimer
{
    public GameObject SpectateInfoWait;

    public GameObject SpectateUIElement;

    public GameObject SpectateList;

    public GameObject SpectateContainer;
    public GameObject SpectateRoom;


    private List<Room> _rooms;

    public UISpectatorRoom spectateRoom;

        
    public MatchSettingsUI MatchSettings;

    public void Start(){
        spectateRoom.back.onClick.AddListener(goBackSpectator);

    }

    public void CharSelectSpectator(SocketIOEvent e)
    {
        show();
        cleanUI();
        SpectateRoom.SetActive(false);
        SpectateList.SetActive(false);

        manager.voiceManager.leaveChannels();
        SpectateInfoWait.SetActive(true);

    }
    
    
        private void addSpectator(Room room)
        {
            var newui = Instantiate(SpectateUIElement,SpectateContainer.transform) as GameObject;
            newui.GetComponent<SpectateElement>().SetUp(room.players, room.query);
            newui.GetComponent<SpectateElement>().watchButton.onClick.AddListener(()=>{
                SpectateList.SetActive(false);
                SpectateRoom.SetActive(true);
                manager.Connect.Spectate(room.voiceid);
                // TODO: New spectate room logic.
                spectateRoom.SetUp(room.players, room.query);
            });
           
            newui.name = "Room" + room.voiceid;
            newui.SetActive(true);

            _createdUI.Add(newui);
        }

        public void setUpSpectator(List<Room> rooms)
        {

            cleanUI();
            show();
            SpectateList.SetActive(true);
            SpectateRoom.SetActive(false);
            SpectateInfoWait.SetActive(false);


            setTime(float.Parse(MatchSettings.GetComponent<MatchSettingsUI>().Timer.text));
            _rooms = rooms;
            foreach (var room in rooms)
            {
                addSpectator(room);
            }
        }

        private void goBackSpectator()
        {
            SpectateList.SetActive(true);
            SpectateRoom.SetActive(false);
            manager.voiceManager.leaveChannels();
        }
        
        public void SpectatorRooms(SocketIOEvent e)
        {
            var rooms = e.data.GetField("rooms");

            List<Room> throughput = new List<Room>();
            for (var i = 0; i < rooms.Count; i++)
            {
                var players = manager.Connect.getPlayersFromJSON(rooms[i].GetField("players"));
                var room = new Room();
                room.query = rooms[i].GetField("query").str;
                room.players = players;
                
                room.voiceid = rooms[i].GetField("voicechat").str;
                throughput.Add(room);
            }
            setUpSpectator(throughput);
        }


}
}