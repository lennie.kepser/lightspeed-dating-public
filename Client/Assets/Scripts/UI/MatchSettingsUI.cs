using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DefaultNamespace;

using SocketIO;
using TMPro;

public class MatchSettingsUI : MonoBehaviour
{
    public GameObject UIElement;    
    public Toggle RandomizeVoices;
    public Toggle ExtraVoiceEffects;
    public Toggle Replays;
    public Toggle JudgementRoom;
    public InputField Timer;
    public SocketConnect _connect;
    public CanvasGroup canvasGroup;

    public GameObject GameModeMenu;

    public Button GameModeMenuButton;
    
    bool canEdit = false;

    [SerializeField]
    public List<GameModeButton> GameModeButtonsList = new List<GameModeButton>();
    
    [System.Serializable]
    public class GameModeButton
    {
        public int backEndInt;
        public Button button;
        public string frontEndName;
    }

    private int GameModeSelected;
    
    
    // Start is called before the first frame update
    void Start()
    {
        GameModeSelected = GameModeButtonsList[0].backEndInt;
        RandomizeVoices.onValueChanged.AddListener(delegate {
            sendUpdate();
        });
        ExtraVoiceEffects.onValueChanged.AddListener(delegate {
            sendUpdate();
        });
        Replays.onValueChanged.AddListener(delegate {
            sendUpdate();
        });
        JudgementRoom.onValueChanged.AddListener(delegate {
            sendUpdate();
        });
        Timer.onEndEdit.AddListener(delegate {
            sendUpdate();
        });
        
        GameModeMenuButton.onClick.AddListener(() =>
        {
            GameModeMenu.SetActive(true);
        });
        
        foreach(GameModeButton buttonObj in GameModeButtonsList)
        {
            buttonObj.button.onClick.AddListener(() =>
            {
                GameModeSelected = buttonObj.backEndInt;
                sendUpdate();
                CloseGameModeMenu();
                SetGameModeMenuButtonText(buttonObj.frontEndName);
            });
        }
        
    }

    private void SetGameModeMenuButtonText(string text)
    {
        GameModeMenuButton.GetComponentInChildren<TextMeshProUGUI>().text = text;
    }

    private void CloseGameModeMenu()
    {
        GameModeMenu.SetActive(false);
    }

    public void show(){
        UIElement.SetActive(true);
    }

    public void ChangeEditable(bool editable)
    {
        canEdit = editable;
        canvasGroup.interactable = editable;
        canvasGroup.alpha = (editable ? 1f : 0.7f);
    }

    private void sendUpdate()
    {
        if (!canEdit)
        {
            return;
        }
        var settingDictionary = new Dictionary<string, string>();
        settingDictionary["roundtime"] = Timer.text;
        settingDictionary["voiceeffects"] = (ExtraVoiceEffects.isOn ? "true" : "false");
        settingDictionary["randomvoices"] = (RandomizeVoices.isOn ? "true" : "false");
        settingDictionary["replays"] = (Replays.isOn ? "true" : "false");
        settingDictionary["judgementchance"] = (JudgementRoom.isOn ? "1" : "0");
        settingDictionary["gameMode"] = GameModeSelected.ToString();
        _connect.UpdateSettings(settingDictionary);
    }

    public void setValues(Dictionary<string, string> newvals)
    {
        Timer.text = newvals["roundtime"];
        ExtraVoiceEffects.isOn = (newvals["voiceeffects"] == "true");
        RandomizeVoices.isOn = (newvals["randomvoices"] == "true");
        JudgementRoom.isOn = (newvals["judgementchance"] == "1");
        Replays.isOn = (newvals["replays"] == "true");
        foreach (GameModeButton buttonObj in GameModeButtonsList)
        {
            if (buttonObj.backEndInt == int.Parse(newvals["gameMode"]))
            {
                GameModeSelected = int.Parse(newvals["gameMode"]);
                SetGameModeMenuButtonText(buttonObj.frontEndName);
            }
        }


    }
    public void RecieveSettings(SocketIOEvent e)
    {
        ChangeEditable(e.data.GetField("admin").b);
        Dictionary<string, string> settings = new Dictionary<string, string>();
        foreach(var key in e.data.GetField("settings").keys)
        {
            settings[key] = e.data.GetField("settings").GetField(key).str;

        }
        setValues(settings);          
    }
      
    // Update is called once per frame
    void Update()
    {
        
    }
}
