using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using TMPro;
using UnityEngine;

public class MatchResultsUI : MonoBehaviour
{
    [Serializable]
    public class Results
    {
        public string headline;
        public string tip;
        public bool deathAnimation;
    }

    [SerializeField] private List<Results> _resultScreens;

    [SerializeField] private GameObject resultObject;
    [SerializeField] private TextMeshProUGUI headline;
    [SerializeField] private TextMeshProUGUI tip;

    [SerializeField] private DeathAnimation _deathAnimation;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ShowResultScreen(int resultScreenIndex, string user1, string user2)
    {
        if (_resultScreens[resultScreenIndex].deathAnimation)
        {
            _deathAnimation.StartAnimation();

        }
        headline.text = _resultScreens[resultScreenIndex].headline.Replace("[ user1 ]", user1)
            .Replace("[ user2 ]", user2);
        tip.text = _resultScreens[resultScreenIndex].tip.Replace("[ user1 ]", user1)
            .Replace("[ user2 ]", user2);
        resultObject.SetActive(true);
    }

    public void HideResult()
    {
        resultObject.SetActive(false);
    }
    
    

    // Update is called once per frame
    void Update()
    {
        
    }
}
