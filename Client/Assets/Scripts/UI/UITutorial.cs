﻿using System.Collections.Generic;
using UnityEngine;

namespace DefaultNamespace
{
    public class UITutorial: UIWindow
    {
        public UIWindow lastWindow;
        public List<GameObject> helpWindows;

        public void onSelectorChange(int value)
        {
            foreach (var window in helpWindows)
            {
                window.SetActive(false);
            }
            helpWindows[value].SetActive(true);
        }

        public void openTutorial(UIWindow uiWindow)
        {
            lastWindow = uiWindow;
            show();
        }

        public void closeTutorial()
        {
            lastWindow.show();
        }
    }
}