using System.Collections;
using System.Collections.Generic;
using JsonStuff;
using UnityEngine;
using UnityEngine.UI;

public class Fullscreen : MonoBehaviour
{
    public Toggle fullscreen;
    // Start is called before the first frame update
    void Start()
    {

        if (PlayerPrefs.HasKey("fullscreen"))
        {
            fullscreen.isOn = (PlayerPrefs.GetInt("fullscreen") == 1);
        }
    }
    
    public void changeValue(bool val){
        if (val)
        {
            Screen.fullScreenMode = FullScreenMode.FullScreenWindow;
        }
        else
        {
            Screen.fullScreenMode = FullScreenMode.Windowed;
        }

        Screen.fullScreen = val;
        PlayerPrefs.SetInt("fullscreen", (val ? 1 : 0));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
