using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Epic.OnlineServices.UnityIntegration;
using TMPro;

namespace DefaultNamespace{
public class UIAuth : UIWindow
{
    public TMP_InputField credName;
    public Button debugLogin;
    public Button loginButton;
    public EOSManager eOSManager;
    public ErrorUI uiError;

    // Start is called before the first frame update
    void Start()
    {
        debugLogin.onClick.AddListener(DebugLogin);
        loginButton.onClick.AddListener(()=>
        {
            DeviceIDLogin(credName.text);
        });
    }

    public void DeviceIDLogin(string username){
        if (username.Length < 3){
            uiError.DisplayError("Username too short", "auth");
            return;
        }
        hide();
        eOSManager.DeviceIDLogin(username);
    }

    public void DebugLogin(){
        hide();
        eOSManager.Login(credName.text, "3002");

    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
}