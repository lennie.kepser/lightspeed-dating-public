﻿using System.Collections;
using System.Collections.Generic;
using Epic.OnlineServices;
using UnityEngine;
using UnityEngine.UI;
using JsonStuff;
using Michsky.UI.ModernUIPack;
using SocketIO;


namespace DefaultNamespace{
public class UILobby : UIWindowTimer
{

    public MatchSettingsUI MatchSettings;

    public ReplayUI ReplayUI;

    public Button StartGameButton;
    public Text LobbyInfo;

    public GameObject timer;

    public ButtonManagerWithIcon buttonIcon;

    public GameObject Container;
    public GameObject AdminUI;

    public GameObject PlayerUIElement;

    public GameObject YouAreAMonster;
    public GameObject YouAreInnocent;
    public GameObject YouAreKevin;
    public GameObject YouAreSpectator;
    public GameObject endGameUI;

    public MatchResultsUI _matchResults;

    public DeathAnimation deathAnimation;

    public GameObject socials;
    public void Start(){
        StartGameButton.onClick.AddListener(StartGame);
        buttonIcon.clickEvent.AddListener(() =>
        {
            GUIUtility.systemCopyBuffer = buttonIcon.buttonText;
        });
    }

    public override void show(){
        base.show();
        manager.audioController.LowerMainMenuMusic();
        
        manager.audioController.PlayMainMenuMusic();
        
    }

    public override void hide()
    {
        base.hide();
        reset();
    }

    public void reset(){
        LobbyInfo.gameObject.SetActive(false);
        YouAreInnocent.SetActive(false);
        YouAreAMonster.SetActive(false);
        YouAreSpectator.SetActive(false);
        YouAreKevin.SetActive(false);
        timer.SetActive(false);

    }

    private string[] lobbytexts = new[] { "Game starts in:", "Shapeshifters picking characters..." };


    public void Kevin(){
        YouAreKevin.SetActive(true);
    }

    public void ShowSpectatorInfo()
    {
        YouAreSpectator.SetActive(true);
    }

    public void NewAdmin(SocketIOEvent e)
    {
        var username = e.data.GetField("admin").str;

        foreach (GameObject elem in _createdUI)
            {
                if (elem.name == "Player" + username)
                {
                    elem.GetComponent<PlayerLobby>().setAdmin(true);
                }
            }

            if (username == manager.GM.username && manager.GM.getState() == 0)
            {
                MatchSettings.GetComponent<MatchSettingsUI>().ChangeEditable(true);
                AdminUI.SetActive(true);
            }
    }
    
    public void HideAdminPanel()
    {
        AdminUI.SetActive(false);
    }

    public void showMonster()
    {
        YouAreAMonster.SetActive(true);
    }

    public void ShowInnocent()
    {
        YouAreInnocent.SetActive(true);

    }

    public void waitTillStart()
    {
        setTime(manager.timers[0]);
        timer.SetActive(true);

        LobbyInfo.gameObject.SetActive(true);
        LobbyInfo.text = lobbytexts[0];
        socials.SetActive(false);

    }

    public void monstersSelect()
    {
        show();
        setTime(manager.timers[1]);
        timer.SetActive(true);

        LobbyInfo.gameObject.SetActive(true);
        LobbyInfo.text = lobbytexts[1];
        manager.spriteManager.HideAllCharacters();
    }   
    
        private void addPlayer(Player player)
        {
            var newui = Instantiate(PlayerUIElement, Container.transform) as GameObject;
            newui.GetComponent<PlayerLobby>().setAdmin(player.admin);
            newui.GetComponent<PlayerLobby>().setUsername(player.username);
            newui.GetComponent<PlayerLobby>().setMonster(player.monster);
            newui.GetComponent<PlayerLobby>().setUserID(ProductUserId.FromString(player.puid));
            if (player.username == manager.GM.username)
            {
                newui.GetComponent<PlayerLobby>().isSelf = true;
            }
            newui.name = "Player" + player.username;
            _createdUI.Add(newui);
        }

        public void RemovePlayer(string player)
        {
            foreach (var _ui in _createdUI.ToArray())
            {
                PlayerLobby playerLobby = _ui.GetComponent<PlayerLobby>();
                if (playerLobby != null && playerLobby.getUsername() == player)
                {
                    removeUI(_ui);
                }
            }
        }

    public void createLobbyUI(List<Player> players)
    {
        cleanUI();
        MatchSettings.show();
        manager.VoiceWearManager.selectSkin(-1);
        AdminUI.SetActive(false);

        foreach (var player in players)
        {
            this.addPlayer(player);
            if (player.username == manager.GM.username && player.admin)
            {
                AdminUI.SetActive(true);
            }
        }
            
    }

    
        public void SetServerID(string serverid)
        {
            buttonIcon.buttonText = serverid;
            buttonIcon.UpdateUI();
        }
      

    public void addLobbyPlayer(Player player)
    {
        addPlayer(player);
    }
     private void StartGame()
        {
            manager.Connect.StartGame();
        }

     public void EndRound(SocketIOEvent e)
        {
            if (!endGameUI.activeInHierarchy){
                reset();
                show();
                
            }
            manager.voiceManager.leaveChannels();
            _matchResults.ShowResultScreen((int) e.data.GetField("ending").f, e.data.GetField("user1").str, e.data.GetField("user2").str);
        }

     public void LobbyVC(SocketIOEvent e)
        {
            // TODO: find place for replay/lobby ui management
            /*
            manager.GM.lobbyvc = e.data.GetField("channel").str;
            if (MatchSettings.Replays.isOn){
                manager.voiceManager.joinChannel(manager.GM.lobbyvc+"-replays");
                ReplayUI.setLobbyVC(manager.GM.lobbyvc);
            }else{
                manager.VivoxManager.joinChannel(manager.GM.lobbyvc);

            }
            */
        }
}
}
