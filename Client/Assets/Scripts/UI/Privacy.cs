using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.UI;

public class Privacy : UINextWindow
{
    [SerializeField] private Toggle toggle;

    [SerializeField] private ErrorUI _error;

    [SerializeField] private UIWindow finalWindow;
    // Start is called before the first frame update
    
    // Update is called once per frame

    private void Start()
    {
        if (PlayerPrefs.HasKey("accepted_privacy"))
        {
            finalWindow.show();
        }
    }

    void Update()
    {
        
    }

    public void Accept()
    {
        if (!toggle.isOn)
        {
            _error.DisplayError("You must accept the agreement to continue.","privacy");
        }
        else
        {
            ShowNextWindow();
            PlayerPrefs.SetInt("accepted_privacy",1);
        }
    }

    public void Decline()
    {
        Application.Quit();
    }
}
