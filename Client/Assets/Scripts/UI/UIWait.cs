using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SocketIO;

namespace DefaultNamespace{
public class UIWait : UIWindowTimer
{   
    public MatchSettingsUI MatchSettings; 
    public void Alone(SocketIOEvent e){
        setTime(float.Parse(MatchSettings.GetComponent<MatchSettingsUI>().Timer.text));

        show();
    }
    
}
}