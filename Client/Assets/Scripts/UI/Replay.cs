using System.Collections;
using System.Collections.Generic;
using System;
using Unity;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace{

    public class Replay : MonoBehaviour
    {
        
        public Button playButton;
        public Text question;
      
        public Button stopButton;

        public Button pauseButton;
        public int maxPosition = -1;

        public Slider slider;

        private float position;

        public string id;

        private int sampleRate;

        public GameObject userContainer;
        public GameObject replayUserFab;
        private ReplayUI.ReplayState state = ReplayUI.ReplayState.Stopped;

        public GameObject playerContainer;
        
        public GameObject corruptText;

        public Toggle voiceFilterToggle;
        
        public ReplayUI parent;

        public Outline outline;
        // Start is called before the first frame update

        public bool IsPlaying(){
            return state == ReplayUI.ReplayState.Playing;
        }

        public int getPosition(){
            return (int) Math.Floor(slider.value);
        }
        void Start()
        {
            stopButton.onClick.AddListener(()=>{parent.StopReplay(id);});
            pauseButton.onClick.AddListener(()=>{parent.PauseReplay(id, getPosition());});
            playButton.onClick.AddListener(()=>{parent.PlayReplay(id, getPosition());});
            voiceFilterToggle.onValueChanged.AddListener(VoiceFilterToggle);
        }

        public void ChangeVoiceFilterValue(bool val)
        {
            voiceFilterToggle.SetIsOnWithoutNotify(val);
            
        }

        public bool getVoiceFilterToggle()
        {
            return voiceFilterToggle.isOn;
        }

        public void VoiceFilterToggle(bool val)
        {
            var data = new JSONObject();
            var details = new JSONObject();
            details.SetField("id", JSONObject.StringObject(id));
            details.SetField("val", val);
            data.SetField("details", details);
            data.SetField("action", JSONObject.StringObject("toggle voice filter"));
            parent.manager.Connect.SendGameAction(data);
        }

        public void Corrupt(){
            playerContainer.SetActive(false);
            corruptText.SetActive(true);
        }

        public void SliderEvent(){
            parent.PlayReplay(id, getPosition());
        }

        public void setParams(List<JSONObject> players, string question, string id, int maxTimeInMS){
            this.question.text = question;
            foreach(var player in players){
                var newui = Instantiate(replayUserFab,userContainer.transform) as GameObject;
                newui.GetComponent<ReplayUser>().SetUp(player.GetField("username").str, player.GetField("disguise").str, player.GetField("dead").b, player.GetField("disguise").str!=player.GetField("username").str);

            }
            
            maxPosition = maxTimeInMS;
            slider.maxValue = maxPosition;
            Debug.Log("max position: "+maxPosition);

            this.id = id;
        }

        public void Update(){
            if (state == ReplayUI.ReplayState.Playing){

                position += Time.deltaTime;
                slider.value = position*100;
            }
        }

        public void SetState(ReplayUI.ReplayState val, int position){
            slider.value = position;
            this.position = position/100;
            state = val;
            switch (val){
                case ReplayUI.ReplayState.Playing:
                    playButton.gameObject.SetActive(false);
                    pauseButton.gameObject.SetActive(true);
                    outline.enabled = true;
                    break;
                case ReplayUI.ReplayState.Paused:
                    playButton.gameObject.SetActive(true);
                    outline.enabled = false;

                    pauseButton.gameObject.SetActive(false);
                    break;
                case ReplayUI.ReplayState.Stopped:
                    outline.enabled = false;

                    playButton.gameObject.SetActive(true);
                    pauseButton.gameObject.SetActive(false);
                    break;
        }
        }
    }
}
