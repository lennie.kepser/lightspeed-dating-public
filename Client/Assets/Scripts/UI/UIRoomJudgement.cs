using System;
using System.Collections;
using System.Collections.Generic;
using Epic.OnlineServices.Lobby;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Epic.OnlineServices.UnityIntegration;
using Epic.OnlineServices.RTCAudio;
using Michsky.UI.ModernUIPack;
using SocketIO;

namespace DefaultNamespace{
    public class UIRoomJudgement : UIRoom
    {
        private string infoText = "Choose the real [Name]!";

        [SerializeField]
        private TextMeshProUGUI turnIndicatorObject;

        [SerializeField]
        private Button playerA;

        [SerializeField]
        private Button playerB;

        [SerializeField] private CanvasGroup _canvasGroupA;
        [SerializeField] private CanvasGroup _canvasGroupB;
        [SerializeField] private CanvasGroup _canvasGroupA2;
        [SerializeField] private CanvasGroup _canvasGroupB2;


        [SerializeField] private TextMeshProUGUI turnTimer;

        [SerializeField] private PieChart pieChart;

        private bool started = false;

        private bool myTurn = false;

        private string roomID = "";
        private int maxTime = 0;
        private int timePerTurn = 1;
        private string username = "";
        private void Start()
        {
            playerA.onClick.AddListener(()=>{
                Debug.Log("Select A");
                manager.Connect.CastVote("A");
                resetButtonPressed();
                playerA.interactable = false;
                _canvasGroupA.alpha = 1;
            });
            playerB.onClick.AddListener(()=>{
                Debug.Log("Select B");
                manager.Connect.CastVote("B");
                resetButtonPressed();
                playerB.interactable = false;
                _canvasGroupB.alpha = 1;
            });
        }

        private void resetButtonPressed()
        {
            _canvasGroupA.alpha = 0;
            _canvasGroupB.alpha = 0;
            _canvasGroupA2.alpha = 1;
            _canvasGroupB2.alpha = 1;
            playerA.interactable = true;
            playerB.interactable = true;
        }

        public void updateVotes(int votesA, int votesB)
        {
            pieChart.ChangeValue(0, votesA);
            pieChart.ChangeValue(1, votesB);
            pieChart.SetData(pieChart.chartData);

            pieChart.UpdateIndicators();

        }

        private void setName(string name)
        {
            List<PieChart.PieChartDataNode> oldData = pieChart.chartData;
            oldData[0].name = name + " A";
            oldData[1].name = name + " B";
            pieChart.SetData(oldData);
            pieChart.UpdateIndicators();
        } 

        public void JoinRoom(string name, string query,int voiceskin, string recordingID, bool player, int character, float turnTime)
        {
            int roomType = 4;
            if (!player)
            {
                roomType++;
            }
            pieChart.ChangeValue(0, 0);
            pieChart.ChangeValue(1, 0);

            username = name;
            base.JoinRoom(query, voiceskin, recordingID, character, roomType, name, "", turnTime);

            Debug.Log("Join Judgement");
            maxTime = (int) turnTime;

            playerA.gameObject.SetActive(!player);
            playerB.gameObject.SetActive(!player);
        }

        public void JoinEventSpectator(SocketIOEvent e)
        {

            var name = e.data.GetField("name").str;
            var query = e.data.GetField("query").str;
            var turn = e.data.GetField("turn").str;
            var character = (int) e.data.GetField("character").n;
            var turnTime = e.data.GetField("roundTimeAll").n;
            timePerTurn = (int) e.data.GetField("timePerTurn").n;

            if (!started)
            {
                resetButtonPressed();
                JoinRoom(name, query, -1, "", false, character, turnTime);
            }

            base.query.text = query;
            turnIndicatorObject.text = name + " " + turn + "'s turn to speak.";

        }

        public void JoinEventPlayer(SocketIOEvent e)
        {
            var name = e.data.GetField("name").str;
            var query = e.data.GetField("query").str;
            var recordingID = e.data.GetField("recordingID").str;
            int voiceskin = (int) e.data.GetField("voiceskin").n;
            var me = e.data.GetField("type").str;
            var turn = e.data.GetField("turn").str;
            var turnTime = e.data.GetField("roundTimeAll").n;
            timePerTurn = (int) e.data.GetField("timePerTurn").n;
 
            myTurn = me == turn;
            roomID = recordingID;

            if (!started)
            {
                JoinRoom(name, query, voiceskin, recordingID, true, voiceskin, turnTime);
            }
            else
            {
                AfterRoomJoinFinish();
            }
            base.query.text = query;

            string preText = "your";
            if (!myTurn)
            {
                preText = "the other players";
            }
            turnIndicatorObject.text = "You are " +name+" "+ me +". It is " + preText +" turn to speak.";
        


        }

       

        public void VoteEvent(SocketIOEvent e)
        {
            int votesA = (int) e.data.GetField("votesA").n;
            int votesB = (int) e.data.GetField("votesB").n;
            print("vote update");
            print(votesA);
            print(votesB);
            updateVotes(votesA,votesB);
        }

        protected override void AfterRoomJoinFinish()
        {
            setName(username);
            if (roomID != "")
            {
                manager.voiceManager.setMute(!myTurn, roomID);
            }

        }

        public override void show()
        {
            base.show();
            started = true;
        }

        public override void hide()
        {
            base.hide();
            started = false;
        }

        override protected void Update()
        {
            base.Update();
            turnTimer.text = Math.Ceiling(timeRemaining % timePerTurn).ToString(); 
        }

    }

}