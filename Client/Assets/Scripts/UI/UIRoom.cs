using System;
using System.Collections;
using System.Collections.Generic;
using Epic.OnlineServices.RTC;
using UnityEngine;
using Unity;
using Epic.OnlineServices.UnityIntegration;
using SocketIO;
using TMPro;
using UnityEngine.UI;


namespace DefaultNamespace
{
    public class UIRoom : UIWindowTimer
    {

        public LightColors _LightColors;
        public DeathAnimation deathAnimation;
        public SpriteManager spriteManager;

        public TextMeshProUGUI query;

        public AudioController audioController;

        public MatchSettingsUI MatchSettings;

        public UIWindow UILoading;

        
        public AudioRecorder audioRecorder;

        public PreRoomUI PreRoomUI;
        
        public Button RefreshQuery;

        private string VoiceChatRoom;
        public void JoinRoom(string question, int voiceskin, string recordingID, int otherPlayerSkin, int roomType, string otherPlayerName, string myName, float turnTime){
            audioController.InRoomSounds(turnTime);
            manager.GM.setCharacter(voiceskin);
            manager.VoiceWearManager.selectSkin(voiceskin);
            var savedTime = Time.time;
            query.text = question;
            spriteManager.ShowCharacterPreview(otherPlayerSkin);
            audioController.StopMainMenuMusic();
            VoiceChatRoom = recordingID;
            RefreshQuery.gameObject.SetActive(true);
            deathAnimation.currentCharacter = otherPlayerSkin;
            
            if (MatchSettings.ExtraVoiceEffects.isOn)
            {
                manager.VoiceWearManager.randomizeParams(2);
            }
            else
            {
                manager.VoiceWearManager.resetParams();
            }
            if (manager.voiceManager.State != VoiceState.Connected){
                PreRoomUI.show(roomType, otherPlayerName, myName);
                manager.voiceManager.joinRoomCallback = ((info)=>{
                    Debug.Log("join room callback");
                    show();

                    spriteManager.ShowCharacter(otherPlayerSkin);
                    manager.VoiceWearManager.selectSkin(voiceskin);
                    setTime(turnTime - (Time.time - savedTime));
                    AfterRoomJoinFinish();
                    manager.voiceManager.joinRoomCallback = null;
                    manager.STT.setSendToConnect(true);

                    if (MatchSettings.Replays.isOn && recordingID != "")
                    { 
                        audioRecorder.StartRecording(recordingID);
                    }
                    
                });
            }else{
                show();
                spriteManager.ShowCharacter(otherPlayerSkin);
                manager.VoiceWearManager.selectSkin(voiceskin);
                AfterRoomJoinFinish();
                setTime(turnTime-(Time.time - savedTime));
                manager.STT.setSendToConnect(true);

                if (MatchSettings.Replays.isOn && recordingID != "")
                { 
                    audioRecorder.StartRecording(recordingID);
                }
            }
        }


        protected virtual void  AfterRoomJoinFinish()
        {
            
        }

        
        public override void hide(){
            if (me.activeInHierarchy){
                audioRecorder.StopRecording();
                audioController.StopRoomSounds();
                manager.STT.setSendToConnect(false);

                _LightColors.SetMode(LightColors.Mode.Off);
                RoomButtonHover.resetStaticVariables();
                manager.voiceManager.leaveRoom(VoiceChatRoom);
                if (!deathAnimation.InProgress()){
                    spriteManager.HideCharacter();
                    spriteManager.Lights(0);
                }
            }
            base.hide();
            // Stop recording
     
        }

       

        new void Update(){
            base.Update();
            if (timeRemaining < 9){
                if (!audioController.EndRiserIsPlaying()){
                    audioController.PlayEndRiser();
                }
            }
        }
    }
}