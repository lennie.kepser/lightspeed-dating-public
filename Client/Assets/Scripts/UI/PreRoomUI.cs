﻿using TMPro;
using UnityEngine;
using System.Collections.Generic;
using SocketIO;

namespace DefaultNamespace
{
    public class PreRoomUI : UIWindow
    {
        [SerializeField] private TextMeshProUGUI roomName;
        [SerializeField] private TextMeshProUGUI objective;

        [SerializeField] private List<string> roomNames;
        [SerializeField] private List<string> objectives;

        public void show(int roomID, string otherName, string myName)
        {
            base.show();
            var roomNameOut = roomNames[roomID];
            var objectiveOut = objectives[roomID];
            if (objectiveOut == null || roomNameOut == null)
            {
                Debug.LogError(roomID+" type room not found.");
                return;
            }
            roomName.text = roomNameOut;

            objectiveOut = objectiveOut.Replace("[Username]", myName);
            objectiveOut = objectiveOut.Replace("[Other Name]", otherName);
            objective.text = "Your objective will be to "+ objectiveOut;
        }
        
        public void PreJudgement(SocketIOEvent e)
        {
            var username = e.data.GetField("username").str;
            show(5, username, username);
        }
    }
}