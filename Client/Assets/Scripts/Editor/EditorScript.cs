/*
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEditor;

public class SetPropertyWindow : EditorWindow
{
    // Start is called before the first frame update
    private bool on = false;
    
    private string myString;
    [MenuItem("Window/Set Property Window")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        EditorWindow.GetWindow(typeof(SetPropertyWindow));
    }

    void OnGUI()
    {
        var myString = "Lightspeed Dating needs to use your Microphone for Voice Chat purposes.";
        GUILayout.Label ("Extra MacOS settings", EditorStyles.boldLabel);
        var descriptionProperty = typeof(UnityEditor.PlayerSettings.macOS).GetProperty("microphoneUsageDescription", BindingFlags.NonPublic | BindingFlags.Static);
        EditorGUI.BeginChangeCheck();
        myString = EditorGUILayout.TextField ("Microphone Usage Description", myString);

        if (EditorGUI.EndChangeCheck())
        {
            descriptionProperty.SetValue(null, myString);
        }
    }
}
*/
#if UNITY_EDITOR
using UnityEditor;
 
[InitializeOnLoad]
public class SetBuildArchitecture {
    static SetBuildArchitecture()
    {
        #if UNITY_EDITOR && UNITY_STANDALONE_OSX
        UnityEditor.OSXStandalone.UserBuildSettings.architecture = UnityEditor.OSXStandalone.MacOSArchitecture.x64;
        #endif
    }
}
#endif