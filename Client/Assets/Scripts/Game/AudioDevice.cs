// Copyright Epic Games, Inc. All Rights Reserved.

namespace DefaultNamespace
{
	public class AudioDevice
	{
		public string Id;
		public string Name;
		public bool IsDefault;

		public AudioDevice()
		{
		}
	}
}
