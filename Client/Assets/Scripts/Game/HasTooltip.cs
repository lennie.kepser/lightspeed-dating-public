﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class HasTooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public string tooltipText;
    public Tooltip tooltipScript;
        public void OnPointerEnter(PointerEventData pointerEventData)
        {
            tooltipScript.show(tooltipText,pointerEventData);
        }

        //Detect when Cursor leaves the GameObject
        public void OnPointerExit(PointerEventData pointerEventData)
        {
            tooltipScript.hide();
        }
    }
