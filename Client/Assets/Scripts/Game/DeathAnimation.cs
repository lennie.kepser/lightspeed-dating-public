using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DefaultNamespace{
public class DeathAnimation : MonoBehaviour
{
    // Start is called before the first frame update

    public AnimatorScript[] charAnimators;
    private AnimatorScript currentAnimator;

    public GameObject[] eyeImages;

    private GameObject currentEyeImage;

    private float interval = 0.1f;

    private float currentStep = 0.0f;

    private int amntSteps = -1;

    private bool flipflop = false;

    public SpriteManager spriteManager;

    public AudioController audioController;

    public CanvasGroup UICanvasGroup;
    public UISettings uISettings;

    public int currentCharacter = 0;

    void Start()
    {
    }

    public bool InProgress(){
        return amntSteps>-1;
    }

    public void StartAnimation(){
        amntSteps = 22;
        currentStep = interval;
        currentEyeImage = eyeImages[currentCharacter];
        currentAnimator = charAnimators[currentCharacter];
        spriteManager.ShowCharacterDark(currentCharacter);
        flipflop = false; 
        currentEyeImage.SetActive(true);
        audioController.PlayMonsterSound();
        UICanvasGroup.interactable = false;
        UICanvasGroup.alpha = 0;
    }

    public void CancelAnimation()
    {
        amntSteps = -1;
    }

    public void ShowEyes(int character)
    {
        eyeImages[character].SetActive(true);
    }

    public void HideEyes()
    {
        foreach (var eyes in eyeImages)
        {
            eyes.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        currentStep -= Time.deltaTime;
        if (currentStep<=0&&amntSteps>-1){
            amntSteps--;
            currentStep = interval;

            if (amntSteps>10){
                spriteManager.ShowCharacterDark(currentCharacter);

                flipflop = !flipflop;

                if (flipflop || uISettings.IsEpilepsyProtectionOn()){
                    spriteManager.Lights(0);
                }else{
                    spriteManager.Lights(2);

                }
            }
            else if(amntSteps == 10){
                spriteManager.HideCharacter();
                currentEyeImage.SetActive(false);
            }
            else if (amntSteps == 0){
                UICanvasGroup.interactable = true;
                UICanvasGroup.alpha = 1;
                amntSteps = -1;
                
                audioController.PlayCrowdCheer();
            }
        }      
    }
}
}