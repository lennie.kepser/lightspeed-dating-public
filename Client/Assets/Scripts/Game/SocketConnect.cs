﻿using System.Collections.Generic;
using UnityEngine;
using JsonStuff;
using Epic.OnlineServices.UnityIntegration;
using System.Net;
using System.Linq;
using SocketIO;
using System;
using Newtonsoft.Json;

namespace DefaultNamespace
{
    public class SocketConnect : MonoBehaviour
    {
        public ReplayUI ReplayUI;
        public UIJoinServer UIJoinServer;
        public UILobby UILobby;
        public UISpectator UISpectator;
        public UIRoomJudgement UIRoomJudgement;
        public UIEndGame UIEndGame;
        public UIWait UIWait;
        public PreRoomUI PreRoomUI;

        public Subtitles _Subtitles;
        public AudioRecorder audioRecorder;
        public LightColors LightColorsObject;
        public UIManager UIManager;
        public GameManager GameManager;
        public UISettings UISettings;

        public ErrorUI UIError;
        public EOSVoiceManager VoiceManager;
        public VoiceWearManagerLSD VoiceWearManager;
        public MatchSettingsUI MatchSettings;
        public Steam _Steam;
        private SocketIOComponent socket;
        private string _secret = "Lennie";

        public void Start()
        {
            GameObject go = GameObject.Find("SocketIO");
            socket = go.GetComponent<SocketIOComponent>();
            socket.On("error event", Error);
            socket.On("join successful", JoinGame);
            socket.On("player join", PlayerJoin);
            socket.On("player leave", PlayerLeave);
            socket.On("test", Test);
            socket.On("alone", UIWait.Alone);
            socket.On("new admin", UILobby.NewAdmin);
            socket.On("game start info", GameManager.GameInit);
            socket.On("monster start info", GameManager.MonsterInit);
            socket.On("charselect", GameManager.CharSelect);
            socket.On("monster char select", GameManager.MonsterCharSelect);
            socket.On("in room", GameManager.JoinRoom);
            socket.On("endgame", UIEndGame.Endgame);
            socket.On("endgame other", UIEndGame.EndGameOtherReason);
            socket.On("lobby", GameManager.Lobby);
            socket.On("player assignment failure", GameManager.RoomFailure);

            socket.On("join voiceroom", VoiceManager.JoinVCRoomEvent);
            // Not sure what this is for?
            //socket.On("killed monster",KilledMonster);
            socket.On("charselect spectator",GameManager.CharSelect);
            socket.On("spared", GameManager.Spared);
            socket.On("update settings", MatchSettings.RecieveSettings);
            socket.On("play replay", GameManager.PlayReplay);
            socket.On("stop replay", GameManager.StopReplay);
            socket.On("connect", (SocketIOEvent e) =>
            {
                Unlock();
                _Steam.Authenticate();
            });
            socket.On("spectator rooms", UISpectator.SpectatorRooms);
            socket.On("round end", UILobby.EndRound);
            socket.On("last rounds", ReplayUI.getAfterRoundData);
            socket.On("in room triple", GameManager.JoinRoomTriple);
            socket.On("crash", UIError.Crash);
            socket.On("disconnect", Disconnect);
            socket.On("subtitle", OnSubtitleData);
            socket.On("judgement room spectator", UIRoomJudgement.JoinEventSpectator);
            socket.On("judgement room player", UIRoomJudgement.JoinEventPlayer);
            socket.On("judgement votes", UIRoomJudgement.VoteEvent);
            socket.On("judgement preroom", PreRoomUI.PreJudgement);
            socket.On("endgame mayhem", UIEndGame.EndgameMayhem);
            socket.On("light colors", LightColorsObject.SocketEvent);
            socket.On("steam auth", _Steam.OnAuthenticate);
        }

        public void AddSocketListener(String ev, Action<SocketIOEvent> callback)
        {
            socket.On(ev, callback);
        }

        public void SendGameAction(JSONObject jsonObject)
        {
            socket.Emit("game action", jsonObject);
        }

        public void SendEmptyGameAction(String action)
        {
            JSONObject data = new JSONObject();
            data.AddField("action", action);
            data.AddField("details", JSONObject.nullJO);
            socket.Emit("game action", data);

        }

        public void SendSubtitleData(String phrases)
        {
            JSONObject data = new JSONObject();
            data.AddField("phrase", phrases);
            socket.Emit("subtitle", data);
        }

        public void OnSubtitleData(SocketIOEvent e)
        {
            Debug.Log("Recieved subtitle");
            Debug.Log(e.data.GetField("phrase").str);
            _Subtitles.addText(e.data.GetField("phrase").str, e.data.GetField("user").str);
        }

        public void SteamAuth(string ticket)
        {
            JSONObject data = new JSONObject();
            data.AddField("ticket", ticket);
            socket.Emit("steam authenticate", data);
        }

        public void SendColor(int color)
        {
            JSONObject data = new JSONObject();
            data.AddField("action", "light colors");
            data.AddField("details", color);
            socket.Emit("game action", data);

        }

        private void Disconnect(SocketIOEvent e)
        {
            UIJoinServer.LeaveServer();
            UIError.DisplayError("socket", "The connection to the server was lost.");
        }

       

        public void SendReplayData(string id, int timeInMS)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data["action"] = "replay data";
            var details = new Dictionary<string, string>();
            details["id"] = id;
            var JSONdata = new JSONObject(data);
            JSONdata["details"] = new JSONObject(details);
            JSONdata["details"]["maxTime"] = new JSONObject(timeInMS);
            socket.Emit("game action", JSONdata);
        }

        public void SendTripleChoice(int choice)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data["action"] = "choose triple";
            var details = new Dictionary<string, string>();
            var JSONdata = new JSONObject(data);
            JSONdata["details"] = new JSONObject(details);

            JSONdata["details"]["choice"] = new JSONObject(choice);

            socket.Emit("game action", JSONdata);

        }

        public void CastVote(string vote)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data["action"] = "judgement vote";
            data["details"] = vote;
            socket.Emit("game action", new JSONObject(data));

        }

        public void LeaveSession()
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data["action"] = "leave game";
            socket.Emit("game action", new JSONObject(data));
        }

        public void EnterEchoRoom()
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data["puid"] = VoiceManager.userId.ToString();


            socket.Emit("join echo room", new JSONObject(data));

        }

        public void spare()
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data["action"] = "spare";
            socket.Emit("game action", new JSONObject(data));
        }



        private void Unlock()
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data["secret"] = _secret;
            socket.Emit("unlock", new JSONObject(data));

        }

        public void Sus()
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data["action"] = "kill";
            socket.Emit("game action", new JSONObject(data));
        }

        public void PickDisguise(string username)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data["action"] = "pick disguise";
            data["details"] = username;
            socket.Emit("game action", new JSONObject(data));
        }

        public void PickTarget(string target)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data["action"] = "pick target";
            data["details"] = target;
            socket.Emit("game action", new JSONObject(data));
        }

        private void Test(SocketIOEvent e)
        {
            Debug.Log("test");
        }

        private void PlayerJoin(SocketIOEvent e)
        {
            var cur = e.data;
            GameManager.addPlayer(new Player(cur.GetField("username").str, cur.GetField("admin").b,
                cur.GetField("character").n, cur.GetField("monster").b, !cur.GetField("dead").b,
                cur.GetField("disguise").str, cur.GetField("puid").str, cur.GetField("spectating").b));

        }



        public void StartGame()
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data["action"] = "start game";
            socket.Emit("game action", new JSONObject(data));
        }

        public void Spectate(string roomid)
        {
            var data = new JSONObject();
            data.AddField("action", "spectate");

            Dictionary<string, string> details = new Dictionary<string, string>();
            details["room"] = roomid;
            data.AddField("details", new JSONObject(details));
            socket.Emit("game action", data);
        }

        public void UpdateSettings(Dictionary<string, string> settings)
        {
            var data = new JSONObject();
            data.AddField("action", "set settings");

            data.AddField("details", new JSONObject(settings));
            socket.Emit("game action", data);
        }

        public void UpdatePlayerSettings(Dictionary<string, JSONObject> settings)
        {
            var data = new JSONObject();
            data.AddField("action", "player setting");

            data.AddField("details", new JSONObject(settings));
            socket.Emit("game action", data);
        }

        private void PlayerLeave(SocketIOEvent e)
        {
            GameManager.removePlayer(e.data.GetField("username").str);
        }


        private void Error(SocketIOEvent e)
        {
            Debug.Log("Error");
            Debug.Log(e);

            var type = e.data.GetField("type").str;
            var msg = e.data.GetField("msg").str;
            UIError.DisplayError(msg, type);
        }

        public void joinServer(string username, string serverid)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data["room"] = serverid;
            data["puid"] = VoiceManager.userId.ToString();

            data["username"] = username;
            GameManager.username = username;
            socket.Emit("join", new JSONObject(data));
        }

        public List<Player> getPlayersFromJSON(JSONObject data)
        {
            var players = new List<Player>();

            for (var i = 0; i < data.Count; i++)
            {
                var cur = data[i];
                players.Add(new Player(cur.GetField("username").str, cur.GetField("admin").b,
                    cur.GetField("character").n, cur.GetField("monster").b, !cur.GetField("dead").b,
                    cur.GetField("disguise").str, cur.GetField("puid").str, cur.GetField("spectating").b));
            }

            return players;
        }

        public List<Player> getPlayersFromSocketEvent(SocketIOEvent e, string fieldname)
        {
            return getPlayersFromJSON(e.data.GetField(fieldname));
        }

        private void JoinGame(SocketIOEvent e)
        {
            var timers = e.data.GetField("times");
            UIManager.setUpTimers(timers.GetField("pregame").n, timers.GetField("disguiseselect").n);
            UILobby.SetServerID(e.data.GetField("serverid").str);
            GameManager.setPlayers(getPlayersFromSocketEvent(e, "players"));
            UILobby.show();
            UILobby.createLobbyUI(getPlayersFromSocketEvent(e, "players"));
            GameManager.SetInGame(true);
            _Steam.joinGame();
            var characters = new List<JSONObject>();
            characters.Add(JSONObject.CreateStringObject("0"));
            characters.Add(JSONObject.CreateStringObject("1"));
            if (Steamworks.SteamApps.IsDlcInstalled(2231240)){

                characters.Add(JSONObject.CreateStringObject("2"));
                //characters.Add(JSONObject.CreateStringObject("3"));
        
            }
            SendListGameAction("set characters", characters);
        }

        public void CreateServer(string username)
        {
            GameManager.username = username;
            Dictionary<string, string> data = new Dictionary<string, string>();
            data["username"] = username;
            data["puid"] = VoiceManager.userId.ToString();

            socket.Emit("create server", new JSONObject(data));
        }

        public void SendListGameAction (string eventName, List<JSONObject> list)
        {
            var json = new JSONObject();
            json.AddField("action",eventName);
            json.AddField("details",new JSONObject(list.ToArray()));
            socket.Emit("game action", json);
        }
        
    }
}