using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ColorAnimation : MonoBehaviour
{
    public Image image;
    public float alpha;
    private float Hue;
    private float Saturation;
    private float Value;
    private float AlphaAdd = (float)Math.PI ;
    private float Alpha = 1.0f;

    private float satAdd = (float) Math.PI;
    // Start is called before the first frame update
    void Start()
    {
        Color.RGBToHSV(image.color, out Hue, out Saturation, out Value);
    }

    // Update is called once per frame

    private void hueChange(float deltaTime){
        var newcolor = Color.HSVToRGB(0.5f + (float)Math.Sin(Hue)/2, 0.125f + ((float)Math.Sin(satAdd)/8f), Value);
        newcolor.a = image.color.a;
        image.color = newcolor;
        Hue -= (deltaTime/10);
        satAdd -= (deltaTime/29);
        if (Hue < - (float) Math.PI){
            Hue = (float)Math.PI;
        }
        if (satAdd < 0){
            satAdd = (float)Math.PI;
        }
    }

    private void pulsate(float deltaTime){
        
        AlphaAdd -= deltaTime;
        if(AlphaAdd < - (float) Math.PI*1.3){
            AlphaAdd = (float)Math.PI ;

        }else if(AlphaAdd >= - (float) Math.PI){
            var newcolor = Color.HSVToRGB(Hue, Saturation, Value);
            newcolor.a = 0.25f + (float) Math.Cos(AlphaAdd)/4f;
            image.color = newcolor;
        }
        
    }

    void Update()
    {
        if (image.gameObject.activeSelf){
            pulsate(Time.deltaTime);
            hueChange(Time.deltaTime);
        }
        
    }
}
