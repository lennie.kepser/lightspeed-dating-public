﻿using System.Collections;
using System.Collections.Generic;
using System;
using Epic.OnlineServices.UnityIntegration;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using Epic.OnlineServices.Stats;
using SocketIO;
using Modulate;
using Steamworks;

namespace DefaultNamespace
{
    public class Wrapper<T>
    {
        public T Value { get; set; }
    }

    public class VoiceWearManagerLSD : MonoBehaviour
    {
        [SerializeField] private float radio;
        [SerializeField] private float intimidator;
        [SerializeField] private float helm;
        [SerializeField] private float vivid;
        [SerializeField] private float presence;
        [SerializeField] private float bass;



        public AudioController audioController;
        private modulate_parameters parameters;
        public ModulateManager modulateManager;
        private VoiceWearManager manager;

        public AudioRecorder audioRecorder;
        
        public EOSVoiceManager voiceManager;

        private DateTime voiceSkinSelected;

        private int lastVoiceSkin = -1;

        public MatchSettingsUI MatchSettings;
        

        // Start is called before the first frame update
        void Start()
        {

            manager = modulateManager.manager;
            Initialize();
        }

        public void endEcho()
        {
            //modulateManager.SetEcho(false);
        }
        public void startEcho()
        {
            //modulateManager.SetEcho(true);
        }

        public void setParams()
        {
            parameters.radio_strength = radio;
            parameters.intimidator_strength = intimidator;
            parameters.helm_strength = helm;
            parameters.vivid_strength = vivid;
            parameters.presence_strength = presence;
            parameters.bass_booster_strength = bass;
        }

        public void SetLowQualAudioFiliter(Boolean on){
            if (on){
                vivid = 0.7f;
            }else{
                vivid = 0f;

            }
            setParams();
        }

        public void randomizeParams(int amntParams)
        {
            resetParams();
            for(int i = 0; i < amntParams; i++)
            {
                setParam(UnityEngine.Random.Range((int)0, (int)3), UnityEngine.Random.Range(0f, 0.7f));
                
            }
            setParams();
        }

        public void resetParams()
        {
            for (int i = 0; i < 5; i++)
            {
                setParam(i, 0f);
            }
        }

        public void setParam(int param, float val)
        {
            switch (param)
            {
                case 0:
                    radio = val;
                    break;
                case 1:
                    intimidator = val;
                    break;
                case 2:
                    helm = val;
                    break;
                case 3:
                    presence = val;
                    break;
                case 4:
                    bass = val;
                    break;
                case 5:
                    vivid = val;
                    break;
            }
        }

        public void Initialize()
        {
            parameters = manager.get_modulate_params();
            voiceSkinSelected = DateTime.Now;
            setParams();
        }

        public void selectSkin(int skin)
        {
            setParams();
            Debug.Log("Selected skin "+skin);
            if (skin == -1)
            {
                if (lastVoiceSkin != -1)
                {
                    var difference = (DateTime.Now - voiceSkinSelected).TotalHours;
                    Steam.updateVoiceSkinStat(lastVoiceSkin.ToString(),(float) difference);
                } 
                modulateManager.manager.set_enabled(false);
                
            }
            else
            {
                modulateManager.load_skin(skin);
                
            }

            lastVoiceSkin = skin;
            voiceSkinSelected = DateTime.Now;

        }


        public static void ClipToWav(short[] data, int length, int frequency, string name)
        {
            

            byte[] bytes = new byte[length * 2];

            for (int ii = 0; ii < length; ii++)
            {
                short uint16 = data[ii];
                byte[] vs = BitConverter.GetBytes(uint16);
                bytes[ii * 2] = vs[0];
                bytes[ii * 2 + 1] = vs[1];
            }

            byte[] wav = new byte[44 + bytes.Length];

            byte[] header = {0x52, 0x49, 0x46, 0x46, 0x00, 0x00, 0x00, 0x00,
                          0x57, 0x41, 0x56, 0x45, 0x66, 0x6D, 0x74, 0x20,
                          0x10, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                          0x04, 0x00, 0x10, 0x00, 0x64, 0x61, 0x74, 0x61 };

            Buffer.BlockCopy(header, 0, wav, 0, header.Length);
            Buffer.BlockCopy(BitConverter.GetBytes(36 + bytes.Length), 0, wav, 4, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(1), 0, wav, 22, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(frequency), 0, wav, 24, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(frequency * 1 * 2), 0, wav, 28, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(1 * 2), 0, wav, 32, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(bytes.Length), 0, wav, 40, 4);
            Buffer.BlockCopy(bytes, 0, wav, 44, bytes.Length);

            File.WriteAllBytes(Application.dataPath + "/"+name+".wav", wav);
            Debug.Log("saving replay " + Application.dataPath + "/" + name + ".wav");

        }
    }
}