﻿
/*using System;
using UnityEngine;
using Unity;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net.Mime;
using System.Net.Sockets;
using JsonStuff;
using SocketIO;

namespace DefaultNamespace
{
    public class UIGame : MonoBehaviour
    {
        public VivoxManager vivoxManager;
        public GameObject LobbyUI;

        public GameObject MonsterSelectContainer;
        public GameObject SpectateContainer;

        public GameObject MatchUI;
        public GameObject PlayerUIElement;
        public GameObject SpectateRoom;
        public GameObject MonsterUIElement;
        public GameObject SpectateUIElement;
        public GameObject MonsterUI;
        
        public GameObject EndGameUI;
        public GameObject WaitUI;
        public GameObject KilledUI;
        public GameObject SparedInnocent;
        public GameObject SparedMonster;
        public GameObject SpectateList;
        public GameObject SpectateInfoWait;
        public GameObject MonsterKilledUI;
        public GameObject SparedUI;
        public GameObject SpectatorUI;
        public GameObject MatchSettings;
        public Button MonsterButton;
        public Button SpareButton;
        public Button FakeSpareButton;
        
        public Text RoundTimer;
        public Text MonsterTimer;
        public Text ServerID;
        public Text MonsterQuery;
        public Text HumanQuery;

       
        public Text MonsterMatchTimer;
        public Text SpectateTimer;
        public Connect Connect;
        public Button StartButton;
        public Button SpectatorBackButton;

        public Button EndGameButton;
        public GameManager GameManager;

        private Connect _connect;
        private GameManager _gm;
        private Text _roundtimer;
        private Text _lobbytimer;
        private Text _lobbyinfo;
        private Text _monstertimer;
        private Text _monstermatchtimer;
        private Text _waittimer;
        private Text _spectateTimer;

        private Button _endgamebutton;
        private Button _spectatorBackButton;
        private Button _startbutton;
        private Button _monsterbutton;
        private Button _sparebutton;
        private Button _fakesparebutton;

        private VivoxManager _vivoxManager;

        private float timeRemaining;
        private List<GameObject> _createdui;


        public void Start()
        {

            _waittimer = WaitTimer.GetComponent<Text>();
            _lobbytimer = LobbyTimer.GetComponent<Text>();
            _roundtimer = RoundTimer.GetComponent<Text>();
            _monstertimer = MonsterTimer.GetComponent<Text>();
            _monstermatchtimer = MonsterMatchTimer.GetComponent<Text>();
            _lobbyinfo = LobbyInfo.GetComponent<Text>();
            _endgamebutton = EndGameButton.GetComponent<Button>();
            _connect = Connect.GetComponent<Connect>();
            _monsterbutton = MonsterButton.GetComponent<Button>();
            _gm = GameManager.GetComponent<GameManager>();
            _sparebutton = SpareButton.GetComponent<Button>();
            _fakesparebutton = FakeSpareButton.GetComponent<Button>();
            this._spectateTimer = SpectateTimer.GetComponent<Text>();
            _createdui = new List<GameObject>();
            _startbutton = StartButton.GetComponent<Button>();
            _spectatorBackButton = SpectatorBackButton.GetComponent<Button>();

            _vivoxManager = vivoxManager.GetComponent<VivoxManager>();

            _startbutton.onClick.AddListener(StartGame);
            _monsterbutton.onClick.AddListener(MonsterButtonPressed);
            _spectatorBackButton.onClick.AddListener(goBackSpectator);

            _sparebutton.onClick.AddListener(Spare);
            _fakesparebutton.onClick.AddListener(Spare);

            _endgamebutton.onClick.AddListener(EndGameButtonPressed);

            _rooms = new List<Room>();
            Room room = new Room();
            room.player1 = new Player("Len", false, 0, true, true, "");
            room.player2 = new Player("Len2", false, 0, false, true, "");
            room.voiceid = "a";
            room.query = "suck";

        }

        public void Spare()
        {
            _connect.spare();
            _sparebutton.gameObject.SetActive(false);
            _fakesparebutton.gameObject.SetActive(false);
        }

        public void SetServerID(string serverid)
        {
            ServerID.GetComponent<Text>().text = "Server ID: " + serverid;
            ServerID.gameObject.SetActive(true);
        }


        public void Spared(bool ismonster)
        {
            if (ismonster)
            {
                SparedMonster.SetActive(true);
            }
            else
            {
                SparedInnocent.SetActive(true);

            }
        }


        public void SparedFinal(SocketIOEvent e)
        {
            resetUI();
            _vivoxManager.leaveChannel();
            SparedUI.SetActive(true);
        }

        public void waitSpectator()
        {
            cleanUI();
            resetUI();
            SpectatorUI.SetActive(true);


            SpectateInfoWait.SetActive(true);
        }

        


        public void resetUI()
        {
            EndGameUI.SetActive(false);
            MonsterMatchUI.SetActive(false);
            MonsterSelectUI.SetActive(false);
            MatchUI.SetActive(false);
            LobbyUI.SetActive(false);
            MatchSettings.SetActive(false);
            WaitUI.SetActive(false);
            KilledUI.SetActive(false);
            SpectateInfoWait.SetActive(false);
            SpectateRoom.SetActive(false);
            SpectateList.SetActive(false);
            MonsterInfo.SetActive(false);
            SparedUI.SetActive(false);
            MonsterKilledUI.SetActive(false);
            ServerID.gameObject.SetActive(false);

            _sparebutton.gameObject.SetActive(true);
            _fakesparebutton.gameObject.SetActive(true);
        }



        private void EndGameButtonPressed()
        {
            resetUI();
            LobbyTimer.gameObject.SetActive(false);
            LobbyInfo.gameObject.SetActive(false);
            LobbyUI.SetActive(true);
            MatchSettings.SetActive(true);
        }

        public void Alone()
        {
            resetUI();
            WaitUI.SetActive(true);
            timeRemaining = float.Parse(MatchSettings.GetComponent<MatchSettingsUI>().Timer.text);
        }

        public void Killed()
        {
            resetUI();
            KilledUI.SetActive(true);
        }

        public void Endgame(bool monster)
        {

            resetUI();
            EndGameUI.SetActive(true);
            if (monster)
            {
                EndGameUI.transform.GetChild(0).gameObject.SetActive(false);
                EndGameUI.transform.GetChild(1).gameObject.SetActive(true);
            }
            else
            {
                EndGameUI.transform.GetChild(0).gameObject.SetActive(true);
                EndGameUI.transform.GetChild(1).gameObject.SetActive(false);
            }
        }

        private void MonsterButtonPressed()
        {
            _connect.Sus();
        }

        private void StartGame()
        {
            _connect.StartGame();
        }

        
        
      
        

        private void addSpectator(Room room)
        {
            var newui = Instantiate(SpectateUIElement,SpectateContainer.transform) as GameObject;
            newui.transform.GetChild(1).GetComponent<Text>().text = room.player1.username;
            newui.transform.GetChild(0).gameObject.SetActive(room.player1.monster);
            newui.transform.GetChild(2).gameObject.SetActive(room.player2.monster);
            newui.transform.GetChild(3).GetComponent<Text>().text = room.player2.username;
            newui.transform.GetChild(4).GetComponent<Button>().onClick.AddListener(() =>
            {
                SpectateList.SetActive(false);
                SpectateRoom.SetActive(true);
                vivoxManager.spectate(room.voiceid);
                SpectateRoom.transform.GetChild(0).GetChild(0).gameObject.SetActive(room.player1.monster);
                SpectateRoom.transform.GetChild(0).GetChild(2).gameObject.SetActive(room.player2.monster);
                SpectateRoom.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = room.player1.username;
                SpectateRoom.transform.GetChild(0).GetChild(3).GetComponent<Text>().text = room.player2.username;
                SpectateRoom.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = room.query;
            });
            newui.name = "Room" + room.voiceid;
            newui.SetActive(true);

            _createdui.Add(newui);
        }

        public void setUpSpectator(List<Room> rooms)
        {

            cleanUI();
            resetUI();
            SpectatorUI.SetActive(true);
            SpectateList.SetActive(true);
            SpectateRoom.SetActive(false);

            this.timeRemaining = float.Parse(MatchSettings.GetComponent<MatchSettingsUI>().Timer.text);
            _rooms = rooms;
            foreach (var room in rooms)
            {
                addSpectator(room);
            }
        }

        private void goBackSpectator()
        {
            SpectateList.SetActive(true);
            SpectateRoom.SetActive(false);
            vivoxManager.leaveChannel();
        }
        

        
        
        
    


        

        public void KilledMonster()
        {
            resetUI();
            
        }
        
        
    }
}
*/