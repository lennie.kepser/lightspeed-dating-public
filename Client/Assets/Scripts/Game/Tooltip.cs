using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class Tooltip : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI text;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void show(string text, PointerEventData pointerEventData)
    {
        gameObject.SetActive(true);
        this.text.text = text;
    }

    public void hide()
    {
        gameObject.SetActive(false);

    }
    
  
    // Update is called once per frame
    void Update()
    {
        if (gameObject.activeSelf)
        {
            if (Input.GetMouseButtonUp(0))
            {
                Steamworks.SteamFriends.OpenStoreOverlay(2231240);

            }
        }
        transform.position = Input.mousePosition;
        
    }

    public void onClick()
    {
    }
}
