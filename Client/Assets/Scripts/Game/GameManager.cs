﻿using System;
using Unity;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JsonStuff;
using Epic.OnlineServices.UnityIntegration;
using SocketIO;


namespace DefaultNamespace
{
    public class GameManager : MonoBehaviour
    {
        public SpriteManager spriteManager;
        public UILobby UILobby;
        public ReplayUI UIReplay;
        public UIWindow UILoading;

        public UIRoomDecidee UIRoomDecidee;
        public UIRoomDecider UIRoomDecider;
        public VoiceWearManagerLSD VoiceWearManager;
        public EOSVoiceManager voiceManager;
        public MatchSettingsUI MatchSettings;
        public MonsterSelectUI MonsterSelectUI;
        public MonsterMatchUI MonsterMatchUI;

        public UIWindowTimer RoomFailureUI;

        public AudioRecorder audioRecorder;
        public UISettings UISettings;

        public UIInnocent UIInnocent;
        public AudioController audioController;
        
        public SocketConnect Connect;

        public string lobbyvc = "";


        private int _gamestate;
        private List<Player> players;
        private Player _player;
        public string username;

        private bool inGame = false;

        public void PlayReplay(SocketIOEvent e){
            UIReplay.PlayReplaySet(e.data.GetField("id").str, (int) e.data.GetField("position").n);
            audioRecorder.PlayRecording(e.data.GetField("id").str, (int) e.data.GetField("position").n);
        }

        public void StopReplay(SocketIOEvent e)
        {
            audioRecorder.StopPlaying();
            UIReplay.StopReplaySet(e.data.GetField("id").str, (int) e.data.GetField("position").n);

        }


        public void RoomFailure(SocketIOEvent e)
        {
            RoomFailureUI.show();
            RoomFailureUI.setTime(3f);
        }

        public void JoinRoom(SocketIOEvent e)
        {
            
            int character = (int) e.data.GetField("voiceskin").n;
            var otherplayer = e.data.GetField("otherplayer").str;
            int otherPlayerSkin = (int) e.data.GetField("otherPlayerSkin").n;
            var query = e.data.GetField("query").str;
            int gameMode = (int) e.data.GetField("gameMode").n;
            if (isMonster() && gameMode != 1){
                MonsterMatchUI.JoinRoom(otherplayer, query,character,e.data.GetField("recording").str, otherPlayerSkin, e.data.GetField("myskin").str);
            }else{
                UIInnocent.JoinRoom(otherplayer, query,character,e.data.GetField("recording").str, otherPlayerSkin);
            }
             
            
        }

         public void JoinRoomTriple(SocketIOEvent e){
            int character = (int) e.data.GetField("voiceskin").n;
            var query = e.data.GetField("query").str;
            int otherPlayerSkin = (int) e.data.GetField("otherPlayerSkin").n;

            if (e.data.GetField("decider").b){
                string[] puids = {e.data.GetField("puids")[0].str,e.data.GetField("puids")[1].str};
                UIRoomDecider.JoinRoom(e.data.GetField("name").str,puids,e.data.GetField("recording").str, query, character, e.data.GetField("recording").str, otherPlayerSkin);
            }else{
                UIRoomDecidee.JoinRoom(e.data.GetField("name").str,e.data.GetField("nameDecider").str,query, character, e.data.GetField("recording").str, otherPlayerSkin);
            }
        }

         public void SetInGame(bool val)
         {
             inGame = val;
         }
         
         public bool GetInGame()
         {
             return inGame;
         }

        public void Lobby(SocketIOEvent e)
        {
            setState(0);
            setPlayers(Connect.getPlayersFromSocketEvent(e, "info"));
            UILobby.createLobbyUI(players);

        }
        

        public void Start()
        {
            players = new List<Player>();
            _gamestate = 0;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape) && Debug.isDebugBuild)
            {
                Debug.LogError("Open Sesame");

            }

            if (Input.GetKeyDown(KeyCode.F4))
            {
                VoiceWearManager.selectSkin(-1);
                voiceManager.leaveChannels();
                StartCoroutine(EndApplicationAfterSeconds(2));

            }
        }

        IEnumerator EndApplicationAfterSeconds(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            Application.Quit();
        }

        public void Spared(SocketIOEvent e)
        {
                MonsterMatchUI.Spared();
                UIInnocent.Spared();
        }

        

    

        public bool isMonster()
        {
            return _player.monster;
        }

        public void pregame()
        {
            UILobby.waitTillStart();
            audioRecorder.ClearRecordings();
            spriteManager.SetUpAudience();
        }

        public void CharSelect(SocketIOEvent e)
        {
            voiceManager.leaveChannels();
            UILobby.monstersSelect();
        }

        public void setCharacter(int character)
        {
            _player.character = character;
        }

        public int getCharacter()
        {
            return _player.character;
        }

        public void setPlayers(List<Player> newplayers)
        {
            players = newplayers;
            foreach (var player in players)
            {
                if (player.username == username)
                {
                    _player = player;
                }
            }


        }

        

        public void removePlayer(string username)
        {
            foreach (var player in players.ToArray())
            {
                if (player.username == username)
                {
                    players.Remove(player);
                }
            }

            UILobby.RemovePlayer(username);
        }

        public void addPlayer(Player player)
        {
            players.Add(player);
            UILobby.addLobbyPlayer(player);
        }

        public int getState()
        {
            return _gamestate;
        }

        public void setState(int state)
        {
            _gamestate = state;
        }

        public void GameInit(SocketIOEvent e)
        {
            UILobby.show();
            voiceManager.leaveChannels();
            pregame();
            setState(1);
            UILobby.HideAdminPanel();
            if ((int) e.data.GetField("character").n != -1)
            {
                UILobby.ShowInnocent();
                setCharacter((int)e.data.GetField("character").n);
                spriteManager.SimpleShowCharacter((int)e.data.GetField("character").n);
            }
            else
            {
                UILobby.ShowSpectatorInfo();
            }

            if (e.data.HasField("kevin") && e.data.GetField("kevin").b)
            {
                UILobby.Kevin();
            }
            UISettings.hideEchoSettings();
        }

        
        public void MonsterInit(SocketIOEvent e)
        {
            spriteManager.ShowRandomCharacterMonster();
            setPlayers(Connect.getPlayersFromSocketEvent(e, "gameinfo"));
            UILobby.showMonster();
            UILobby.createLobbyUI(players);

            pregame();
            setState(1);
            UILobby.HideAdminPanel();
            UISettings.hideEchoSettings();

        }

         public void MonsterCharSelect(SocketIOEvent e)
         {
            var gameMode = (int) e.data.GetField("gameMode").n;
            voiceManager.leaveChannels();
            setState(1);
            setPlayers(Connect.getPlayersFromSocketEvent(e, "gameinfo"));
            MonsterSelectUI.createMonsterUI(players, gameMode);

        }

        public string getDisguise(){
            return _player.disguisedas;
        }
    }
}