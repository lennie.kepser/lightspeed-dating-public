﻿using System;

namespace JsonStuff
{
    [Serializable]
    public class Player
    {
        public string username;
        public bool admin;
        public int character;
        public bool monster;
        public bool alive;
        public string disguisedas;
        public string puid;
        public bool spectating;

        public Player(string username, bool admin, float character, bool monster, bool alive, string disguisedas, string puid, bool spectating)

        {
            this.username = username;
            this.admin = admin;
            this.character = (int) character;
            this.monster = monster;
            this.alive = alive;
            this.puid = puid;
            this.disguisedas = disguisedas;
            this.spectating = spectating;
        }
    }
}