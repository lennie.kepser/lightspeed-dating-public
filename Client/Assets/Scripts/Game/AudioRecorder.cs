using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Epic.OnlineServices.RTCAudio;
using Epic.OnlineServices.UnityIntegration;
using System.Threading;
using System;


namespace DefaultNamespace{

    public class AudioRecorder : MonoBehaviour {
        private Dictionary<string, List<(short, short)>> recordings = new Dictionary<string, List<(short, short)>>();
        private string currentlyRecording = "";
        private int stopRecordingAt = 0;

        private int sampleRate = 0;

        [SerializeField] private ReplayUI _replayUI;
        [SerializeField] private VoskSpeechToText _STT;

        [SerializeField]
        private EOSVoiceManager voiceManager;

        [SerializeField]
        private SocketConnect connect;

		List<(AudioBuffer, AudioBuffer)> audioBuffers = new List<(AudioBuffer, AudioBuffer)>();

        List<System.Timers.Timer> timers = new List<System.Timers.Timer>();

        private bool playUnfiltered = false;

        public void setPlayUnfiltered(bool val)
        {
	        playUnfiltered = val;
        }

        public int GetMaxPosition(string id){
            return recordings[id].Count;
        }

        public void RecordAudio(short[] audioVoiceFilter, short[] audioNoFilter, int channels, int sampleRate){
            this.sampleRate = sampleRate;
            List<short> monoNoFilter = new List<short>();

            for (int i = 0; i < audioVoiceFilter.Length; i += channels)
            {
	            monoNoFilter.Add(audioNoFilter[i]);

            }
            _STT.VoiceProcessorOnOnFrameCaptured(monoNoFilter.ToArray());

            if (currentlyRecording == ""){
                return;
            }
            for (int i = 0; i < audioVoiceFilter.Length; i += channels)
            {
                recordings[currentlyRecording].Add((audioVoiceFilter[i], audioNoFilter[i]));
            }
            if (stopRecordingAt != 0 && recordings[currentlyRecording].Count > stopRecordingAt){
                
                FinalizeRecording();
            }
        }

        private void FinalizeRecording()
        {
            int startRange = stopRecordingAt - (stopRecordingAt%sampleRate) - 1;
            if (startRange < 0){
                startRange = 0;
            }
	        recordings[currentlyRecording].RemoveRange(startRange, stopRecordingAt%sampleRate);
	        connect.SendReplayData(currentlyRecording,(int) Math.Floor((float) recordings[currentlyRecording].Count/(sampleRate/100)+20));
                
	        currentlyRecording = "";
	        stopRecordingAt = 0;
        }

        public void PlayRecording(string replay, int position){
            if (!recordings.ContainsKey(replay)){
                return;
            }
            StartCoroutine(PlayShortList(recordings[replay].GetRange(position*sampleRate/100,recordings[replay].Count - (position*sampleRate/100)),(int) sampleRate, replay));
        }

        
        public void StartRecording(string filename)
        {
			Debug.Log("start recording" + filename);
            if (currentlyRecording!="")
            {
	            ForceStopRecording(currentlyRecording);
                return;
            }
            recordings.Add(filename, new List<(short, short)>());
            currentlyRecording = filename;
        }

        // Sometime the recording doesn't finish because it leaves the voice chat room before another sample rate cycle is reached. This prevents that.
        private IEnumerator RecordingFailSafe(string fileID)
        {
	        yield return new WaitForSeconds(2f);
	        if (currentlyRecording == fileID)
	        {
		        ForceStopRecording(fileID);
	        }
        }

        private void ForceStopRecording(string fileID){
            Debug.Log("force stop recording" + fileID);

		    stopRecordingAt = recordings[fileID].Count;
		    FinalizeRecording();
        }
       

        public void StopRecording() {
	       
            Debug.Log("stop recording" + currentlyRecording);
            if (stopRecordingAt != 0 || !recordings.ContainsKey(currentlyRecording))
            {
                return;
            }
			
            
            stopRecordingAt = recordings[currentlyRecording].Count + sampleRate;
            StartCoroutine(RecordingFailSafe(currentlyRecording));

        }

        
        public void ClearRecordings(){
            recordings.Clear();
        }

    
        public void StopPlaying(){
			audioBuffers.Clear();
			StopTimers();

		}

  		public IEnumerator PlayShortList(List<(short, short)> audio, int sample_rate, string name)
        {
			StopPlaying();
			Thread bufferThread = new Thread(()=>{BufferAudio(audio, sample_rate);});
			bufferThread.Start();
			string sendingRoom =  voiceManager.GetReplayRoom();
           
			if (sendingRoom == ""){
				Debug.LogError("Couldn't find replay room");
			}
				// This is obviously a race condition, but it should be fine.
            yield return new WaitForSeconds(0.2f);
				Debug.Log("start replay");
			Debug.Log(audioBuffers.Count);

			
			var tmr = new System.Timers.Timer();
            timers.Add(tmr);
			tmr.Elapsed += (sender, args) => PlayAudio(sendingRoom, tmr, name);
			tmr.AutoReset = true;
			tmr.Interval = 10;
			tmr.Start();
            
		}

		public void StopTimers(){
            foreach(var timer in timers){
                timer.Stop();
                timer.Dispose();
            }
            timers.Clear();
        }
				
		public void PlayAudio(string sendingRoom, System.Timers.Timer timer, string name){
						
			if (audioBuffers.Count==0){
				Debug.Log("stop replay");
                _replayUI.SetReplay(name, 0, false);
                timer.Stop();

                StopTimers();
				return;
			}
			var buffer = audioBuffers[0];
			if (playUnfiltered)
			{
				voiceManager.InjectAudio(buffer.Item1, sendingRoom);
			}
			else
			{
				voiceManager.InjectAudio(buffer.Item2, sendingRoom);
			}
			audioBuffers.Remove(buffer);
		}

		public void BufferAudio(List<(short, short)> audio, int sample_rate){
			audioBuffers.Clear();
			var sample_length = sample_rate/100;
			for (int i = 0; i < audio.Count/sample_length; i++){
				var newBuffer1 = new AudioBuffer(){
					Frames = new short[sample_length],
					SampleRate = (uint) sample_rate,
					Channels = 1
				};
				var newBuffer2 = new AudioBuffer(){
					Frames = new short[sample_length],
					SampleRate = (uint) sample_rate,
					Channels = 1
				};
				for (int b = 0; b < sample_length; b++){
					newBuffer1.Frames[b] = audio[i*sample_length+b].Item1;
					newBuffer2.Frames[b] = audio[i*sample_length+b].Item2;

				}
				
				audioBuffers.Add((newBuffer1, newBuffer2));
			}
		}

    }
}