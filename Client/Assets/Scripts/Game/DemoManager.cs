using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DemoManager : MonoBehaviour
{
    [SerializeField] private Tooltip tooltip;
    [SerializeField] private List<CanvasGroup> demoFeatures;
    [SerializeField] private List<Image> logos;
    [SerializeField] private Sprite regular;
    [SerializeField] private Sprite deluxe;
    

    // Start is called before the first frame update
    public void Init()
    {
    Debug.Log("DLC Installed: "+Steamworks.SteamApps.IsDlcInstalled(2231240));
        if (!Steamworks.SteamApps.IsDlcInstalled(2231240))
        {
            foreach (var group in demoFeatures)
            {
                group.alpha = 0.6f;
                group.interactable = false;
                var hasTooltip = group.gameObject.AddComponent<HasTooltip>();
                hasTooltip.tooltipScript = tooltip;
                hasTooltip.tooltipText = "Available in deluxe version";
                
            }

            foreach (var logo in logos)
            {
                logo.sprite = regular;
            }

        } else{
            foreach (var group in demoFeatures)
            {
                group.alpha = 1f;
                group.interactable = true;
            }
            foreach (var logo in logos)
            {
                logo.sprite = deluxe;
            }
        }
     
         
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
