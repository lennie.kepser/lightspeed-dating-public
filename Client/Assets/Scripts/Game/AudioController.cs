using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DefaultNamespace{
public class AudioController : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioSource whispers;
    public AudioSource lightsounds;  
    public AudioSource ticktock;  
    public AudioSource monsterSound;
    public AudioSource crowdCheer;
    public AudioSource musicRound;
    public AudioSource musicMainMenu;
    public AudioSource roundEndRiser;
    private float maxTime = 1.0f;
    private float currentTime = 0.0f;
    private float volumeMultiplier = 1.0f;
    private float musicMultiplier = 1.0f;

    public Dictionary<AudioSource, float> audioLevels;
    public Dictionary<AudioSource, float> musicLevels;
    void Start()
    {
        audioLevels = new Dictionary<AudioSource, float>{
            {whispers, whispers.volume},
            {lightsounds, lightsounds.volume},
            {ticktock, ticktock.volume},
            {monsterSound, monsterSound.volume},
            {crowdCheer, crowdCheer.volume}
        };
        musicLevels = new Dictionary<AudioSource, float>{

            {musicRound, musicRound.volume},
            {musicMainMenu, musicRound.volume},
            {roundEndRiser, musicRound.volume},
        };
    }

     

    public void SetVolumeMultiplier(float volume){
        volumeMultiplier = volume;
        UpdateVolumes();
    }

     public void SetMusicMultiplier(float volume){
        musicMultiplier = volume;
        UpdateVolumesMusic();
    }

    public void UpdateVolumes(){
        foreach(KeyValuePair<AudioSource, float> entry in audioLevels){
            entry.Key.volume = entry.Value*volumeMultiplier;
        }
    }

    public void UpdateVolumesMusic(){
        foreach(KeyValuePair<AudioSource, float> entry in musicLevels){
            entry.Key.volume = entry.Value*musicMultiplier;
        }
    }

    public void SetVolume(AudioSource source, float newVolume){
        source.volume = volumeMultiplier* newVolume;
        audioLevels[source] = newVolume;
    }

    public void SetVolumeMusic(AudioSource source, float newVolume){
        source.volume = musicMultiplier* newVolume;
        musicLevels[source] = newVolume;
    }

    public void StartWhispers(){
        whispers.Play();
        SetVolume(whispers,0.2f);
    }

    public void PlayEndRiser(){
        roundEndRiser.Play();
    }

    public void LowerMainMenuMusic(){
        SetVolumeMusic(musicMainMenu, 0.1f);
    }

    public bool EndRiserIsPlaying(){
        return roundEndRiser.isPlaying;
    }

    public void StopEndRiser(){
        roundEndRiser.Stop();
    }

    public void PlayMainMenuMusic(){
        if (!musicMainMenu.isPlaying)
        {
            musicMainMenu.Play();
        }
    }

    public void StopMainMenuMusic(){
        musicMainMenu.Pause();
    }

    public void CrowdCheerTestSound(){
        crowdCheer.Stop();
        crowdCheer.Play();
    }

    public void PlayMonsterSound(){
        monsterSound.Play();
        whispers.Stop();
        
    }

    public void PlayCrowdCheer(){
        crowdCheer.Play();
    }

    public void StopCrowdCheer(){
        crowdCheer.Stop();
    }


    public void StopWhispers(){
        whispers.Stop();
    }

    public void IncreaseWhisperVolume(float volume){
        SetVolume(whispers,audioLevels[whispers]+volume);
    }

    public void InRoomSounds(float time){
        maxTime = time;
        currentTime = time;
        whispers.Play();
        ticktock.Play();
        SetVolume(ticktock,0.2f);
        musicRound.Play();

    }

    public void StopRoomSounds(){
        ticktock.Stop();
        musicRound.Stop();
        StopEndRiser();
        currentTime = -999f;
    }

    public void LightSounds(){
        lightsounds.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentTime > 0)
        {
            currentTime -= Time.deltaTime;
            SetVolume(ticktock,0.4f+(currentTime/maxTime*0.5f));
        }else if(currentTime == -999f){
            StopRoomSounds();
        }else{
            currentTime = -999f;
        }
    }
}
}