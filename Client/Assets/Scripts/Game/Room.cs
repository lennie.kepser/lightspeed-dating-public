﻿using JsonStuff;
using System.Collections.Generic;

namespace DefaultNamespace
{
    public struct Room
    {
        public List<Player> players;
        public string voiceid;
        public string query;
    }
}