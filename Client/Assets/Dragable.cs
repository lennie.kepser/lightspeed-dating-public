using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;



namespace DefaultNamespace{
[RequireComponent(typeof(Slider))]
public class Dragable : MonoBehaviour, IPointerUpHandler
{
    public Replay daddy;

    public float timeout = 0;
    // Start is called before the first frame update
    public void OnPointerUp(PointerEventData data){
        if (timeout > 0){
            return;
        }
        timeout = 1f;
        this.GetComponent<Slider>().interactable = false;
        daddy.SliderEvent();
    }
    
    public void Update(){
        if (timeout == -999){
            return;
        }
        if (timeout <= 0){
            timeout = -999;
            this.GetComponent<Slider>().interactable = true;
        }
        timeout -= Time.deltaTime;

    }
}
}