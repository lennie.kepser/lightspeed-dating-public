using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DevelopmentHide : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        #if DEBUG
        gameObject.SetActive(true);
#else
                gameObject.SetActive(false);

        #endif
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
