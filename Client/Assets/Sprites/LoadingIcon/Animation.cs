using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Animation : MonoBehaviour {

	public Sprite[] sprites;
    public Text text;
	public float spritePerFrame = 0.05f;
	public bool loop = true;
	public bool destroyOnEnd = false;

	private int index = 0;

	private Image image;
	private float frame = 0;

    private int textFrame = 0;

    private string[] textFrames = {"Loading","Loading.","Loading..","Loading..."};

	void Awake() {
		image = GetComponent<Image> ();
	}

	void Update () {
        textFrame+=1;
        if (textFrame>=textFrames.Length*120){
            textFrame = 0;
        }
        text.text = textFrames[(textFrame-textFrame%120)/120];
        
		if (!loop && index == sprites.Length) return;
        
        
		frame+=Time.deltaTime;
		if (frame < spritePerFrame) return;
		image.sprite = sprites [index];
		frame = 0;
		index ++;
		if (index >= sprites.Length) {
			if (loop) index = 0;
			if (destroyOnEnd) Destroy (gameObject);
		}
        
	}
}