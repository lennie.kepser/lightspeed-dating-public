const { SlashCommandBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('ping')
		.setDescription('Replies with Pong!'),
	async execute(interaction) {
        console.log(interaction.channelId)
        console.log(interaction.user.id)
		await interaction.reply('Pong!');
	},
};