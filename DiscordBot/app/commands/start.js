const { SlashCommandBuilder, VoiceChannel } = require('discord.js');
const { REST, VoiceConnection, joinVoiceChannel } = require('@discordjs/voice');
const GatewayAdaptor = require('../GatewayAdaptor');
const { minPlayers } = require('../config.json');
const { createDiscordJSAdapter } = require('../Adapter.js')

module.exports = {
	data: new SlashCommandBuilder()
		.setName('start')
		.setDescription('Starts game!'),
	async execute(interaction, sessionManager) {
		const channel = interaction.member.voice.channel
		const textChannel = await interaction.member.guild.channels.fetch(interaction.channelId)
		if (!channel){
			await interaction.reply('Please join a voice channel to start game.');
			return
		}
		if (channel.members.size < minPlayers){
			await interaction.reply('Not enough people in voice chat.');
			return
		}
		let userIds = Array.from(channel.members.keys())
		console.log(channel.members)
		const connection = joinVoiceChannel({
			selfDeaf: false,
			selfMute: false,
			channelId: channel.id,
			guildId: channel.guild.id,
			adapterCreator: channel.guild.voiceAdapterCreator,
			debug: true
		});
		console.log(userIds)
        await interaction.reply('Starting round!')

		sessionManager.createSession(channel, textChannel, channel.guild, connection, userIds)

	},
};