var ffi = require('ffi-napi')
var ref = require('ref-napi')
const axios = require('axios');
const { timeStamp } = require('console');
var ArrayType = require('ref-array-di')(ref)

var FloatArray = ArrayType(ref.types.float)
var ShortArray = ArrayType(ref.types.int16)

const sampleRate = 48000
class VoiceWear{
    lib = ffi.Library('voicewear.dll', {
        CSharp_MODULATE_VERSION_get: ['uint',[]],
        CSharp_new_VoiceWearManager: ['pointer',[]],
        CSharp_VoiceWearManager_initialize : ['void',['pointer', 'int', 'int', 'int', 'int']],
        CSharp_VoiceWearManager_create_voice_skin: ['int', ['pointer', 'string', 'int']],
        CSharp_modulate_voice_skin_create: ['int',['int','string','pointer']],
        CSharp_modulate_voice_skin_create_authentication_message: ['int', ['pointer','string','string','int']],
        CSharp_VoiceWearManager_uninitialize: ['void',['pointer']],
        CSharp_delete_VoiceWearManager: ['void',['pointer']],
        CSharp_VoiceWearManager_finish_voice_skin_authentication: ['void', ['string', 'string']],
        CSharp_modulate_voice_skin_check_authentication_message: ['int',['pointer', 'string']],
        CSharp_VoiceWearManager_raw_convert: ['void',['pointer',FloatArray,'int', 'int']],
        CSharp_modulate_voice_skin_helper_create: ['int', ['pointer','int']],
        CSharp_modulate_voice_skin_helper_reset: ['int', ['pointer','int']],
        CSharp_modulate_voice_skin_helper_generate: ['int', ['pointer','pointer', FloatArray, FloatArray, 'int','int','pointer']],
        CSharp_modulate_build_default_parameters_struct__SWIG_0: ['pointer',[]],
        CSharp_modulate_voice_skin_reset: ['int', ['pointer']]
        })

    voiceSkins = {

    }

    constructor(){
        this.manager = this.lib.CSharp_new_VoiceWearManager()
     

    }

    async initialize(){
        this.voiceSkins[0] = ref.alloc('pointer');
        this.helper = ref.alloc('pointer')
        this.pararms = this.lib.CSharp_modulate_build_default_parameters_struct__SWIG_0();
        console.log(this.lib.CSharp_modulate_voice_skin_helper_create(this.helper, sampleRate))
        console.log(this.lib.CSharp_modulate_voice_skin_helper_reset(ref.readPointer(this.helper,0, 10000), sampleRate))

        console.log(this.lib.CSharp_modulate_voice_skin_create(1920, "REMOVED",  this.voiceSkins[0] ));
        console.log(this.lib.CSharp_modulate_voice_skin_reset(ref.readPointer(this.voiceSkins[0],0, 10000)));
        let test1 = Buffer.alloc(618);
        console.log(this.lib.CSharp_modulate_voice_skin_create_authentication_message(ref.readPointer( this.voiceSkins[0] , 0, 10000), "REMOVED", test1, 618))
        await this.authenticate(test1.readCString(), "REMOVED",  this.voiceSkins[0] )
 
    }
    

    transform(array, voiceskinindex){
        array = new Int16Array(array.buffer)
        const monoArray  = []
        for (let i = 0; i < array.length/2; i+=1)
        {
            monoArray.push(array[i*2]/32767)

        }
        
        let ma = new FloatArray(monoArray)

        this.lib.CSharp_modulate_voice_skin_helper_generate(ref.readPointer(this.voiceSkins[voiceskinindex], 0, 10000),ref.readPointer(this.helper, 0, 10000),ma, ma, ma.length, sampleRate, this.pararms)
        console.log("aaa")
        let output = new ShortArray(array.length)
        for (let i = 0; i < ma.length; i++){
            output[i*2] = Math.round(ma[i]*32767)

            output[i*2 + 1] = Math.round(ma[i]*32767)
        }
        console.log(output[200])

        return output.buffer
    }

    async authenticate(message, name, vs){
        let payload = { request_string: message };


        const res = await axios.post('REMOVED',payload)
        console.log(this.lib.CSharp_modulate_voice_skin_check_authentication_message(ref.readPointer(vs,0, 10000), res.data.signed_response))
        //this.lib.CSharp_VoiceWearManager_finish_voice_skin_authentication(name, res.data.signed_response)
        
        
    }



    unitialize(){
    }
}

module.exports = {VoiceWear}