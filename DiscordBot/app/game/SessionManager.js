import Player from "./Player.js";
import PlayerTypes from "./PlayerTypes.js";
import Session from "./Session.js"
import shuffle from "./shuffle.js";

export default class SessionManager{

    sessions = []
    client;

    VoiceWear;

    constructor(client){
        this.client = client;
    }

    playerListFromUserIds(userIds){
        let players = [];
        userIds = shuffle(userIds)
        players.push(
            new Player(userIds[0], PlayerTypes.Monster)
        )
        players.push(
            new Player(userIds[1], PlayerTypes.Decideee)
        )
        for (let i = 2; i < userIds.length; i++){
            players.push(
                new Player(
                    userIds[i], PlayerTypes.Decider
                )
            )
        }
       return players
    }

    createSession(voiceChannel, textChannel, guild, connection, userIds){
        
        const playerList = this.playerListFromUserIds(userIds);
        const newSession = new Session(
            this.client,
            voiceChannel,
            textChannel,
            guild, 
            connection,
            playerList,
            this.VoiceWear
        )
        this.sessions.push(newSession)
    }
}