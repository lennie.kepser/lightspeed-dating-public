import {  GuildManager, TextChannel } from "discord.js";
import { EndBehaviorType, NoSubscriberBehavior, createAudioPlayer, createAudioResource, entersState, getVoiceConnection, getVoiceConnections, joinVoiceChannel, StreamType, VoiceConnection, VoiceConnectionStatus } from '@discordjs/voice';
import opus from '@discordjs/opus';

import { createWriteStream } from "fs";
import { createListeningStream } from "./createListeningStream.js";
import { Readable, Transform } from "stream";

import * as prism from "prism-media"

const player = createAudioPlayer({
	behaviors: {
		noSubscriber: NoSubscriberBehavior.Play,
		//maxMissedFrames: Math.round(config.maxTransmissionGap / 20),
	},
});

class VoiceWearTransformer extends Transform{
    constructor(voiceWear, options){
        super(options)
        this.voiceWear = voiceWear;
    }
    _transform(chunk, encoding, callback){
        this.push(this.voiceWear.transform(chunk, 0))
        console.log("a")
        return callback();
    }
}

export default class Session{
    guild;
    voiceChannel;
    textChannel;
    client;
    players;
    connection;
    voiceWear;
    constructor(client, voiceChannel, textChannel, guild, connection, playerList, voiceWear){
        this.client = client
        this.guild = guild;
        this.voiceChannel = voiceChannel;
        this.textChannel = textChannel;
        this.players = playerList
        this.connection = connection
        this.voiceWear = voiceWear
        this.startGame()

    }

   

    async sendMessage(msg){
        
        await this.textChannel.send(msg)
    }

    async startGame(){
        await this.sendMessage("start game")
        this.recordRound(this.players[0].userID)
    }

 

    async recordRound(user){
        //const recStream = receiver.subscribe("111958683163893760")
        await entersState(this.connection, VoiceConnectionStatus.Ready)
        console.log(getVoiceConnections())
        console.log(this.connection.debug())
        //connection = getVoiceConnection(this.guild.id)
		const receiver = this.connection.receiver;

		const opusStream = receiver.subscribe(user, {
            end: {
                behavior: EndBehaviorType.Manual
            },
        }).pipe(
            new prism.opus.Decoder({ rate: 48000, channels: 2, frameSize: 960 })
        ).pipe(
            new VoiceWearTransformer(this.voiceWear)
        )
    
        const encoder = new opus.OpusEncoder(48000, 2);
    
        let buffers = []
        opusStream.on("data",(chunk)=>{
            buffers.push(chunk)
            //const decoded = encoder.decode(chunk);
            //buffers.push(decoded)
            //prism.opus.Decoder({})
        })

        //recStream.pipe(createWriteStream('./everyone.wav'));
        console.log(this.voiceChannel)
        this.sendMessage("record round")
        setTimeout(()=>{
            //console.log(recStream)
            this.playBackRound(buffers)
        },5000)

    }


    async playBackRound(buffers){
        this.sendMessage("playback round")
        console.log(getVoiceConnections())
        const encoder = new opus.OpusEncoder(48000, 2);
        
        this.connection.subscribe(player)
        const readable = new Readable.from(buffers)
        //.pipe(new prism.opus.Encoder({ rate: 48000, channels: 2, frameSize: 960 })) 
        const rs = createAudioResource(readable,{inputType: StreamType.Raw})
        player.play(rs)
        setTimeout(()=>{
            this.endGame()
            console.log(readable)

        },6000)
    }

    async disconnectFromVC(){
        if (this.connection){
            this.connection.destroy()     
            this.connection = undefined;
        }
    }
t 
    async endGame(){
        this.disconnectFromVC()
        await this.sendMessage("end game")

    }
}