import PlayerTypes from "./PlayerTypes.js";

export default class Player{
    userID;
    // Default
    type =  PlayerTypes.Decider

    constructor(id, type){
        this.userID = id;
        this.type = type
    }

}