# Lightspeed Dating

This is the public repo of Lightspeed Dating, a (litle?) student project social deduction indie game featuring voice modulation. All proprietary code and api keys have been expunged, so it definetly won't compile. It merely serves as an example project for the community and to show off some code. For the final result, including screenshots and videos see:

https://store.steampowered.com/app/2096680/Lightspeed_Dating

# Overview

The two important projects are the folder Client and Server. Client is the core game, built in Unity 2D. Server is a node.js based custom Server for the game. The two components communicate over websockets, facilitated through Socket.io. The node.js server uses the standard socket.io npm package, the Client uses a somewhat outdated socket.io package, which I would have not used had I known there was an alternative when I started this project. More about this in the section Socket.io.

There are also two additional side projects "DiscordBot" and "Servermanager". Both have been put on hold indefinetely, but can still be inspected for some inspiration. 
Server manager was supposed to be a custom load balancer. The core code exists (AWS and database integration), but requires some more functions to be completely operational. Discord Bot was supposed to be a mini version of LSD, playabale in a discord server and is in a similar state.

# Client

The client is pretty expansive, and to be frank, not amazingly organized. Here's a short overview of it's most important/fun features and where to find them.

## Socket.io

The connection to the server is managed through socket.io. The most important class here is the SocketConnect found in Assets/Scripts/Game/SocketConnect.cs. This class contains most of the socket logic, as well as my greates shame. When I started this project, I didn't properly think through how to handle events coming and going, since it initially wasn't supposed to be as big as it became in the end, so I made the unfortunate design decision to let SocketConnect handle all outgoing and incoming events, creating an incredibly bloated and shitty class. If I had the time for a refactor, I would have instead let the classes that actually need the events handle the logic in recieving them, and merely passed the function handle to SocketConnect, but alas I do not, so here we are.
I also used an oudated and depricated Socket.io class, more regret, but changing this would require an even bigger refactor. If I can offer some concelation, I wrote this in my second semester of Uni, I have grown since then.

## UI

The UI is actually the most expansive feature set. All of it can be found in Assets/Scripts/UI. The most important class to look at here is probably the UIRoom class and its children, since these are where the actual gameplay happens. These classes handle all the important bits of showing graphics, playing sounds, waiting for the people to join a room before its shown, etc. etc. Calling them UI is really a misnomer, but I also didn't know about MVC patterns when I started this. In any case, I am still proud of the way I use inheritance here, as there is a neat hierarchy at play allowing me to easily add new UI windows by inherting from UIWindow or UIWindowTimer, which makes it easy to ensure that only one Window is shown at a time, and gives me access to the manager "singletons" without having to use an actual singleton. The UIRoom hierarchy also ties neatly into the Room hierarchy of the Server.

## Replays

One of the neatest parts of this game is its replay feature. After each round the players can listen to the replays of the rounds together, playing for all of them at the same time. The UI part of this is handled by UIReplay and was a massive pain in the ***, as it had to be synchronized with all players. The audio part is handled by Assets/Scripts/AudioRecorder.cs. The way the replay feature works is by locally saving the audio input of each player on their individual devices. Once a replay is played, a signal is sent to both players who were in that replay room and then their saved audio buffer is sent through the EOS voice chat system. This allows me to offer a replay without saving their data on my servers (which would have a whole lot of privacy issues), or care about encryption, *or even to pay for the bandwith of the audio streams*. That's a huge advantage for a small indie dev! I couldn't be happier when it actually turned out to work! 

## Etc.

There is a whole lot of stuff in this game, it might be best to just play it or watch a video of someone playing it and then to explore. I hope you won't judge it too harshly, I focused most of my elegance on the server, as it was especially important that it ran as bug free and securely as possible. One crashed client is only one pissed of client, one crashed server pisses everyone of.

# Server

The server is much more straightforward and elegantly designed. All gameplay related classes can be found in /src/game, all socket.io classes and functionality in src/io, with a subfolder containing the io events in src/io/Events. The biggest thing I would do different is to split up the GameAction event class into smaller events in their own directory instead of the incredibly silly bloated mega class it became, but again I didnt anticipate this project becoming as big as it did.

The entry into everything is src/index.ts, which creates a SocketHandler class (src/io/SocketHandler.ts) for each socket.io connection. The SocketHandler class is essentially the API between the Client socket.io and the Server class (src/game/Server.ts) and the GameSession class (src/game/GameSession). The Server class is created for each group of players playing together. When they start a round a GameSession object is created, which handles all the gameplay stuff until the game is over and the players return to a lobby. Each round the players are put into rooms together, these are represented by the Room class hierarchy (src/game/Rooom.ts,RoomTriple.ts,RoomJudgement.ts). There are also additional game modes which are implemented through inherited classes from the GameSession class. Some gameplay functions can also be found in the Player class (src/game/Player.ts). Basically anything that affects a player and their status directly is handled through that. 
