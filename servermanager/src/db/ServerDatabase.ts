import Pool from "./Pool";
const axios = require('axios');
import shuffle from "../utility/shuffle";
import {
    DescribeAddressesResult,
    DescribeInstanceStatusResult,
    DescribePublicIpv4PoolsResult,
    StartInstancesResult
} from "aws-sdk/clients/ec2";
import {AWSError} from "aws-sdk";
var AWS = require('aws-sdk');
AWS.config.loadFromPath('./config/aws.json');
const config = require('../../config/config.json')
AWS.config.update({region: config.region});

export default class ServerDatabase{
    private ec2 = new AWS.EC2({apiVersion: '2016-11-15'});
    private params = {
        DryRun: false
    };
    private port = 3000;

    private getEC2Instances(): Promise<any>{
        return new Promise((resolve, reject)=>{
            this.ec2.describeInstances(this.params, function(err: any, data: any) {
                if (err) {
                    console.log("Error", err.stack);
                    reject(err);
                } else {
                    resolve(data.Reservations);
                }
            });
        })

    }

    private async clearDB(): Promise<boolean>{
        Pool.query('DELETE FROM servers')
            .then(()=>{
                return true;
            })
            .catch((e: any)=>{
                return false;
            })
        return false;
    }

    public async updateDBFromAWS(){
        try{
            const EC2Instances = await this.getEC2Instances();
            for (let i = 0; i < EC2Instances.length; i++){
            const instance =  EC2Instances[i].Instances[0];
            let lsdServer = false
            for (let tag of instance.Tags){
                if (tag.Key==='lsd-version' && tag.Value==='1.0'){
                    lsdServer = true;
                }
            }
            if (!lsdServer){
                continue;
            }

            const res = await Pool.query('SELECT * FROM servers WHERE instance_id = $1',[instance.InstanceId]);
            if (res.rows.length>0){
                await Pool.query('UPDATE servers SET turned_on = $1, ip = $3 WHERE instance_id = $2', [(instance.State.Name=='running'),instance.InstanceId, instance.PublicIpAddress])
            }else{
                await Pool.query('INSERT INTO servers(ip, turned_on, instance_id, region) VALUES($1, $2, $3, $4)',[instance.PublicIpAddress,(instance.State.Name=='running'), instance.InstanceId, config.region])
            }
        }
        }catch(e){
            console.error(e);
        }
    }

    public async checkIfInstanceIsRunning(instanceID: string): Promise<boolean>{
        return new Promise((resolve, reject)=>{
            this.ec2.describeInstanceStatus({InstanceIds:[instanceID]},(err:AWSError, data:DescribeInstanceStatusResult)=>{
                if (err) {
                    reject(err);
                }else if(!data.InstanceStatuses || data.InstanceStatuses.length === 0 || !data.InstanceStatuses[0].InstanceState){
                    resolve(false)
                }else{
                    if (data.InstanceStatuses![0].InstanceState!.Name==="running"){
                        resolve(true);
                    }else{
                        resolve(false);
                    }
                }
            });
        })

    }

    public async getServerIPWithSpace(): Promise<string>{
        let output = "";
        try{
            const res = await Pool.query('SELECT ip FROM servers WHERE turned_on = True');
            if (res.rows.length === 0){
                return output;
            }
            const shuffledRows = shuffle(res.rows);
            for (let i = 0; i < res.rows.length; i++){
                const row = shuffledRows[i];
                const ip = row.ip.trim();
                console.log(ip);
                const space = await this.getServerSpace(ip);
                console.log(space)
                if (space > 0){
                    return ip;
                }
            }

        }catch(e){
            Promise.reject(e);
        }
        return output;
    }
    public async launchServer(): Promise<string>{
        return new Promise((resolve, reject)=>{
            Pool.query('SELECT instance_id FROM servers WHERE turned_on = False')
                .then((res: any)=>{
                    if (res.rows.length === 0) {
                        reject(1);
                        return;
                    }
                    let instance = res.rows[0];
                    this.ec2.startInstances({InstanceIds:[instance.instance_id]}, (err:AWSError, data: StartInstancesResult)=>{
                        if (err){
                            console.error(err);
                            reject(err);
                            return;
                        }
                        setInterval(async function (ServerDB: ServerDatabase){
                            ServerDB.checkIfInstanceIsRunning(instance.instance_id)
                                .then(async (isRunning:boolean)=>{
                                    if (isRunning){
                                        ServerDB.updateDBFromAWS()
                                            .then(()=>{
                                                Pool.query('SELECT ip FROM servers WHERE instance_id = $1',[instance.instance_id])
                                                    .then((res: any)=>{
                                                        if (res.rows.length===0){
                                                            reject(500);
                                                        }
                                                        resolve(res.rows[0].ip)
                                                    })
                                                    .catch((e: any)=> {
                                                        reject(e);
                                                    })


                                            })
                                            .catch((e: any)=>{
                                                reject(e);
                                            });

                                    }
                                })
                                .catch((e: any)=>{
                                    if (e!=="Not found"){
                                        reject(e);
                                    }
                                });
                        },2000, this)
                    })
                })
                .catch((e: any)=>{
                    console.error(e);
                    reject(e);
                })

        })
    }
    public async getServerSpace(ip: string): Promise<number>{
        return new Promise((resolve, reject) => {
            axios({
                method: "GET",
                url: "http://"+ip+":"+this.port+'/get-space',
            })
                .then((res: any)=>{
                    resolve(res.data*1);
                })
                .catch((e: any)=>{
                    reject(e);
                })
        })
    }
}