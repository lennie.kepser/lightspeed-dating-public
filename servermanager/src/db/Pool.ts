const { Pool } = require('pg')

export default new Pool({
    user: 'postgres',
    host: '127.0.0.1',
    database: 'postgres',
    password: 'REMOVED',
    port: 5432,
})