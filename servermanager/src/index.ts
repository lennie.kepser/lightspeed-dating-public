import ServerDatabase from "./db/ServerDatabase";

const app = require('express')();
const http = require('http').createServer(app);
const cors = require('cors');

app.use(cors());
const serverDB = new ServerDatabase();
serverDB.updateDBFromAWS();
serverDB.checkIfInstanceIsRunning('i-0370d9ad00f8c2675');

app.get('/create-room', async function(req: any, res: any) {
    let foundip = "";
    await serverDB.getServerIPWithSpace()
        .then((ip)=>{
            foundip = ip
        })
        .catch((e)=>{
            res.sendStatus(500);
            console.error(e.stack);
        })
    if (foundip != ""){
        res.send(foundip);
        return;
    }
    await serverDB.launchServer()
        .then((ip)=>{
            foundip = ip;
            res.send(ip);

        })
        .catch((e)=>{
            if (e===1) {
                res.send("Error: All servers full");
            }else{
                console.error(e.stack)
                res.sendStatus(500);
                return;
            }
        })

});

app.get('/join-room',(req: any, res: any) => {

})

http.listen(5000, () => {
    console.log('listening on *:5000');
});
