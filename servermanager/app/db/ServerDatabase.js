"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Pool_1 = __importDefault(require("./Pool"));
var axios = require('axios');
var shuffle_1 = __importDefault(require("../utility/shuffle"));
var AWS = require('aws-sdk');
AWS.config.loadFromPath('./config/aws.json');
var config = require('../../config/config.json');
AWS.config.update({ region: config.region });
var ServerDatabase = /** @class */ (function () {
    function ServerDatabase() {
        this.ec2 = new AWS.EC2({ apiVersion: '2016-11-15' });
        this.params = {
            DryRun: false
        };
        this.port = 3000;
    }
    ServerDatabase.prototype.getEC2Instances = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.ec2.describeInstances(_this.params, function (err, data) {
                if (err) {
                    console.log("Error", err.stack);
                    reject(err);
                }
                else {
                    resolve(data.Reservations);
                }
            });
        });
    };
    ServerDatabase.prototype.clearDB = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                Pool_1.default.query('DELETE FROM servers')
                    .then(function () {
                    return true;
                })
                    .catch(function (e) {
                    return false;
                });
                return [2 /*return*/, false];
            });
        });
    };
    ServerDatabase.prototype.updateDBFromAWS = function () {
        return __awaiter(this, void 0, void 0, function () {
            var EC2Instances, i, instance, lsdServer, _i, _a, tag, res, e_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 9, , 10]);
                        return [4 /*yield*/, this.getEC2Instances()];
                    case 1:
                        EC2Instances = _b.sent();
                        i = 0;
                        _b.label = 2;
                    case 2:
                        if (!(i < EC2Instances.length)) return [3 /*break*/, 8];
                        instance = EC2Instances[i].Instances[0];
                        lsdServer = false;
                        for (_i = 0, _a = instance.Tags; _i < _a.length; _i++) {
                            tag = _a[_i];
                            if (tag.Key === 'lsd-version' && tag.Value === '1.0') {
                                lsdServer = true;
                            }
                        }
                        if (!lsdServer) {
                            return [3 /*break*/, 7];
                        }
                        return [4 /*yield*/, Pool_1.default.query('SELECT * FROM servers WHERE instance_id = $1', [instance.InstanceId])];
                    case 3:
                        res = _b.sent();
                        if (!(res.rows.length > 0)) return [3 /*break*/, 5];
                        return [4 /*yield*/, Pool_1.default.query('UPDATE servers SET turned_on = $1, ip = $3 WHERE instance_id = $2', [(instance.State.Name == 'running'), instance.InstanceId, instance.PublicIpAddress])];
                    case 4:
                        _b.sent();
                        return [3 /*break*/, 7];
                    case 5: return [4 /*yield*/, Pool_1.default.query('INSERT INTO servers(ip, turned_on, instance_id, region) VALUES($1, $2, $3, $4)', [instance.PublicIpAddress, (instance.State.Name == 'running'), instance.InstanceId, config.region])];
                    case 6:
                        _b.sent();
                        _b.label = 7;
                    case 7:
                        i++;
                        return [3 /*break*/, 2];
                    case 8: return [3 /*break*/, 10];
                    case 9:
                        e_1 = _b.sent();
                        console.error(e_1);
                        return [3 /*break*/, 10];
                    case 10: return [2 /*return*/];
                }
            });
        });
    };
    ServerDatabase.prototype.checkIfInstanceIsRunning = function (instanceID) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        _this.ec2.describeInstanceStatus({ InstanceIds: [instanceID] }, function (err, data) {
                            if (err) {
                                reject(err);
                            }
                            else if (!data.InstanceStatuses || data.InstanceStatuses.length === 0 || !data.InstanceStatuses[0].InstanceState) {
                                resolve(false);
                            }
                            else {
                                if (data.InstanceStatuses[0].InstanceState.Name === "running") {
                                    resolve(true);
                                }
                                else {
                                    resolve(false);
                                }
                            }
                        });
                    })];
            });
        });
    };
    ServerDatabase.prototype.getServerIPWithSpace = function () {
        return __awaiter(this, void 0, void 0, function () {
            var output, res, shuffledRows, i, row, ip, space, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        output = "";
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 7, , 8]);
                        return [4 /*yield*/, Pool_1.default.query('SELECT ip FROM servers WHERE turned_on = True')];
                    case 2:
                        res = _a.sent();
                        if (res.rows.length === 0) {
                            return [2 /*return*/, output];
                        }
                        shuffledRows = shuffle_1.default(res.rows);
                        i = 0;
                        _a.label = 3;
                    case 3:
                        if (!(i < res.rows.length)) return [3 /*break*/, 6];
                        row = shuffledRows[i];
                        ip = row.ip.trim();
                        console.log(ip);
                        return [4 /*yield*/, this.getServerSpace(ip)];
                    case 4:
                        space = _a.sent();
                        console.log(space);
                        if (space > 0) {
                            return [2 /*return*/, ip];
                        }
                        _a.label = 5;
                    case 5:
                        i++;
                        return [3 /*break*/, 3];
                    case 6: return [3 /*break*/, 8];
                    case 7:
                        e_2 = _a.sent();
                        Promise.reject(e_2);
                        return [3 /*break*/, 8];
                    case 8: return [2 /*return*/, output];
                }
            });
        });
    };
    ServerDatabase.prototype.launchServer = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        Pool_1.default.query('SELECT instance_id FROM servers WHERE turned_on = False')
                            .then(function (res) {
                            if (res.rows.length === 0) {
                                reject(1);
                                return;
                            }
                            var instance = res.rows[0];
                            _this.ec2.startInstances({ InstanceIds: [instance.instance_id] }, function (err, data) {
                                if (err) {
                                    console.error(err);
                                    reject(err);
                                    return;
                                }
                                setInterval(function (ServerDB) {
                                    return __awaiter(this, void 0, void 0, function () {
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            ServerDB.checkIfInstanceIsRunning(instance.instance_id)
                                                .then(function (isRunning) { return __awaiter(_this, void 0, void 0, function () {
                                                return __generator(this, function (_a) {
                                                    if (isRunning) {
                                                        ServerDB.updateDBFromAWS()
                                                            .then(function () {
                                                            Pool_1.default.query('SELECT ip FROM servers WHERE instance_id = $1', [instance.instance_id])
                                                                .then(function (res) {
                                                                if (res.rows.length === 0) {
                                                                    reject(500);
                                                                }
                                                                resolve(res.rows[0].ip);
                                                            })
                                                                .catch(function (e) {
                                                                reject(e);
                                                            });
                                                        })
                                                            .catch(function (e) {
                                                            reject(e);
                                                        });
                                                    }
                                                    return [2 /*return*/];
                                                });
                                            }); })
                                                .catch(function (e) {
                                                if (e !== "Not found") {
                                                    reject(e);
                                                }
                                            });
                                            return [2 /*return*/];
                                        });
                                    });
                                }, 2000, _this);
                            });
                        })
                            .catch(function (e) {
                            console.error(e);
                            reject(e);
                        });
                    })];
            });
        });
    };
    ServerDatabase.prototype.getServerSpace = function (ip) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        axios({
                            method: "GET",
                            url: "http://" + ip + ":" + _this.port + '/get-space',
                        })
                            .then(function (res) {
                            resolve(res.data * 1);
                        })
                            .catch(function (e) {
                            reject(e);
                        });
                    })];
            });
        });
    };
    return ServerDatabase;
}());
exports.default = ServerDatabase;
