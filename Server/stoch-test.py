from random import random

stoch_array = []
amnt_turns = 10
amnt_players = 6
monsters = []
amnt_chosen = {}
import math
import random

import matplotlib.pyplot as plt

for b in range(amnt_players):
    amnt_chosen[b] = [0]

stabilisation_factor = 0.5

for i in range(amnt_turns):
    for b in range(amnt_players):
        for c in range(math.ceil(stoch_array.count(b)*stabilisation_factor+1)):
            stoch_array.append(b)
        amnt_chosen[b].append(amnt_chosen[b][-1])
    monster = random.choice(stoch_array)
    #monster = math.floor(random.random()*amnt_players)
    monsters.append(monster)
    for b in range(math.ceil(stoch_array.count(monster)*1)):
        stoch_array.remove(monster)

    #stoch_array = list(filter(lambda a: a != monster,stoch_array))
    amnt_chosen[monster][-1] +=1
counter = 0
for i in amnt_chosen.values():
    plt.plot(i,label=counter)
    counter+=1
plt.legend()
plt.show()