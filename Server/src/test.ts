import {fork} from "child_process";
const request = require('request');


let amntplayers = +process.argv[3];


class Tester{
    forks: Array<any> = [];
    amnttests = +process.argv[2];
    roundTest(){
        for (let i = 0; i < this.forks.length; i++){
            this.forks[i].send("start");
        }
    }

    joinServer(serverid: string){
        for (let i = 0; i < this.forks.length; i++){
            this.forks[i].send("join_"+serverid);
        }
    }

    test(players: number){

        let joined = 0;
        let ended = 0;
        for (let i = 0; i < players; i++){
            this.forks[i] = fork('./app/test-client.js',[(players-i-1).toString()]);

            this.forks[i].on("message",(msg:any)=>{
                if (msg.msg === "joined"){
                    joined++;
                    if (msg.serverid === ""){
                        if (joined===players){
                            this.roundTest();
                        }
                    }else{
                        this.joinServer(msg.serverid);
                    }

                }
                if (msg.msg==="game end"){
                    ended++;
                    this.forks[i].kill('SIGINT');
                    if (ended===players){
                        this.amnttests--;
                        if (this.amnttests>0){
                            this.test(amntplayers);
                        }
                    }
                }

            })

        }

    }
}



request("http://localhost:3000/",{},(err: any, res: any, body: any)=>{
    if (err){
        throw "Server not running";
    }
    if (body!=='<h1>Why are you here</h1>'){
        console.log(body);
        throw "Wrong server running? idk this is weird."
    }

    let amntservers = 0;
    setInterval(()=>{
        if (amntservers>+process.argv[4]){
            return;
        }
        amntservers++;
        console.log("Servers: "+amntservers+" Players: "+(amntservers*amntplayers));

        const tester = new Tester();
        tester.test(amntplayers);

    },2000)

})