import createId from "./game/functions/createID";

const io = require('socket.io-client');
const socket = io("ws://localhost:3000");

let username = "";
let players = [];
let randomtesting = false;
let serverid = "";


function randomFloat(): number{
    return Math.random()*100;
}

function randomInt(): number{
    return Math.floor(randomFloat())
}

function randomString(): string{
    return createId(randomInt())
}

function randomObject(depth = 0): any{
    if (depth>5 || Math.random()>0.8){
        return {};
    }
    let output: any = {}
    for (let i = 0; i < randomInt(); i++){
        output[randomString()] = giveRandom(depth+1);
    }
    return output;
}

function randomArray(depth = 0): any{
    if (depth>5 || Math.random()>0.8){
        return [];
    }
    let output: any = []
    for (let i = 0; i < randomInt(); i++){
        output.push(giveRandom(depth+1));
    }
    return output;
}

function randomEtc(){
    const rand = Math.random();
    if (rand > 0.8)
        return undefined;
    if (rand > 0.6)
        return null;
    return 0;
}

function giveRandom(depth = 0): any{
    let randomfunctions = [randomFloat,randomInt,randomString,randomObject,randomEtc,randomArray];
    return randomfunctions[Math.floor(Math.random()*randomfunctions.length)](depth)
}

const socketevents = [
    {event:'unlock',data:["Lennie"]},
    {event:'join',data:["username","room"]},
]

const gameactions = [
    {name: 'start game'},
    {name: 'pick disguise'},
    {name: 'pick target'},
    {name: 'kill'},
    {name: 'spare'},

]

function doRandomSocket(){
    const event = socketevents[Math.floor(Math.random()*socketevents.length)];
    if (Math.random()>0.5){
        const data: any = randomObject();
        for (let i = 0; i < event.data.length; i++){
            data[event.data[i]] = giveRandom();
        }
        socket.emit(event.event,data);
    }else{
        socket.emit(event.event,giveRandom());
    }
    if (Math.random()<0.01){
        socket.disconnect();
    }
}

function doRandomGameAction(){
    const gameaction = gameactions[Math.floor(Math.random()*gameactions.length)];
    socket.emit('game action',{action: gameaction.name, details: giveRandom()});

}

function crashTestDummy(){
    doRandomSocket();
    doRandomGameAction();
}

socket.on("connected",()=>{
    socket.emit("unlock",{secret:"Lennie"});
})

let serverCreater = false;

socket.on("unlock",(data:any)=>{
    username = createId(8);
    const crazyness = Math.floor(Math.random()*10000)+1000;
    if (randomtesting)
        setInterval(crashTestDummy,crazyness);
    if (process.argv[2]==="0"){
        serverCreater = true;
        socket.emit("create server",{username});
    }else{
        if (serverid!==""){
            socket.emit("join",{username,room:serverid});
        }
    }

})

socket.on("join successful",(data: any)=>{
    if (serverCreater){
        // @ts-ignore
        process.send({msg:"joined", serverid: data.serverid});

    }else{
        // @ts-ignore
        process.send({msg:"joined",serverid:""});
    }
})

process.on("message",(msg,data)=>{
    if (msg === "start"){
        setTimeout(()=>{
            socket.emit("game action",{action: "start game"});
        },100)
    }else if(msg.split("_")[0] === "join"){
        serverid = msg.split("_")[1];
        if (username){
            socket.emit("join",{username,room:serverid});
        }

    }

})

socket.on("game start info",(data: any)=>{
    players = data.gameinfo;
})

socket.on("monster char select",(data: any)=>{
    players = data.gameinfo;
    socket.emit("game action",{action: "pick disguise",details:players[Math.floor((Math.random()*players.length))].username});
})

socket.on("in room",()=>{
    if (Math.random()>0.5){
        socket.emit("game action",{action: "kill"});
    }else{
        socket.emit("game action",{action: "spare"});

    }
})

socket.on("endgame",()=>{

    // @ts-ignore
    process.send({msg:"game end"});
})

socket.on('disconnect',()=>{
    //process.exit();
})