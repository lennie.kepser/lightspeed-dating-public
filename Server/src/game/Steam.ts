const axios = require('axios')

export default class Steam{
    private static readonly api_key: string = "REMOVED"
    private static readonly app_id: number = 0
    private static readonly dlc_app_id: number = 0
    public static async authenticateUserTicket(ticket: string): Promise<string | undefined>{
        let response;
        try{
            response = await axios.get("https://partner.steam-api.com/ISteamUserAuth/AuthenticateUserTicket/v1/?"+
            "key="+Steam.api_key+"&appid="+Steam.app_id+"&ticket="+ticket,{
                headers:{
                    "Content-Type":"application/x-www-form-urlencoded"
                }
            })
            
            return response.data.response.params.steamid;
            //return JSON.parse(steamID).response.params.steamid;

        }catch(e: any){
            console.error("Error validating user");
            console.error(e)
            console.error(response.data.response.error)
            return;
        }
    }

    public static async getUserStats(){
        let response = await axios.get("https://partner.steam-api.com/ISteamUserStats/GetGlobalStatsForGame/v1/?"+
        "key="+Steam.api_key+
        "&appid="+Steam.app_id+
        "&count="+7+
        "&name[0]=VOICESKIN_USAGE_0"+
        "&name[1]=VOICECHAT_USAGE"+
        "&name[2]=HOURS_PLAYED_NEW"+
        "&name[3]=VOICESKIN_USAGE_1"+
        "&name[4]=VOICESKIN_USAGE_2"+
        "&name[5]=VOICESKIN_USAGE_3"+
        "&name[6]=HOURS_PLAYED",

        {
            headers:{
                "Content-Type":"application/x-www-form-urlencoded"
            }
        })
        console.log(response.data.response.globalstats)
    }

    public static async ownsDLC(userID: string){
        let response = await axios.get("https://partner.steam-api.com/ISteamUser/CheckAppOwnership/v2/?"+
        "key="+Steam.api_key+
        "&appid="+this.dlc_app_id+
        "&steamid="+userID)
        return response.data.appownership.ownsapp;
    }
    
}