import { basename } from "path";
import config from "../config/config";
import { GameMode } from "../types/GameMode";
import getRandomQuery from "./functions/getRandomQuery";
import shuffle from "./functions/shuffle";
import GameSession from "./GameSession";
import Player from "./Player";
import Room from "./Room";
import Server from "./Server";

export default class RoomJudgement extends Room{
    private judgement_turn: string = "B";
    public readonly roundTimeAll = Math.floor(this.server.settings.roundtime* config().judgement_room_scale)
    public readonly amount_turns = config().judgement_amount_turns;

    public readonly timePerTurn = Math.floor(this.roundTimeAll / this.amount_turns)

    public human: Player | undefined;
    public monster: Player | undefined;
    public spectatorVotes: {[id: string]: string} = {};
    private spectators: {[id: string]: Player} = {};

    
    public constructor(targetRoomDict: {[id: string]: Room}, players: Array<Player>,server: Server, gameSession: GameSession) {
        super(targetRoomDict, players, server, gameSession)
        let gameMode = gameSession.getGameMode();
        if (gameMode === GameMode.Mayhem){
            let shuffledPlayers = this.players
            this.human = shuffledPlayers[0]
            this.monster = shuffledPlayers[1]
            this.monster!.setDisguise(this.human!.getUsername())
            this.human!.setDisguise(this.human!.getUsername())
        }else{
            for (let player of players){
                if (!player.monster){
                    this.human = player;
                }else{
                    this.monster = player;
                }
            }
        }
        if (!this.human || !this.monster){
            this.server.crash("Error creating Judgement Room.");
            return;
        }
        this.spectators = gameSession.getSpectators()
        for (let key in this.spectators){
            this.spectatorVotes[key] = "";
            this.spectators[key].room = this
        }
    }

    public StartRoom(){
        this.server.sendPlayersToVCRoom(this.getVoiceChatID(), Object.values(this.spectators), true)

        this.sendToVcRoom();
        for (let i = 0; i < this.amount_turns; i++){
            this.gameSession.timers.judgement[i] = setTimeout(()=>{
                this.socketEmit();
    
            }, (i*this.timePerTurn*1000)+1)
        }
    }

    public finalizeRoom(): void {
        super.finalizeRoom();
        for (let key in this.spectators){
            this.spectators[key].room = undefined;
        }

    }

    public vote(player: Player, choice: string){
        this.spectatorVotes[player.getSocketID()] = choice;
        this.sendVotes();
    }

    public sendVotes() {
        let votesA = 0;
        let votesB = 0;
        for (let id in this.spectatorVotes) {
            if (this.spectatorVotes[id] === 'A') {
                votesA++;
            } else if (this.spectatorVotes[id] === 'B') {
                votesB++;
            }
        }
        this.server.emitToAll('judgement votes', { votesA, votesB })
    }

    public fightTimeOut(){
         // if postive player As team wins, if negative player Bs team, if 0 monsters win.
         let votes = 0;
         for (let id in this.spectatorVotes){
             if (this.spectatorVotes[id] === "A"){
                 votes++;
             }
             if (this.spectatorVotes[id] === "B"){
                 votes--;
             }                
         }
         if (votes === 0){
            this.human?.die(true, true)
         }
         if (votes > 0){
            this.players[0].die((this.players[0] === this.human), true)

         }else{
            this.players[1].die((this.players[1] === this.human), true)
         }
         
    }
   

    public getHuman(): Player{
        if (!this.human){
            this.server.crash("Tried to access judgement room human parameter before assigned")
        }
        return this.human!;
    }
    

    public socketEmit(): void {
        
        this.query = getRandomQuery(this.server.getQuestionPacks(), []);


        this.judgement_turn = (this.judgement_turn === "A" ? "B" : "A");
        let turn = this.judgement_turn;

        if (!this.human){
            this.server.crash("Judgement Room emited without human role assigned.")
            return;
        }
        
        for (let id in this.spectators){
            this.spectators[id].emit("judgement room spectator",{turn, timePerTurn: this.timePerTurn, query: this.query, name: this.human.getUsername(), character: this.human.character, roundTimeAll: this.roundTimeAll})
        }
        
        this.players[0].emit("judgement room player",{roundTimeAll: this.roundTimeAll, type:"A",timePerTurn: this.timePerTurn, turn, voiceskin: this.human.character, query: this.query, name: this.human.getUsername(), recordingID: this.voicechatid})
        this.players[1].emit("judgement room player",{roundTimeAll: this.roundTimeAll, type:"B",timePerTurn: this.timePerTurn, turn, voiceskin: this.human.character, query: this.query, name: this.human.getUsername(), recordingID: this.voicechatid})
   
    }
}