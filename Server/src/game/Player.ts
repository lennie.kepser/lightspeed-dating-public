import Server from "./Server";
import Room from "./Room";
import GameSession from "./GameSession";
import { GamePhase } from "../types/GamePhase";
import { GameMode } from "../types/GameMode";
import RoomJudgement from "./RoomJudgement";
import { type } from "os";
import config from "../config/config";
import { Socket } from "socket.io";
import Steam from "./Steam";
import SocketHandler from "../io/SocketHandler";
import { MetricConfig } from "@pm2/io/build/main/features/metrics";
import RoomTriple from "./RoomTriple";

export default class Player{

    private _gamesession: GameSession|undefined;
    private _room: Room | undefined;
    private socket: any;
    private username: string;
    private id: string;
    public readonly ip: string;
    private _dead: boolean = false;
    private _puid: string;
    private _character: number = -1;
    private _monster: boolean = false;
    private disguisedas: string = "";
    private targetplayer: string = "";
    private killedByHuman: boolean = false;
    private kevinWin: boolean = false;
    private admin: boolean;
    private server: Server;
    private availableCharacters: Array<number> = config()["characters"]
    private playerSettings: {[id: string]: boolean | number | string} = {... config()["player_default"]}

    private characterChoices = [0,1]

    public setPlayerSettings(settings: {[id: string]: boolean | number | string}) : number{
        for (let key in settings){
            if (this.playerSettings[key] === undefined){
                return 1;
            }
            if (typeof this.playerSettings[key] !== typeof settings[key]){
                return 2;
            }
    
            this.playerSettings[key] = settings[key];
        }
        
        return 0
    }

    public randomCharacter(){
        this._character = this.availableCharacters[Math.floor(Math.random()*this.availableCharacters.length)];
        console.log(this._character)
    }

    public getPlayerSetting(settingName: string): boolean | number | string{
        return this.playerSettings[settingName];
    }

    public getServerId(): string{
        return this.server.id
    }

    public setDisguise(disguise: string){
        this.disguisedas = disguise;
    }

    public setCharacterChoices(choices: Array<number>):void {
        if (choices.length > 20){

            this.characterChoices = [0,1]
            return
        }
        this.characterChoices = choices;
    }

    public getCharacterChoices(): Array<number>{
        return this.characterChoices
    }

    constructor(socket: Socket, username: string, admin: boolean, puid: string, server: Server, socketHandler: SocketHandler) {
        this.socket = socket;
        this.username = username;
        this.admin = admin;
        this.id = socket.id;
        this.server = server;
        this._puid = puid;
        const splitIP = socket.handshake.address.split(":");
        this.ip = splitIP[splitIP.length-1];
    }

    public async initializeCharactersAndDLCOwned(socketHandler: SocketHandler){
        const ownsDlc = await Steam.ownsDLC(socketHandler.steamID);
        if (!ownsDlc){
            this.availableCharacters = [0,1]
        }
    }

    public getTargetPlayer(): string{
        return this.targetplayer;
    }

    public getVoiceSkin(): number{
        if (this.monster){
            const disguise = this.server.getPlayerFromUsername(this.disguisedas);
            if (!disguise){
                this.server.crash("Tried to get voiceskin from non-existant player")
                return -1;
            }
            return disguise.character;
        }else{
            return this.character;
        }
    }

    public setSkip(){
        this.targetplayer = "!skip"
    }

    public setTargetPlayerSafe(target: string): number{

        if (!this.monster){
            return 1 //Humans can't pick next player
        }
        if (!this.gamesession||!this.gamesession.isPhase(GamePhase.CharacterSelect)){
            return 2 // Not in right gamephase.
        }
        if (target==="!skip"){
            this.targetplayer = "!skip";
            this.gamesession.checkFightStart();
            return 0;
        }
        let player = this.server.getPlayerFromUsername(target);
        if (!player){
            return 3; // User not found.
        }
        if (player.dead || player.monster){
            return 4; // Invalid player selected.
        }
        this.targetplayer = target;
        this.gamesession.checkFightStart();
        return 0;
    }

    public setTargetPlayerUnsafe(target: string){
        this.targetplayer = target;
    }

    public emit(event: string, data: any) {
        this.socket.emit(event,data);
    }

    public notDisguised(): boolean{
        return (this.disguisedas === "");
    }


    //Checks whether a monsterplayer has finished the pick character phase
    public hasPicked(): boolean{
        if (this.notDisguised()){
            return false;
        }
        if (this.targetplayer===""){
            return false;
        }
        return true;
    }

    public die(killedbymonster:boolean, kevinWin: boolean){
        this.dead = true;
        this.kevinWin = kevinWin;
        this.killedByHuman = !killedbymonster;
        //This is a little convoluted, but it makes implementing player deaths less bug prone
        this.server.gamesession!.makeSpectator(this);
    }

    public getDisplayName(){
        if (this.monster){
            return this.disguisedas;
        }else{
            return this.username;
        }
    }

    public getDisplayNamePlusIdentifier(): string{
        let output = this.getDisplayName();
        if (this.room! instanceof RoomTriple){
            output += " "+this.room.getIdentifier(this);
        }
        return output;
    }

    public kevinWinCondition(): boolean{
        return this.dead && this.kevinWin;
    }

    public getPublicInfo(): {username: string, admin: boolean, character: number, monster: boolean, dead: boolean, disguise: string, puid: string, spectating: any}{
        return {
            username: this.username,
            admin: this.admin,
            character: this._character,
            monster: this._monster,
            dead: this._dead,
            disguise: this.disguisedas,
            puid: this._puid,
            spectating: this.getPlayerSetting("spectating")
        }
    }

    public getPrintInfo(): string {
        return this.username+": Character: "+this.character+", Monster: "+this.monster+", Dead: "+this.dead+", Disguise: "+this.disguisedas +", Spectating: "+this.getPlayerSetting("spectating");
    }


    public castJudgementVote(choice: string): number{
        if (!this.dead){
            return 2;
        }
        if (!(choice === "A" || choice === "B")){
            return 3;
        }
        if (!this.room || !(this.room instanceof RoomJudgement)){
            return 4;
        }
        this.room.vote(this,choice)
        return 0;
    }

    public pickDisguise(username: string): number{
        if (!this._gamesession||!this._gamesession.isPhase(GamePhase.CharacterSelect)){
            return 2; // Not in right phase;
        }
        if (!this.monster){
            return 1; // Player is not a monster;
        }
        if (username == this.username && this._gamesession.getGameMode() !== GameMode.Mayhem ){
            return 3; // Can't disguise as self.
        }
        
        this.disguisedas = username;
        this._gamesession.checkFightStart();
        return 0;
    }

    public isSkipping(): boolean {
        return this.targetplayer === "!skip";
    }


    public getUsername(): string{
        return this.username;
    }

    public getAdmin(): boolean{
        return this.admin;
    }

    public setAdmin(admin: boolean): void{
        this.admin = admin;
    }

    public getSocketID(){
        return this.socket.id;
    }

    get dead(): boolean {
        return this._dead;
    }


    set dead(value: boolean) {
        this._dead = value;
    }
    set character(value: number) {
        this._character = value;
    }
    get character(): number {
        return this._character;
    }
    get monster(): boolean {
        return this._monster;
    }
    set monster(value: boolean) {
        this._monster = value;
    }
    get room(): Room | undefined {
        return this._room;
    }

    set room(value: Room | undefined) {
        this._room = value;
    }

    get gamesession(): GameSession | undefined {
        return this._gamesession;
    }

    set gamesession(value: GameSession | undefined) {
        this._gamesession = value;
    }
    get puid(): string {
        return this._puid;
    }


    public resetDisguise(){
        this.disguisedas = "";
        this.targetplayer = "";
    }
}