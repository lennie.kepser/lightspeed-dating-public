import Room from "./Room";
import Player from "./Player";
import Server from "./Server";
import assignRooms from "./functions/assignRooms";
import getSpectatorRooms from "./functions/getSpectatorRooms";


import config from "../config/config";
import { time } from "console";
import { RoomType } from "../types/RoomType";

import { GamePhase } from "../types/GamePhase";
import RoomTriple from "./RoomTriple";
import RoomJudgement from "./RoomJudgement";
import { GameMode } from "../types/GameMode";
import assignPlayersToMayhemRooms from "./functions/assignRoomsMayhem";
import shuffle from "./functions/shuffle";
import assignRoles from "./functions/assignRoles";
import getRandomQuery from "./functions/getRandomQuery";
import { runInThisContext } from "vm";


export default class GameSession{

    protected readonly server: Server;

    protected humans: {[id: string]: Player} = {};
    protected monsters: {[id: string]: Player} = {};
    protected spectators: {[id: string]: Player} = {};

    private phase: GamePhase = GamePhase.Pregame;
    private rooms: {[id: string]:Room} = {};
    private roomAssignmentFailureCounter = 0;

    public timers: {[name: string]:any} = {
        endfight: 0,
        disguiseSelect: 0,
        charselect: 0,
        prejudgement: 0,
        judgement: {}
    }

    public constructor(server: Server, players: {[id: string]: Player}) {
        this.server = server;
        this.setUpPlayersAndRoles(players); 

    }

    protected setUpPlayersAndRoles(players: {[id: string]: Player}){
        assignRoles(players, this, this.server)
    }

    //TODO: Clean up
    public spare(player: Player): number{
        if (this.phase !== GamePhase.Fight){
            return 1; // Not right game phase
        }

        let room = player.room;
        if (!room){
            return 2; // Not in a room
        }

        if (room.hasSpared(player)){
            return 3; // Already spared
        }
        let otherplayer = room.getOtherPlayer(player)

        if (this.playerIsMonster(otherplayer)) {
            player.die(true, false);
            player.emit("round end", {ending: 0, user1: otherplayer.getUsername(), user2: otherplayer.getDisplayName()})
            otherplayer.emit("round end", {ending: 2, user1: player.getUsername(), user2: otherplayer.getDisplayName()});
            this.deleteRoomAndCheckEndFight(room.roomid);
            return 0;
        }


        
        if (room.sparesLeft() > 1){
            room.spare(player);
            otherplayer.emit("spared",{});
            if (config().debug){
                console.log(player.getUsername()+" spared "+otherplayer.getUsername());
            }
        }else{
            this.deleteRoomAndCheckEndFight(room.roomid);

            player.emit("round end",{ending: 3, user1: otherplayer.getUsername(), user2: ""});
            otherplayer.emit("round end",{ending: 3, user1: player.getUsername(), user2: ""});
        }


        return 0;
    }

    public playerIsMonster(player: Player){
        return player.monster

    }

    public chooseTripleRoom(player: Player, choice: number): number {
        let room = player.room;
        if (!room){
            return 2; // Not in a room
        }
        
        if (!(room instanceof RoomTriple)){
            return 4 // Not a valid triple room
        }
        if (!room.isDecider(player)){
            return 3;
        }
        const notChosenPlayer = room.getTripleChoice(Math.abs(choice-1))!;
        notChosenPlayer.die(true, true);
        const chosenPlayer = room.getTripleChoice(choice);
        
        if (this.playerIsMonster(chosenPlayer)){
            notChosenPlayer.emit("round end", {ending: 4, user1: player.getUsername(), user2: chosenPlayer.getUsername()})
            player.emit("round end", {ending: 0, user1: chosenPlayer.getUsername(), user2: chosenPlayer.getDisplayName()})
            chosenPlayer.emit("round end", {ending: 2, user1: player.getUsername(), user2: notChosenPlayer.getDisplayName()})
            player.die(true, false);
        }else{
            notChosenPlayer.emit("round end", {ending: 5, user1: player.getUsername(), user2: chosenPlayer.getUsername()})
            player.emit("round end", {ending: 6, user1: chosenPlayer.getUsername(), user2: chosenPlayer.getDisplayName()})
            chosenPlayer.emit("round end", {ending:3, user1: player.getUsername(), user2: ""})
        }
       
        
        this.deleteRoomAndCheckEndFight(room.roomid);

        return 0;
    }

    public isPlayerAllowedToKill(player: Player): boolean{
        return !player.monster && !player.dead;
    }   

    public refreshQuestion(player: Player): number{
        if (player.room === undefined){
            return 1;
        }
        const prevQuery = player.room!.getQuery();
        const newQuery = getRandomQuery(this.server.getQuestionPacks(), [prevQuery]);
        player.room!.setQuery(newQuery);
        player.room!.emitToPlayers("new query",{newQuery})
        return 0;
    }
 
    public kill(player: Player): number{
        let room = player.room;
        if (!room){
            return 2; // Not in a room
        }
        if (!this.isPlayerAllowedToKill(player)){
            return 3;
        }
        let killedPlayer = room.getOtherPlayer(player);
        killedPlayer.die(false, true);

        if (this.playerIsMonster(killedPlayer)){
            killedPlayer.emit("round end", {ending: 5, user1: player.getUsername(), user2: killedPlayer.getDisplayName()})
            player.emit("round end", {ending: 6, user1: killedPlayer.getUsername(), user2: killedPlayer.getDisplayName()})
        }else{
            player.die(false, false);
            killedPlayer.emit("round end", {ending: 1, user1: player.getUsername(), user2: killedPlayer.getDisplayName()})
            player.emit("round end", {ending: 7, user1: killedPlayer.getUsername(), user2: killedPlayer.getDisplayName()})
        }
        this.deleteRoomAndCheckEndFight(room.roomid);


        return 0;
    }

    protected checkEndFightConditions(): boolean{
        return (!this.areRoomsLeft() || this.getMonsterAmnt() === 0)
    }

    private deleteRoomAndCheckEndFight(roomid: string){
        if (config().debug){
            console.log("Deleting room "+roomid)
        }
        this.rooms[roomid].finalizeRoom();
        delete this.rooms[roomid];
        if (this.checkEndFightConditions()){
            this.endFight();
        }else{
            if (config().debug){
                this.printGameState();
            }
        }
    }

    public removePlayer(player:Player){
        if (!player.dead){
            this.endGameOther(player.getUsername()+ " has left. The game has been aborted.");
        }
        // TODO: Implement proper player leaving logic
       
    }

    protected gameStartInfo(player:Player){
        if (player.monster){
            return {gameinfo: this.server.getPlayerInfoArray()};
        }else{
            return {character:player.character, kevin: false}
        }
    }

    // Game phase events

    public startGame(){


        for (let id in this.monsters){
            this.monsters[id].emit("monster start info", this.gameStartInfo(this.monsters[id]));
        }
        for (let id in this.humans){
            this.humans[id].emit("game start info",this.gameStartInfo(this.humans[id]));

        }
        for (let id in this.spectators){
            this.spectators[id].emit("game start info",{character:-1});

        }

        this.phase = GamePhase.Pregame;
        this.printGameState();
        this.timers.charselect = setTimeout(()=>{
            this.startCharSelect();

        },config().times.pregame*1000)

        return 0;
    }

  

    public getGameMode(){
        return GameMode.Classic
    }


    public getAmountOfRooms(): number{
        return Object.keys(this.rooms).length;
    }
    
    public getAlivePlayers(): Array<Player>{
        let output = []
        for (let key in this.humans){
            output.push(this.humans[key])
        }
        for (let key in this.monsters){
            output.push(this.monsters[key])
        }
        return output;
    }


    protected prepareJudgementRoom(){
        this.phase = GamePhase.CharacterSelect
        let judgementRoom = new RoomJudgement(this.rooms, this.getAlivePlayers(),this.server, this);

        this.server.emitToAll("judgement preroom", {username: judgementRoom.getHuman().getUsername()})
        this.timers.prejudgement = setTimeout(()=>{
            this.startJudgementRoom(judgementRoom)
        },config().times.prejudgementroom*1000)
    }

    private startJudgementRoom(judgementRoom: RoomJudgement){
        this.phase = GamePhase.Judgement
        this.timers.judgement = {}

        judgementRoom.StartRoom();
       

        this.timers.judgement[judgementRoom.amount_turns] = setTimeout(()=>{
            this.endJudgementRoom(judgementRoom)
        },judgementRoom.roundTimeAll*1000)
    }


    private endJudgementRoom(judgementRoom: RoomJudgement){
        if (!this.isPhase(GamePhase.Judgement)){
            return;
        }
        this.clearAllTimers();

        judgementRoom.fightTimeOut();
        this.deleteRoomAndCheckEndFight(judgementRoom.getRoomID())
    }

    

    protected startCharSelect(){
        this.phase = GamePhase.CharacterSelect;
        let playerInfo = this.server.getPlayerInfoArray();
        for (let id in this.spectators){
            this.spectators[id].emit("charselect spectator",{})
        }
        for (let id in this.monsters){
            this.monsters[id].resetDisguise()
            this.monsters[id].emit("monster char select",{gameinfo: playerInfo, gameMode: this.getGameMode()});
        }
        for (let id in this.humans){
            this.humans[id].emit("charselect",{});
        }
        this.printGameState();
        this.timers.disguiseSelect = setTimeout(()=>{
            this.startFight();
        },config().times.disguiseselect*1000);
    }

    protected assignRandomDisguise(monster: Player){
        let playerArray = this.humans;
       
        let player = playerArray[Object.keys(playerArray)[Math.floor(Math.random()*Object.keys(playerArray).length)]];
        monster.pickDisguise(player.getUsername());
    }

    protected assignPlayersToRooms(){
        return assignRooms(this.humans, this.monsters, this.server)
        
    }

    protected startFight(){
        try{
        if (this.phase !== GamePhase.CharacterSelect){
            return;
        }
        this.clearAllTimers()
        if (config().debug){
            console.log("Start fight");
        }
        //If monster is undisguised pick random disguise:
        for (let id in this.monsters){

            if (this.monsters[id].notDisguised()){
                if (config().debug){
                    console.log(this.monsters[id].getUsername()+" has not picked a disguise. Assigning randomly:")
                }
                this.assignRandomDisguise(this.monsters[id])
            }
        }
        this.phase = GamePhase.Fight;
        this.rooms = this.assignPlayersToRooms()!;
        
        if (this.rooms === undefined){
            this.server.crash("Error assigning rooms. Server crashed. Sorry!")
            return;
        }
        if (Object.keys(this.rooms).length === 0){
            if (this.roomAssignmentFailureCounter > 3){
                this.endGameOther("Room assignment has failed three times. Aborting game.")
                return;
            }
            this.roomAssignmentFailureCounter += 1;
            this.server.emitToAll("player assignment failure",{})
            setTimeout(()=>{
                this.startCharSelect();
            },3000);
            return;
        }
        this.roomAssignmentFailureCounter = 0;
        this.printGameState();
        
        //Give dead players and certain monsters ability to spectate
        this.setUpSpectators();
        this.timers.endfight = setTimeout(()=>{
            this.endFight();
        }, this.server.settings.roundtime * 1000);
        }catch(e: any){
            console.error(this.server.id+": CRASH! Error starting fight.");
            console.error(e.stack)
            this.server.crash("An Unexpected Error occured while starting the fight. Crashing server. Sorry!");
        }
    }



    private endFight(){
        if (this.phase !== GamePhase.Fight && this.phase !== GamePhase.Judgement){
            return;
        }
        if (config().debug){
            console.log("Ending fight")
        }
        this.clearAllTimers();
        for (let id in this.rooms){
            this.rooms[id].fightTimeOut();
            this.rooms[id].finalizeRoom();
            delete this.rooms[id];
        }

        this.printGameState()

        // Randomize voices if enabled
        if (this.server.settings.randomvoices){
            this.randomizePlayerCharacters();
        }
        
        this.checkGameEndOrNextRound();
       
    }

    protected checkGameEndOrNextRound(){
        if (this.getHumansAmnt() === 1 && this.getMonsterAmnt() === 1 && Math.random() <= this.server.settings.judgementchance){
            // Skip player select
            this.prepareJudgementRoom();
        } else if (this.getHumansAmnt()>0&&this.getMonsterAmnt()>0){
            //Game continues
            this.startCharSelect();
        } else{
            this.endGame();
        }
    }

    public clearAllTimers(){
        for (let key in this.timers){
            if (key === "judgement"){
                for (let timerid in this.timers[key]){
                    clearTimeout(this.timers[key][timerid])
                }
            }else{
                clearTimeout(this.timers[key])
            }
        }
    } 

    public endGameOther(reason: string){
        this.clearAllTimers();
        Object.values(this.rooms).forEach((room: Room)=>room.finalizeRoom())
        if (config().debug){
            console.log("Round end due to "+reason);
        }
        this.server.emitToAll("endgame other",{reason})
        this.server.endGame();

    }

    protected emitWinner(){
        let humanswin = (this.getMonsterAmnt() === 0);
        if (config().debug){
            if (humanswin){
                console.log("Humans win.");
            }else{
                console.log("Monsters win.");
            }
        }
        this.server.emitToAll("endgame",{monster:!humanswin});
    }
    
    protected endGame(){
        
        Object.values(this.rooms).forEach((room: Room)=>room.finalizeRoom())
        this.emitWinner();
        this.clearAllTimers();

        this.server.endGame();
    }

    //Utility

    public checkFightStart(){
        for (let id in this.monsters){
            if (!this.monsters[id].hasPicked()){
                return;
            }
        }
        this.startFight();
    }


    protected isAllowedToSpectate(player: Player){

        return player.isSkipping()
    }

    private setUpSpectators(){
        let rooms = getSpectatorRooms(this.rooms);
        for (let id in this.spectators){
            this.spectators[id].emit("spectator rooms",{rooms});
        }
        for (let id in this.monsters){
            if (this.isAllowedToSpectate(this.monsters[id])){
                this.monsters[id].emit("spectator rooms", {rooms})
            }
        }
    }

    public getSpectators(): {[id:string]:Player}{
        return this.spectators;
    }

    private printGameState(){
        if (!config().debug){
            return;
        }
        console.log("Game Phase: "+this.phase+" Players alive: "+(this.getHumansAmnt())+" Monsters Alive: "+this.getMonsterAmnt());
        for (let id in this.humans){
            console.log(this.humans[id].getPrintInfo());
        }
        for (let id in this.monsters){
            console.log(this.monsters[id].getPrintInfo());
        }
        for (let id in this.spectators){
            console.log(this.spectators[id].getPrintInfo());
        }
        for (let id in this.rooms){
            this.rooms[id].printInfo();

        }
    }

    private randomizePlayerCharacters(){
        for (let id in this.humans){
            this.humans[id].randomCharacter();
        }
    }

    private deletePlayerFromPool(player: Player){
        for (let id in this.spectators){
            if (this.spectators[id].getUsername() === player.getUsername()){
                delete this.spectators[id];
            }
        }
        for (let id in this.monsters){
            if (this.monsters[id].getUsername() === player.getUsername()){
                delete this.monsters[id];
            }
        }
        for (let id in this.humans){
            if (this.humans[id].getUsername() === player.getUsername()){
                delete this.humans[id];
            }
        }
    }

    public makeSpectator(player: Player){
        this.deletePlayerFromPool(player);
        this.spectators[player.getSocketID()] = player;
    }

    public isSpectator(player: Player): boolean{
        return !!this.spectators[player.getSocketID()];

    }

    protected areRoomsLeft(): boolean{
        return (Object.keys(this.rooms).length!==0);
    }

    protected getMonsterAmnt(): number{
        return Object.keys(this.monsters).length;
    }

    private getHumansAmnt(): number{
        return Object.keys(this.humans).length;
    }

    public isPhase(phase: GamePhase){
        return phase == this.phase
    }

    //Getters and Setters

    public addHuman(player: Player){
        this.humans[player.getSocketID()] = player;
    }

    public addSpectator(player: Player){
        this.spectators[player.getSocketID()] = player;
    }


    public addMonster(player: Player){
        this.monsters[player.getSocketID()] = player;
    }

    public getPhase(): number{
        return this.phase;
    }
}