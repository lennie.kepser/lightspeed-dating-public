import GameSession from "./GameSession";
import Player from "./Player";

import assignRoomsMayhem from "./functions/assignRoomsMayhem";
import { GameMode } from "../types/GameMode";
import config from "../config/config";
import getCharacterFromPlayer from "./functions/assignCharacter";


export default class GameSessionMayhem extends GameSession{
    public playerIsMonster(player: Player): boolean {
        return player.getDisplayName() !== player.getUsername();
    }

    public getGameMode(): GameMode {
        return GameMode.Mayhem
    }

    public isPlayerAllowedToKill(player: Player): boolean {
        return !player.dead;
    }

    
    protected assignRandomDisguise(monster: Player){
        let playerArray = this.monsters;
       
        let player = playerArray[Object.keys(playerArray)[Math.floor(Math.random()*Object.keys(playerArray).length)]];
        monster.pickDisguise(player.getUsername());
    }

    protected assignPlayersToRooms(){
        return assignRoomsMayhem(Object.values(this.monsters), this.server)
    }

    protected isAllowedToSpectate(player: Player){
        return !player.room
    }

    protected checkGameEndOrNextRound(){
        if (this.getMonsterAmnt() === 2 && Math.random() <= this.server.settings.judgementchance){
            this.prepareJudgementRoom();
        } else if(this.getMonsterAmnt() > 1){
            this.startCharSelect();
        } else {
            this.endGame();
        }
    }

    public checkFightStart(){
        for (let id in this.monsters){
            if (this.monsters[id].notDisguised()){
                return;
            }
        }
        this.startFight();
    }

    protected emitWinner(){
        let winningPlayer;
        for (let id in this.monsters){
            winningPlayer = this.monsters[id].getUsername();
        }
        if (!winningPlayer){
            winningPlayer = "No one"
        }
        if (config().debug){
            console.log(winningPlayer+" won!")
        }
        this.server.emitToAll("endgame mayhem",{winner: winningPlayer})
     

    }

    protected setUpPlayersAndRoles(players: {[id: string]: Player}){

        for (let id in players){
            players[id].monster = true;
            players[id].dead = false;
            players[id].gamesession = this;
            players[id].character = getCharacterFromPlayer(players[id])
    
            this.addMonster(players[id]);
        }
        
    }
}