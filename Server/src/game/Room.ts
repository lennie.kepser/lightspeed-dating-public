import Player from "../game/Player";
import {Participant} from "../types/Participant";
import Server from "./Server";
import {RoomType} from "../types/RoomType";
import getRandomQuery from "./functions/getRandomQuery";
import createId from "./functions/createID";
import GameSession from "./GameSession";
import config from "../config/config";


export default class Room {


    public readonly roomid: string;

    protected players: Array<Player>;
    protected voicechatid: string;
    protected spared: { [username: string]: boolean } = {};
    protected query: string
    protected server: Server;
    protected gameSession: GameSession;

    public constructor(targetRoomDict: {[id: string]: Room}, players: Array<Player>,server: Server, gameSession: GameSession) {
       
        this.query = getRandomQuery(server.getQuestionPacks(), []);
        this.players = players;
        this.server = server;
        this.gameSession = gameSession;

        this.voicechatid = this.getNewUniquieRoomVCID(server)
        this.roomid = this.putInRoomObjectAndReturnID(targetRoomDict)
        for (let i = 0; i < players.length; i++) {
            this.spared[players[i].getUsername()] = false;
        }

        
        players.forEach((player: Player)=>{
            player.room = this;
        })

        server.addLastRound(this.voicechatid, this.query,players);

    }

    public getRoomID(): string{
        return this.roomid;
    }

    private putInRoomObjectAndReturnID(roomDict: {[id: string]: Room}){
        let keysOfRoomDictAsNumb = Object.keys(roomDict).map((e)=>parseInt(e));

        let roomid;
        if (keysOfRoomDictAsNumb.length === 0){
            roomid = "0"
        }else{
            roomid = (Math.max(...keysOfRoomDictAsNumb) + 1).toString();
        }
        
        roomDict[roomid] = this;
        return roomid;
    }

    private getNewUniquieRoomVCID(server: Server): string{
        let outputID = "";
        let iDisUnique = false;

        while (!iDisUnique){
            outputID = createId(6);
            iDisUnique = server.isUniqueRoundName(outputID);
        }
        return outputID;
    }

    public hasSpared(player: Player): boolean {
        return this.spared[player.getUsername()];
    }

    public spare(player: Player) {
        this.spared[player.getUsername()] = true;
    }

 
    public sparesLeft(){
        let output = 0;
        for (let name in this.spared){
            if (!this.spared[name]){
                output++;
            }
        }
        return output;
    }

    public finalizeRoom(){
        this.server.setPostRoundStatus(this.getPlayers(), this.getVoiceChatID());
        this.players.forEach(element => {
            element.room = undefined;
        });
    }

    public fightTimeOut(){
  
        if (this.server.gamesession!.playerIsMonster(this.players[0])){
            this.players[1].die(true, false);
        }else if (this.server.gamesession!.playerIsMonster(this.players[1])){
            this.players[0].die(true, false);
        }

    }

    public getInfoObj(){
        return {players:this.players.map((player: Player)=>{
            return player.getPublicInfo()
            }),voicechat:this.voicechatid,query:this.query}
    }

    public sendToVcRoom(){
        this.server.sendPlayersToVCRoom(this.voicechatid, this.players)
            .then(r => {

        })
        .catch((e: any)=>{
            this.server.crash("Error sending players to voice chat room. Aborting!");
        });
    }

    public socketEmit(){
        this.sendToVcRoom();

        this.players[0].emit("in room",
            {
                myskin: this.players[0].getDisplayName(),
                otherplayer: this.players[1].getDisplayName(),
                recording: this.voicechatid,
                time: config().times.fight,
                query: this.query,
                voiceskin: this.players[0].getVoiceSkin(),
                otherPlayerSkin: this.players[1].getVoiceSkin(),
                gameMode: this.server.gamesession!.getGameMode()
            })
        this.players[1].emit("in room",
            {
                myskin: this.players[1].getDisplayName(),
                otherplayer: this.players[0].getDisplayName(),
                recording: this.voicechatid,
                time: config().times.fight,
                query: this.query,
                voiceskin: this.players[1].getVoiceSkin(),
                otherPlayerSkin: this.players[0].getVoiceSkin(),
                gameMode: this.server.gamesession!.getGameMode()

            })


    }

    public getOtherPlayer(player: Player): Player{
        let otherplayer = this.players[0];
        if (otherplayer.getUsername() === player.getUsername()){
            otherplayer = this.players[1];
        }
        return otherplayer;
    }

    public pickOtherPlayer(player: Player): Player{
        if (this.players[0].getUsername() === player.getUsername()){
            return this.players[1];
        }
        return this.players[0];

    }

    public emitToPlayers(event: string, data: any){

        for (let player of this.players){
            player.emit(event, data);
        }
    }

    public printInfo(){
        if (!config().debug){
            return
        }
        console.log("Room "+this.roomid+": ")
        this.players.forEach(( player)=>{
            console.log(player.getPrintInfo());
        })

    }


    public getVoiceChatID(): string{
        return this.voicechatid;
    }

    public getQuery(): string{
        return this.query;
    }

    public setQuery(query: string){
        this.query = query;
    }

    public getPlayers(): Array<Player>{
        return this.players;
    }
}