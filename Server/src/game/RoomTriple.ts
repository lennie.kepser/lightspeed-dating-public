import { basename } from "path";
import config from "../config/config";
import shuffle from "./functions/shuffle";
import GameSession from "./GameSession";
import Player from "./Player";
import Room from "./Room";
import Server from "./Server";

export default class RoomTriple extends Room {
    private decidees: Array<Player> = [];
    private decider: Player | undefined;
    private monster: Player | undefined;
    private human: Player | undefined;
    

    public constructor(targetRoomDict: {[id: string]: Room}, players: Array<Player>,server: Server, gameSession: GameSession) {
        super(targetRoomDict, players, server, gameSession)
        let gamesession = this.server.gamesession!

        // Find monster
        for (let player of this.players){
            if (gamesession.playerIsMonster(player)){
                this.monster = player;
                this.decidees.push(player);
            }
            
        }
        if (!this.monster){
            server.crash("Error assigning triple room players");
            return;
        }
        for (let player of this.players){
            if (player.getUsername() === this.monster.getDisplayName()){
                this.decidees.push(player);
                this.human = player;
            }else if (!gamesession.playerIsMonster(player)) {
                this.decider = player;
            }
        }
        shuffle(this.decidees);
        if (this.decidees.length < 2 || !this.decider || !this.human){
            server.crash("Error assigning triple room players");
            return
        }
    }

    public isDecider(player: Player){
        return player === this.decider;
    }

    public isConfiguredTripleRoom(): boolean {
        return (this.decidees.length == 2);
    }

    
    public getTripleChoice(choice: number): Player {
        return this.decidees[choice];
    }

    public fightTimeOut(): void {
        this.players.forEach((player)=>{
            if (!(this.server.gamesession!.playerIsMonster(player))){
                player.die(true, false);
            }
        })
    }

    public getIdentifier(player: Player): string{
        let output = "";
        this.decidees.forEach((thisPlayer: Player, index) => {
            if (player == thisPlayer){
                output = ["A", "B"][index];
            }
        })
        return output;
    }

    public socketEmit(): void {
        this.sendToVcRoom();
        let voiceskin = this.human?.getVoiceSkin();

        this.decider!.emit("in room triple",
            {
                decider: true,
                query:this.query,
                time:config().times.fight,
                recording: this.voicechatid,
                voiceskin: -1,
                name: this.decidees[0].getDisplayName(),
                otherPlayerSkin:voiceskin,
                puids: [this.decidees[0].puid, this.decidees[1].puid]
            })

        this.decidees.forEach((player: Player, index)=>{

            player.emit("in room triple",
                {
                    decider: false,
                    query: this.query,
                    time: config().times.fight,
                    recording: this.voicechatid,
                    voiceskin: voiceskin,
                    otherPlayerSkin: voiceskin,
                    name: this.human!.getDisplayName() + " "+["A","B"][index],
                    nameDecider: this.decider!.getDisplayName()
                })
        })
    }
}
