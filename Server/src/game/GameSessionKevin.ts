import GameSession from "./GameSession";
import Player from "./Player";

import assignRoomsMayhem from "./functions/assignRoomsMayhem";
import { GameMode } from "../types/GameMode";
import config from "../config/config";
import assignRoles from "./functions/assignRoles";


export default class GameSessionKevin extends GameSession{
    private kevin: Player | undefined;

    public getGameMode(): GameMode {
        return GameMode.Kevin
    }

    

    protected emitWinner(){
        let winningPlayer;
        if (this.kevin?.kevinWinCondition()){
            winningPlayer = this.kevin?.getUsername();
            if (config().debug){
                console.log(winningPlayer+" won!")
            }
            this.server.emitToAll("endgame mayhem",{winner: winningPlayer})
        }else{
            super.emitWinner();
        }
       
    }

    protected gameStartInfo(player:Player){
        if (player.monster){
            return {gameinfo: this.server.getPlayerInfoArray()};
        } else if(this.kevin === player){
            return {character:player.character, kevin: true}

        }
        else{
            return {character:player.character, kevin: false}
        }
    }

    protected checkEndFightConditions(): boolean{
        return (!this.areRoomsLeft() || this.getMonsterAmnt() === 0 || !!this.kevin?.kevinWinCondition())
    }

    protected setUpPlayersAndRoles(players: {[id: string]: Player}){
        assignRoles(players, this, this.server)
        this.kevin = Object.values(this.humans)[Math.floor(Math.random()*(Object.keys(this.humans).length))]

        
    }

    protected checkGameEndOrNextRound(){
        if (this.kevin?.kevinWinCondition()){
            this.endGame();
        }else{
            super.checkGameEndOrNextRound()
        }
    }
}