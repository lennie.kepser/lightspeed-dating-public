import Server from "./Server";
import checkServerOK from "./functions/checkServerOk";
import SocketHandler from "../io/SocketHandler";
import createId from "./functions/createID";
import EOS from "./EOS";
import Player from "./Player";
const npmIo = require('@pm2/io')

import config from "../config/config";

export default class ServerHandler{
    private io: any;
    private servers: { [id: string] : Server};

    public eos: EOS;
    private playerMetric = npmIo.metric({
        name: 'Online Players',
        unit: 'People'
    })

    private serverMetric = npmIo.metric({
        name: 'Active Servers',
        unit: 'Servers'
    })


    public getMaxServerAmount(){
        return config().maxServers;
    }



    constructor(io:any){
        this.servers = {};
        this.io = io;
        this.eos = new EOS();
        setInterval(()=>{
            let players = 0;
            for (let serv in this.servers){
                players+=this.servers[serv].getServerInfo().players.length;
            }
            this.playerMetric.set(players)
            this.serverMetric.set(Object.keys(this.servers).length)

        }, 20000)
    }

    public getServerAmount(): number{
        return Object.keys(this.servers).length;
    }

    public createServer(id?: string){
        let final_id;
        if (id===undefined){
            final_id = createId(config().codelength);
            while (this.servers[final_id]){
                final_id = createId(config().codelength);
            }
        }else{
            final_id = id;
        }

        this.servers[final_id] = new Server(final_id,this.io, this, this.eos);
        if (config().debug){
            console.log("Created server: " + final_id);
        }
        return final_id;
    }
    //Returns: 1: Server not found, 2: Server full, 3: Username not unique, 4: puid invalid, 5: Unknown,  0: Success
    public joinServer(data: any, socket: any, sockethandler: SocketHandler){
        try{
        let id = data.room;
        let puid = data.puid;
        if (puid == undefined || typeof  puid !== "string"){
            return 4;
        }
        let username = data.username;
        let server = this.servers[id];
        let serverok = checkServerOK(server,username);
        if (serverok!==0){
            return serverok;
        }

        sockethandler.player = server.addPlayer(socket, username, puid, data.ip, sockethandler);
        server.emitToAll("player join",sockethandler.player?.getPublicInfo(),sockethandler.player);
            return 0;

        }catch(err: any){
            console.error("Error joining player "+data);
            console.error(err);
            return 5;
        }
    }

    public getServerInfo(serverid: string){
        let server: Server | undefined = this.servers[serverid];
        if (server === undefined){
            return {};
        }
        return server.getServerInfo();
    }

    public leaveServer(serverid: string, player: Player){
        let server = this.servers[serverid];
        if (server === undefined){
            return;
        }
        if (config().debug){
            console.log(player.getUsername()+" left server "+serverid);
        }
        server.removePlayer(player);
    }

    public deleteServer(serverid: string){
        // Dont delete server if its a testing server
        if (serverid.length === 1)
            return;
        if (config().debug){
            console.log("Deleting Server "+serverid);
        }
        this.servers[serverid].RemoveGameSession();
        delete this.servers[serverid];
    }

    public getServer(serverid: string): Server{
        return this.servers[serverid];
    }
}