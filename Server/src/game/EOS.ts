import { isIPv4 } from "net";
import {Participant} from "../types/Participant";
import {VoiceResponse} from "../types/VoiceResponse";
const axios = require('axios')

export default class EOS {
    private clientID = "REMOVED";
    private clientSecret = "REMOVED";
    private accessToken = "";
    private deploymentID = "REMOVED";

    constructor() {
        this.GetAccessToken();
    }

    private GetAccessToken(){
        let combined = this.clientID+":"+this.clientSecret;
        let encoded = Buffer.from(combined).toString('base64');
        axios.post("https://api.epicgames.dev/auth/v1/oauth/token","grant_type=client_credentials&deployment_id="+this.deploymentID,{
            headers:{
                "Authorization": "Basic "+encoded,
                "Content-Type":"application/x-www-form-urlencoded",
                "Accept":"application/json"
            }
        })
            .then((res: any)=>{
                this.accessToken = res.data.access_token
                setTimeout(()=>this.GetAccessToken(), 1000*(res.data.expires_in-10));
            })
            .catch((err: any)=>{
                console.error("CRITICAL: EOS AUTHORIZATION FAILED")
                console.log(err);
                setTimeout(()=>this.GetAccessToken(), 1000);
            })
    }

    public async GetRoomTokens(roomID: string, participants: Array<Participant>): Promise<VoiceResponse | undefined> {
        try{
            for (let i = 0; i < participants.length; i++){
                if (participants[i].clientIp == undefined || typeof participants[i].clientIp == "string" || isIPv4(participants[i].clientIp!) ){
                    delete participants[i].clientIp;
                }
            }
            let response = await axios.post("https://api.epicgames.dev/rtc/v1/"+this.deploymentID+"/room/"+roomID,
            {participants},
            {headers:{
                "Authorization": "Bearer "+this.accessToken
                }})
            let output: VoiceResponse = {
                participants: {},
                clientBaseUrl: response.data.clientBaseUrl,
                roomName: roomID
            }
            for (let i = 0; i < response.data.participants.length; i++){
                let par = response.data.participants[i];
                output.participants[par.puid] = par.token;

            }
            return output;

        }catch(err: any){
            console.error(err);
            return undefined;
        }
        return undefined;

    }
    

}