import Server from "../Server";

export default function checkServerOK(server: Server, username: string){
    if (server == undefined){
        return 1; // Server not found
    }
    let join = server.joinable(username);
    if (join != 0){
        return join;
    }
    return 0;
}