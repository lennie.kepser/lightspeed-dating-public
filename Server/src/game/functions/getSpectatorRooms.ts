import Room from "../Room";

export default function getSpectatorRooms(rooms: { [p: number]: Room }){
    let output = [];
    for (let id in rooms){
        output.push(rooms[id].getInfoObj());
    }
    return output;
}