import Room from "../Room";
import Player from "../Player";
import shuffle from "./shuffle";
import getSpectatorRooms from "./getSpectatorRooms";
import Server from "../Server";
import {RoomType} from "../../types/RoomType";
import RoomTriple from "../RoomTriple";
import GameSession from "../GameSession";

export default function assignRooms(humans: { [p: string]: Player }, monsters: { [p: string]: Player }, server: Server): { [p: string] : Room }|undefined{
    try{
        let rooms: { [p: string]: Room } = {};
        let humansleft = shuffle(Object.values(humans));
        let monstersleft = shuffle(Object.values(monsters));
        const couples: Array<Array<Player>> = []

        // Assign monsters that haven't picked a target a target.
        monstersleft.forEach((monster)=>{
            if (monster.getTargetPlayer() === ""){
                if (Math.random() > 0.5){
                    monster.setTargetPlayerUnsafe("!skip");
                }else{
                    humansleft.forEach((human)=>{
                        // Check if another monster is targeting that human
                        if (monstersleft.filter((monster2)=>{return monster2.getTargetPlayer() === human.username}).length == 0 && human.username !== monster.getDisplayName()){
                            monster.setTargetPlayerUnsafe(human.username);
                        }
                    })
                }
            }
            // If there was no target left, skip round
            if (monster.getTargetPlayer() === "") {
                monster.setTargetPlayerUnsafe("!skip");
            }
        })

        monstersleft = monstersleft.filter((monster)=>{return !monster.isSkipping()});
        for (let i = monstersleft.length-1; i>=0; i--){
            const monster = monstersleft[i];
            const target = server.getPlayerFromUsername(monster.getTargetPlayer());
            if (target){
                couples.push([monster, target])
                monstersleft.splice(i,1);
                humansleft = humansleft.filter((human)=>human.username!=target.getUsername());
            }else{
                console.error("Player "+ monster.getTargetPlayer()+" targeted by "+ monster.username+" not found.");

            }
        }



        for (let i = 0; i+1 < humansleft.length; i+=2){
            couples.push([humansleft[i],humansleft[i+1]])
        }
        humansleft.splice(0,humansleft.length-(humansleft.length%2));

        if (humansleft.length > 1) {
            throw "Humans not assigned to rooms";
        }else if (humansleft.length === 1){
            // Try to create a three person room
            const remainingPlayer = humansleft[0];
            for (let room of couples){
                let monster: Player | undefined;
                let human: Player | undefined;
                for (let i = 0; i < room.length; i++){
                    if (room[i].monster){
                        monster = room[i];
                    }else{
                        human = room[i]
                    }
                }
                if (monster && human){
                    if (monster.getDisplayName() === remainingPlayer.username){
                        room.push(remainingPlayer);
                        humansleft.shift();
                        break;
                    }
                }
            }
        }

        couples.forEach((couple)=>{
            let roomClass = Room;
            if (couple.length > 2){
                roomClass = RoomTriple
            }
            new roomClass(rooms,couple, server, server.gamesession!);
        })

        // If there's still humans left, make them alone
        for (let i = 0; i < humansleft.length; i++){
            humansleft[i].emit("alone",{});
        }

        for (let id in rooms){
            rooms[id].socketEmit();
        }

        return rooms;

    }catch(err: any){
        console.error(server.toString()+": CRASH! Error assigning rooms.");
        console.error(err);
        console.error(err.stackTrace);

        return undefined;

    }
}

