import Player from "../Player";
import Server from "../Server";
import GameSession from "../GameSession";
import { GameMode } from "../../types/GameMode";
import config from "../../config/config";
import getCharacterFromPlayer from "./assignCharacter";

function chooseMonster(players: Array<string>, server: Server){
    let monsterChoiceArray: Array<string> = server.getMonsterChoiceID();
    monsterChoiceArray = monsterChoiceArray.filter(id => players.includes(id))

    const stabilisation_factor = 0.4
    for (const playerID of players){
        let addAmount = Math.ceil(monsterChoiceArray.filter( x => x == playerID).length * stabilisation_factor + 1)
        for (let i = 0; i < addAmount; i++){
            monsterChoiceArray.push(playerID);
        }
    }
    let chosenMonster = monsterChoiceArray[Math.floor(Math.random()*monsterChoiceArray.length)]
    monsterChoiceArray = monsterChoiceArray.filter(x => x != chosenMonster)
    server.setMonsterChoiceID(monsterChoiceArray)
    return chosenMonster
}

export default function assignRoles(players: {[id: string]: Player}, gamesession: GameSession, server: Server){
    let playercount: number = Object.keys(players).length;
    let monsteramnt: number = config().roles[playercount];

    const characters: Array<number> = config().characters;
    let unassignedplayers: Array<string> = [];
    for (let id in players) {
        if (players[id].getPlayerSetting("spectating")){
            gamesession.addSpectator(players[id])
            players[id].dead = true;
            players[id].monster = false;
            players[id].gamesession = gamesession;
            players[id].character = -1;
            continue;
        }
        unassignedplayers.push(id);
    }
    
    
    for (let i = 0; i < monsteramnt; i++){
        let id: string = chooseMonster(unassignedplayers, server)
        players[id].monster = true;
        players[id].dead = false;
        players[id].character = -1;

        players[id].gamesession = gamesession;
        
        unassignedplayers = unassignedplayers.filter((value)=>{return value!==id});
        gamesession.addMonster(players[id]);
    }
    for (let i = 0; i < unassignedplayers.length; i++){
        let player = players[unassignedplayers[i]];
        player.character = getCharacterFromPlayer(player);
        player.monster = false;
        player.dead = false;
        player.gamesession = gamesession;
        gamesession.addHuman(player);

    }
}
