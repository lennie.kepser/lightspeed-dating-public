export default function usernameValid(username: string): boolean{
    if (typeof username !== "string"){
        return false;
    }
    if (username.length<2){
        return false;
    }
    if (username.length>32){
        return false
    }
    return true;
}