import Player from "../Player";

export default function getCharacterFromPlayer(player: Player): number{
    const characterChoices = player.getCharacterChoices();
    const char = characterChoices[Math.floor(Math.random()*characterChoices.length)]
    return char
}