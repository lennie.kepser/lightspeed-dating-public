import {queries} from "../../config/queries";

export default function getRandomQuery(questionPacks: Array<string>, ignoreArray: Array<string>): string{
    let fullQueryList: Array<string> = []
    
    questionPacks.forEach((element: string)=>{
        fullQueryList = fullQueryList.concat(queries[element])
    })
    const filteredQueryList: Array<string> = [];
    fullQueryList.forEach((elem:string)=>{
        if (!(elem in ignoreArray)){
            filteredQueryList.push(elem);
        }
    })
    return filteredQueryList[Math.floor(Math.random()*filteredQueryList.length)]
}