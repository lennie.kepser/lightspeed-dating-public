import config from "../../config/config";
import Player from "../Player";
import Room from "../Room";
import RoomTriple from "../RoomTriple";
import Server from "../Server";
import getSpectatorRooms from "./getSpectatorRooms";
import shuffle from "./shuffle";

function findThreePlayerRooms(players: Array<Player>):Array<Player>{
    for(let i = 0; i < players.length; i++){
        for (let b = i+1; b < players.length; b++){
            if (players[i].getDisplayName() === players[b].getUsername() && players[b].getUsername() === players[b].getDisplayName()){
                for (let c = 0; c < players.length; c++){
                    if (c === i || b == c){
                        continue;
                    }
                    // We make the third player disguised as themselves. In the future this could be changed depending how confusing this is for the players.
                    players[c].setDisguise(players[c].getUsername());
                    return [players[i],players[b],players[c]]
                }
            }
        }
    }
    return []
}

function findTwoPlayerRoom(players: Array<Player>){
    for (let i = 0; i < players.length; i++){
        for (let b = i+1; b < players.length; b++){
            if (players[i].getDisplayName() !== players[b].getDisplayName() 
                && players[i].getUsername() !== players[b].getDisplayName()
                && players[i].getDisplayName() !== players[b].getUsername()
                && players[i].getUsername() !== players[b].getUsername()) {
                return [players[i],players[b]]
            }
        }
    }
    return []
}

export default function assignRoomsMayhem(players: Array<Player>, server: Server): { [p: string] : Room }|undefined{
    let rooms: { [p: string]: Room } = {};
    if (config().debug){
        console.log("Assign rooms mayhem")
    }
    shuffle(players)
    let couples: Array<Array<Player>> = []
    // Three player room
    let continueLoop = true;
    while (continueLoop){
        let playerObjects = findThreePlayerRooms(players)
        if (playerObjects.length === 0){
            continueLoop = false
        }else{
            players = players.filter((a)=> !playerObjects.includes(a))
            couples.push(playerObjects);
        }
    }
    continueLoop = true;
    while (continueLoop){
        let playerObjects = findTwoPlayerRoom(players)
        if (playerObjects.length === 0){
            continueLoop = false;
        }else{
            players = players.filter((a)=> !playerObjects.includes(a))
            couples.push(playerObjects);
        }
    }
    couples.forEach((couple)=>{
        let roomClass = Room;
        if (couple.length > 2){
            roomClass = RoomTriple
        }
        new roomClass(rooms, couple, server, server.gamesession!);
    })

    

    for (let id in players){
        players[id].setSkip()
    }
    for (let id in rooms){
        rooms[id].socketEmit();
    }
    return rooms;
}