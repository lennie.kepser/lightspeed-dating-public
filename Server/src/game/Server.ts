import Player from "./Player";
import {ServerInfo} from "../types/ServerInfo";
import assignRoles from "./functions/assignRoles";
import createID from "./functions/createID";
import GameSession from "./GameSession";
import ServerHandler from "./ServerHandler";
import {Round} from "../types/Round";
import EOS from "./EOS";
import {Participant} from "../types/Participant";
import {VoiceResponse} from "../types/VoiceResponse";
import { GamePhase } from "../types/GamePhase";
import { GameMode } from "../types/GameMode";
import GameSessionMayhem from "./GameSessionMayhem";
import GameSessionKevin from "./GameSessionKevin";

const npmIo = require('@pm2/io')

import config from "../config/config";
import { queries } from "../config/queries";
import SocketHandler from "../io/SocketHandler";

export default class Server{

    //Server Info
    public readonly id: string;

    //Socket Io Node
    private io: any;

    private static gamesStartedMetric = npmIo.counter({
        name: 'Games Started',
        unit: 'Games'
    })

    //Server handler
    private serverHandler: ServerHandler;

    //EOS Handker
    private eos: EOS;

    //Settings
    private readonly maxplayers: number = config().maxplayers;
    private readonly minplayers: number = config().minplayers;
    public settings: {[setting: string]: any} = {};

    //Player info
    private players: {[id: string]: Player} = {};
    

    //Game stuff
    gamesession: GameSession|undefined;
    private lobbyvc: string = "";
    private monsterChoiceArray: Array<string> = [];
    private lastRounds: Array<Round> = [];

    private readonly defaultQuestion = "casual"

    private questionPacks: Array<string> = [this.defaultQuestion];


    constructor(id: string, io: any, serverHandler: ServerHandler, eos: EOS){
        this.id = id;
        this.io = io;
        this.eos = eos;
        this.serverHandler = serverHandler;
        for (let setting in config().default){
            if (config().default.hasOwnProperty(setting)){
                this.settings[setting] = config().default[setting];
            }
        }
        if (this.settings.lobbyvoicechat){
            this.lobbyvc = this.id+"-"+createID(6);
        }
    }

    public getLastRounds(){
        return this.lastRounds;
    }

    public SetQuestionPacks(array: Array<string>) {
        this.questionPacks = []
        array.forEach(element => {
            if (element in queries){
                this.questionPacks.push(element)
            }
        });
        if (this.questionPacks.length === 0){
            this.questionPacks.push(this.defaultQuestion)
        }
        
    }

    public getQuestionPacks(): Array<string>{
        return this.questionPacks;
    }

    public async GetRoomTokens(roomID: string, players: Array<Player>, muted =false): Promise<VoiceResponse>{
        let participants: Array<Participant> = [];
        for (let i = 0; i < players.length; i++){

            let newParticipant:Participant = {
                puid: players[i].puid,
                hardMuted: muted
            };
            newParticipant.clientIp = players[i].ip
            participants.push(newParticipant);
        }
        let result = await this.eos.GetRoomTokens(roomID,participants);
        if (result == undefined){
            this.crash("Failed to get room tokens");
        }
        return result!;
    }

    // Player actions
    public kill(player: Player){
        if (!this.gamesession||!this.gamesession.isPhase(GamePhase.Fight)){
            return 1; // Not right game phase
        }
       
        return this.gamesession.kill(player);

    }

    public castJudgementVote(player: Player, choice: string): number{
        if (!this.gamesession || !this.gamesession.isPhase(GamePhase.Judgement)){
            return 1;
        }
        return player.castJudgementVote(choice);
    }

    public chooseTripleRoom(player: Player, choice: number): number{
        if (!this.gamesession|| !this.gamesession.isPhase(GamePhase.Fight)){
            return 1; // Not right game phase
        }
        return this.gamesession.chooseTripleRoom(player, choice);
    }

    public spare(player: Player){
        if (!this.gamesession || !this.gamesession.isPhase(GamePhase.Fight)){
            return 1; // Not right game phase
        }
        return this.gamesession.spare(player);

    }

    // Filters out spectating players
    private getAmountPlayingPlayers(): number{
        return Object.values(this.players).filter((e)=>!e.getPlayerSetting("spectating")).length
    }

    private getAmountPlayers(): number{
        return Object.keys(this.players).length;
    }

    public addPlayer(socket: any, username:string, puid: string, ip:string |undefined, socketHandler: SocketHandler): Player{
        let admin = false;
        if (this.getAmountPlayers() === 0){
            admin = true;
        }
        this.players[socket.id] = new Player(socket, username, admin, puid, this, socketHandler);
        socket.join(this.id);
        return this.players[socket.id];
    }



    private sendRoundsToPlayers(){
        this.emitToAll("last rounds",{rounds:this.lastRounds});
    }

    public playReplay(id: string, position: number){
        for (let i = 0; i < this.lastRounds.length; i++){
            let round = this.lastRounds[i];
            if (round.id === id){
                this.emitToAll("play replay",{id,position})

            }
        }
    }

    public setPostRoundData(id: string, maxTime: number){
        for (let i = 0; i < this.lastRounds.length; i++){
            if (this.lastRounds[i].id === id){
                if (this.lastRounds[i].maxTimeInMS == 0 || this.lastRounds[i].maxTimeInMS>maxTime){
                    this.lastRounds[i].maxTimeInMS = maxTime;
                }
            }
        }
        this.sendRoundsToPlayers();
    }

    public isUniqueRoundName(id: string): boolean{
        for (let i = 0; i < this.lastRounds.length; i++){
            if (this.lastRounds[i].id === id){
                return false;
            }
        }
        return true;
    }

    private removePlayersLastRounds(username: string): void{
        let initialLength = this.lastRounds.length
        for (let i = this.lastRounds.length-1; i >= 0; i--)
        {
            let round = this.lastRounds[i];
            for (let b = 0; b < round.players.length; b++){
                if (round.players[b].username===username){
                    this.lastRounds.splice(i,1);
                    break;
                }
            }
        }
        if (this.lastRounds.length!==initialLength){
            this.sendRoundsToPlayers();
        }
    }

    
    public addLastRound(voiceChatId: string, question: string, players: Array<Player>): void{
        let finalID = voiceChatId;
        let newRound: Round = {players:[],question, id: finalID, maxTimeInMS: 0};
        for (let player of players){
            newRound.players.push({monster:player.monster,username:player.getUsername(),disguise:player.getDisplayName(), socketid:player.getSocketID(), dead:false});

        }
        this.lastRounds.push(newRound);
    }

    public setPostRoundStatus(players: Array<Player>, room:string){
        for (let i = 0; i < this.lastRounds.length; i++){
            if (this.lastRounds[i].id === room){
                for (let playerGet of players){
                    for (let playerSet of this.lastRounds[i].players){
                        if (playerGet.getUsername() === playerSet.username){
                            playerSet.dead = playerGet.dead;
                        }
                    }
                }
            }
        }
    }

    public RemoveGameSession(){
        this.gamesession?.clearAllTimers();
        delete this.gamesession;
    }

    public removePlayer(player: Player): void{
        try{
            let newadmin = false;
            this.emitToAll("player leave",{username:player.getUsername()});

            // Check if new admin is necessary
            if (player.getAdmin() && this.getAmountPlayers() > 1){
                newadmin = true;
            }

           

            // Delete player and remove from socketio room
            delete this.players[player.getSocketID()];

            // Do game related stuff:
            this.gamesession?.removePlayer(player);
            this.removePlayersLastRounds(player.getUsername());
            
            // Delete server
            if (this.getAmountPlayers() === 0){
                this.serverHandler.deleteServer(this.id);
                return;
            }

            // Assign new admin if necessary
            if (newadmin){
                let keys = Object.keys(this.players);
                let player = this.players[keys[Math.floor(Math.random()*keys.length)]];
                player.setAdmin(true);
                this.emitToAll("new admin", {admin: player.getUsername()})
            }


        }catch(e){
            this.crash("Unexpected error removing player. Crashing server. Sorry!");
        }
    }

    public setSettings(player: Player, settings: {[setting: string]: string}): number{
        try {
        if (!player.getAdmin()){
            return 1;
        }
        for(let setting in settings){
            if (!(setting in this.settings)){
                if (config().debug){
                    console.log(setting, this.settings);
                }
                return 2;
            }
            if (config().constrains[setting].type === "number"){
                // @ts-ignore
                if (isNaN(settings[setting])){
                    return 3;
                }

                this.settings[setting] = parseInt(settings[setting]);
                if (this.settings[setting]<config().constrains[setting].min){
                    this.settings[setting] = config().constrains[setting].min
                    return 4;
                }
                if (this.settings[setting]>config().constrains[setting].max){
                    this.settings[setting] = config().constrains[setting].max
                    return 5;
                }
            }
            if (config().constrains[setting].type === "bool"){
                this.settings[setting] = (settings[setting] === "true");
            }
        }
        this.sendSettings();
        }
        catch(err){
            console.error(err);
            return 99;
        }
        return 0;
    }

    public getClientSettings(): {[setting: string]: string}{
        let outputSettings: {[setting: string]: string} = {};
        for (let s in this.settings){
            outputSettings[s] = this.settings[s].toString();
        }
        return outputSettings;
    }
 

    private sendSettings(){
        let outputSettings = this.getClientSettings();
        for (let id in this.players){
            const player = this.players[id];
            if (player.getAdmin()){
                continue;
            }
            player.emit("update settings",{settings:outputSettings, admin:player.getAdmin()});
        }
    }

    public joinable(username: string): number{
        if (username==undefined){
            return 4; // Invalid input
        }
        if (this.getAmountPlayers()>=this.maxplayers){
            return 2; // Max players reached
        }
        for (let id in this.players){
            if (this.players[id].getUsername() === username){
                return 3; // Username not unique
            }
        }
        return 0; // Joinable
    }



    // Socket stuff

    public emitToAll(event: string,data: object,except?: Player){
        for (let id in this.players){
            if (this.players[id]!== except){
                this.players[id].emit(event,data);
            }
        }
    }

    public crash(msg: string){
        console.error("Server crashed", msg);
        this.emitToAll("crash",{msg});
        this.serverHandler.deleteServer(this.id);
    }

   

    public startGame(player: Player): number{
        if (this.getAmountPlayingPlayers() < this.minplayers){
            return 1; // Not enough players;
        }
        if (this.gamesession){
            return 2; // Game already started;
        }
        if (!player.getAdmin()){
            return 3; // Player does not have rights to start game;
        }
        let gameSessionClass = GameSession;
        if (this.settings.gameMode as GameMode === GameMode.Mayhem){
            gameSessionClass = GameSessionMayhem
        }else if(this.settings.gameMode as GameMode === GameMode.Kevin){
            gameSessionClass = GameSessionKevin
        }
        this.lastRounds = [];
        this.gamesession = new gameSessionClass(this, this.players);
        this.gamesession.startGame();

        Server.gamesStartedMetric.inc();
        if (config().debug){
            console.log("starting game for server "+this.id);
        }

        return 0;
    }

    async endGame(){
        for (let id in this.players){
            this.players[id].dead = false;
            this.players[id].character = 0;
            this.players[id].monster = false;
            this.players[id].resetDisguise();
            delete this.players[id].gamesession;
            delete this.players[id].room;
        }
        let info = this.getPlayerInfoArray();

        this.emitToAll("lobby", {info});

        if (this.settings.lobbyvoicechat){
            this.lobbyvc = this.id+"-"+createID(6);
            this.sendPlayersToVCRoom(this.lobbyvc,Object.values(this.players));
        }

        if (this.settings.replays){
            this.sendRoundsToPlayers();
            this.sendPlayersToVCRoom(this.lobbyvc+"-replays",Object.values(this.players));
        }

        delete this.gamesession;
    }

    public async sendPlayersToVCRoom(roomid: string, players: Array<Player>, muted = false){
        let tokens = await this.GetRoomTokens(roomid,players, muted);
        for (let key in players){
            let player = players[key];
            player.emit("join voiceroom",{
                baseurl: tokens.clientBaseUrl,
                roomname: tokens.roomName,
                token: tokens.participants[player.puid],
                voiceskin: player.getVoiceSkin(),
                echo: false,
                muted
            })
        }
    }


    // Game phase utility functions

    public toString(): string{
        return this.id
    }

    // Getters and Setters

    public getMonsterChoiceID(): Array<string>{
        return this.monsterChoiceArray;
    }

    public setMonsterChoiceID(newVal: Array<string>): void{
        this.monsterChoiceArray = newVal;
    }

    // "Dynamic"
    public getPlayerInfoArray(){
        let gameinfo = []
        for (let id in this.players){
            gameinfo.push(this.players[id].getPublicInfo())

        }
        return gameinfo;
    }

    public getPlayerFromUsername(username: string): Player | undefined{
        for (let id in this.players){
            if (this.players[id].getUsername() === username){
                return this.players[id];
            }
        }
        return undefined;
    }

    public getServerInfo(): ServerInfo{
        let serverinfo: ServerInfo = {
            serverid:this.id,
            players: [],
            gamestate: (this.gamesession ? this.gamesession.getPhase() : 0),
            times: config().times,
            lobbyvc: this.lobbyvc
        }
        serverinfo.times.fight = this.settings.roundtime;
        serverinfo.players = this.getPlayerInfoArray();
        return serverinfo;
    }


    public getUsername(id: string){
        return this.players[id].getUsername();
    }

    // "Static"


    public getLobbyVC(){
        return this.lobbyvc;
    }

}