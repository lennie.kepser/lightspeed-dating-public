export type Participant = {
    puid: string // EOS ProductUserID
    clientIp?: string // IP
    hardMuted: boolean
}