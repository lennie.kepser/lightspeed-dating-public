export type ServerInfo = {
    serverid: string,
    players:Array<{monster:boolean,character:number,admin:boolean,username:string}>,
    gamestate: number,
    times: {"disguiseselect": number, "pregame": number, "fight": number},
    lobbyvc: string,
}