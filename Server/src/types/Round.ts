export type Round = {
    players:Array<{monster:boolean,disguise:string,username:string, socketid: any, dead: boolean}>,
    question: string,
    id: string,
    maxTimeInMS: number
}