export enum GamePhase {
    Pregame,
    CharacterSelect,
    Fight,
    Judgement,
    End
}