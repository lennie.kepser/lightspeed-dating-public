import {Participant} from "./Participant";

export type VoiceResponse = {
    participants: { [puid: string]: string}
    clientBaseUrl: string,
    roomName: string
}