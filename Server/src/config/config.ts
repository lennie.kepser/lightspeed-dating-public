
let configProd = require("./config.json");
let configtest = require("./config-test.json");

const tx2 = require('tx2')

tx2.action('update config',(reply:any)=>{
  console.log(require.resolve('./config.json'))
  delete require.cache[require.resolve('./config.json')]
  configProd = require("./config.json");
  delete require.cache[require.resolve('./config-test.json')]
  configtest = require("./config-test.json");
  reply({answer: "success"})
})

export default function config (){
  if (process.argv[2]&&process.argv[2]==="testing") {
      return configtest;
  }else{
      return configProd;
  }
};