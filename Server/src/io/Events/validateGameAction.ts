
import * as D from "io-ts/Decoder"
import config from "../../config/config";

const detail_types: {[key: string]: any} = {
    "set settings":  D.struct({
        roundtime: D.number,
        voiceeffects: D.boolean,
        randomvoices: D.boolean,
        replays: D.boolean,
        judgementchance: D.number,
        gameMode: D.number
      }),
    "pick disguise": D.string,
    "judgement vote": D.string,
    "choose triple":D.struct({
        "choice": D.number
    }),
    
    "pick target": D.string,
    "spectate": D.struct({
        "room": D.string
    }),
    "set player settings": D.fromStruct(config()["player_default"]),
    "replay": D.struct({
        "id": D.string,
        "position": D.number
    }),
   
    "replay data":
    D.struct({
        "id": D.string,
        "maxTime": D.number
    }),
    
    "light colors": D.number,
    "set questions": D.array(
        D.string
    ),
    "set characters": D.array(
        D.string
    ),
    "toggle voice filter": D.struct({
        "id": D.string,
        "val": D.boolean
    })
}


export default function validateGameAction(event: string, details: any){
    if (event in detail_types){
        const detail_type = detail_types[event]
        return (detail_type.decode(details))
    }
    return true
}