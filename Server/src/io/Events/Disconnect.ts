import ServerHandler from "../../game/ServerHandler";
import SocketHandler from "../SocketHandler";

export default function Disconnect(socket: any, servHandler: ServerHandler, socketHandler: SocketHandler){
    return ()=>{
        if (!socketHandler.unlocked){
            return;
        }
        if (socketHandler.player){
            servHandler.leaveServer(socketHandler.serverid, socketHandler.player!);

        }
    }
}