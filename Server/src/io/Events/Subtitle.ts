import Player from "../../game/Player";
import ServerHandler from "../../game/ServerHandler";
import SocketHandler from "../SocketHandler";

export default function Subtitle(socket: any, servHandler: ServerHandler, socketHandler: SocketHandler){
    return async (data: { phrase: string }) => {
        if (!socketHandler.unlocked){
            return;
        }
        if (!socketHandler.player){
            return;
        }
        if (!socketHandler.player.room){
            return;
        }
        socketHandler.player.room.getPlayers().forEach((player: Player)=>{
            if (player == socketHandler.player){
                return;
            }
            player.emit("subtitle", {phrase: data.phrase, user: socketHandler.player?.getDisplayNamePlusIdentifier()})
        })
        
    }
}