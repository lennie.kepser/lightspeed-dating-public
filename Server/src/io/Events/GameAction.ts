import Room from "../../game/Room";
import RoomTriple from "../../game/RoomTriple";
import ServerHandler from "../../game/ServerHandler";
import SocketHandler from "../SocketHandler";
import errorHandle from "./errorHandle";
import validateGameAction from "./validateGameAction";

import config from "../../config/config";


export default function GameAction(socket:any, serverhandler: ServerHandler, sockethandler: SocketHandler){
    return async (data: { action: string, details?: any }) => {
        try{
            if (!sockethandler.unlocked) {
                return;
            }
            let player = sockethandler.player;
            let server = serverhandler.getServer(sockethandler.serverid);
            if (config().debug) {
                console.log(player?.getUsername() + ": " + data.action)
                console.log(data.details)
            }
            if (server === undefined) {
                errorHandle(socket, "action", "Not in a game. (Server object not found)");
                return;
            }
            if (player === undefined) {
                errorHandle(socket, "action", "Not in a game. (Player object not found)");
                return;
            }
            if (!("action" in data)){
                errorHandle(socket, "action", "Action undefined")
                return
            }

            if (!validateGameAction(data.action, data.details)){
                errorHandle(socket, "action", "Wrong detail format", false)
                return
            }
            let code = 0;
            switch (data.action) {
                case "toggle voice filter":
                    if (server.isUniqueRoundName(data.details.id)){
                        errorHandle(socket, "action", "Replay not found.")
                        return;
                    }
                    server.emitToAll("toggle voice filter",data.details)
                    break;
                case "refresh question":
                    if (!server.gamesession){
                        errorHandle(socket, "action", "Wrong game phase");
                        return
                    }
                    const errorCode = server.gamesession!.refreshQuestion(player);
                    if (errorCode !== 0){
                        errorHandle(socket, "action", "Unkown Error: "+errorCode);
                        return
                    }
                    break;
                case "set questions":
                    if (!player.getAdmin()){
                        errorHandle(socket, "action", "No permissions");
                        return;
                    }
                    server.SetQuestionPacks(data.details)
                    break;
                case "set characters":
                    let characters: Array<number> = []
                    data.details.forEach((element: string) => {
                        const intChar = parseInt(element);
                        if (!(element in config().characters)){
                            errorHandle(socket, "action", "Invalid character")
                            return;
                        }
                        characters.push(intChar)
                    });
                    if (characters.length=== 0 || characters.length > 20){
                        errorHandle(socket, "action", "Wrong format");
                        return;
                    }
                    player.setCharacterChoices(characters)
                    break;
                case "set settings":
                    code = server.setSettings(player, data.details);
                    if (code === 1) {
                        errorHandle(socket, "action", "Not admin")
                        return
                    }
                    if (code === 2) {
                        errorHandle(socket, "action", "Invalid Settings (Maybe update LSD?)");
                        return
                    }
                    if (code === 3) {
                        errorHandle(socket, "action", "Not a number.");
                        return
                    }
                    if (code === 4) {
                        errorHandle(socket, "action", "Number too small.");
                        return
                    }
                    if (code === 5) {
                        errorHandle(socket, "action", "Number too large.");
                        return
                    }
                    if (code === 99) {
                        errorHandle(socket, "action", "Unknown Error occured");
                        return;
                    }
                    break;
                case "leave game":
                    server.removePlayer(player);
                    break
                case "start game":

                    code = server.startGame(player);
                    if (code === 1) {
                        errorHandle(socket, "action", "At least 3 players required. There is currently no public matchmaking. Consider joining our Discord (bottom right corner) to find people to play with!");
                        return;
                    }
                    if (code === 2) {
                        errorHandle(socket, "action", "Game already started.");
                        return;
                    }
                    if (code === 3) {
                        errorHandle(socket, "action", "Player does not have right to start game.");
                        return;
                    }
                    break;
                case "pick disguise":
                    code = player.pickDisguise(data.details);
                    if (code === 1) {
                        errorHandle(socket, "action", "You're not a monster.");
                        return;
                    }
                    if (code === 2) {
                        errorHandle(socket, "action", "Not right game phase.");
                        return;
                    }
                    if (code === 3) {
                        errorHandle(socket, "action", "You can't pick yourself.");
                        return;
                    }
                    if (code === 4) {
                        errorHandle(socket, "action", "Not an actual player.");
                        return
                    }
                    break;
                case "kill":
                    code = server.kill(player);
                    if (code === 1) {
                        errorHandle(socket, "action", "Not right game phase.");
                        return;
                    }
                    if (code === 2) {
                        errorHandle(socket, "action", "Not in a room.");
                        return;
                    }
                    if (code === 3) {
                        errorHandle(socket, "action", "Monsters can't kill.");
                        return;
                    }
                    break;
                case "judgement vote":
                    code = server.castJudgementVote(player, data.details);
                    if (code === 1) {
                        errorHandle(socket, "action", "Not right game phase.");
                        return;
                    }
                    if (code === 2) {
                        errorHandle(socket, "action", "Not a spectator.");
                        return;
                    }
                    if (code === 3) {
                        errorHandle(socket, "action", "Wrong choice input.");
                        return;
                    }
                    if (code === 4){
                        errorHandle(socket, "action", "Not in a room");
                    }
                    break;
                case "choose triple":
                    code = server.chooseTripleRoom(player, data.details.choice);
                    if (code === 1) {
                        errorHandle(socket, "action", "Not right game phase.");
                        return;
                    }
                    if (code === 2) {
                        errorHandle(socket, "action", "Not in a room.");
                        return;
                    }
                    if (code === 3) {
                        errorHandle(socket, "action", "Monsters can't kill.");
                        return;
                    }
                    if (code === 4){
                        errorHandle(socket, "action", "Not a valid triple rooms");
                        return;
                    }
                    break;

                case "spare":
                    code = server.spare(player);
                    if (code === 1) {
                        errorHandle(socket, "action", "Not right game phase.");
                        return;
                    }
                    if (code === 2) {
                        errorHandle(socket, "action", "Not in a room.");
                        return;
                    }
                    if (code === 3) {
                        errorHandle(socket, "action", "Already spared.");
                        return;
                    }
                    break;
                case "pick target":
                    code = player.setTargetPlayerSafe(data.details);
                    if (code === 0) {
                        return;
                    }
                    const errormessages = ["Not a monster", "Wrong game phase", "Selected player wasn't found", "Invalid player selected."];
                    errorHandle(socket, "action", errormessages[code]);
                    break;
                case "spectate":
                    if (!server.gamesession){
                        errorHandle(socket, "action", "Wrong game phase");
                        return;
                    }
                    if (!server.gamesession!.isSpectator(player) && !(player.monster && player.isSkipping())) {
                        errorHandle(socket, "action", "Not allowed to spectate");
                        return;
                    }
                    await server.sendPlayersToVCRoom(data.details.room, [player], true)

                    break;
                case "player setting":
                    code = player.setPlayerSettings(data.details);
                    if (code === 1){
                        errorHandle(socket, "action", "Invalid setting")
                    }
                    if (code === 2){
                        errorHandle(socket, "action", "Invalid datattype")
                    }
                    break;
                case "replay":
                    if (server.isUniqueRoundName(data.details.id)){
                        server.emitToAll("replay corrupted", {id: data.details.id})
                        console.error("replay corruped " + data.details.id)
                        console.error(server.getLastRounds())
                        //errorHandle(socket, "action", "Replay not found, play "+data.details.id);
                        return;
                    }
                    if (data.details.playing){
                        server.playReplay(data.details.id, data.details.position);
                    }else{
                        server.emitToAll("stop replay",{id: data.details.id, position: data.details.position});
                    }
                    break;
                case "replay data":
                    if (server.isUniqueRoundName(data.details.id)){
                        errorHandle(socket, "action", "Replay not found data "+data.details.id);
                        return;
                    }
                    server.setPostRoundData(data.details.id, data.details.maxTime);
                    break;
                case "light colors":
                    if (!player.room){
                        return;
                    }
                    if (player.room instanceof Room){
                        player.room.getOtherPlayer(player).emit("light colors",{"color":data.details})
                    }
                    if (player.room instanceof RoomTriple){
                        if (data.details === 0){
                            player.room.getTripleChoice(0).emit("light colors",{"color":0})
                            player.room.getTripleChoice(1).emit("light colors",{"color":0})
                        }else{
                            player.room.getTripleChoice(data.details-1).emit("light colors",{"color":1})
                            player.room.getTripleChoice(Math.abs(data.details - 2)).emit("light colors",{"color":2})

                        }
                    }
                    break;
                
            } 
        }catch(e: any){
            errorHandle(socket,"action",e, false, sockethandler.player)
            console.error(e.stackTrace);
        }
    }
}