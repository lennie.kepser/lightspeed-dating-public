import ServerHandler from "../../game/ServerHandler";
import usernameValid from "../../game/functions/usernameValid";
import errorHandle from "./errorHandle";
import SocketHandler from "../SocketHandler";
import Join from "./Join";
import config from "../../config/config";
import socketValidator from "../socketValidator";
import Steam from "../../game/Steam";

export default function CreateServer(socket:any, serverhandler: ServerHandler, sockethandler: SocketHandler){
    return async (data: any)=>{
        try {
            if (!socketValidator("create server", data)){
                errorHandle(socket, "socket", "Invalid data format", false)
                return
            }
            if (!sockethandler.unlocked) {
                errorHandle(socket, "create server", "Locked Client", false);

                return;
            }
            if (!data ||!data.username || !usernameValid(data.username)) {
                errorHandle(socket, "create server", "Username is invalid");
                return;
            }
            if (serverhandler.getServerAmount() > serverhandler.getMaxServerAmount()){
                const ownsDLC = await Steam.ownsDLC(sockethandler.steamID)
                if (!ownsDLC){
                    errorHandle(socket, "create server", "Too many people are playing right now. Please try again later or purchase the Deluxe edition to get queue priority.", false);
                    return
                }
             
            }
            let serverid = serverhandler.createServer();
            if (!serverid){
                errorHandle(socket, "create server", "Server can't be created. (Unkown Error)", false);
                return
            }
            if (config().debug) {
                console.log(data.username + " created server " + serverid);
            }
            data.room = serverid;

            Join(socket, serverhandler, sockethandler)(data);

            return;
        }catch(err: any){
            console.error(err);
            errorHandle(socket, "create server", "Unknown Error occured: "+err.message, false)
        }
    }
}