import ServerHandler from "../../game/ServerHandler";
import usernameValid from "../../game/functions/usernameValid";
import errorHandle from "./errorHandle";
import SocketHandler from "../SocketHandler";
import config from "../../config/config";
import socketValidator from "../socketValidator";


export default function Join(socket:any, serverhandler: ServerHandler, sockethandler: SocketHandler){
    return async (data: any) => {
        try {
          
            if (!sockethandler.unlocked) {
                errorHandle(socket, "join", "Locked Client");

                return;
            }
            if (sockethandler.steamID === "" && !config().testing_server){
                errorHandle(socket, "join", "Not logged into Steam")
                return;
            }
            if (!socketValidator("join", data)){
                errorHandle(socket, "socket", "Invalid data format")
                return
            }
            if (!data || !data.username || !usernameValid(data.username)) {
                errorHandle(socket, "join", "Username is invalid");
                return;
            }
            let code = serverhandler.joinServer(data, socket, sockethandler);
            if (code === 1) {
                errorHandle(socket, "join", "Server doesn't exist.");
                return;
            }
            if (code === 2) {
                errorHandle(socket, "join", "Server is full.");
                return;
            }
            if (code === 3) {
                errorHandle(socket, "join", "Username not unique.");
                return;
            }
            if (code === 4) {
                errorHandle(socket, "join", "PUID invalid.");
                return;
            }
            if (code === 5) {
                errorHandle(socket, "join", "Unknown Error.", false);
                return;
            }
            if (config().debug) {
                console.log(data.username + " joined server " + data.room);
            }
            const server = serverhandler.getServer(data.room);
            if (server.getServerInfo().gamestate===0){
                await server.sendPlayersToVCRoom(server.getLobbyVC(), [server.getPlayerFromUsername(data.username)!])

            }
            socket.emit("join successful",server.getServerInfo());

            socket.emit("update settings", {
                settings: server.getClientSettings(),
                admin: server.getPlayerFromUsername(data.username)!.getAdmin()
            });
            sockethandler.serverid = data.room;

            return;
        } catch (err: any) {
            console.error(err);
            errorHandle(socket, "join", "Unknown Error occured: " + err.message ,false)
        }
    }
}