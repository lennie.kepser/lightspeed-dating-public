import { Socket } from "socket.io";
import config from "../../config/config";
import Player from "../../game/Player";
const io = require('@pm2/io');

export default function errorHandle(socket: Socket, type: string, msg: string, trivial = true, player: Player | undefined = undefined){
    if (!trivial || config().debug){
        console.error(type,msg);
        let info: {custom: {user?: string , server?: string}} = {custom: {}};
        
        if (player !== undefined){
            info = {
                custom: {
                    user: player?.getUsername(),
                    server: player?.getServerId()
                }
            }
            console.error(player?.getUsername());
            console.error(player?.getServerId());

        }
        io.notifyError(new Error(type+ " "+msg), info);
  
    }
    if (!msg){
        msg = "Undefined Error Message";
    }
    socket.emit("error event",{type,msg})

}