import ServerHandler from "../../game/ServerHandler";
import Steam from "../../game/Steam";
import SocketHandler from "../SocketHandler";
import socketValidator from "../socketValidator";
import errorHandle from "./errorHandle";

export default function SteamAuth(socket: any, servHandler: ServerHandler, socketHandler: SocketHandler){
    return async (data: {"ticket": string} )=>{
        if (!socketValidator("steam auth", data)){
            errorHandle(socket, "socket", "Invalid data format")
            return
        }
        let steamId = await Steam.authenticateUserTicket(data.ticket)
        if (!steamId){
            errorHandle(socket,"steam","Unable to authenticate user with Steam. Is your internet connection stable, and your Steam open?")
            return;
        }
        socketHandler.steamID = steamId;
        socket.emit("steam auth",{steamId});
    }
}