
export default function debugging(socket: any){
    socket.on("test",()=>{
        console.log("test");
    })
    var onevent = socket.onevent;
    socket.onevent = function (packet: any) {
        var args = packet.data || [];
        onevent.call (this, packet);    // original call
        packet.data = ["*"].concat(args);
        onevent.call(this, packet);      // additional call to catch-all
    };
    socket.on("*",function(event: any,data: any) {
        if (event == "subtitle"){
            return;
        }
        console.debug(event,data);
    });
}