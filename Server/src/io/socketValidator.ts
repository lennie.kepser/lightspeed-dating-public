import * as D from "io-ts/Decoder"

const decoders: {[key: string]: any} = {
    "join echo room":D.struct({
        "puid": D.string,
        "ip": D.string,
    }),
    "unlock": D.string,
    "create server": D.struct({
        "username": D.string,
        "room": D.string
    }),
    "join": D.struct({
        "username": D.string,
        "room": D.string,
        "puid": D.string,
        "ip": D.string
    }),
    "steam auth": D.struct({
        "ticket": D.string
    })

}

export default function socketValidator(event: string, data: any){
    if (event in decoders){
        const detail_type = decoders[event]
        return (detail_type.decode(data))
    }
    return true
}