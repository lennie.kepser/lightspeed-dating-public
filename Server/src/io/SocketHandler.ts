import Disconnect from "./Events/Disconnect";
import Join from "./Events/Join";
import Debugger from "./Debugger";
import ServerHandler from "../game/ServerHandler";
import Player from "../game/Player";
import GameAction from "./Events/GameAction";
import CreateServer from "./Events/CreateServer";
import EOS from "../game/EOS";
import createId from "../game/functions/createID";
import {VoiceResponse} from "../types/VoiceResponse";
import errorHandle from "./Events/errorHandle";
import SteamAuth from "./Events/SteamAuth";
import config from "../config/config";
import socketValidator from "./socketValidator";
import { Socket } from "socket.io";
import Subtitle from "./Events/Subtitle";

const secret = "Lennie";


export default class SocketHandler {

    private socket: any;
    public unlocked: boolean = false;
    private readonly ServHandler: ServerHandler;
    public player: Player | undefined;
    private readonly eos: EOS;
    public serverid: string = "";
    public steamID: string = "";

    constructor(ServHandler: ServerHandler) {
        this.ServHandler = ServHandler;
        this.eos = this.ServHandler.eos;
    }

    public handle(socket: Socket){
        this.socket = socket;

        if (config().socketdebug){
            Debugger(socket);
        }
        socket.emit("connected");
        socket.on("join echo room",(data:any)=>{
            if (!socketValidator("join echo room", data)){
                errorHandle(socket, "socket", "Invalid data format")
                return
            }

            const splitIP = socket.handshake.address.split(":");
            const ip = splitIP[splitIP.length-1];
            this.eos.GetRoomTokens(data.puid+createId(10),[{
                puid:data.puid,
                clientIp:ip,
                hardMuted: false
            }])
            .then((response: VoiceResponse | undefined)=>{

                if (response == undefined){
                    errorHandle(socket,"join echo room","Error joining voice echo room.")
                
                }
                socket.emit("join voiceroom",{
                    baseurl: response!.clientBaseUrl,
                    roomname: response!.roomName,
                    token: response!.participants[data.puid],
                    voiceskin: 1,
                    muted: false,
                    echo: true
                })
            })
            .catch((e: any)=>{
                errorHandle(socket,"join echo room","Error joining voice echo room.")
            })
        })
        socket.on("unlock",(data:any)=>{
           
            try{
                if (!socketValidator("unlock", data)){
                    errorHandle(socket, "socket", "Invalid data format")
                    return
                }
                if (!data.secret){
                    return;
                }
                if (data.secret === secret){
                    socket.emit("unlock");
                    this.unlocked = true;
                }
            }catch(e){
                console.error(e);
            }


        })

        
        socket.on("disconnect",Disconnect(socket,this.ServHandler,this));
        socket.on("join",Join(socket, this.ServHandler,this));
        socket.on("create server",CreateServer(socket, this.ServHandler,this));
        socket.on("game action",GameAction(socket, this.ServHandler, this));
        socket.on("steam authenticate",SteamAuth(socket, this.ServHandler, this));
        socket.on("subtitle",Subtitle(socket, this.ServHandler, this));
    }

}