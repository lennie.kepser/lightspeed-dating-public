const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http, {
    cors: {
      origin: "*"
    },
    allowEIO3: true
  });
const cors = require('cors');

import ServerHandler from "./game/ServerHandler";
import SocketHandler from "./io/SocketHandler";
import config from "./config/config";
import Steam from "./game/Steam";
const maxServers = 0;

const ServHandler = new ServerHandler(io);

app.get('/', (req: any, res: any) => {
    res.send('<h1>Why are you here</h1>');
});

app.get('/get-space', cors(), (req: any, res: any) => {
    res.send(maxServers-ServHandler.getServerAmount()+"");
})

io.on('connection', (socket:any)=>{
    const SokHandler = new SocketHandler(ServHandler);
    SokHandler.handle(socket);
});

http.listen(3000, () => {
    console.log('listening on *:3000');
});

if (config().testing_server){
    ServHandler.createServer('A');
    ServHandler.createServer('B');
}

Steam.getUserStats();