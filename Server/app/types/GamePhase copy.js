"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GameMode = void 0;
var GameMode;
(function (GameMode) {
    GameMode[GameMode["Classic"] = 0] = "Classic";
    GameMode[GameMode["Mayhem"] = 1] = "Mayhem";
})(GameMode = exports.GameMode || (exports.GameMode = {}));
