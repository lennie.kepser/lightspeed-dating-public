"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GamePhase = void 0;
var GamePhase;
(function (GamePhase) {
    GamePhase[GamePhase["Pregame"] = 0] = "Pregame";
    GamePhase[GamePhase["CharacterSelect"] = 1] = "CharacterSelect";
    GamePhase[GamePhase["Fight"] = 2] = "Fight";
    GamePhase[GamePhase["Judgement"] = 3] = "Judgement";
    GamePhase[GamePhase["End"] = 4] = "End";
})(GamePhase = exports.GamePhase || (exports.GamePhase = {}));
