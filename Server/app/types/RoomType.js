"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoomType = void 0;
var RoomType;
(function (RoomType) {
    RoomType[RoomType["Normal"] = 0] = "Normal";
    RoomType[RoomType["Triple"] = 1] = "Triple";
    RoomType[RoomType["Judgement"] = 2] = "Judgement";
})(RoomType = exports.RoomType || (exports.RoomType = {}));
