"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http, {
    cors: {
        origin: "*"
    },
    allowEIO3: true
});
var cors = require('cors');
var ServerHandler_1 = __importDefault(require("./game/ServerHandler"));
var SocketHandler_1 = __importDefault(require("./io/SocketHandler"));
var config_1 = __importDefault(require("./config/config"));
var Steam_1 = __importDefault(require("./game/Steam"));
var maxServers = 0;
var ServHandler = new ServerHandler_1.default(io);
app.get('/', function (req, res) {
    res.send('<h1>Why are you here</h1>');
});
app.get('/get-space', cors(), function (req, res) {
    res.send(maxServers - ServHandler.getServerAmount() + "");
});
io.on('connection', function (socket) {
    var SokHandler = new SocketHandler_1.default(ServHandler);
    SokHandler.handle(socket);
});
http.listen(3000, function () {
    console.log('listening on *:3000');
});
if ((0, config_1.default)().testing_server) {
    ServHandler.createServer('A');
    ServHandler.createServer('B');
}
Steam_1.default.getUserStats();
