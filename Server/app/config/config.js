"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var configProd = require("./config.json");
var configtest = require("./config-test.json");
var tx2 = require('tx2');
tx2.action('update config', function (reply) {
    console.log(require.resolve('./config.json'));
    delete require.cache[require.resolve('./config.json')];
    configProd = require("./config.json");
    delete require.cache[require.resolve('./config-test.json')];
    configtest = require("./config-test.json");
    reply({ answer: "success" });
});
function config() {
    if (process.argv[2] && process.argv[2] === "testing") {
        return configtest;
    }
    else {
        return configProd;
    }
}
exports.default = config;
;
