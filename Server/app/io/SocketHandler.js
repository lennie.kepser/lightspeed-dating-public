"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Disconnect_1 = __importDefault(require("./Events/Disconnect"));
var Join_1 = __importDefault(require("./Events/Join"));
var Debugger_1 = __importDefault(require("./Debugger"));
var GameAction_1 = __importDefault(require("./Events/GameAction"));
var CreateServer_1 = __importDefault(require("./Events/CreateServer"));
var createID_1 = __importDefault(require("../game/functions/createID"));
var errorHandle_1 = __importDefault(require("./Events/errorHandle"));
var SteamAuth_1 = __importDefault(require("./Events/SteamAuth"));
var config_1 = __importDefault(require("../config/config"));
var socketValidator_1 = __importDefault(require("./socketValidator"));
var Subtitle_1 = __importDefault(require("./Events/Subtitle"));
var secret = "Lennie";
var SocketHandler = /** @class */ (function () {
    function SocketHandler(ServHandler) {
        this.unlocked = false;
        this.serverid = "";
        this.steamID = "";
        this.ServHandler = ServHandler;
        this.eos = this.ServHandler.eos;
    }
    SocketHandler.prototype.handle = function (socket) {
        var _this = this;
        this.socket = socket;
        if ((0, config_1.default)().socketdebug) {
            (0, Debugger_1.default)(socket);
        }
        socket.emit("connected");
        socket.on("join echo room", function (data) {
            if (!(0, socketValidator_1.default)("join echo room", data)) {
                (0, errorHandle_1.default)(socket, "socket", "Invalid data format");
                return;
            }
            var splitIP = socket.handshake.address.split(":");
            var ip = splitIP[splitIP.length - 1];
            _this.eos.GetRoomTokens(data.puid + (0, createID_1.default)(10), [{
                    puid: data.puid,
                    clientIp: ip,
                    hardMuted: false
                }])
                .then(function (response) {
                if (response == undefined) {
                    (0, errorHandle_1.default)(socket, "join echo room", "Error joining voice echo room.");
                }
                socket.emit("join voiceroom", {
                    baseurl: response.clientBaseUrl,
                    roomname: response.roomName,
                    token: response.participants[data.puid],
                    voiceskin: 1,
                    muted: false,
                    echo: true
                });
            })
                .catch(function (e) {
                (0, errorHandle_1.default)(socket, "join echo room", "Error joining voice echo room.");
            });
        });
        socket.on("unlock", function (data) {
            try {
                if (!(0, socketValidator_1.default)("unlock", data)) {
                    (0, errorHandle_1.default)(socket, "socket", "Invalid data format");
                    return;
                }
                if (!data.secret) {
                    return;
                }
                if (data.secret === secret) {
                    socket.emit("unlock");
                    _this.unlocked = true;
                }
            }
            catch (e) {
                console.error(e);
            }
        });
        socket.on("disconnect", (0, Disconnect_1.default)(socket, this.ServHandler, this));
        socket.on("join", (0, Join_1.default)(socket, this.ServHandler, this));
        socket.on("create server", (0, CreateServer_1.default)(socket, this.ServHandler, this));
        socket.on("game action", (0, GameAction_1.default)(socket, this.ServHandler, this));
        socket.on("steam authenticate", (0, SteamAuth_1.default)(socket, this.ServHandler, this));
        socket.on("subtitle", (0, Subtitle_1.default)(socket, this.ServHandler, this));
    };
    return SocketHandler;
}());
exports.default = SocketHandler;
