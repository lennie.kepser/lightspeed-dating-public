"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var D = __importStar(require("io-ts/Decoder"));
var config_1 = __importDefault(require("../../config/config"));
var detail_types = {
    "set settings": D.struct({
        roundtime: D.number,
        voiceeffects: D.boolean,
        randomvoices: D.boolean,
        replays: D.boolean,
        judgementchance: D.number,
        gameMode: D.number
    }),
    "pick disguise": D.string,
    "judgement vote": D.string,
    "choose triple": D.struct({
        "choice": D.number
    }),
    "pick target": D.string,
    "spectate": D.struct({
        "room": D.string
    }),
    "set player settings": D.fromStruct((0, config_1.default)()["player_default"]),
    "replay": D.struct({
        "id": D.string,
        "position": D.number
    }),
    "replay data": D.struct({
        "id": D.string,
        "maxTime": D.number
    }),
    "light colors": D.number,
    "set questions": D.array(D.string),
    "set characters": D.array(D.string),
    "toggle voice filter": D.struct({
        "id": D.string,
        "val": D.boolean
    })
};
function validateGameAction(event, details) {
    if (event in detail_types) {
        var detail_type = detail_types[event];
        return (detail_type.decode(details));
    }
    return true;
}
exports.default = validateGameAction;
