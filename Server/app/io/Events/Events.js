"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Disconnect_1 = __importDefault(require("./Disconnect"));
var events = {
    'disconnect': Disconnect_1.default
};
exports.default = events;
