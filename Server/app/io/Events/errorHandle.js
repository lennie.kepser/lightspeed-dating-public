"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../config/config"));
var io = require('@pm2/io');
function errorHandle(socket, type, msg, trivial, player) {
    if (trivial === void 0) { trivial = true; }
    if (player === void 0) { player = undefined; }
    if (!trivial || (0, config_1.default)().debug) {
        console.error(type, msg);
        var info = { custom: {} };
        if (player !== undefined) {
            info = {
                custom: {
                    user: player === null || player === void 0 ? void 0 : player.getUsername(),
                    server: player === null || player === void 0 ? void 0 : player.getServerId()
                }
            };
            console.error(player === null || player === void 0 ? void 0 : player.getUsername());
            console.error(player === null || player === void 0 ? void 0 : player.getServerId());
        }
        io.notifyError(new Error(type + " " + msg), info);
    }
    if (!msg) {
        msg = "Undefined Error Message";
    }
    socket.emit("error event", { type: type, msg: msg });
}
exports.default = errorHandle;
