"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function Disconnect(socket, servHandler, socketHandler) {
    return function () {
        if (!socketHandler.unlocked) {
            return;
        }
        if (socketHandler.player) {
            servHandler.leaveServer(socketHandler.serverid, socketHandler.player);
        }
    };
}
exports.default = Disconnect;
