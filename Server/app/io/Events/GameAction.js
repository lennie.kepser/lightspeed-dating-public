"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Room_1 = __importDefault(require("../../game/Room"));
var RoomTriple_1 = __importDefault(require("../../game/RoomTriple"));
var errorHandle_1 = __importDefault(require("./errorHandle"));
var validateGameAction_1 = __importDefault(require("./validateGameAction"));
var config_1 = __importDefault(require("../../config/config"));
function GameAction(socket, serverhandler, sockethandler) {
    var _this = this;
    return function (data) { return __awaiter(_this, void 0, void 0, function () {
        var player, server, code, _a, errorCode, characters_1, errormessages, e_1;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _b.trys.push([0, 21, , 22]);
                    if (!sockethandler.unlocked) {
                        return [2 /*return*/];
                    }
                    player = sockethandler.player;
                    server = serverhandler.getServer(sockethandler.serverid);
                    if ((0, config_1.default)().debug) {
                        console.log((player === null || player === void 0 ? void 0 : player.getUsername()) + ": " + data.action);
                        console.log(data.details);
                    }
                    if (server === undefined) {
                        (0, errorHandle_1.default)(socket, "action", "Not in a game. (Server object not found)");
                        return [2 /*return*/];
                    }
                    if (player === undefined) {
                        (0, errorHandle_1.default)(socket, "action", "Not in a game. (Player object not found)");
                        return [2 /*return*/];
                    }
                    if (!("action" in data)) {
                        (0, errorHandle_1.default)(socket, "action", "Action undefined");
                        return [2 /*return*/];
                    }
                    if (!(0, validateGameAction_1.default)(data.action, data.details)) {
                        (0, errorHandle_1.default)(socket, "action", "Wrong detail format", false);
                        return [2 /*return*/];
                    }
                    code = 0;
                    _a = data.action;
                    switch (_a) {
                        case "toggle voice filter": return [3 /*break*/, 1];
                        case "refresh question": return [3 /*break*/, 2];
                        case "set questions": return [3 /*break*/, 3];
                        case "set characters": return [3 /*break*/, 4];
                        case "set settings": return [3 /*break*/, 5];
                        case "leave game": return [3 /*break*/, 6];
                        case "start game": return [3 /*break*/, 7];
                        case "pick disguise": return [3 /*break*/, 8];
                        case "kill": return [3 /*break*/, 9];
                        case "judgement vote": return [3 /*break*/, 10];
                        case "choose triple": return [3 /*break*/, 11];
                        case "spare": return [3 /*break*/, 12];
                        case "pick target": return [3 /*break*/, 13];
                        case "spectate": return [3 /*break*/, 14];
                        case "player setting": return [3 /*break*/, 16];
                        case "replay": return [3 /*break*/, 17];
                        case "replay data": return [3 /*break*/, 18];
                        case "light colors": return [3 /*break*/, 19];
                    }
                    return [3 /*break*/, 20];
                case 1:
                    if (server.isUniqueRoundName(data.details.id)) {
                        (0, errorHandle_1.default)(socket, "action", "Replay not found.");
                        return [2 /*return*/];
                    }
                    server.emitToAll("toggle voice filter", data.details);
                    return [3 /*break*/, 20];
                case 2:
                    if (!server.gamesession) {
                        (0, errorHandle_1.default)(socket, "action", "Wrong game phase");
                        return [2 /*return*/];
                    }
                    errorCode = server.gamesession.refreshQuestion(player);
                    if (errorCode !== 0) {
                        (0, errorHandle_1.default)(socket, "action", "Unkown Error: " + errorCode);
                        return [2 /*return*/];
                    }
                    return [3 /*break*/, 20];
                case 3:
                    if (!player.getAdmin()) {
                        (0, errorHandle_1.default)(socket, "action", "No permissions");
                        return [2 /*return*/];
                    }
                    server.SetQuestionPacks(data.details);
                    return [3 /*break*/, 20];
                case 4:
                    characters_1 = [];
                    data.details.forEach(function (element) {
                        var intChar = parseInt(element);
                        if (!(element in (0, config_1.default)().characters)) {
                            (0, errorHandle_1.default)(socket, "action", "Invalid character");
                            return;
                        }
                        characters_1.push(intChar);
                    });
                    if (characters_1.length === 0 || characters_1.length > 20) {
                        (0, errorHandle_1.default)(socket, "action", "Wrong format");
                        return [2 /*return*/];
                    }
                    player.setCharacterChoices(characters_1);
                    return [3 /*break*/, 20];
                case 5:
                    code = server.setSettings(player, data.details);
                    if (code === 1) {
                        (0, errorHandle_1.default)(socket, "action", "Not admin");
                        return [2 /*return*/];
                    }
                    if (code === 2) {
                        (0, errorHandle_1.default)(socket, "action", "Invalid Settings (Maybe update LSD?)");
                        return [2 /*return*/];
                    }
                    if (code === 3) {
                        (0, errorHandle_1.default)(socket, "action", "Not a number.");
                        return [2 /*return*/];
                    }
                    if (code === 4) {
                        (0, errorHandle_1.default)(socket, "action", "Number too small.");
                        return [2 /*return*/];
                    }
                    if (code === 5) {
                        (0, errorHandle_1.default)(socket, "action", "Number too large.");
                        return [2 /*return*/];
                    }
                    if (code === 99) {
                        (0, errorHandle_1.default)(socket, "action", "Unknown Error occured");
                        return [2 /*return*/];
                    }
                    return [3 /*break*/, 20];
                case 6:
                    server.removePlayer(player);
                    return [3 /*break*/, 20];
                case 7:
                    code = server.startGame(player);
                    if (code === 1) {
                        (0, errorHandle_1.default)(socket, "action", "At least 3 players required. There is currently no public matchmaking. Consider joining our Discord (bottom right corner) to find people to play with!");
                        return [2 /*return*/];
                    }
                    if (code === 2) {
                        (0, errorHandle_1.default)(socket, "action", "Game already started.");
                        return [2 /*return*/];
                    }
                    if (code === 3) {
                        (0, errorHandle_1.default)(socket, "action", "Player does not have right to start game.");
                        return [2 /*return*/];
                    }
                    return [3 /*break*/, 20];
                case 8:
                    code = player.pickDisguise(data.details);
                    if (code === 1) {
                        (0, errorHandle_1.default)(socket, "action", "You're not a monster.");
                        return [2 /*return*/];
                    }
                    if (code === 2) {
                        (0, errorHandle_1.default)(socket, "action", "Not right game phase.");
                        return [2 /*return*/];
                    }
                    if (code === 3) {
                        (0, errorHandle_1.default)(socket, "action", "You can't pick yourself.");
                        return [2 /*return*/];
                    }
                    if (code === 4) {
                        (0, errorHandle_1.default)(socket, "action", "Not an actual player.");
                        return [2 /*return*/];
                    }
                    return [3 /*break*/, 20];
                case 9:
                    code = server.kill(player);
                    if (code === 1) {
                        (0, errorHandle_1.default)(socket, "action", "Not right game phase.");
                        return [2 /*return*/];
                    }
                    if (code === 2) {
                        (0, errorHandle_1.default)(socket, "action", "Not in a room.");
                        return [2 /*return*/];
                    }
                    if (code === 3) {
                        (0, errorHandle_1.default)(socket, "action", "Monsters can't kill.");
                        return [2 /*return*/];
                    }
                    return [3 /*break*/, 20];
                case 10:
                    code = server.castJudgementVote(player, data.details);
                    if (code === 1) {
                        (0, errorHandle_1.default)(socket, "action", "Not right game phase.");
                        return [2 /*return*/];
                    }
                    if (code === 2) {
                        (0, errorHandle_1.default)(socket, "action", "Not a spectator.");
                        return [2 /*return*/];
                    }
                    if (code === 3) {
                        (0, errorHandle_1.default)(socket, "action", "Wrong choice input.");
                        return [2 /*return*/];
                    }
                    if (code === 4) {
                        (0, errorHandle_1.default)(socket, "action", "Not in a room");
                    }
                    return [3 /*break*/, 20];
                case 11:
                    code = server.chooseTripleRoom(player, data.details.choice);
                    if (code === 1) {
                        (0, errorHandle_1.default)(socket, "action", "Not right game phase.");
                        return [2 /*return*/];
                    }
                    if (code === 2) {
                        (0, errorHandle_1.default)(socket, "action", "Not in a room.");
                        return [2 /*return*/];
                    }
                    if (code === 3) {
                        (0, errorHandle_1.default)(socket, "action", "Monsters can't kill.");
                        return [2 /*return*/];
                    }
                    if (code === 4) {
                        (0, errorHandle_1.default)(socket, "action", "Not a valid triple rooms");
                        return [2 /*return*/];
                    }
                    return [3 /*break*/, 20];
                case 12:
                    code = server.spare(player);
                    if (code === 1) {
                        (0, errorHandle_1.default)(socket, "action", "Not right game phase.");
                        return [2 /*return*/];
                    }
                    if (code === 2) {
                        (0, errorHandle_1.default)(socket, "action", "Not in a room.");
                        return [2 /*return*/];
                    }
                    if (code === 3) {
                        (0, errorHandle_1.default)(socket, "action", "Already spared.");
                        return [2 /*return*/];
                    }
                    return [3 /*break*/, 20];
                case 13:
                    code = player.setTargetPlayerSafe(data.details);
                    if (code === 0) {
                        return [2 /*return*/];
                    }
                    errormessages = ["Not a monster", "Wrong game phase", "Selected player wasn't found", "Invalid player selected."];
                    (0, errorHandle_1.default)(socket, "action", errormessages[code]);
                    return [3 /*break*/, 20];
                case 14:
                    if (!server.gamesession) {
                        (0, errorHandle_1.default)(socket, "action", "Wrong game phase");
                        return [2 /*return*/];
                    }
                    if (!server.gamesession.isSpectator(player) && !(player.monster && player.isSkipping())) {
                        (0, errorHandle_1.default)(socket, "action", "Not allowed to spectate");
                        return [2 /*return*/];
                    }
                    return [4 /*yield*/, server.sendPlayersToVCRoom(data.details.room, [player], true)];
                case 15:
                    _b.sent();
                    return [3 /*break*/, 20];
                case 16:
                    code = player.setPlayerSettings(data.details);
                    if (code === 1) {
                        (0, errorHandle_1.default)(socket, "action", "Invalid setting");
                    }
                    if (code === 2) {
                        (0, errorHandle_1.default)(socket, "action", "Invalid datattype");
                    }
                    return [3 /*break*/, 20];
                case 17:
                    if (server.isUniqueRoundName(data.details.id)) {
                        server.emitToAll("replay corrupted", { id: data.details.id });
                        console.error("replay corruped " + data.details.id);
                        console.error(server.getLastRounds());
                        //errorHandle(socket, "action", "Replay not found, play "+data.details.id);
                        return [2 /*return*/];
                    }
                    if (data.details.playing) {
                        server.playReplay(data.details.id, data.details.position);
                    }
                    else {
                        server.emitToAll("stop replay", { id: data.details.id, position: data.details.position });
                    }
                    return [3 /*break*/, 20];
                case 18:
                    if (server.isUniqueRoundName(data.details.id)) {
                        (0, errorHandle_1.default)(socket, "action", "Replay not found data " + data.details.id);
                        return [2 /*return*/];
                    }
                    server.setPostRoundData(data.details.id, data.details.maxTime);
                    return [3 /*break*/, 20];
                case 19:
                    if (!player.room) {
                        return [2 /*return*/];
                    }
                    if (player.room instanceof Room_1.default) {
                        player.room.getOtherPlayer(player).emit("light colors", { "color": data.details });
                    }
                    if (player.room instanceof RoomTriple_1.default) {
                        if (data.details === 0) {
                            player.room.getTripleChoice(0).emit("light colors", { "color": 0 });
                            player.room.getTripleChoice(1).emit("light colors", { "color": 0 });
                        }
                        else {
                            player.room.getTripleChoice(data.details - 1).emit("light colors", { "color": 1 });
                            player.room.getTripleChoice(Math.abs(data.details - 2)).emit("light colors", { "color": 2 });
                        }
                    }
                    return [3 /*break*/, 20];
                case 20: return [3 /*break*/, 22];
                case 21:
                    e_1 = _b.sent();
                    (0, errorHandle_1.default)(socket, "action", e_1, false, sockethandler.player);
                    console.error(e_1.stackTrace);
                    return [3 /*break*/, 22];
                case 22: return [2 /*return*/];
            }
        });
    }); };
}
exports.default = GameAction;
