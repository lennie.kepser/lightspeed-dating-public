"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var usernameValid_1 = __importDefault(require("../../game/functions/usernameValid"));
var errorHandle_1 = __importDefault(require("./errorHandle"));
var config_1 = __importDefault(require("../../config/config"));
var socketValidator_1 = __importDefault(require("../socketValidator"));
function Join(socket, serverhandler, sockethandler) {
    var _this = this;
    return function (data) { return __awaiter(_this, void 0, void 0, function () {
        var code, server, err_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 3, , 4]);
                    if (!sockethandler.unlocked) {
                        (0, errorHandle_1.default)(socket, "join", "Locked Client");
                        return [2 /*return*/];
                    }
                    if (sockethandler.steamID === "" && !(0, config_1.default)().testing_server) {
                        (0, errorHandle_1.default)(socket, "join", "Not logged into Steam");
                        return [2 /*return*/];
                    }
                    if (!(0, socketValidator_1.default)("join", data)) {
                        (0, errorHandle_1.default)(socket, "socket", "Invalid data format");
                        return [2 /*return*/];
                    }
                    if (!data || !data.username || !(0, usernameValid_1.default)(data.username)) {
                        (0, errorHandle_1.default)(socket, "join", "Username is invalid");
                        return [2 /*return*/];
                    }
                    code = serverhandler.joinServer(data, socket, sockethandler);
                    if (code === 1) {
                        (0, errorHandle_1.default)(socket, "join", "Server doesn't exist.");
                        return [2 /*return*/];
                    }
                    if (code === 2) {
                        (0, errorHandle_1.default)(socket, "join", "Server is full.");
                        return [2 /*return*/];
                    }
                    if (code === 3) {
                        (0, errorHandle_1.default)(socket, "join", "Username not unique.");
                        return [2 /*return*/];
                    }
                    if (code === 4) {
                        (0, errorHandle_1.default)(socket, "join", "PUID invalid.");
                        return [2 /*return*/];
                    }
                    if (code === 5) {
                        (0, errorHandle_1.default)(socket, "join", "Unknown Error.", false);
                        return [2 /*return*/];
                    }
                    if ((0, config_1.default)().debug) {
                        console.log(data.username + " joined server " + data.room);
                    }
                    server = serverhandler.getServer(data.room);
                    if (!(server.getServerInfo().gamestate === 0)) return [3 /*break*/, 2];
                    return [4 /*yield*/, server.sendPlayersToVCRoom(server.getLobbyVC(), [server.getPlayerFromUsername(data.username)])];
                case 1:
                    _a.sent();
                    _a.label = 2;
                case 2:
                    socket.emit("join successful", server.getServerInfo());
                    socket.emit("update settings", {
                        settings: server.getClientSettings(),
                        admin: server.getPlayerFromUsername(data.username).getAdmin()
                    });
                    sockethandler.serverid = data.room;
                    return [2 /*return*/];
                case 3:
                    err_1 = _a.sent();
                    console.error(err_1);
                    (0, errorHandle_1.default)(socket, "join", "Unknown Error occured: " + err_1.message, false);
                    return [3 /*break*/, 4];
                case 4: return [2 /*return*/];
            }
        });
    }); };
}
exports.default = Join;
