"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function debugging(socket) {
    socket.on("test", function () {
        console.log("test");
    });
    var onevent = socket.onevent;
    socket.onevent = function (packet) {
        var args = packet.data || [];
        onevent.call(this, packet); // original call
        packet.data = ["*"].concat(args);
        onevent.call(this, packet); // additional call to catch-all
    };
    socket.on("*", function (event, data) {
        if (event == "subtitle") {
            return;
        }
        console.debug(event, data);
    });
}
exports.default = debugging;
