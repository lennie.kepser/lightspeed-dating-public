"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var createID_1 = __importDefault(require("./game/functions/createID"));
var io = require('socket.io-client');
var socket = io("ws://localhost:3000");
var username = "";
var players = [];
var randomtesting = false;
var serverid = "";
function randomFloat() {
    return Math.random() * 100;
}
function randomInt() {
    return Math.floor(randomFloat());
}
function randomString() {
    return (0, createID_1.default)(randomInt());
}
function randomObject(depth) {
    if (depth === void 0) { depth = 0; }
    if (depth > 5 || Math.random() > 0.8) {
        return {};
    }
    var output = {};
    for (var i = 0; i < randomInt(); i++) {
        output[randomString()] = giveRandom(depth + 1);
    }
    return output;
}
function randomArray(depth) {
    if (depth === void 0) { depth = 0; }
    if (depth > 5 || Math.random() > 0.8) {
        return [];
    }
    var output = [];
    for (var i = 0; i < randomInt(); i++) {
        output.push(giveRandom(depth + 1));
    }
    return output;
}
function randomEtc() {
    var rand = Math.random();
    if (rand > 0.8)
        return undefined;
    if (rand > 0.6)
        return null;
    return 0;
}
function giveRandom(depth) {
    if (depth === void 0) { depth = 0; }
    var randomfunctions = [randomFloat, randomInt, randomString, randomObject, randomEtc, randomArray];
    return randomfunctions[Math.floor(Math.random() * randomfunctions.length)](depth);
}
var socketevents = [
    { event: 'unlock', data: ["Lennie"] },
    { event: 'join', data: ["username", "room"] },
];
var gameactions = [
    { name: 'start game' },
    { name: 'pick disguise' },
    { name: 'pick target' },
    { name: 'kill' },
    { name: 'spare' },
];
function doRandomSocket() {
    var event = socketevents[Math.floor(Math.random() * socketevents.length)];
    if (Math.random() > 0.5) {
        var data = randomObject();
        for (var i = 0; i < event.data.length; i++) {
            data[event.data[i]] = giveRandom();
        }
        socket.emit(event.event, data);
    }
    else {
        socket.emit(event.event, giveRandom());
    }
    if (Math.random() < 0.01) {
        socket.disconnect();
    }
}
function doRandomGameAction() {
    var gameaction = gameactions[Math.floor(Math.random() * gameactions.length)];
    socket.emit('game action', { action: gameaction.name, details: giveRandom() });
}
function crashTestDummy() {
    doRandomSocket();
    doRandomGameAction();
}
socket.on("connected", function () {
    socket.emit("unlock", { secret: "Lennie" });
});
var serverCreater = false;
socket.on("unlock", function (data) {
    username = (0, createID_1.default)(8);
    var crazyness = Math.floor(Math.random() * 10000) + 1000;
    if (randomtesting)
        setInterval(crashTestDummy, crazyness);
    if (process.argv[2] === "0") {
        serverCreater = true;
        socket.emit("create server", { username: username });
    }
    else {
        if (serverid !== "") {
            socket.emit("join", { username: username, room: serverid });
        }
    }
});
socket.on("join successful", function (data) {
    if (serverCreater) {
        // @ts-ignore
        process.send({ msg: "joined", serverid: data.serverid });
    }
    else {
        // @ts-ignore
        process.send({ msg: "joined", serverid: "" });
    }
});
process.on("message", function (msg, data) {
    if (msg === "start") {
        setTimeout(function () {
            socket.emit("game action", { action: "start game" });
        }, 100);
    }
    else if (msg.split("_")[0] === "join") {
        serverid = msg.split("_")[1];
        if (username) {
            socket.emit("join", { username: username, room: serverid });
        }
    }
});
socket.on("game start info", function (data) {
    players = data.gameinfo;
});
socket.on("monster char select", function (data) {
    players = data.gameinfo;
    socket.emit("game action", { action: "pick disguise", details: players[Math.floor((Math.random() * players.length))].username });
});
socket.on("in room", function () {
    if (Math.random() > 0.5) {
        socket.emit("game action", { action: "kill" });
    }
    else {
        socket.emit("game action", { action: "spare" });
    }
});
socket.on("endgame", function () {
    // @ts-ignore
    process.send({ msg: "game end" });
});
socket.on('disconnect', function () {
    //process.exit();
});
