"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var net_1 = require("net");
var axios = require('axios');
var EOS = /** @class */ (function () {
    function EOS() {
        this.clientID = "REMOVED";
        this.clientSecret = "REMOVED";
        this.accessToken = "";
        this.deploymentID = "REMOVED";
        this.GetAccessToken();
    }
    EOS.prototype.GetAccessToken = function () {
        var _this = this;
        var combined = this.clientID + ":" + this.clientSecret;
        var encoded = Buffer.from(combined).toString('base64');
        axios.post("https://api.epicgames.dev/auth/v1/oauth/token", "grant_type=client_credentials&deployment_id=" + this.deploymentID, {
            headers: {
                "Authorization": "Basic " + encoded,
                "Content-Type": "application/x-www-form-urlencoded",
                "Accept": "application/json"
            }
        })
            .then(function (res) {
            _this.accessToken = res.data.access_token;
            setTimeout(function () { return _this.GetAccessToken(); }, 1000 * (res.data.expires_in - 10));
        })
            .catch(function (err) {
            console.error("CRITICAL: EOS AUTHORIZATION FAILED");
            console.log(err);
            setTimeout(function () { return _this.GetAccessToken(); }, 1000);
        });
    };
    EOS.prototype.GetRoomTokens = function (roomID, participants) {
        return __awaiter(this, void 0, void 0, function () {
            var i, response, output, i, par, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        for (i = 0; i < participants.length; i++) {
                            if (participants[i].clientIp == undefined || typeof participants[i].clientIp == "string" || (0, net_1.isIPv4)(participants[i].clientIp)) {
                                delete participants[i].clientIp;
                            }
                        }
                        return [4 /*yield*/, axios.post("https://api.epicgames.dev/rtc/v1/" + this.deploymentID + "/room/" + roomID, { participants: participants }, { headers: {
                                    "Authorization": "Bearer " + this.accessToken
                                } })];
                    case 1:
                        response = _a.sent();
                        output = {
                            participants: {},
                            clientBaseUrl: response.data.clientBaseUrl,
                            roomName: roomID
                        };
                        for (i = 0; i < response.data.participants.length; i++) {
                            par = response.data.participants[i];
                            output.participants[par.puid] = par.token;
                        }
                        return [2 /*return*/, output];
                    case 2:
                        err_1 = _a.sent();
                        console.error(err_1);
                        return [2 /*return*/, undefined];
                    case 3: return [2 /*return*/, undefined];
                }
            });
        });
    };
    return EOS;
}());
exports.default = EOS;
