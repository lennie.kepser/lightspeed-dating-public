"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Player_1 = __importDefault(require("./Player"));
var createID_1 = __importDefault(require("./functions/createID"));
var GameSession_1 = __importDefault(require("./GameSession"));
var GamePhase_1 = require("../types/GamePhase");
var GameMode_1 = require("../types/GameMode");
var GameSessionMayhem_1 = __importDefault(require("./GameSessionMayhem"));
var GameSessionKevin_1 = __importDefault(require("./GameSessionKevin"));
var npmIo = require('@pm2/io');
var config_1 = __importDefault(require("../config/config"));
var queries_1 = require("../config/queries");
var Server = /** @class */ (function () {
    function Server(id, io, serverHandler, eos) {
        //Settings
        this.maxplayers = (0, config_1.default)().maxplayers;
        this.minplayers = (0, config_1.default)().minplayers;
        this.settings = {};
        //Player info
        this.players = {};
        this.lobbyvc = "";
        this.monsterChoiceArray = [];
        this.lastRounds = [];
        this.defaultQuestion = "casual";
        this.questionPacks = [this.defaultQuestion];
        this.id = id;
        this.io = io;
        this.eos = eos;
        this.serverHandler = serverHandler;
        for (var setting in (0, config_1.default)().default) {
            if ((0, config_1.default)().default.hasOwnProperty(setting)) {
                this.settings[setting] = (0, config_1.default)().default[setting];
            }
        }
        if (this.settings.lobbyvoicechat) {
            this.lobbyvc = this.id + "-" + (0, createID_1.default)(6);
        }
    }
    Server.prototype.getLastRounds = function () {
        return this.lastRounds;
    };
    Server.prototype.SetQuestionPacks = function (array) {
        var _this = this;
        this.questionPacks = [];
        array.forEach(function (element) {
            if (element in queries_1.queries) {
                _this.questionPacks.push(element);
            }
        });
        if (this.questionPacks.length === 0) {
            this.questionPacks.push(this.defaultQuestion);
        }
    };
    Server.prototype.getQuestionPacks = function () {
        return this.questionPacks;
    };
    Server.prototype.GetRoomTokens = function (roomID, players, muted) {
        if (muted === void 0) { muted = false; }
        return __awaiter(this, void 0, void 0, function () {
            var participants, i, newParticipant, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        participants = [];
                        for (i = 0; i < players.length; i++) {
                            newParticipant = {
                                puid: players[i].puid,
                                hardMuted: muted
                            };
                            newParticipant.clientIp = players[i].ip;
                            participants.push(newParticipant);
                        }
                        return [4 /*yield*/, this.eos.GetRoomTokens(roomID, participants)];
                    case 1:
                        result = _a.sent();
                        if (result == undefined) {
                            this.crash("Failed to get room tokens");
                        }
                        return [2 /*return*/, result];
                }
            });
        });
    };
    // Player actions
    Server.prototype.kill = function (player) {
        if (!this.gamesession || !this.gamesession.isPhase(GamePhase_1.GamePhase.Fight)) {
            return 1; // Not right game phase
        }
        return this.gamesession.kill(player);
    };
    Server.prototype.castJudgementVote = function (player, choice) {
        if (!this.gamesession || !this.gamesession.isPhase(GamePhase_1.GamePhase.Judgement)) {
            return 1;
        }
        return player.castJudgementVote(choice);
    };
    Server.prototype.chooseTripleRoom = function (player, choice) {
        if (!this.gamesession || !this.gamesession.isPhase(GamePhase_1.GamePhase.Fight)) {
            return 1; // Not right game phase
        }
        return this.gamesession.chooseTripleRoom(player, choice);
    };
    Server.prototype.spare = function (player) {
        if (!this.gamesession || !this.gamesession.isPhase(GamePhase_1.GamePhase.Fight)) {
            return 1; // Not right game phase
        }
        return this.gamesession.spare(player);
    };
    // Filters out spectating players
    Server.prototype.getAmountPlayingPlayers = function () {
        return Object.values(this.players).filter(function (e) { return !e.getPlayerSetting("spectating"); }).length;
    };
    Server.prototype.getAmountPlayers = function () {
        return Object.keys(this.players).length;
    };
    Server.prototype.addPlayer = function (socket, username, puid, ip, socketHandler) {
        var admin = false;
        if (this.getAmountPlayers() === 0) {
            admin = true;
        }
        this.players[socket.id] = new Player_1.default(socket, username, admin, puid, this, socketHandler);
        socket.join(this.id);
        return this.players[socket.id];
    };
    Server.prototype.sendRoundsToPlayers = function () {
        this.emitToAll("last rounds", { rounds: this.lastRounds });
    };
    Server.prototype.playReplay = function (id, position) {
        for (var i = 0; i < this.lastRounds.length; i++) {
            var round = this.lastRounds[i];
            if (round.id === id) {
                this.emitToAll("play replay", { id: id, position: position });
            }
        }
    };
    Server.prototype.setPostRoundData = function (id, maxTime) {
        for (var i = 0; i < this.lastRounds.length; i++) {
            if (this.lastRounds[i].id === id) {
                if (this.lastRounds[i].maxTimeInMS == 0 || this.lastRounds[i].maxTimeInMS > maxTime) {
                    this.lastRounds[i].maxTimeInMS = maxTime;
                }
            }
        }
        this.sendRoundsToPlayers();
    };
    Server.prototype.isUniqueRoundName = function (id) {
        for (var i = 0; i < this.lastRounds.length; i++) {
            if (this.lastRounds[i].id === id) {
                return false;
            }
        }
        return true;
    };
    Server.prototype.removePlayersLastRounds = function (username) {
        var initialLength = this.lastRounds.length;
        for (var i = this.lastRounds.length - 1; i >= 0; i--) {
            var round = this.lastRounds[i];
            for (var b = 0; b < round.players.length; b++) {
                if (round.players[b].username === username) {
                    this.lastRounds.splice(i, 1);
                    break;
                }
            }
        }
        if (this.lastRounds.length !== initialLength) {
            this.sendRoundsToPlayers();
        }
    };
    Server.prototype.addLastRound = function (voiceChatId, question, players) {
        var finalID = voiceChatId;
        var newRound = { players: [], question: question, id: finalID, maxTimeInMS: 0 };
        for (var _i = 0, players_1 = players; _i < players_1.length; _i++) {
            var player = players_1[_i];
            newRound.players.push({ monster: player.monster, username: player.getUsername(), disguise: player.getDisplayName(), socketid: player.getSocketID(), dead: false });
        }
        this.lastRounds.push(newRound);
    };
    Server.prototype.setPostRoundStatus = function (players, room) {
        for (var i = 0; i < this.lastRounds.length; i++) {
            if (this.lastRounds[i].id === room) {
                for (var _i = 0, players_2 = players; _i < players_2.length; _i++) {
                    var playerGet = players_2[_i];
                    for (var _a = 0, _b = this.lastRounds[i].players; _a < _b.length; _a++) {
                        var playerSet = _b[_a];
                        if (playerGet.getUsername() === playerSet.username) {
                            playerSet.dead = playerGet.dead;
                        }
                    }
                }
            }
        }
    };
    Server.prototype.RemoveGameSession = function () {
        var _a;
        (_a = this.gamesession) === null || _a === void 0 ? void 0 : _a.clearAllTimers();
        delete this.gamesession;
    };
    Server.prototype.removePlayer = function (player) {
        var _a;
        try {
            var newadmin = false;
            this.emitToAll("player leave", { username: player.getUsername() });
            // Check if new admin is necessary
            if (player.getAdmin() && this.getAmountPlayers() > 1) {
                newadmin = true;
            }
            // Delete player and remove from socketio room
            delete this.players[player.getSocketID()];
            // Do game related stuff:
            (_a = this.gamesession) === null || _a === void 0 ? void 0 : _a.removePlayer(player);
            this.removePlayersLastRounds(player.getUsername());
            // Delete server
            if (this.getAmountPlayers() === 0) {
                this.serverHandler.deleteServer(this.id);
                return;
            }
            // Assign new admin if necessary
            if (newadmin) {
                var keys = Object.keys(this.players);
                var player_1 = this.players[keys[Math.floor(Math.random() * keys.length)]];
                player_1.setAdmin(true);
                this.emitToAll("new admin", { admin: player_1.getUsername() });
            }
        }
        catch (e) {
            this.crash("Unexpected error removing player. Crashing server. Sorry!");
        }
    };
    Server.prototype.setSettings = function (player, settings) {
        try {
            if (!player.getAdmin()) {
                return 1;
            }
            for (var setting in settings) {
                if (!(setting in this.settings)) {
                    if ((0, config_1.default)().debug) {
                        console.log(setting, this.settings);
                    }
                    return 2;
                }
                if ((0, config_1.default)().constrains[setting].type === "number") {
                    // @ts-ignore
                    if (isNaN(settings[setting])) {
                        return 3;
                    }
                    this.settings[setting] = parseInt(settings[setting]);
                    if (this.settings[setting] < (0, config_1.default)().constrains[setting].min) {
                        this.settings[setting] = (0, config_1.default)().constrains[setting].min;
                        return 4;
                    }
                    if (this.settings[setting] > (0, config_1.default)().constrains[setting].max) {
                        this.settings[setting] = (0, config_1.default)().constrains[setting].max;
                        return 5;
                    }
                }
                if ((0, config_1.default)().constrains[setting].type === "bool") {
                    this.settings[setting] = (settings[setting] === "true");
                }
            }
            this.sendSettings();
        }
        catch (err) {
            console.error(err);
            return 99;
        }
        return 0;
    };
    Server.prototype.getClientSettings = function () {
        var outputSettings = {};
        for (var s in this.settings) {
            outputSettings[s] = this.settings[s].toString();
        }
        return outputSettings;
    };
    Server.prototype.sendSettings = function () {
        var outputSettings = this.getClientSettings();
        for (var id in this.players) {
            var player = this.players[id];
            if (player.getAdmin()) {
                continue;
            }
            player.emit("update settings", { settings: outputSettings, admin: player.getAdmin() });
        }
    };
    Server.prototype.joinable = function (username) {
        if (username == undefined) {
            return 4; // Invalid input
        }
        if (this.getAmountPlayers() >= this.maxplayers) {
            return 2; // Max players reached
        }
        for (var id in this.players) {
            if (this.players[id].getUsername() === username) {
                return 3; // Username not unique
            }
        }
        return 0; // Joinable
    };
    // Socket stuff
    Server.prototype.emitToAll = function (event, data, except) {
        for (var id in this.players) {
            if (this.players[id] !== except) {
                this.players[id].emit(event, data);
            }
        }
    };
    Server.prototype.crash = function (msg) {
        console.error("Server crashed", msg);
        this.emitToAll("crash", { msg: msg });
        this.serverHandler.deleteServer(this.id);
    };
    Server.prototype.startGame = function (player) {
        if (this.getAmountPlayingPlayers() < this.minplayers) {
            return 1; // Not enough players;
        }
        if (this.gamesession) {
            return 2; // Game already started;
        }
        if (!player.getAdmin()) {
            return 3; // Player does not have rights to start game;
        }
        var gameSessionClass = GameSession_1.default;
        if (this.settings.gameMode === GameMode_1.GameMode.Mayhem) {
            gameSessionClass = GameSessionMayhem_1.default;
        }
        else if (this.settings.gameMode === GameMode_1.GameMode.Kevin) {
            gameSessionClass = GameSessionKevin_1.default;
        }
        this.lastRounds = [];
        this.gamesession = new gameSessionClass(this, this.players);
        this.gamesession.startGame();
        Server.gamesStartedMetric.inc();
        if ((0, config_1.default)().debug) {
            console.log("starting game for server " + this.id);
        }
        return 0;
    };
    Server.prototype.endGame = function () {
        return __awaiter(this, void 0, void 0, function () {
            var id, info;
            return __generator(this, function (_a) {
                for (id in this.players) {
                    this.players[id].dead = false;
                    this.players[id].character = 0;
                    this.players[id].monster = false;
                    this.players[id].resetDisguise();
                    delete this.players[id].gamesession;
                    delete this.players[id].room;
                }
                info = this.getPlayerInfoArray();
                this.emitToAll("lobby", { info: info });
                if (this.settings.lobbyvoicechat) {
                    this.lobbyvc = this.id + "-" + (0, createID_1.default)(6);
                    this.sendPlayersToVCRoom(this.lobbyvc, Object.values(this.players));
                }
                if (this.settings.replays) {
                    this.sendRoundsToPlayers();
                    this.sendPlayersToVCRoom(this.lobbyvc + "-replays", Object.values(this.players));
                }
                delete this.gamesession;
                return [2 /*return*/];
            });
        });
    };
    Server.prototype.sendPlayersToVCRoom = function (roomid, players, muted) {
        if (muted === void 0) { muted = false; }
        return __awaiter(this, void 0, void 0, function () {
            var tokens, key, player;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.GetRoomTokens(roomid, players, muted)];
                    case 1:
                        tokens = _a.sent();
                        for (key in players) {
                            player = players[key];
                            player.emit("join voiceroom", {
                                baseurl: tokens.clientBaseUrl,
                                roomname: tokens.roomName,
                                token: tokens.participants[player.puid],
                                voiceskin: player.getVoiceSkin(),
                                echo: false,
                                muted: muted
                            });
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    // Game phase utility functions
    Server.prototype.toString = function () {
        return this.id;
    };
    // Getters and Setters
    Server.prototype.getMonsterChoiceID = function () {
        return this.monsterChoiceArray;
    };
    Server.prototype.setMonsterChoiceID = function (newVal) {
        this.monsterChoiceArray = newVal;
    };
    // "Dynamic"
    Server.prototype.getPlayerInfoArray = function () {
        var gameinfo = [];
        for (var id in this.players) {
            gameinfo.push(this.players[id].getPublicInfo());
        }
        return gameinfo;
    };
    Server.prototype.getPlayerFromUsername = function (username) {
        for (var id in this.players) {
            if (this.players[id].getUsername() === username) {
                return this.players[id];
            }
        }
        return undefined;
    };
    Server.prototype.getServerInfo = function () {
        var serverinfo = {
            serverid: this.id,
            players: [],
            gamestate: (this.gamesession ? this.gamesession.getPhase() : 0),
            times: (0, config_1.default)().times,
            lobbyvc: this.lobbyvc
        };
        serverinfo.times.fight = this.settings.roundtime;
        serverinfo.players = this.getPlayerInfoArray();
        return serverinfo;
    };
    Server.prototype.getUsername = function (id) {
        return this.players[id].getUsername();
    };
    // "Static"
    Server.prototype.getLobbyVC = function () {
        return this.lobbyvc;
    };
    Server.gamesStartedMetric = npmIo.counter({
        name: 'Games Started',
        unit: 'Games'
    });
    return Server;
}());
exports.default = Server;
