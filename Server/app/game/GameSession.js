"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var assignRooms_1 = __importDefault(require("./functions/assignRooms"));
var getSpectatorRooms_1 = __importDefault(require("./functions/getSpectatorRooms"));
var config_1 = __importDefault(require("../config/config"));
var GamePhase_1 = require("../types/GamePhase");
var RoomTriple_1 = __importDefault(require("./RoomTriple"));
var RoomJudgement_1 = __importDefault(require("./RoomJudgement"));
var GameMode_1 = require("../types/GameMode");
var assignRoles_1 = __importDefault(require("./functions/assignRoles"));
var getRandomQuery_1 = __importDefault(require("./functions/getRandomQuery"));
var GameSession = /** @class */ (function () {
    function GameSession(server, players) {
        this.humans = {};
        this.monsters = {};
        this.spectators = {};
        this.phase = GamePhase_1.GamePhase.Pregame;
        this.rooms = {};
        this.roomAssignmentFailureCounter = 0;
        this.timers = {
            endfight: 0,
            disguiseSelect: 0,
            charselect: 0,
            prejudgement: 0,
            judgement: {}
        };
        this.server = server;
        this.setUpPlayersAndRoles(players);
    }
    GameSession.prototype.setUpPlayersAndRoles = function (players) {
        (0, assignRoles_1.default)(players, this, this.server);
    };
    //TODO: Clean up
    GameSession.prototype.spare = function (player) {
        if (this.phase !== GamePhase_1.GamePhase.Fight) {
            return 1; // Not right game phase
        }
        var room = player.room;
        if (!room) {
            return 2; // Not in a room
        }
        if (room.hasSpared(player)) {
            return 3; // Already spared
        }
        var otherplayer = room.getOtherPlayer(player);
        if (this.playerIsMonster(otherplayer)) {
            player.die(true, false);
            player.emit("round end", { ending: 0, user1: otherplayer.getUsername(), user2: otherplayer.getDisplayName() });
            otherplayer.emit("round end", { ending: 2, user1: player.getUsername(), user2: otherplayer.getDisplayName() });
            this.deleteRoomAndCheckEndFight(room.roomid);
            return 0;
        }
        if (room.sparesLeft() > 1) {
            room.spare(player);
            otherplayer.emit("spared", {});
            if ((0, config_1.default)().debug) {
                console.log(player.getUsername() + " spared " + otherplayer.getUsername());
            }
        }
        else {
            this.deleteRoomAndCheckEndFight(room.roomid);
            player.emit("round end", { ending: 3, user1: otherplayer.getUsername(), user2: "" });
            otherplayer.emit("round end", { ending: 3, user1: player.getUsername(), user2: "" });
        }
        return 0;
    };
    GameSession.prototype.playerIsMonster = function (player) {
        return player.monster;
    };
    GameSession.prototype.chooseTripleRoom = function (player, choice) {
        var room = player.room;
        if (!room) {
            return 2; // Not in a room
        }
        if (!(room instanceof RoomTriple_1.default)) {
            return 4; // Not a valid triple room
        }
        if (!room.isDecider(player)) {
            return 3;
        }
        var notChosenPlayer = room.getTripleChoice(Math.abs(choice - 1));
        notChosenPlayer.die(true, true);
        var chosenPlayer = room.getTripleChoice(choice);
        if (this.playerIsMonster(chosenPlayer)) {
            notChosenPlayer.emit("round end", { ending: 4, user1: player.getUsername(), user2: chosenPlayer.getUsername() });
            player.emit("round end", { ending: 0, user1: chosenPlayer.getUsername(), user2: chosenPlayer.getDisplayName() });
            chosenPlayer.emit("round end", { ending: 2, user1: player.getUsername(), user2: notChosenPlayer.getDisplayName() });
            player.die(true, false);
        }
        else {
            notChosenPlayer.emit("round end", { ending: 5, user1: player.getUsername(), user2: chosenPlayer.getUsername() });
            player.emit("round end", { ending: 6, user1: chosenPlayer.getUsername(), user2: chosenPlayer.getDisplayName() });
            chosenPlayer.emit("round end", { ending: 3, user1: player.getUsername(), user2: "" });
        }
        this.deleteRoomAndCheckEndFight(room.roomid);
        return 0;
    };
    GameSession.prototype.isPlayerAllowedToKill = function (player) {
        return !player.monster && !player.dead;
    };
    GameSession.prototype.refreshQuestion = function (player) {
        if (player.room === undefined) {
            return 1;
        }
        var prevQuery = player.room.getQuery();
        var newQuery = (0, getRandomQuery_1.default)(this.server.getQuestionPacks(), [prevQuery]);
        player.room.setQuery(newQuery);
        player.room.emitToPlayers("new query", { newQuery: newQuery });
        return 0;
    };
    GameSession.prototype.kill = function (player) {
        var room = player.room;
        if (!room) {
            return 2; // Not in a room
        }
        if (!this.isPlayerAllowedToKill(player)) {
            return 3;
        }
        var killedPlayer = room.getOtherPlayer(player);
        killedPlayer.die(false, true);
        if (this.playerIsMonster(killedPlayer)) {
            killedPlayer.emit("round end", { ending: 5, user1: player.getUsername(), user2: killedPlayer.getDisplayName() });
            player.emit("round end", { ending: 6, user1: killedPlayer.getUsername(), user2: killedPlayer.getDisplayName() });
        }
        else {
            player.die(false, false);
            killedPlayer.emit("round end", { ending: 1, user1: player.getUsername(), user2: killedPlayer.getDisplayName() });
            player.emit("round end", { ending: 7, user1: killedPlayer.getUsername(), user2: killedPlayer.getDisplayName() });
        }
        this.deleteRoomAndCheckEndFight(room.roomid);
        return 0;
    };
    GameSession.prototype.checkEndFightConditions = function () {
        return (!this.areRoomsLeft() || this.getMonsterAmnt() === 0);
    };
    GameSession.prototype.deleteRoomAndCheckEndFight = function (roomid) {
        if ((0, config_1.default)().debug) {
            console.log("Deleting room " + roomid);
        }
        this.rooms[roomid].finalizeRoom();
        delete this.rooms[roomid];
        if (this.checkEndFightConditions()) {
            this.endFight();
        }
        else {
            if ((0, config_1.default)().debug) {
                this.printGameState();
            }
        }
    };
    GameSession.prototype.removePlayer = function (player) {
        if (!player.dead) {
            this.endGameOther(player.getUsername() + " has left. The game has been aborted.");
        }
        // TODO: Implement proper player leaving logic
    };
    GameSession.prototype.gameStartInfo = function (player) {
        if (player.monster) {
            return { gameinfo: this.server.getPlayerInfoArray() };
        }
        else {
            return { character: player.character, kevin: false };
        }
    };
    // Game phase events
    GameSession.prototype.startGame = function () {
        var _this = this;
        for (var id in this.monsters) {
            this.monsters[id].emit("monster start info", this.gameStartInfo(this.monsters[id]));
        }
        for (var id in this.humans) {
            this.humans[id].emit("game start info", this.gameStartInfo(this.humans[id]));
        }
        for (var id in this.spectators) {
            this.spectators[id].emit("game start info", { character: -1 });
        }
        this.phase = GamePhase_1.GamePhase.Pregame;
        this.printGameState();
        this.timers.charselect = setTimeout(function () {
            _this.startCharSelect();
        }, (0, config_1.default)().times.pregame * 1000);
        return 0;
    };
    GameSession.prototype.getGameMode = function () {
        return GameMode_1.GameMode.Classic;
    };
    GameSession.prototype.getAmountOfRooms = function () {
        return Object.keys(this.rooms).length;
    };
    GameSession.prototype.getAlivePlayers = function () {
        var output = [];
        for (var key in this.humans) {
            output.push(this.humans[key]);
        }
        for (var key in this.monsters) {
            output.push(this.monsters[key]);
        }
        return output;
    };
    GameSession.prototype.prepareJudgementRoom = function () {
        var _this = this;
        this.phase = GamePhase_1.GamePhase.CharacterSelect;
        var judgementRoom = new RoomJudgement_1.default(this.rooms, this.getAlivePlayers(), this.server, this);
        this.server.emitToAll("judgement preroom", { username: judgementRoom.getHuman().getUsername() });
        this.timers.prejudgement = setTimeout(function () {
            _this.startJudgementRoom(judgementRoom);
        }, (0, config_1.default)().times.prejudgementroom * 1000);
    };
    GameSession.prototype.startJudgementRoom = function (judgementRoom) {
        var _this = this;
        this.phase = GamePhase_1.GamePhase.Judgement;
        this.timers.judgement = {};
        judgementRoom.StartRoom();
        this.timers.judgement[judgementRoom.amount_turns] = setTimeout(function () {
            _this.endJudgementRoom(judgementRoom);
        }, judgementRoom.roundTimeAll * 1000);
    };
    GameSession.prototype.endJudgementRoom = function (judgementRoom) {
        if (!this.isPhase(GamePhase_1.GamePhase.Judgement)) {
            return;
        }
        this.clearAllTimers();
        judgementRoom.fightTimeOut();
        this.deleteRoomAndCheckEndFight(judgementRoom.getRoomID());
    };
    GameSession.prototype.startCharSelect = function () {
        var _this = this;
        this.phase = GamePhase_1.GamePhase.CharacterSelect;
        var playerInfo = this.server.getPlayerInfoArray();
        for (var id in this.spectators) {
            this.spectators[id].emit("charselect spectator", {});
        }
        for (var id in this.monsters) {
            this.monsters[id].resetDisguise();
            this.monsters[id].emit("monster char select", { gameinfo: playerInfo, gameMode: this.getGameMode() });
        }
        for (var id in this.humans) {
            this.humans[id].emit("charselect", {});
        }
        this.printGameState();
        this.timers.disguiseSelect = setTimeout(function () {
            _this.startFight();
        }, (0, config_1.default)().times.disguiseselect * 1000);
    };
    GameSession.prototype.assignRandomDisguise = function (monster) {
        var playerArray = this.humans;
        var player = playerArray[Object.keys(playerArray)[Math.floor(Math.random() * Object.keys(playerArray).length)]];
        monster.pickDisguise(player.getUsername());
    };
    GameSession.prototype.assignPlayersToRooms = function () {
        return (0, assignRooms_1.default)(this.humans, this.monsters, this.server);
    };
    GameSession.prototype.startFight = function () {
        var _this = this;
        try {
            if (this.phase !== GamePhase_1.GamePhase.CharacterSelect) {
                return;
            }
            this.clearAllTimers();
            if ((0, config_1.default)().debug) {
                console.log("Start fight");
            }
            //If monster is undisguised pick random disguise:
            for (var id in this.monsters) {
                if (this.monsters[id].notDisguised()) {
                    if ((0, config_1.default)().debug) {
                        console.log(this.monsters[id].getUsername() + " has not picked a disguise. Assigning randomly:");
                    }
                    this.assignRandomDisguise(this.monsters[id]);
                }
            }
            this.phase = GamePhase_1.GamePhase.Fight;
            this.rooms = this.assignPlayersToRooms();
            if (this.rooms === undefined) {
                this.server.crash("Error assigning rooms. Server crashed. Sorry!");
                return;
            }
            if (Object.keys(this.rooms).length === 0) {
                if (this.roomAssignmentFailureCounter > 3) {
                    this.endGameOther("Room assignment has failed three times. Aborting game.");
                    return;
                }
                this.roomAssignmentFailureCounter += 1;
                this.server.emitToAll("player assignment failure", {});
                setTimeout(function () {
                    _this.startCharSelect();
                }, 3000);
                return;
            }
            this.roomAssignmentFailureCounter = 0;
            this.printGameState();
            //Give dead players and certain monsters ability to spectate
            this.setUpSpectators();
            this.timers.endfight = setTimeout(function () {
                _this.endFight();
            }, this.server.settings.roundtime * 1000);
        }
        catch (e) {
            console.error(this.server.id + ": CRASH! Error starting fight.");
            console.error(e.stack);
            this.server.crash("An Unexpected Error occured while starting the fight. Crashing server. Sorry!");
        }
    };
    GameSession.prototype.endFight = function () {
        if (this.phase !== GamePhase_1.GamePhase.Fight && this.phase !== GamePhase_1.GamePhase.Judgement) {
            return;
        }
        if ((0, config_1.default)().debug) {
            console.log("Ending fight");
        }
        this.clearAllTimers();
        for (var id in this.rooms) {
            this.rooms[id].fightTimeOut();
            this.rooms[id].finalizeRoom();
            delete this.rooms[id];
        }
        this.printGameState();
        // Randomize voices if enabled
        if (this.server.settings.randomvoices) {
            this.randomizePlayerCharacters();
        }
        this.checkGameEndOrNextRound();
    };
    GameSession.prototype.checkGameEndOrNextRound = function () {
        if (this.getHumansAmnt() === 1 && this.getMonsterAmnt() === 1 && Math.random() <= this.server.settings.judgementchance) {
            // Skip player select
            this.prepareJudgementRoom();
        }
        else if (this.getHumansAmnt() > 0 && this.getMonsterAmnt() > 0) {
            //Game continues
            this.startCharSelect();
        }
        else {
            this.endGame();
        }
    };
    GameSession.prototype.clearAllTimers = function () {
        for (var key in this.timers) {
            if (key === "judgement") {
                for (var timerid in this.timers[key]) {
                    clearTimeout(this.timers[key][timerid]);
                }
            }
            else {
                clearTimeout(this.timers[key]);
            }
        }
    };
    GameSession.prototype.endGameOther = function (reason) {
        this.clearAllTimers();
        Object.values(this.rooms).forEach(function (room) { return room.finalizeRoom(); });
        if ((0, config_1.default)().debug) {
            console.log("Round end due to " + reason);
        }
        this.server.emitToAll("endgame other", { reason: reason });
        this.server.endGame();
    };
    GameSession.prototype.emitWinner = function () {
        var humanswin = (this.getMonsterAmnt() === 0);
        if ((0, config_1.default)().debug) {
            if (humanswin) {
                console.log("Humans win.");
            }
            else {
                console.log("Monsters win.");
            }
        }
        this.server.emitToAll("endgame", { monster: !humanswin });
    };
    GameSession.prototype.endGame = function () {
        Object.values(this.rooms).forEach(function (room) { return room.finalizeRoom(); });
        this.emitWinner();
        this.clearAllTimers();
        this.server.endGame();
    };
    //Utility
    GameSession.prototype.checkFightStart = function () {
        for (var id in this.monsters) {
            if (!this.monsters[id].hasPicked()) {
                return;
            }
        }
        this.startFight();
    };
    GameSession.prototype.isAllowedToSpectate = function (player) {
        return player.isSkipping();
    };
    GameSession.prototype.setUpSpectators = function () {
        var rooms = (0, getSpectatorRooms_1.default)(this.rooms);
        for (var id in this.spectators) {
            this.spectators[id].emit("spectator rooms", { rooms: rooms });
        }
        for (var id in this.monsters) {
            if (this.isAllowedToSpectate(this.monsters[id])) {
                this.monsters[id].emit("spectator rooms", { rooms: rooms });
            }
        }
    };
    GameSession.prototype.getSpectators = function () {
        return this.spectators;
    };
    GameSession.prototype.printGameState = function () {
        if (!(0, config_1.default)().debug) {
            return;
        }
        console.log("Game Phase: " + this.phase + " Players alive: " + (this.getHumansAmnt()) + " Monsters Alive: " + this.getMonsterAmnt());
        for (var id in this.humans) {
            console.log(this.humans[id].getPrintInfo());
        }
        for (var id in this.monsters) {
            console.log(this.monsters[id].getPrintInfo());
        }
        for (var id in this.spectators) {
            console.log(this.spectators[id].getPrintInfo());
        }
        for (var id in this.rooms) {
            this.rooms[id].printInfo();
        }
    };
    GameSession.prototype.randomizePlayerCharacters = function () {
        for (var id in this.humans) {
            this.humans[id].randomCharacter();
        }
    };
    GameSession.prototype.deletePlayerFromPool = function (player) {
        for (var id in this.spectators) {
            if (this.spectators[id].getUsername() === player.getUsername()) {
                delete this.spectators[id];
            }
        }
        for (var id in this.monsters) {
            if (this.monsters[id].getUsername() === player.getUsername()) {
                delete this.monsters[id];
            }
        }
        for (var id in this.humans) {
            if (this.humans[id].getUsername() === player.getUsername()) {
                delete this.humans[id];
            }
        }
    };
    GameSession.prototype.makeSpectator = function (player) {
        this.deletePlayerFromPool(player);
        this.spectators[player.getSocketID()] = player;
    };
    GameSession.prototype.isSpectator = function (player) {
        return !!this.spectators[player.getSocketID()];
    };
    GameSession.prototype.areRoomsLeft = function () {
        return (Object.keys(this.rooms).length !== 0);
    };
    GameSession.prototype.getMonsterAmnt = function () {
        return Object.keys(this.monsters).length;
    };
    GameSession.prototype.getHumansAmnt = function () {
        return Object.keys(this.humans).length;
    };
    GameSession.prototype.isPhase = function (phase) {
        return phase == this.phase;
    };
    //Getters and Setters
    GameSession.prototype.addHuman = function (player) {
        this.humans[player.getSocketID()] = player;
    };
    GameSession.prototype.addSpectator = function (player) {
        this.spectators[player.getSocketID()] = player;
    };
    GameSession.prototype.addMonster = function (player) {
        this.monsters[player.getSocketID()] = player;
    };
    GameSession.prototype.getPhase = function () {
        return this.phase;
    };
    return GameSession;
}());
exports.default = GameSession;
