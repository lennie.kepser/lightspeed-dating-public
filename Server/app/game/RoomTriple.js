"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../config/config"));
var shuffle_1 = __importDefault(require("./functions/shuffle"));
var Room_1 = __importDefault(require("./Room"));
var RoomTriple = /** @class */ (function (_super) {
    __extends(RoomTriple, _super);
    function RoomTriple(targetRoomDict, players, server, gameSession) {
        var _this = _super.call(this, targetRoomDict, players, server, gameSession) || this;
        _this.decidees = [];
        var gamesession = _this.server.gamesession;
        // Find monster
        for (var _i = 0, _a = _this.players; _i < _a.length; _i++) {
            var player = _a[_i];
            if (gamesession.playerIsMonster(player)) {
                _this.monster = player;
                _this.decidees.push(player);
            }
        }
        if (!_this.monster) {
            server.crash("Error assigning triple room players");
            return _this;
        }
        for (var _b = 0, _c = _this.players; _b < _c.length; _b++) {
            var player = _c[_b];
            if (player.getUsername() === _this.monster.getDisplayName()) {
                _this.decidees.push(player);
                _this.human = player;
            }
            else if (!gamesession.playerIsMonster(player)) {
                _this.decider = player;
            }
        }
        (0, shuffle_1.default)(_this.decidees);
        if (_this.decidees.length < 2 || !_this.decider || !_this.human) {
            server.crash("Error assigning triple room players");
            return _this;
        }
        return _this;
    }
    RoomTriple.prototype.isDecider = function (player) {
        return player === this.decider;
    };
    RoomTriple.prototype.isConfiguredTripleRoom = function () {
        return (this.decidees.length == 2);
    };
    RoomTriple.prototype.getTripleChoice = function (choice) {
        return this.decidees[choice];
    };
    RoomTriple.prototype.fightTimeOut = function () {
        var _this = this;
        this.players.forEach(function (player) {
            if (!(_this.server.gamesession.playerIsMonster(player))) {
                player.die(true, false);
            }
        });
    };
    RoomTriple.prototype.getIdentifier = function (player) {
        var output = "";
        this.decidees.forEach(function (thisPlayer, index) {
            if (player == thisPlayer) {
                output = ["A", "B"][index];
            }
        });
        return output;
    };
    RoomTriple.prototype.socketEmit = function () {
        var _this = this;
        var _a;
        this.sendToVcRoom();
        var voiceskin = (_a = this.human) === null || _a === void 0 ? void 0 : _a.getVoiceSkin();
        this.decider.emit("in room triple", {
            decider: true,
            query: this.query,
            time: (0, config_1.default)().times.fight,
            recording: this.voicechatid,
            voiceskin: -1,
            name: this.decidees[0].getDisplayName(),
            otherPlayerSkin: voiceskin,
            puids: [this.decidees[0].puid, this.decidees[1].puid]
        });
        this.decidees.forEach(function (player, index) {
            player.emit("in room triple", {
                decider: false,
                query: _this.query,
                time: (0, config_1.default)().times.fight,
                recording: _this.voicechatid,
                voiceskin: voiceskin,
                otherPlayerSkin: voiceskin,
                name: _this.human.getDisplayName() + " " + ["A", "B"][index],
                nameDecider: _this.decider.getDisplayName()
            });
        });
    };
    return RoomTriple;
}(Room_1.default));
exports.default = RoomTriple;
