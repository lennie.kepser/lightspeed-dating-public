"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var createID_1 = __importDefault(require("./createID"));
var getRandomQuery_1 = __importDefault(require("./getRandomQuery"));
function putInRoom(players, roomClass, rooms, server) {
    var serverid = server.id;
    if (players.length < 2) {
        return;
    }
    var voicechatid = "";
    var idisunique = false;
    while (!idisunique) {
        voicechatid = (0, createID_1.default)(6);
        idisunique = server.isUniqueRoundName(serverid + "-" + voicechatid);
    }
    var query = (0, getRandomQuery_1.default)();
    var roomid = Object.keys(rooms).length;
    rooms[roomid] = new roomClass(roomid, players, serverid + "-" + voicechatid, query, server);
    players.forEach(function (player) {
        player.room = rooms[roomid];
    });
    server.addLastRound(serverid + "-" + voicechatid, query, players);
    return rooms[roomid];
}
exports.default = putInRoom;
