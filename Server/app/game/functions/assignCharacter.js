"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getCharacterFromPlayer(player) {
    var characterChoices = player.getCharacterChoices();
    var char = characterChoices[Math.floor(Math.random() * characterChoices.length)];
    return char;
}
exports.default = getCharacterFromPlayer;
