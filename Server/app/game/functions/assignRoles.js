"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../config/config"));
var assignCharacter_1 = __importDefault(require("./assignCharacter"));
function chooseMonster(players, server) {
    var monsterChoiceArray = server.getMonsterChoiceID();
    monsterChoiceArray = monsterChoiceArray.filter(function (id) { return players.includes(id); });
    var stabilisation_factor = 0.4;
    var _loop_1 = function (playerID) {
        var addAmount = Math.ceil(monsterChoiceArray.filter(function (x) { return x == playerID; }).length * stabilisation_factor + 1);
        for (var i = 0; i < addAmount; i++) {
            monsterChoiceArray.push(playerID);
        }
    };
    for (var _i = 0, players_1 = players; _i < players_1.length; _i++) {
        var playerID = players_1[_i];
        _loop_1(playerID);
    }
    var chosenMonster = monsterChoiceArray[Math.floor(Math.random() * monsterChoiceArray.length)];
    monsterChoiceArray = monsterChoiceArray.filter(function (x) { return x != chosenMonster; });
    server.setMonsterChoiceID(monsterChoiceArray);
    return chosenMonster;
}
function assignRoles(players, gamesession, server) {
    var playercount = Object.keys(players).length;
    var monsteramnt = (0, config_1.default)().roles[playercount];
    var characters = (0, config_1.default)().characters;
    var unassignedplayers = [];
    for (var id in players) {
        if (players[id].getPlayerSetting("spectating")) {
            gamesession.addSpectator(players[id]);
            players[id].dead = true;
            players[id].monster = false;
            players[id].gamesession = gamesession;
            players[id].character = -1;
            continue;
        }
        unassignedplayers.push(id);
    }
    var _loop_2 = function (i) {
        var id = chooseMonster(unassignedplayers, server);
        players[id].monster = true;
        players[id].dead = false;
        players[id].character = -1;
        players[id].gamesession = gamesession;
        unassignedplayers = unassignedplayers.filter(function (value) { return value !== id; });
        gamesession.addMonster(players[id]);
    };
    for (var i = 0; i < monsteramnt; i++) {
        _loop_2(i);
    }
    for (var i = 0; i < unassignedplayers.length; i++) {
        var player = players[unassignedplayers[i]];
        player.character = (0, assignCharacter_1.default)(player);
        player.monster = false;
        player.dead = false;
        player.gamesession = gamesession;
        gamesession.addHuman(player);
    }
}
exports.default = assignRoles;
