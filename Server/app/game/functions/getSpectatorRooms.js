"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getSpectatorRooms(rooms) {
    var output = [];
    for (var id in rooms) {
        output.push(rooms[id].getInfoObj());
    }
    return output;
}
exports.default = getSpectatorRooms;
