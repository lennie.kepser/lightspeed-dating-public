"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var queries_1 = require("../../config/queries");
function getRandomQuery(questionPacks, ignoreArray) {
    var fullQueryList = [];
    questionPacks.forEach(function (element) {
        fullQueryList = fullQueryList.concat(queries_1.queries[element]);
    });
    var filteredQueryList = [];
    fullQueryList.forEach(function (elem) {
        if (!(elem in ignoreArray)) {
            filteredQueryList.push(elem);
        }
    });
    return filteredQueryList[Math.floor(Math.random() * filteredQueryList.length)];
}
exports.default = getRandomQuery;
