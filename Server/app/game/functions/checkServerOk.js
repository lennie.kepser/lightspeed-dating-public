"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function checkServerOK(server, username) {
    if (server == undefined) {
        return 1; // Server not found
    }
    var join = server.joinable(username);
    if (join != 0) {
        return join;
    }
    return 0;
}
exports.default = checkServerOK;
