"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function usernameValid(username) {
    if (typeof username !== "string") {
        return false;
    }
    if (username.length < 2) {
        return false;
    }
    if (username.length > 32) {
        return false;
    }
    return true;
}
exports.default = usernameValid;
