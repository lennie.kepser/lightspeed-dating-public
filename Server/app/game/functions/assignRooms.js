"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Room_1 = __importDefault(require("../Room"));
var shuffle_1 = __importDefault(require("./shuffle"));
var RoomTriple_1 = __importDefault(require("../RoomTriple"));
function assignRooms(humans, monsters, server) {
    try {
        var rooms_1 = {};
        var humansleft_1 = (0, shuffle_1.default)(Object.values(humans));
        var monstersleft_1 = (0, shuffle_1.default)(Object.values(monsters));
        var couples = [];
        // Assign monsters that haven't picked a target a target.
        monstersleft_1.forEach(function (monster) {
            if (monster.getTargetPlayer() === "") {
                if (Math.random() > 0.5) {
                    monster.setTargetPlayerUnsafe("!skip");
                }
                else {
                    humansleft_1.forEach(function (human) {
                        // Check if another monster is targeting that human
                        if (monstersleft_1.filter(function (monster2) { return monster2.getTargetPlayer() === human.username; }).length == 0 && human.username !== monster.getDisplayName()) {
                            monster.setTargetPlayerUnsafe(human.username);
                        }
                    });
                }
            }
            // If there was no target left, skip round
            if (monster.getTargetPlayer() === "") {
                monster.setTargetPlayerUnsafe("!skip");
            }
        });
        monstersleft_1 = monstersleft_1.filter(function (monster) { return !monster.isSkipping(); });
        var _loop_1 = function (i) {
            var monster = monstersleft_1[i];
            var target = server.getPlayerFromUsername(monster.getTargetPlayer());
            if (target) {
                couples.push([monster, target]);
                monstersleft_1.splice(i, 1);
                humansleft_1 = humansleft_1.filter(function (human) { return human.username != target.getUsername(); });
            }
            else {
                console.error("Player " + monster.getTargetPlayer() + " targeted by " + monster.username + " not found.");
            }
        };
        for (var i = monstersleft_1.length - 1; i >= 0; i--) {
            _loop_1(i);
        }
        for (var i = 0; i + 1 < humansleft_1.length; i += 2) {
            couples.push([humansleft_1[i], humansleft_1[i + 1]]);
        }
        humansleft_1.splice(0, humansleft_1.length - (humansleft_1.length % 2));
        if (humansleft_1.length > 1) {
            throw "Humans not assigned to rooms";
        }
        else if (humansleft_1.length === 1) {
            // Try to create a three person room
            var remainingPlayer = humansleft_1[0];
            for (var _i = 0, couples_1 = couples; _i < couples_1.length; _i++) {
                var room = couples_1[_i];
                var monster = void 0;
                var human = void 0;
                for (var i = 0; i < room.length; i++) {
                    if (room[i].monster) {
                        monster = room[i];
                    }
                    else {
                        human = room[i];
                    }
                }
                if (monster && human) {
                    if (monster.getDisplayName() === remainingPlayer.username) {
                        room.push(remainingPlayer);
                        humansleft_1.shift();
                        break;
                    }
                }
            }
        }
        couples.forEach(function (couple) {
            var roomClass = Room_1.default;
            if (couple.length > 2) {
                roomClass = RoomTriple_1.default;
            }
            new roomClass(rooms_1, couple, server, server.gamesession);
        });
        // If there's still humans left, make them alone
        for (var i = 0; i < humansleft_1.length; i++) {
            humansleft_1[i].emit("alone", {});
        }
        for (var id in rooms_1) {
            rooms_1[id].socketEmit();
        }
        return rooms_1;
    }
    catch (err) {
        console.error(server.toString() + ": CRASH! Error assigning rooms.");
        console.error(err);
        console.error(err.stackTrace);
        return undefined;
    }
}
exports.default = assignRooms;
