"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../../config/config"));
var Room_1 = __importDefault(require("../Room"));
var RoomTriple_1 = __importDefault(require("../RoomTriple"));
var shuffle_1 = __importDefault(require("./shuffle"));
function findThreePlayerRooms(players) {
    for (var i = 0; i < players.length; i++) {
        for (var b = i + 1; b < players.length; b++) {
            if (players[i].getDisplayName() === players[b].getUsername() && players[b].getUsername() === players[b].getDisplayName()) {
                for (var c = 0; c < players.length; c++) {
                    if (c === i || b == c) {
                        continue;
                    }
                    // We make the third player disguised as themselves. In the future this could be changed depending how confusing this is for the players.
                    players[c].setDisguise(players[c].getUsername());
                    return [players[i], players[b], players[c]];
                }
            }
        }
    }
    return [];
}
function findTwoPlayerRoom(players) {
    for (var i = 0; i < players.length; i++) {
        for (var b = i + 1; b < players.length; b++) {
            if (players[i].getDisplayName() !== players[b].getDisplayName()
                && players[i].getUsername() !== players[b].getDisplayName()
                && players[i].getDisplayName() !== players[b].getUsername()
                && players[i].getUsername() !== players[b].getUsername()) {
                return [players[i], players[b]];
            }
        }
    }
    return [];
}
function assignRoomsMayhem(players, server) {
    var rooms = {};
    if ((0, config_1.default)().debug) {
        console.log("Assign rooms mayhem");
    }
    (0, shuffle_1.default)(players);
    var couples = [];
    // Three player room
    var continueLoop = true;
    var _loop_1 = function () {
        var playerObjects = findThreePlayerRooms(players);
        if (playerObjects.length === 0) {
            continueLoop = false;
        }
        else {
            players = players.filter(function (a) { return !playerObjects.includes(a); });
            couples.push(playerObjects);
        }
    };
    while (continueLoop) {
        _loop_1();
    }
    continueLoop = true;
    var _loop_2 = function () {
        var playerObjects = findTwoPlayerRoom(players);
        if (playerObjects.length === 0) {
            continueLoop = false;
        }
        else {
            players = players.filter(function (a) { return !playerObjects.includes(a); });
            couples.push(playerObjects);
        }
    };
    while (continueLoop) {
        _loop_2();
    }
    couples.forEach(function (couple) {
        var roomClass = Room_1.default;
        if (couple.length > 2) {
            roomClass = RoomTriple_1.default;
        }
        new roomClass(rooms, couple, server, server.gamesession);
    });
    for (var id in players) {
        players[id].setSkip();
    }
    for (var id in rooms) {
        rooms[id].socketEmit();
    }
    return rooms;
}
exports.default = assignRoomsMayhem;
