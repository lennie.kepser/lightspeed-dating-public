"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("../config/config"));
var GameMode_1 = require("../types/GameMode");
var getRandomQuery_1 = __importDefault(require("./functions/getRandomQuery"));
var Room_1 = __importDefault(require("./Room"));
var RoomJudgement = /** @class */ (function (_super) {
    __extends(RoomJudgement, _super);
    function RoomJudgement(targetRoomDict, players, server, gameSession) {
        var _this = _super.call(this, targetRoomDict, players, server, gameSession) || this;
        _this.judgement_turn = "B";
        _this.roundTimeAll = Math.floor(_this.server.settings.roundtime * (0, config_1.default)().judgement_room_scale);
        _this.amount_turns = (0, config_1.default)().judgement_amount_turns;
        _this.timePerTurn = Math.floor(_this.roundTimeAll / _this.amount_turns);
        _this.spectatorVotes = {};
        _this.spectators = {};
        var gameMode = gameSession.getGameMode();
        if (gameMode === GameMode_1.GameMode.Mayhem) {
            var shuffledPlayers = _this.players;
            _this.human = shuffledPlayers[0];
            _this.monster = shuffledPlayers[1];
            _this.monster.setDisguise(_this.human.getUsername());
            _this.human.setDisguise(_this.human.getUsername());
        }
        else {
            for (var _i = 0, players_1 = players; _i < players_1.length; _i++) {
                var player = players_1[_i];
                if (!player.monster) {
                    _this.human = player;
                }
                else {
                    _this.monster = player;
                }
            }
        }
        if (!_this.human || !_this.monster) {
            _this.server.crash("Error creating Judgement Room.");
            return _this;
        }
        _this.spectators = gameSession.getSpectators();
        for (var key in _this.spectators) {
            _this.spectatorVotes[key] = "";
            _this.spectators[key].room = _this;
        }
        return _this;
    }
    RoomJudgement.prototype.StartRoom = function () {
        var _this = this;
        this.server.sendPlayersToVCRoom(this.getVoiceChatID(), Object.values(this.spectators), true);
        this.sendToVcRoom();
        for (var i = 0; i < this.amount_turns; i++) {
            this.gameSession.timers.judgement[i] = setTimeout(function () {
                _this.socketEmit();
            }, (i * this.timePerTurn * 1000) + 1);
        }
    };
    RoomJudgement.prototype.finalizeRoom = function () {
        _super.prototype.finalizeRoom.call(this);
        for (var key in this.spectators) {
            this.spectators[key].room = undefined;
        }
    };
    RoomJudgement.prototype.vote = function (player, choice) {
        this.spectatorVotes[player.getSocketID()] = choice;
        this.sendVotes();
    };
    RoomJudgement.prototype.sendVotes = function () {
        var votesA = 0;
        var votesB = 0;
        for (var id in this.spectatorVotes) {
            if (this.spectatorVotes[id] === 'A') {
                votesA++;
            }
            else if (this.spectatorVotes[id] === 'B') {
                votesB++;
            }
        }
        this.server.emitToAll('judgement votes', { votesA: votesA, votesB: votesB });
    };
    RoomJudgement.prototype.fightTimeOut = function () {
        var _a;
        // if postive player As team wins, if negative player Bs team, if 0 monsters win.
        var votes = 0;
        for (var id in this.spectatorVotes) {
            if (this.spectatorVotes[id] === "A") {
                votes++;
            }
            if (this.spectatorVotes[id] === "B") {
                votes--;
            }
        }
        if (votes === 0) {
            (_a = this.human) === null || _a === void 0 ? void 0 : _a.die(true, true);
        }
        if (votes > 0) {
            this.players[0].die((this.players[0] === this.human), true);
        }
        else {
            this.players[1].die((this.players[1] === this.human), true);
        }
    };
    RoomJudgement.prototype.getHuman = function () {
        if (!this.human) {
            this.server.crash("Tried to access judgement room human parameter before assigned");
        }
        return this.human;
    };
    RoomJudgement.prototype.socketEmit = function () {
        this.query = (0, getRandomQuery_1.default)(this.server.getQuestionPacks(), []);
        this.judgement_turn = (this.judgement_turn === "A" ? "B" : "A");
        var turn = this.judgement_turn;
        if (!this.human) {
            this.server.crash("Judgement Room emited without human role assigned.");
            return;
        }
        for (var id in this.spectators) {
            this.spectators[id].emit("judgement room spectator", { turn: turn, timePerTurn: this.timePerTurn, query: this.query, name: this.human.getUsername(), character: this.human.character, roundTimeAll: this.roundTimeAll });
        }
        this.players[0].emit("judgement room player", { roundTimeAll: this.roundTimeAll, type: "A", timePerTurn: this.timePerTurn, turn: turn, voiceskin: this.human.character, query: this.query, name: this.human.getUsername(), recordingID: this.voicechatid });
        this.players[1].emit("judgement room player", { roundTimeAll: this.roundTimeAll, type: "B", timePerTurn: this.timePerTurn, turn: turn, voiceskin: this.human.character, query: this.query, name: this.human.getUsername(), recordingID: this.voicechatid });
    };
    return RoomJudgement;
}(Room_1.default));
exports.default = RoomJudgement;
