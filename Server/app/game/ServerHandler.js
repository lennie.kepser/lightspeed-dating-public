"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Server_1 = __importDefault(require("./Server"));
var checkServerOk_1 = __importDefault(require("./functions/checkServerOk"));
var createID_1 = __importDefault(require("./functions/createID"));
var EOS_1 = __importDefault(require("./EOS"));
var npmIo = require('@pm2/io');
var config_1 = __importDefault(require("../config/config"));
var ServerHandler = /** @class */ (function () {
    function ServerHandler(io) {
        var _this = this;
        this.playerMetric = npmIo.metric({
            name: 'Online Players',
            unit: 'People'
        });
        this.serverMetric = npmIo.metric({
            name: 'Active Servers',
            unit: 'Servers'
        });
        this.servers = {};
        this.io = io;
        this.eos = new EOS_1.default();
        setInterval(function () {
            var players = 0;
            for (var serv in _this.servers) {
                players += _this.servers[serv].getServerInfo().players.length;
            }
            _this.playerMetric.set(players);
            _this.serverMetric.set(Object.keys(_this.servers).length);
        }, 20000);
    }
    ServerHandler.prototype.getMaxServerAmount = function () {
        return (0, config_1.default)().maxServers;
    };
    ServerHandler.prototype.getServerAmount = function () {
        return Object.keys(this.servers).length;
    };
    ServerHandler.prototype.createServer = function (id) {
        var final_id;
        if (id === undefined) {
            final_id = (0, createID_1.default)((0, config_1.default)().codelength);
            while (this.servers[final_id]) {
                final_id = (0, createID_1.default)((0, config_1.default)().codelength);
            }
        }
        else {
            final_id = id;
        }
        this.servers[final_id] = new Server_1.default(final_id, this.io, this, this.eos);
        if ((0, config_1.default)().debug) {
            console.log("Created server: " + final_id);
        }
        return final_id;
    };
    //Returns: 1: Server not found, 2: Server full, 3: Username not unique, 4: puid invalid, 5: Unknown,  0: Success
    ServerHandler.prototype.joinServer = function (data, socket, sockethandler) {
        var _a;
        try {
            var id = data.room;
            var puid = data.puid;
            if (puid == undefined || typeof puid !== "string") {
                return 4;
            }
            var username = data.username;
            var server = this.servers[id];
            var serverok = (0, checkServerOk_1.default)(server, username);
            if (serverok !== 0) {
                return serverok;
            }
            sockethandler.player = server.addPlayer(socket, username, puid, data.ip, sockethandler);
            server.emitToAll("player join", (_a = sockethandler.player) === null || _a === void 0 ? void 0 : _a.getPublicInfo(), sockethandler.player);
            return 0;
        }
        catch (err) {
            console.error("Error joining player " + data);
            console.error(err);
            return 5;
        }
    };
    ServerHandler.prototype.getServerInfo = function (serverid) {
        var server = this.servers[serverid];
        if (server === undefined) {
            return {};
        }
        return server.getServerInfo();
    };
    ServerHandler.prototype.leaveServer = function (serverid, player) {
        var server = this.servers[serverid];
        if (server === undefined) {
            return;
        }
        if ((0, config_1.default)().debug) {
            console.log(player.getUsername() + " left server " + serverid);
        }
        server.removePlayer(player);
    };
    ServerHandler.prototype.deleteServer = function (serverid) {
        // Dont delete server if its a testing server
        if (serverid.length === 1)
            return;
        if ((0, config_1.default)().debug) {
            console.log("Deleting Server " + serverid);
        }
        this.servers[serverid].RemoveGameSession();
        delete this.servers[serverid];
    };
    ServerHandler.prototype.getServer = function (serverid) {
        return this.servers[serverid];
    };
    return ServerHandler;
}());
exports.default = ServerHandler;
