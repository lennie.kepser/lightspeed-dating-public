"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var getRandomQuery_1 = __importDefault(require("./functions/getRandomQuery"));
var createID_1 = __importDefault(require("./functions/createID"));
var config_1 = __importDefault(require("../config/config"));
var Room = /** @class */ (function () {
    function Room(targetRoomDict, players, server, gameSession) {
        var _this = this;
        this.spared = {};
        this.query = (0, getRandomQuery_1.default)(server.getQuestionPacks(), []);
        this.players = players;
        this.server = server;
        this.gameSession = gameSession;
        this.voicechatid = this.getNewUniquieRoomVCID(server);
        this.roomid = this.putInRoomObjectAndReturnID(targetRoomDict);
        for (var i = 0; i < players.length; i++) {
            this.spared[players[i].getUsername()] = false;
        }
        players.forEach(function (player) {
            player.room = _this;
        });
        server.addLastRound(this.voicechatid, this.query, players);
    }
    Room.prototype.getRoomID = function () {
        return this.roomid;
    };
    Room.prototype.putInRoomObjectAndReturnID = function (roomDict) {
        var keysOfRoomDictAsNumb = Object.keys(roomDict).map(function (e) { return parseInt(e); });
        var roomid;
        if (keysOfRoomDictAsNumb.length === 0) {
            roomid = "0";
        }
        else {
            roomid = (Math.max.apply(Math, keysOfRoomDictAsNumb) + 1).toString();
        }
        roomDict[roomid] = this;
        return roomid;
    };
    Room.prototype.getNewUniquieRoomVCID = function (server) {
        var outputID = "";
        var iDisUnique = false;
        while (!iDisUnique) {
            outputID = (0, createID_1.default)(6);
            iDisUnique = server.isUniqueRoundName(outputID);
        }
        return outputID;
    };
    Room.prototype.hasSpared = function (player) {
        return this.spared[player.getUsername()];
    };
    Room.prototype.spare = function (player) {
        this.spared[player.getUsername()] = true;
    };
    Room.prototype.sparesLeft = function () {
        var output = 0;
        for (var name_1 in this.spared) {
            if (!this.spared[name_1]) {
                output++;
            }
        }
        return output;
    };
    Room.prototype.finalizeRoom = function () {
        this.server.setPostRoundStatus(this.getPlayers(), this.getVoiceChatID());
        this.players.forEach(function (element) {
            element.room = undefined;
        });
    };
    Room.prototype.fightTimeOut = function () {
        if (this.server.gamesession.playerIsMonster(this.players[0])) {
            this.players[1].die(true, false);
        }
        else if (this.server.gamesession.playerIsMonster(this.players[1])) {
            this.players[0].die(true, false);
        }
    };
    Room.prototype.getInfoObj = function () {
        return { players: this.players.map(function (player) {
                return player.getPublicInfo();
            }), voicechat: this.voicechatid, query: this.query };
    };
    Room.prototype.sendToVcRoom = function () {
        var _this = this;
        this.server.sendPlayersToVCRoom(this.voicechatid, this.players)
            .then(function (r) {
        })
            .catch(function (e) {
            _this.server.crash("Error sending players to voice chat room. Aborting!");
        });
    };
    Room.prototype.socketEmit = function () {
        this.sendToVcRoom();
        this.players[0].emit("in room", {
            myskin: this.players[0].getDisplayName(),
            otherplayer: this.players[1].getDisplayName(),
            recording: this.voicechatid,
            time: (0, config_1.default)().times.fight,
            query: this.query,
            voiceskin: this.players[0].getVoiceSkin(),
            otherPlayerSkin: this.players[1].getVoiceSkin(),
            gameMode: this.server.gamesession.getGameMode()
        });
        this.players[1].emit("in room", {
            myskin: this.players[1].getDisplayName(),
            otherplayer: this.players[0].getDisplayName(),
            recording: this.voicechatid,
            time: (0, config_1.default)().times.fight,
            query: this.query,
            voiceskin: this.players[1].getVoiceSkin(),
            otherPlayerSkin: this.players[0].getVoiceSkin(),
            gameMode: this.server.gamesession.getGameMode()
        });
    };
    Room.prototype.getOtherPlayer = function (player) {
        var otherplayer = this.players[0];
        if (otherplayer.getUsername() === player.getUsername()) {
            otherplayer = this.players[1];
        }
        return otherplayer;
    };
    Room.prototype.pickOtherPlayer = function (player) {
        if (this.players[0].getUsername() === player.getUsername()) {
            return this.players[1];
        }
        return this.players[0];
    };
    Room.prototype.emitToPlayers = function (event, data) {
        for (var _i = 0, _a = this.players; _i < _a.length; _i++) {
            var player = _a[_i];
            player.emit(event, data);
        }
    };
    Room.prototype.printInfo = function () {
        if (!(0, config_1.default)().debug) {
            return;
        }
        console.log("Room " + this.roomid + ": ");
        this.players.forEach(function (player) {
            console.log(player.getPrintInfo());
        });
    };
    Room.prototype.getVoiceChatID = function () {
        return this.voicechatid;
    };
    Room.prototype.getQuery = function () {
        return this.query;
    };
    Room.prototype.setQuery = function (query) {
        this.query = query;
    };
    Room.prototype.getPlayers = function () {
        return this.players;
    };
    return Room;
}());
exports.default = Room;
