"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePhase_1 = require("../types/GamePhase");
var GameMode_1 = require("../types/GameMode");
var RoomJudgement_1 = __importDefault(require("./RoomJudgement"));
var config_1 = __importDefault(require("../config/config"));
var Steam_1 = __importDefault(require("./Steam"));
var RoomTriple_1 = __importDefault(require("./RoomTriple"));
var Player = /** @class */ (function () {
    function Player(socket, username, admin, puid, server, socketHandler) {
        this._dead = false;
        this._character = -1;
        this._monster = false;
        this.disguisedas = "";
        this.targetplayer = "";
        this.killedByHuman = false;
        this.kevinWin = false;
        this.availableCharacters = (0, config_1.default)()["characters"];
        this.playerSettings = __assign({}, (0, config_1.default)()["player_default"]);
        this.characterChoices = [0, 1];
        this.socket = socket;
        this.username = username;
        this.admin = admin;
        this.id = socket.id;
        this.server = server;
        this._puid = puid;
        var splitIP = socket.handshake.address.split(":");
        this.ip = splitIP[splitIP.length - 1];
    }
    Player.prototype.setPlayerSettings = function (settings) {
        for (var key in settings) {
            if (this.playerSettings[key] === undefined) {
                return 1;
            }
            if (typeof this.playerSettings[key] !== typeof settings[key]) {
                return 2;
            }
            this.playerSettings[key] = settings[key];
        }
        return 0;
    };
    Player.prototype.randomCharacter = function () {
        this._character = this.availableCharacters[Math.floor(Math.random() * this.availableCharacters.length)];
        console.log(this._character);
    };
    Player.prototype.getPlayerSetting = function (settingName) {
        return this.playerSettings[settingName];
    };
    Player.prototype.getServerId = function () {
        return this.server.id;
    };
    Player.prototype.setDisguise = function (disguise) {
        this.disguisedas = disguise;
    };
    Player.prototype.setCharacterChoices = function (choices) {
        if (choices.length > 20) {
            this.characterChoices = [0, 1];
            return;
        }
        this.characterChoices = choices;
    };
    Player.prototype.getCharacterChoices = function () {
        return this.characterChoices;
    };
    Player.prototype.initializeCharactersAndDLCOwned = function (socketHandler) {
        return __awaiter(this, void 0, void 0, function () {
            var ownsDlc;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, Steam_1.default.ownsDLC(socketHandler.steamID)];
                    case 1:
                        ownsDlc = _a.sent();
                        if (!ownsDlc) {
                            this.availableCharacters = [0, 1];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    Player.prototype.getTargetPlayer = function () {
        return this.targetplayer;
    };
    Player.prototype.getVoiceSkin = function () {
        if (this.monster) {
            var disguise = this.server.getPlayerFromUsername(this.disguisedas);
            if (!disguise) {
                this.server.crash("Tried to get voiceskin from non-existant player");
                return -1;
            }
            return disguise.character;
        }
        else {
            return this.character;
        }
    };
    Player.prototype.setSkip = function () {
        this.targetplayer = "!skip";
    };
    Player.prototype.setTargetPlayerSafe = function (target) {
        if (!this.monster) {
            return 1; //Humans can't pick next player
        }
        if (!this.gamesession || !this.gamesession.isPhase(GamePhase_1.GamePhase.CharacterSelect)) {
            return 2; // Not in right gamephase.
        }
        if (target === "!skip") {
            this.targetplayer = "!skip";
            this.gamesession.checkFightStart();
            return 0;
        }
        var player = this.server.getPlayerFromUsername(target);
        if (!player) {
            return 3; // User not found.
        }
        if (player.dead || player.monster) {
            return 4; // Invalid player selected.
        }
        this.targetplayer = target;
        this.gamesession.checkFightStart();
        return 0;
    };
    Player.prototype.setTargetPlayerUnsafe = function (target) {
        this.targetplayer = target;
    };
    Player.prototype.emit = function (event, data) {
        this.socket.emit(event, data);
    };
    Player.prototype.notDisguised = function () {
        return (this.disguisedas === "");
    };
    //Checks whether a monsterplayer has finished the pick character phase
    Player.prototype.hasPicked = function () {
        if (this.notDisguised()) {
            return false;
        }
        if (this.targetplayer === "") {
            return false;
        }
        return true;
    };
    Player.prototype.die = function (killedbymonster, kevinWin) {
        this.dead = true;
        this.kevinWin = kevinWin;
        this.killedByHuman = !killedbymonster;
        //This is a little convoluted, but it makes implementing player deaths less bug prone
        this.server.gamesession.makeSpectator(this);
    };
    Player.prototype.getDisplayName = function () {
        if (this.monster) {
            return this.disguisedas;
        }
        else {
            return this.username;
        }
    };
    Player.prototype.getDisplayNamePlusIdentifier = function () {
        var output = this.getDisplayName();
        if (this.room instanceof RoomTriple_1.default) {
            output += " " + this.room.getIdentifier(this);
        }
        return output;
    };
    Player.prototype.kevinWinCondition = function () {
        return this.dead && this.kevinWin;
    };
    Player.prototype.getPublicInfo = function () {
        return {
            username: this.username,
            admin: this.admin,
            character: this._character,
            monster: this._monster,
            dead: this._dead,
            disguise: this.disguisedas,
            puid: this._puid,
            spectating: this.getPlayerSetting("spectating")
        };
    };
    Player.prototype.getPrintInfo = function () {
        return this.username + ": Character: " + this.character + ", Monster: " + this.monster + ", Dead: " + this.dead + ", Disguise: " + this.disguisedas + ", Spectating: " + this.getPlayerSetting("spectating");
    };
    Player.prototype.castJudgementVote = function (choice) {
        if (!this.dead) {
            return 2;
        }
        if (!(choice === "A" || choice === "B")) {
            return 3;
        }
        if (!this.room || !(this.room instanceof RoomJudgement_1.default)) {
            return 4;
        }
        this.room.vote(this, choice);
        return 0;
    };
    Player.prototype.pickDisguise = function (username) {
        if (!this._gamesession || !this._gamesession.isPhase(GamePhase_1.GamePhase.CharacterSelect)) {
            return 2; // Not in right phase;
        }
        if (!this.monster) {
            return 1; // Player is not a monster;
        }
        if (username == this.username && this._gamesession.getGameMode() !== GameMode_1.GameMode.Mayhem) {
            return 3; // Can't disguise as self.
        }
        this.disguisedas = username;
        this._gamesession.checkFightStart();
        return 0;
    };
    Player.prototype.isSkipping = function () {
        return this.targetplayer === "!skip";
    };
    Player.prototype.getUsername = function () {
        return this.username;
    };
    Player.prototype.getAdmin = function () {
        return this.admin;
    };
    Player.prototype.setAdmin = function (admin) {
        this.admin = admin;
    };
    Player.prototype.getSocketID = function () {
        return this.socket.id;
    };
    Object.defineProperty(Player.prototype, "dead", {
        get: function () {
            return this._dead;
        },
        set: function (value) {
            this._dead = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Player.prototype, "character", {
        get: function () {
            return this._character;
        },
        set: function (value) {
            this._character = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Player.prototype, "monster", {
        get: function () {
            return this._monster;
        },
        set: function (value) {
            this._monster = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Player.prototype, "room", {
        get: function () {
            return this._room;
        },
        set: function (value) {
            this._room = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Player.prototype, "gamesession", {
        get: function () {
            return this._gamesession;
        },
        set: function (value) {
            this._gamesession = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Player.prototype, "puid", {
        get: function () {
            return this._puid;
        },
        enumerable: false,
        configurable: true
    });
    Player.prototype.resetDisguise = function () {
        this.disguisedas = "";
        this.targetplayer = "";
    };
    return Player;
}());
exports.default = Player;
