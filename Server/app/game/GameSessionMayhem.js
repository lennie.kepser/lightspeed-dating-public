"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var GameSession_1 = __importDefault(require("./GameSession"));
var assignRoomsMayhem_1 = __importDefault(require("./functions/assignRoomsMayhem"));
var GameMode_1 = require("../types/GameMode");
var config_1 = __importDefault(require("../config/config"));
var assignCharacter_1 = __importDefault(require("./functions/assignCharacter"));
var GameSessionMayhem = /** @class */ (function (_super) {
    __extends(GameSessionMayhem, _super);
    function GameSessionMayhem() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    GameSessionMayhem.prototype.playerIsMonster = function (player) {
        return player.getDisplayName() !== player.getUsername();
    };
    GameSessionMayhem.prototype.getGameMode = function () {
        return GameMode_1.GameMode.Mayhem;
    };
    GameSessionMayhem.prototype.isPlayerAllowedToKill = function (player) {
        return !player.dead;
    };
    GameSessionMayhem.prototype.assignRandomDisguise = function (monster) {
        var playerArray = this.monsters;
        var player = playerArray[Object.keys(playerArray)[Math.floor(Math.random() * Object.keys(playerArray).length)]];
        monster.pickDisguise(player.getUsername());
    };
    GameSessionMayhem.prototype.assignPlayersToRooms = function () {
        return (0, assignRoomsMayhem_1.default)(Object.values(this.monsters), this.server);
    };
    GameSessionMayhem.prototype.isAllowedToSpectate = function (player) {
        return !player.room;
    };
    GameSessionMayhem.prototype.checkGameEndOrNextRound = function () {
        if (this.getMonsterAmnt() === 2 && Math.random() <= this.server.settings.judgementchance) {
            this.prepareJudgementRoom();
        }
        else if (this.getMonsterAmnt() > 1) {
            this.startCharSelect();
        }
        else {
            this.endGame();
        }
    };
    GameSessionMayhem.prototype.checkFightStart = function () {
        for (var id in this.monsters) {
            if (this.monsters[id].notDisguised()) {
                return;
            }
        }
        this.startFight();
    };
    GameSessionMayhem.prototype.emitWinner = function () {
        var winningPlayer;
        for (var id in this.monsters) {
            winningPlayer = this.monsters[id].getUsername();
        }
        if (!winningPlayer) {
            winningPlayer = "No one";
        }
        if ((0, config_1.default)().debug) {
            console.log(winningPlayer + " won!");
        }
        this.server.emitToAll("endgame mayhem", { winner: winningPlayer });
    };
    GameSessionMayhem.prototype.setUpPlayersAndRoles = function (players) {
        for (var id in players) {
            players[id].monster = true;
            players[id].dead = false;
            players[id].gamesession = this;
            players[id].character = (0, assignCharacter_1.default)(players[id]);
            this.addMonster(players[id]);
        }
    };
    return GameSessionMayhem;
}(GameSession_1.default));
exports.default = GameSessionMayhem;
