"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var GameSession_1 = __importDefault(require("./GameSession"));
var GameMode_1 = require("../types/GameMode");
var config_1 = __importDefault(require("../config/config"));
var assignRoles_1 = __importDefault(require("./functions/assignRoles"));
var GameSessionKevin = /** @class */ (function (_super) {
    __extends(GameSessionKevin, _super);
    function GameSessionKevin() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    GameSessionKevin.prototype.getGameMode = function () {
        return GameMode_1.GameMode.Kevin;
    };
    GameSessionKevin.prototype.emitWinner = function () {
        var _a, _b;
        var winningPlayer;
        if ((_a = this.kevin) === null || _a === void 0 ? void 0 : _a.kevinWinCondition()) {
            winningPlayer = (_b = this.kevin) === null || _b === void 0 ? void 0 : _b.getUsername();
            if ((0, config_1.default)().debug) {
                console.log(winningPlayer + " won!");
            }
            this.server.emitToAll("endgame mayhem", { winner: winningPlayer });
        }
        else {
            _super.prototype.emitWinner.call(this);
        }
    };
    GameSessionKevin.prototype.gameStartInfo = function (player) {
        if (player.monster) {
            return { gameinfo: this.server.getPlayerInfoArray() };
        }
        else if (this.kevin === player) {
            return { character: player.character, kevin: true };
        }
        else {
            return { character: player.character, kevin: false };
        }
    };
    GameSessionKevin.prototype.checkEndFightConditions = function () {
        var _a;
        return (!this.areRoomsLeft() || this.getMonsterAmnt() === 0 || !!((_a = this.kevin) === null || _a === void 0 ? void 0 : _a.kevinWinCondition()));
    };
    GameSessionKevin.prototype.setUpPlayersAndRoles = function (players) {
        (0, assignRoles_1.default)(players, this, this.server);
        this.kevin = Object.values(this.humans)[Math.floor(Math.random() * (Object.keys(this.humans).length))];
    };
    GameSessionKevin.prototype.checkGameEndOrNextRound = function () {
        var _a;
        if ((_a = this.kevin) === null || _a === void 0 ? void 0 : _a.kevinWinCondition()) {
            this.endGame();
        }
        else {
            _super.prototype.checkGameEndOrNextRound.call(this);
        }
    };
    return GameSessionKevin;
}(GameSession_1.default));
exports.default = GameSessionKevin;
