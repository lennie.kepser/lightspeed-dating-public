"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var child_process_1 = require("child_process");
var request = require('request');
var amntplayers = +process.argv[3];
var Tester = /** @class */ (function () {
    function Tester() {
        this.forks = [];
        this.amnttests = +process.argv[2];
    }
    Tester.prototype.roundTest = function () {
        for (var i = 0; i < this.forks.length; i++) {
            this.forks[i].send("start");
        }
    };
    Tester.prototype.joinServer = function (serverid) {
        for (var i = 0; i < this.forks.length; i++) {
            this.forks[i].send("join_" + serverid);
        }
    };
    Tester.prototype.test = function (players) {
        var _this = this;
        var joined = 0;
        var ended = 0;
        var _loop_1 = function (i) {
            this_1.forks[i] = (0, child_process_1.fork)('./app/test-client.js', [(players - i - 1).toString()]);
            this_1.forks[i].on("message", function (msg) {
                if (msg.msg === "joined") {
                    joined++;
                    if (msg.serverid === "") {
                        if (joined === players) {
                            _this.roundTest();
                        }
                    }
                    else {
                        _this.joinServer(msg.serverid);
                    }
                }
                if (msg.msg === "game end") {
                    ended++;
                    _this.forks[i].kill('SIGINT');
                    if (ended === players) {
                        _this.amnttests--;
                        if (_this.amnttests > 0) {
                            _this.test(amntplayers);
                        }
                    }
                }
            });
        };
        var this_1 = this;
        for (var i = 0; i < players; i++) {
            _loop_1(i);
        }
    };
    return Tester;
}());
request("http://localhost:3000/", {}, function (err, res, body) {
    if (err) {
        throw "Server not running";
    }
    if (body !== '<h1>Why are you here</h1>') {
        console.log(body);
        throw "Wrong server running? idk this is weird.";
    }
    var amntservers = 0;
    setInterval(function () {
        if (amntservers > +process.argv[4]) {
            return;
        }
        amntservers++;
        console.log("Servers: " + amntservers + " Players: " + (amntservers * amntplayers));
        var tester = new Tester();
        tester.test(amntplayers);
    }, 2000);
});
